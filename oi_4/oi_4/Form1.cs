﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oi_4
{
    public partial class Form1 : Form
    {
        double[] wt_lin1 = new double[9] { 1, 1, 1, 1, 1, 1, 1, 1, 1 }; // Веса для 3х3
        double[] wt_lin2 = new double[9] { 1, 2, 1, 2, 4, 2, 1, 2, 1 }; // Веса для 3х3
        double[] wt_lin5 = new double[25] { 0.000789, 0.006581, 0.013347, 0.006581,
            0.000789, 0.006581, 0.054901, 0.111345, 0.054901, 0.006581, 0.013347,
            0.111345, 0.225821, 0.111345, 0.013347, 0.006581, 0.054901, 0.111345,
            0.054901, 0.006581, 0.000789, 0.006581, 0.013347, 0.006581, 0.000789 }; // Веса для 5х5
        //double[] wt_sharp = new double[9] { 1, 1, 1, 1, -8, 1, 1, 1, 1 }; // Веса для резкости
        double[] wt_sharp = new double[9] { -1, -1, -1, -1, 9, -1, -1, -1, -1 }; // Веса для резкости

        public Form1()
        {
            InitializeComponent();

            but_open.Click += But_open_Click;

            but_rect.Click += But_rect_Click;
            but_lin.Click += But_lin_Click;
            but_noise.Click += But_noise_Click;
            but_med.Click += But_med_Click;
            but_sharp.Click += But_sharp_Click;

            but_save_rect.Click += But_save_rect_Click;
            but_save_lin1.Click += But_save_lin1_Click;
            but_save_lin2.Click += But_save_lin2_Click;
            but_save_lin5.Click += But_save_lin5_Click;
            but_save_noise.Click += But_save_noise_Click;
            but_save_med.Click += But_save_med_Click;
            but_save_sharp1.Click += But_save_sharp1_Click;
            but_save_sharp2.Click += But_save_sharp2_Click;
            but_save_sharp5.Click += But_save_sharp5_Click;
        }

        private void But_open_Click(object sender, EventArgs e)
        {
            Bitmap image; //Bitmap для открываемого изображения
            OpenFileDialog open_dialog = new OpenFileDialog(); //создание диалогового окна для выбора файла
            open_dialog.Filter = "Image Files(*.TIF;*.TIFF;*.BMP;*.JPG;*.GIF;*.PNG)|*.TIF;*.TIFF;*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*"; //формат файла
            if (open_dialog.ShowDialog() == DialogResult.OK) //если в окне была нажата кнопка "ОК"
            {
                try
                {
                    image = new Bitmap(open_dialog.FileName);
                    //this.pic.Size = image.Size;
                    pic_rect.Image = image;
                    pic_rect.Invalidate();
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void But_lin_Click(object sender, EventArgs e) // Применение линейных сглаживающих фильтров
        {
            Bitmap Bit1 = new Bitmap(pic_rect.Image.Width, pic_rect.Image.Height);
            Bitmap Bit2 = new Bitmap(pic_rect.Image.Width, pic_rect.Image.Height);
            Bitmap Bit5 = new Bitmap(pic_rect.Image.Width, pic_rect.Image.Height);
            double x = 0.0; // Значение канала пикселя
            int height, width; // высота и ширина изображения

            height = Bit1.Height;
            width = Bit1.Width;

            double[,] frame3 = new double[height + 2, width + 2];
            frame3 = frame3_3(pic_rect.Image); // Вычисление рамки для расчетов 3х3

            for (int i = 1; i < height + 1; i++) // Вычисление линейного сглаживающего фильтра 3х3
            {
                for (int j = 1; j < width + 1; j++)
                {
                    x = frame3[i - 1, j - 1] * wt_lin1[0] // Равные веса
                        + frame3[i - 1, j] * wt_lin1[1] + frame3[i - 1, j + 1] * wt_lin1[2]
                        + frame3[i, j - 1] * wt_lin1[3] + frame3[i, j] * wt_lin1[4]
                        + frame3[i, j + 1] * wt_lin1[5] + frame3[i + 1, j - 1] * wt_lin1[6]
                        + frame3[i + 1, j] * wt_lin1[7] + frame3[i + 1, j + 1] * wt_lin1[8];
                    x = x / 9;
                    Bit1.SetPixel(i - 1, j - 1, Color.FromArgb(Convert.ToInt32(x), Convert.ToInt32(x), Convert.ToInt32(x)));

                    x = frame3[i - 1, j - 1] * wt_lin2[0] // Взвешенные веса
                        + frame3[i - 1, j] * wt_lin2[1] + frame3[i - 1, j + 1] * wt_lin2[2]
                        + frame3[i, j - 1] * wt_lin2[3] + frame3[i, j] * wt_lin2[4]
                        + frame3[i, j + 1] * wt_lin2[5] + frame3[i + 1, j - 1] * wt_lin2[6]
                        + frame3[i + 1, j] * wt_lin2[7] + frame3[i + 1, j + 1] * wt_lin2[8];
                    x = x / 16;
                    Bit2.SetPixel(i - 1, j - 1, Color.FromArgb(Convert.ToInt32(x), Convert.ToInt32(x), Convert.ToInt32(x)));
                }
            }

            double[,] frame5 = new double[height + 4, width + 4];
            frame5 = frame5_5(pic_rect.Image, frame3); // Вычисление рамки для расчетов 5х5

            for (int i = 2; i < height + 2; i++) // Вычисление линейного сглаживающего фильтра 5х5
            {
                for (int j = 2; j < width + 2; j++)
                {
                    x = frame5[i - 2, j - 2] * wt_lin5[0] // Взвешенные веса
                        + frame5[i - 2, j - 1] * wt_lin5[1] + frame5[i - 2, j] * wt_lin5[2]
                        + frame5[i - 2, j + 1] * wt_lin5[3] + frame5[i - 2, j + 2] * wt_lin5[4]
                        + frame5[i - 1, j - 2] * wt_lin5[5] + frame5[i - 1, j - 1] * wt_lin5[6]
                        + frame5[i - 1, j] * wt_lin5[7] + frame5[i - 1, j + 1] * wt_lin5[8]
                        + frame5[i - 1, j + 2] * wt_lin5[9] + frame5[i, j - 2] * wt_lin5[10]
                        + frame5[i, j - 1] * wt_lin5[11] + frame5[i, j] * wt_lin5[12]
                        + frame5[i, j + 1] * wt_lin5[13] + frame5[i, j + 2] * wt_lin5[14]
                        + frame5[i + 1, j - 2] * wt_lin5[15] + frame5[i + 1, j - 1] * wt_lin5[16]
                        + frame5[i + 1, j] * wt_lin5[17] + frame5[i + 1, j + 1] * wt_lin5[18]
                        + frame5[i + 1, j + 2] * wt_lin5[19] + frame5[i + 2, j - 2] * wt_lin5[20]
                        + frame5[i + 2, j - 1] * wt_lin5[21] + frame5[i + 2, j] * wt_lin5[22]
                        + frame5[i + 2, j + 1] * wt_lin5[23] + frame5[i + 2, j + 2] * wt_lin5[24];
                    Bit5.SetPixel(i - 2, j - 2, Color.FromArgb(Convert.ToInt32(x), Convert.ToInt32(x), Convert.ToInt32(x)));
                }
            }
            pic_lin1.Image = Bit1;
            pic_lin2.Image = Bit2;
            pic_lin5.Image = Bit5;
        }

        private void But_noise_Click(object sender, EventArgs e) //	Добавление в изображение импульсного шума
        {
            Bitmap noise = new Bitmap(pic_rect.Image);
            double x = 0.0; // Значение канала пикселя
            int height, width; // высота и ширина изображения

            height = noise.Height;
            width = noise.Width;

            Random rnd = new Random();
            for (int i = 0; i < width * 5; i++)
            {
                if (rnd.NextDouble() > 0.5)
                    noise.SetPixel(rnd.Next(height), rnd.Next(width), Color.FromArgb(255, 255, 255));
                else
                    noise.SetPixel(rnd.Next(height), rnd.Next(width), Color.FromArgb(0, 0, 0));
            }
            pic_noise.Image = noise;
        }

        private void But_med_Click(object sender, EventArgs e) // Применение медианного фильтра
        {
            Bitmap med = new Bitmap(pic_lin1.Image);
            //Bitmap med = new Bitmap(pic_noise.Image); !!!!!!!!!!!!!!!!!!


            double x = 0.0; // Значение канала пикселя
            int height, width; // высота и ширина изображения

            height = med.Height;
            width = med.Width;

            double[,] frame3 = new double[height + 2, width + 2];


            frame3 = frame3_3(pic_lin1.Image);
            //frame3 = frame3_3(pic_noise.Image); !!!!!!!!!!!!!!!!!!!!!!



            double[] group;
            double[] group_sort;
            double minW = 256;
            int indMin = 0;
            for (int i = 1; i < height + 1; i++) // Вычисление медианы окрестности
            {
                for (int j = 1; j < width + 1; j++)
                {
                    group = new double[9] { frame3[i - 1, j - 1], frame3[i - 1, j],
                        frame3[i - 1, j + 1], frame3[i, j - 1], frame3[i, j], frame3[i, j + 1],
                        frame3[i + 1, j - 1], frame3[i + 1, j], frame3[i + 1, j + 1]};
                    group_sort = new double[9];

                    for (int k = 0; k < 9; k++)
                    {
                        for (int m = 0; m < 9; m++)
                        {
                            if (group[m] < minW)
                            {
                                minW = group[m];
                                indMin = m;
                            }
                        }
                        group_sort[k] = minW;
                        group[indMin] = 256;
                        minW = 256;
                    }
                    x = group_sort[4];
                    med.SetPixel(i - 1, j - 1, Color.FromArgb(Convert.ToInt32(x), Convert.ToInt32(x), Convert.ToInt32(x)));
                }
            }
            pic_med.Image = med;
        }

        private void But_sharp_Click(object sender, EventArgs e) // Применение фильтра повышения резкости
        {
            Bitmap Bit1 = new Bitmap(pic_rect.Image.Width, pic_rect.Image.Height);
            Bitmap Bit2 = new Bitmap(pic_rect.Image.Width, pic_rect.Image.Height);
            Bitmap Bit5 = new Bitmap(pic_rect.Image.Width + 1, pic_rect.Image.Height + 1);
            double x = 0.0; // Значение канала пикселя
            int height, width; // высота и ширина изображения

            height = Bit1.Height;
            width = Bit1.Width;

            double[,] frame3 = new double[height + 2, width + 2];

            frame3 = frame3_3(pic_rect.Image); // Вычисление рамки для расчетов 3х3
            //frame3 = frame3_3(pic_lin1.Image); // Вычисление рамки для расчетов 3х3

            for (int i = 1; i < height + 1; i++) // Повышение резкости 3х3 (равные веса)
            {
                for (int j = 1; j < width + 1; j++)
                {
                    x = frame3[i - 1, j - 1] * wt_sharp[0]
                        + frame3[i - 1, j] * wt_sharp[1] + frame3[i - 1, j + 1] * wt_sharp[2]
                        + frame3[i, j - 1] * wt_sharp[3] + frame3[i, j] * wt_sharp[4]
                        + frame3[i, j + 1] * wt_sharp[5] + frame3[i + 1, j - 1] * wt_sharp[6]
                        + frame3[i + 1, j] * wt_sharp[7] + frame3[i + 1, j + 1] * wt_sharp[8];
                    if (x > 255)
                        x = 255;
                    if (x < 0)
                        x = 0;
                    Bit1.SetPixel(i - 1, j - 1, Color.FromArgb(Convert.ToInt32(x), Convert.ToInt32(x), Convert.ToInt32(x)));
                }
            }

            frame3 = frame3_3(pic_rect.Image); // Вычисление рамки для расчетов 3х3
            //frame3 = frame3_3(pic_lin2.Image); // Вычисление рамки для расчетов 3х3

            for (int i = 1; i < height + 1; i++) // Повышение резкости 3х3 (взвешенные веса)
            {
                for (int j = 1; j < width + 1; j++)
                {
                    x = frame3[i - 1, j - 1] * wt_sharp[0]
                        + frame3[i - 1, j] * wt_sharp[1] + frame3[i - 1, j + 1] * wt_sharp[2]
                        + frame3[i, j - 1] * wt_sharp[3] + frame3[i, j] * wt_sharp[4]
                        + frame3[i, j + 1] * wt_sharp[5] + frame3[i + 1, j - 1] * wt_sharp[6]
                        + frame3[i + 1, j] * wt_sharp[7] + frame3[i + 1, j + 1] * wt_sharp[8];
                    if (x > 255)
                        x = 255;
                    if (x < 0)
                        x = 0;
                    Bit2.SetPixel(i - 1, j - 1, Color.FromArgb(Convert.ToInt32(x), Convert.ToInt32(x), Convert.ToInt32(x)));
                }
            }

            double[,] frame5 = new double[height + 4, width + 4];
            frame5 = frame5_5(pic_rect.Image, frame3); // Повышение резкости 5х5 (взвешенные веса)
            //frame5 = frame5_5(pic_lin5.Image, frame3); // Повышение резкости 5х5 (взвешенные веса)

            for (int i = 1; i < height + 2; i++) // Вычисление линейного сглаживающего фильтра 5х5
            {
                for (int j = 1; j < width + 2; j++)
                {
                    x = frame5[i - 1, j - 1] * wt_sharp[0]
                        + frame5[i - 1, j] * wt_sharp[1] + frame5[i - 1, j + 1] * wt_sharp[2]
                        + frame5[i, j - 1] * wt_sharp[3] + frame5[i, j] * wt_sharp[4]
                        + frame5[i, j + 1] * wt_sharp[5] + frame5[i + 1, j - 1] * wt_sharp[6]
                        + frame5[i + 1, j] * wt_sharp[7] + frame5[i + 1, j + 1] * wt_sharp[8];
                    if (x > 255)
                        x = 255;
                    if (x < 0)
                        x = 0;
                    Bit5.SetPixel(i - 1, j - 1, Color.FromArgb(Convert.ToInt32(x), Convert.ToInt32(x), Convert.ToInt32(x)));
                }
            }
            pic_sharp1.Image = Bit1;
            pic_sharp2.Image = Bit2;
            pic_sharp5.Image = Bit5;
        }

        private double[,] frame3_3(Image img) // Вычисление рамки для расчетов 3х3
        {
            Bitmap pic = new Bitmap(img);
            int height, width; // высота и ширина изображения

            height = pic.Height;
            width = pic.Width;
            double[,] frame3 = new double[height + 2, width + 2];

            for (int i = 0; i < height + 2; i++)
            {
                for (int j = 0; j < width + 2; j++)
                {
                    if (i == 0)
                    {
                        if (j == 0)
                            frame3[i, j] = pic.GetPixel(i, j).R;
                        else
                        {
                            if (j == width + 1)
                                frame3[i, j] = pic.GetPixel(i, j - 2).R;
                            else
                                frame3[i, j] = pic.GetPixel(i, j - 1).R;
                        }
                    }
                    else
                    {
                        if (i == height + 1)
                        {
                            if (j == 0)
                                frame3[i, j] = pic.GetPixel(i - 2, j).R;
                            else
                            {
                                if (j == width + 1)
                                    frame3[i, j] = pic.GetPixel(i - 2, j - 2).R;
                                else
                                    frame3[i, j] = pic.GetPixel(i - 2, j - 1).R;
                            }
                        }
                        else
                        {
                            if (j == 0)
                                frame3[i, j] = pic.GetPixel(i - 1, j).R;
                            else
                            {
                                if (j == width + 1)
                                    frame3[i, j] = pic.GetPixel(i - 1, j - 2).R;
                                else
                                    frame3[i, j] = pic.GetPixel(i - 1, j - 1).R;
                            }
                        }
                    }
                }
            }
            return frame3;
        }

        private double[,] frame5_5(Image img, double[,] frame3) // Вычисление рамки для расчетов 5х5
        {
            Bitmap pic = new Bitmap(img);
            int height, width; // высота и ширина изображения

            height = pic.Height;
            width = pic.Width;
            double[,] frame5 = new double[height + 4, width + 4];

            for (int i = 0; i < height + 4; i++)
            {
                for (int j = 0; j < width + 4; j++)
                {
                    if (i == 0)
                    {
                        if (j == 0)
                            frame5[i, j] = frame3[i, j];
                        else
                        {
                            if (j == width + 3)
                                frame5[i, j] = frame3[i, j - 2];
                            else
                                frame5[i, j] = frame3[i, j - 1];
                        }
                    }
                    else
                    {
                        if (i == height + 3)
                        {
                            if (j == 0)
                                frame5[i, j] = frame3[i - 2, j];
                            else
                            {
                                if (j == width + 3)
                                    frame5[i, j] = frame3[i - 2, j - 2];
                                else
                                    frame5[i, j] = frame3[i - 2, j - 1];
                            }
                        }
                        else
                        {
                            if (j == 0)
                                frame5[i, j] = frame3[i - 1, j];
                            else
                            {
                                if (j == width + 3)
                                    frame5[i, j] = frame3[i - 1, j - 2];
                                else
                                    frame5[i, j] = frame3[i - 1, j - 1];
                            }
                        }
                    }
                }
            }
            return frame5;
        }

        private void But_rect_Click(object sender, EventArgs e) // Получение изображения период. прямоуг. сигнала
        {
            Bitmap Bitmap = new Bitmap(300, 300);
            int x = 0; // Значение канала пикселя
            int height, width; // высота и ширина изображения

            height = Bitmap.Height;
            width = Bitmap.Width;

            int step = 0;
            int n = 1; // для чередования белого и черного
            for (int i = 0; i < width; i++) // Получение изображения
            {
                for (int j = 0; j < height; j++)
                {
                    if (step == 3)
                    {
                        step = 0;
                        n *= -1;
                    }
                    if (n == 1) // белый 
                        x = 255;
                    else
                        x = 0; // черный                               
                    Bitmap.SetPixel(j, i, Color.FromArgb(x, x, x));
                    step++;
                }
            }
            pic_rect.Image = Bitmap;
        }

        private void But_save_rect_Click(object sender, EventArgs e) // Сохранение прям. сигнала
        {
            save_image(pic_rect.Image);
        }

        private void But_save_lin1_Click(object sender, EventArgs e) // Сохранение после линейного сгл. фильтра
        {
            save_image(pic_lin1.Image);
        }

        private void But_save_lin2_Click(object sender, EventArgs e)
        {
            save_image(pic_lin2.Image);
        }

        private void But_save_lin5_Click(object sender, EventArgs e)
        {
            save_image(pic_lin5.Image);
        }

        private void But_save_noise_Click(object sender, EventArgs e)
        {
            save_image(pic_noise.Image);
        }

        private void But_save_med_Click(object sender, EventArgs e)
        {
            save_image(pic_med.Image);
        }

        private void But_save_sharp5_Click(object sender, EventArgs e)
        {
            save_image(pic_sharp5.Image);
        }

        private void But_save_sharp2_Click(object sender, EventArgs e)
        {
            save_image(pic_sharp2.Image);
        }

        private void But_save_sharp1_Click(object sender, EventArgs e)
        {
            save_image(pic_sharp1.Image);
        }

        private void save_image(Image pic) // Сохранение изображения
        {
            if (pic != null) //если в pictureBox есть изображение
            {
                //создание диалогового окна "Сохранить как..", для сохранения изображения
                SaveFileDialog savedialog = new SaveFileDialog();
                savedialog.Title = "Сохранить картинку как...";
                //отображать ли предупреждение, если пользователь указывает имя уже существующего файла
                savedialog.OverwritePrompt = true;
                //отображать ли предупреждение, если пользователь указывает несуществующий путь
                savedialog.CheckPathExists = true;
                //список форматов файла
                savedialog.Filter = "Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";
                //отображается ли кнопка "Справка" в диалоговом окне
                savedialog.ShowHelp = true;
                if (savedialog.ShowDialog() == DialogResult.OK) //если в диалоговом окне нажата кнопка "ОК"
                {
                    try
                    {
                        pic.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}
