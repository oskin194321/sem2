﻿namespace oi_4
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pic_rect = new System.Windows.Forms.PictureBox();
            this.but_rect = new System.Windows.Forms.Button();
            this.but_save_rect = new System.Windows.Forms.Button();
            this.pic_lin1 = new System.Windows.Forms.PictureBox();
            this.but_save_lin1 = new System.Windows.Forms.Button();
            this.but_lin = new System.Windows.Forms.Button();
            this.pic_lin2 = new System.Windows.Forms.PictureBox();
            this.pic_lin5 = new System.Windows.Forms.PictureBox();
            this.but_save_lin2 = new System.Windows.Forms.Button();
            this.but_save_lin5 = new System.Windows.Forms.Button();
            this.but_save_noise = new System.Windows.Forms.Button();
            this.but_noise = new System.Windows.Forms.Button();
            this.pic_noise = new System.Windows.Forms.PictureBox();
            this.but_save_med = new System.Windows.Forms.Button();
            this.but_med = new System.Windows.Forms.Button();
            this.pic_med = new System.Windows.Forms.PictureBox();
            this.but_save_sharp5 = new System.Windows.Forms.Button();
            this.but_save_sharp2 = new System.Windows.Forms.Button();
            this.pic_sharp5 = new System.Windows.Forms.PictureBox();
            this.pic_sharp2 = new System.Windows.Forms.PictureBox();
            this.but_sharp = new System.Windows.Forms.Button();
            this.but_save_sharp1 = new System.Windows.Forms.Button();
            this.pic_sharp1 = new System.Windows.Forms.PictureBox();
            this.but_open = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pic_rect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_lin1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_lin2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_lin5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_noise)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_med)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_sharp5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_sharp2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_sharp1)).BeginInit();
            this.SuspendLayout();
            // 
            // pic_rect
            // 
            this.pic_rect.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_rect.Location = new System.Drawing.Point(163, 12);
            this.pic_rect.Name = "pic_rect";
            this.pic_rect.Size = new System.Drawing.Size(300, 300);
            this.pic_rect.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_rect.TabIndex = 0;
            this.pic_rect.TabStop = false;
            // 
            // but_rect
            // 
            this.but_rect.Location = new System.Drawing.Point(163, 318);
            this.but_rect.Name = "but_rect";
            this.but_rect.Size = new System.Drawing.Size(300, 30);
            this.but_rect.TabIndex = 1;
            this.but_rect.Text = "Получить изображение пер. пр. сигнала";
            this.but_rect.UseVisualStyleBackColor = true;
            // 
            // but_save_rect
            // 
            this.but_save_rect.Location = new System.Drawing.Point(163, 354);
            this.but_save_rect.Name = "but_save_rect";
            this.but_save_rect.Size = new System.Drawing.Size(300, 30);
            this.but_save_rect.TabIndex = 2;
            this.but_save_rect.Text = "Сохранить";
            this.but_save_rect.UseVisualStyleBackColor = true;
            // 
            // pic_lin1
            // 
            this.pic_lin1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_lin1.Location = new System.Drawing.Point(628, 12);
            this.pic_lin1.Name = "pic_lin1";
            this.pic_lin1.Size = new System.Drawing.Size(300, 300);
            this.pic_lin1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_lin1.TabIndex = 3;
            this.pic_lin1.TabStop = false;
            // 
            // but_save_lin1
            // 
            this.but_save_lin1.Location = new System.Drawing.Point(628, 354);
            this.but_save_lin1.Name = "but_save_lin1";
            this.but_save_lin1.Size = new System.Drawing.Size(300, 30);
            this.but_save_lin1.TabIndex = 4;
            this.but_save_lin1.Text = "Сохранить";
            this.but_save_lin1.UseVisualStyleBackColor = true;
            // 
            // but_lin
            // 
            this.but_lin.Location = new System.Drawing.Point(628, 318);
            this.but_lin.Name = "but_lin";
            this.but_lin.Size = new System.Drawing.Size(912, 30);
            this.but_lin.TabIndex = 5;
            this.but_lin.Text = "Применение лин. сгл. фильтра";
            this.but_lin.UseVisualStyleBackColor = true;
            // 
            // pic_lin2
            // 
            this.pic_lin2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_lin2.Location = new System.Drawing.Point(934, 12);
            this.pic_lin2.Name = "pic_lin2";
            this.pic_lin2.Size = new System.Drawing.Size(300, 300);
            this.pic_lin2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_lin2.TabIndex = 6;
            this.pic_lin2.TabStop = false;
            // 
            // pic_lin5
            // 
            this.pic_lin5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_lin5.Location = new System.Drawing.Point(1240, 12);
            this.pic_lin5.Name = "pic_lin5";
            this.pic_lin5.Size = new System.Drawing.Size(300, 300);
            this.pic_lin5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_lin5.TabIndex = 7;
            this.pic_lin5.TabStop = false;
            // 
            // but_save_lin2
            // 
            this.but_save_lin2.Location = new System.Drawing.Point(934, 354);
            this.but_save_lin2.Name = "but_save_lin2";
            this.but_save_lin2.Size = new System.Drawing.Size(300, 30);
            this.but_save_lin2.TabIndex = 8;
            this.but_save_lin2.Text = "Сохранить";
            this.but_save_lin2.UseVisualStyleBackColor = true;
            // 
            // but_save_lin5
            // 
            this.but_save_lin5.Location = new System.Drawing.Point(1240, 354);
            this.but_save_lin5.Name = "but_save_lin5";
            this.but_save_lin5.Size = new System.Drawing.Size(300, 30);
            this.but_save_lin5.TabIndex = 9;
            this.but_save_lin5.Text = "Сохранить";
            this.but_save_lin5.UseVisualStyleBackColor = true;
            // 
            // but_save_noise
            // 
            this.but_save_noise.Location = new System.Drawing.Point(12, 732);
            this.but_save_noise.Name = "but_save_noise";
            this.but_save_noise.Size = new System.Drawing.Size(300, 30);
            this.but_save_noise.TabIndex = 12;
            this.but_save_noise.Text = "Сохранить";
            this.but_save_noise.UseVisualStyleBackColor = true;
            // 
            // but_noise
            // 
            this.but_noise.Location = new System.Drawing.Point(12, 696);
            this.but_noise.Name = "but_noise";
            this.but_noise.Size = new System.Drawing.Size(300, 30);
            this.but_noise.TabIndex = 11;
            this.but_noise.Text = "Добавить импульсный шум";
            this.but_noise.UseVisualStyleBackColor = true;
            // 
            // pic_noise
            // 
            this.pic_noise.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_noise.Location = new System.Drawing.Point(12, 390);
            this.pic_noise.Name = "pic_noise";
            this.pic_noise.Size = new System.Drawing.Size(300, 300);
            this.pic_noise.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_noise.TabIndex = 10;
            this.pic_noise.TabStop = false;
            // 
            // but_save_med
            // 
            this.but_save_med.Location = new System.Drawing.Point(318, 732);
            this.but_save_med.Name = "but_save_med";
            this.but_save_med.Size = new System.Drawing.Size(300, 30);
            this.but_save_med.TabIndex = 15;
            this.but_save_med.Text = "Сохранить";
            this.but_save_med.UseVisualStyleBackColor = true;
            // 
            // but_med
            // 
            this.but_med.Location = new System.Drawing.Point(318, 696);
            this.but_med.Name = "but_med";
            this.but_med.Size = new System.Drawing.Size(300, 30);
            this.but_med.TabIndex = 14;
            this.but_med.Text = "Применить медианный фильтр";
            this.but_med.UseVisualStyleBackColor = true;
            // 
            // pic_med
            // 
            this.pic_med.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_med.Location = new System.Drawing.Point(318, 390);
            this.pic_med.Name = "pic_med";
            this.pic_med.Size = new System.Drawing.Size(300, 300);
            this.pic_med.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_med.TabIndex = 13;
            this.pic_med.TabStop = false;
            // 
            // but_save_sharp5
            // 
            this.but_save_sharp5.Location = new System.Drawing.Point(1240, 732);
            this.but_save_sharp5.Name = "but_save_sharp5";
            this.but_save_sharp5.Size = new System.Drawing.Size(300, 30);
            this.but_save_sharp5.TabIndex = 22;
            this.but_save_sharp5.Text = "Сохранить";
            this.but_save_sharp5.UseVisualStyleBackColor = true;
            // 
            // but_save_sharp2
            // 
            this.but_save_sharp2.Location = new System.Drawing.Point(934, 732);
            this.but_save_sharp2.Name = "but_save_sharp2";
            this.but_save_sharp2.Size = new System.Drawing.Size(300, 30);
            this.but_save_sharp2.TabIndex = 21;
            this.but_save_sharp2.Text = "Сохранить";
            this.but_save_sharp2.UseVisualStyleBackColor = true;
            // 
            // pic_sharp5
            // 
            this.pic_sharp5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_sharp5.Location = new System.Drawing.Point(1240, 390);
            this.pic_sharp5.Name = "pic_sharp5";
            this.pic_sharp5.Size = new System.Drawing.Size(300, 300);
            this.pic_sharp5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_sharp5.TabIndex = 20;
            this.pic_sharp5.TabStop = false;
            // 
            // pic_sharp2
            // 
            this.pic_sharp2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_sharp2.Location = new System.Drawing.Point(934, 390);
            this.pic_sharp2.Name = "pic_sharp2";
            this.pic_sharp2.Size = new System.Drawing.Size(300, 300);
            this.pic_sharp2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_sharp2.TabIndex = 19;
            this.pic_sharp2.TabStop = false;
            // 
            // but_sharp
            // 
            this.but_sharp.Location = new System.Drawing.Point(628, 696);
            this.but_sharp.Name = "but_sharp";
            this.but_sharp.Size = new System.Drawing.Size(912, 30);
            this.but_sharp.TabIndex = 18;
            this.but_sharp.Text = "Применить фильтр повышения резкости";
            this.but_sharp.UseVisualStyleBackColor = true;
            // 
            // but_save_sharp1
            // 
            this.but_save_sharp1.Location = new System.Drawing.Point(628, 732);
            this.but_save_sharp1.Name = "but_save_sharp1";
            this.but_save_sharp1.Size = new System.Drawing.Size(300, 30);
            this.but_save_sharp1.TabIndex = 17;
            this.but_save_sharp1.Text = "Сохранить";
            this.but_save_sharp1.UseVisualStyleBackColor = true;
            // 
            // pic_sharp1
            // 
            this.pic_sharp1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_sharp1.Location = new System.Drawing.Point(628, 390);
            this.pic_sharp1.Name = "pic_sharp1";
            this.pic_sharp1.Size = new System.Drawing.Size(300, 300);
            this.pic_sharp1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_sharp1.TabIndex = 16;
            this.pic_sharp1.TabStop = false;
            // 
            // but_open
            // 
            this.but_open.Location = new System.Drawing.Point(12, 71);
            this.but_open.Name = "but_open";
            this.but_open.Size = new System.Drawing.Size(122, 54);
            this.but_open.TabIndex = 23;
            this.but_open.Text = "Загрузить изображение";
            this.but_open.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1562, 770);
            this.Controls.Add(this.but_open);
            this.Controls.Add(this.but_save_sharp5);
            this.Controls.Add(this.but_save_sharp2);
            this.Controls.Add(this.pic_sharp5);
            this.Controls.Add(this.pic_sharp2);
            this.Controls.Add(this.but_sharp);
            this.Controls.Add(this.but_save_sharp1);
            this.Controls.Add(this.pic_sharp1);
            this.Controls.Add(this.but_save_med);
            this.Controls.Add(this.but_med);
            this.Controls.Add(this.pic_med);
            this.Controls.Add(this.but_save_noise);
            this.Controls.Add(this.but_noise);
            this.Controls.Add(this.pic_noise);
            this.Controls.Add(this.but_save_lin5);
            this.Controls.Add(this.but_save_lin2);
            this.Controls.Add(this.pic_lin5);
            this.Controls.Add(this.pic_lin2);
            this.Controls.Add(this.but_lin);
            this.Controls.Add(this.but_save_lin1);
            this.Controls.Add(this.pic_lin1);
            this.Controls.Add(this.but_save_rect);
            this.Controls.Add(this.but_rect);
            this.Controls.Add(this.pic_rect);
            this.Name = "Form1";
            this.Text = "4 Пространственные фильтры";
            ((System.ComponentModel.ISupportInitialize)(this.pic_rect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_lin1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_lin2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_lin5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_noise)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_med)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_sharp5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_sharp2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_sharp1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pic_rect;
        private System.Windows.Forms.Button but_rect;
        private System.Windows.Forms.Button but_save_rect;
        private System.Windows.Forms.PictureBox pic_lin1;
        private System.Windows.Forms.Button but_save_lin1;
        private System.Windows.Forms.Button but_lin;
        private System.Windows.Forms.PictureBox pic_lin2;
        private System.Windows.Forms.PictureBox pic_lin5;
        private System.Windows.Forms.Button but_save_lin2;
        private System.Windows.Forms.Button but_save_lin5;
        private System.Windows.Forms.Button but_save_noise;
        private System.Windows.Forms.Button but_noise;
        private System.Windows.Forms.PictureBox pic_noise;
        private System.Windows.Forms.Button but_save_med;
        private System.Windows.Forms.Button but_med;
        private System.Windows.Forms.PictureBox pic_med;
        private System.Windows.Forms.Button but_save_sharp5;
        private System.Windows.Forms.Button but_save_sharp2;
        private System.Windows.Forms.PictureBox pic_sharp5;
        private System.Windows.Forms.PictureBox pic_sharp2;
        private System.Windows.Forms.Button but_sharp;
        private System.Windows.Forms.Button but_save_sharp1;
        private System.Windows.Forms.PictureBox pic_sharp1;
        private System.Windows.Forms.Button but_open;
    }
}

