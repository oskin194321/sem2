﻿namespace oi_2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.pic = new System.Windows.Forms.PictureBox();
            this.pic_res = new System.Windows.Forms.PictureBox();
            this.but_open = new System.Windows.Forms.Button();
            this.but_save = new System.Windows.Forms.Button();
            this.but_gray = new System.Windows.Forms.Button();
            this.but_neg = new System.Windows.Forms.Button();
            this.label_m = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.but_log = new System.Windows.Forms.Button();
            this.but_pow2_5 = new System.Windows.Forms.Button();
            this.but_pow040 = new System.Windows.Forms.Button();
            this.but_lin = new System.Windows.Forms.Button();
            this.but_level = new System.Windows.Forms.Button();
            this.num_level = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_res)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_level)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // pic
            // 
            this.pic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic.Location = new System.Drawing.Point(12, 12);
            this.pic.Name = "pic";
            this.pic.Size = new System.Drawing.Size(450, 450);
            this.pic.TabIndex = 0;
            this.pic.TabStop = false;
            // 
            // pic_res
            // 
            this.pic_res.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_res.Location = new System.Drawing.Point(703, 12);
            this.pic_res.Name = "pic_res";
            this.pic_res.Size = new System.Drawing.Size(450, 450);
            this.pic_res.TabIndex = 1;
            this.pic_res.TabStop = false;
            // 
            // but_open
            // 
            this.but_open.Location = new System.Drawing.Point(12, 531);
            this.but_open.Name = "but_open";
            this.but_open.Size = new System.Drawing.Size(450, 30);
            this.but_open.TabIndex = 2;
            this.but_open.Text = "Загрузить изображение";
            this.but_open.UseVisualStyleBackColor = true;
            // 
            // but_save
            // 
            this.but_save.Location = new System.Drawing.Point(703, 531);
            this.but_save.Name = "but_save";
            this.but_save.Size = new System.Drawing.Size(450, 30);
            this.but_save.TabIndex = 3;
            this.but_save.Text = "Сохранить изображение";
            this.but_save.UseVisualStyleBackColor = true;
            // 
            // but_gray
            // 
            this.but_gray.Enabled = false;
            this.but_gray.Location = new System.Drawing.Point(480, 84);
            this.but_gray.Name = "but_gray";
            this.but_gray.Size = new System.Drawing.Size(200, 30);
            this.but_gray.TabIndex = 4;
            this.but_gray.Text = "Черно-белое";
            this.but_gray.UseVisualStyleBackColor = true;
            // 
            // but_neg
            // 
            this.but_neg.Enabled = false;
            this.but_neg.Location = new System.Drawing.Point(480, 120);
            this.but_neg.Name = "but_neg";
            this.but_neg.Size = new System.Drawing.Size(200, 30);
            this.but_neg.TabIndex = 5;
            this.but_neg.Text = "Негатив";
            this.but_neg.UseVisualStyleBackColor = true;
            // 
            // label_m
            // 
            this.label_m.AutoSize = true;
            this.label_m.Location = new System.Drawing.Point(651, 538);
            this.label_m.Name = "label_m";
            this.label_m.Size = new System.Drawing.Size(16, 17);
            this.label_m.TabIndex = 6;
            this.label_m.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(574, 538);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "max m";
            // 
            // but_log
            // 
            this.but_log.Enabled = false;
            this.but_log.Location = new System.Drawing.Point(480, 156);
            this.but_log.Name = "but_log";
            this.but_log.Size = new System.Drawing.Size(200, 30);
            this.but_log.TabIndex = 8;
            this.but_log.Text = "Логарифмическое";
            this.but_log.UseVisualStyleBackColor = true;
            // 
            // but_pow2_5
            // 
            this.but_pow2_5.Enabled = false;
            this.but_pow2_5.Location = new System.Drawing.Point(480, 192);
            this.but_pow2_5.Name = "but_pow2_5";
            this.but_pow2_5.Size = new System.Drawing.Size(200, 30);
            this.but_pow2_5.TabIndex = 9;
            this.but_pow2_5.Text = "Степенное 2.5";
            this.but_pow2_5.UseVisualStyleBackColor = true;
            // 
            // but_pow040
            // 
            this.but_pow040.Enabled = false;
            this.but_pow040.Location = new System.Drawing.Point(480, 228);
            this.but_pow040.Name = "but_pow040";
            this.but_pow040.Size = new System.Drawing.Size(200, 30);
            this.but_pow040.TabIndex = 10;
            this.but_pow040.Text = "Степенное 0.4";
            this.but_pow040.UseVisualStyleBackColor = true;
            // 
            // but_lin
            // 
            this.but_lin.Enabled = false;
            this.but_lin.Location = new System.Drawing.Point(480, 264);
            this.but_lin.Name = "but_lin";
            this.but_lin.Size = new System.Drawing.Size(200, 30);
            this.but_lin.TabIndex = 11;
            this.but_lin.Text = "Кусочно-линейное";
            this.but_lin.UseVisualStyleBackColor = true;
            // 
            // but_level
            // 
            this.but_level.Enabled = false;
            this.but_level.Location = new System.Drawing.Point(480, 411);
            this.but_level.Name = "but_level";
            this.but_level.Size = new System.Drawing.Size(200, 30);
            this.but_level.TabIndex = 12;
            this.but_level.Text = "Вырезание уровней";
            this.but_level.UseVisualStyleBackColor = true;
            // 
            // num_level
            // 
            this.num_level.Location = new System.Drawing.Point(518, 383);
            this.num_level.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.num_level.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.num_level.Name = "num_level";
            this.num_level.Size = new System.Drawing.Size(122, 22);
            this.num_level.TabIndex = 13;
            this.num_level.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(515, 346);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 17);
            this.label2.TabIndex = 14;
            this.label2.Text = "1 - самый темный";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(515, 363);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 17);
            this.label3.TabIndex = 15;
            this.label3.Text = "8 - самый светлый";
            // 
            // chart1
            // 
            chartArea1.AxisX.Maximum = 255D;
            chartArea1.AxisX.Minimum = 0D;
            chartArea1.AxisY.Maximum = 255D;
            chartArea1.AxisY.Minimum = 0D;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Location = new System.Drawing.Point(1172, 69);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(350, 350);
            this.chart1.TabIndex = 16;
            this.chart1.Text = "chart1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1542, 592);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.num_level);
            this.Controls.Add(this.but_level);
            this.Controls.Add(this.but_lin);
            this.Controls.Add(this.but_pow040);
            this.Controls.Add(this.but_pow2_5);
            this.Controls.Add(this.but_log);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_m);
            this.Controls.Add(this.but_neg);
            this.Controls.Add(this.but_gray);
            this.Controls.Add(this.but_save);
            this.Controls.Add(this.but_open);
            this.Controls.Add(this.pic_res);
            this.Controls.Add(this.pic);
            this.Name = "Form1";
            this.Text = "2 Обработка изображений";
            ((System.ComponentModel.ISupportInitialize)(this.pic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_res)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_level)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pic;
        private System.Windows.Forms.PictureBox pic_res;
        private System.Windows.Forms.Button but_open;
        private System.Windows.Forms.Button but_save;
        private System.Windows.Forms.Button but_gray;
        private System.Windows.Forms.Button but_neg;
        private System.Windows.Forms.Label label_m;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button but_log;
        private System.Windows.Forms.Button but_pow2_5;
        private System.Windows.Forms.Button but_pow040;
        private System.Windows.Forms.Button but_lin;
        private System.Windows.Forms.Button but_level;
        private System.Windows.Forms.NumericUpDown num_level;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
    }
}

