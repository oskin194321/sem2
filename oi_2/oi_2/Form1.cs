﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oi_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            chart1.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;

            but_open.Click += But_open_Click;
            but_save.Click += But_save_Click;

            but_gray.Click += But_gray_Click;
            but_neg.Click += But_neg_Click;
            but_log.Click += But_log_Click;
            but_pow2_5.Click += But_pow2_5_Click;
            but_pow040.Click += But_pow040_Click;
            but_lin.Click += But_lin_Click;
            but_level.Click += But_level_Click;
        }

        private void But_level_Click(object sender, EventArgs e)
        {
            Bitmap Bitmap_lvl = new Bitmap(pic.Image);
            int level; // Уровень
            int height, width; // высота и ширина изображения

            height = Bitmap_lvl.Height;
            width = Bitmap_lvl.Width;
            level = (int)num_level.Value;
            switch (level)
            {
                case 8:
                    for (int i = 0; i < width; i++)
                    {
                        for (int j = 0; j < height; j++)
                        {
                            Color pixel1 = Bitmap_lvl.GetPixel(i, j);
                            if (levels(pixel1.R)[0] == 1)
                                Bitmap_lvl.SetPixel(i, j, Color.FromArgb(0, 0, 0));
                            else
                                Bitmap_lvl.SetPixel(i, j, Color.FromArgb(255, 255, 255));
                        }
                    }
                    break;
                case 7:
                    for (int i = 0; i < width; i++)
                    {
                        for (int j = 0; j < height; j++)
                        {
                            Color pixel1 = Bitmap_lvl.GetPixel(i, j);
                            if (levels(pixel1.R)[0] != 1 && levels(pixel1.R)[1] == 1)
                                Bitmap_lvl.SetPixel(i, j, Color.FromArgb(0, 0, 0));
                            else
                                Bitmap_lvl.SetPixel(i, j, Color.FromArgb(255, 255, 255));
                        }
                    }
                    break;
                case 6:
                    for (int i = 0; i < width; i++)
                    {
                        for (int j = 0; j < height; j++)
                        {
                            Color pixel1 = Bitmap_lvl.GetPixel(i, j);
                            if (levels(pixel1.R)[0] != 1 && levels(pixel1.R)[1] != 1 && levels(pixel1.R)[2] == 1)
                                Bitmap_lvl.SetPixel(i, j, Color.FromArgb(0, 0, 0));
                            else
                                Bitmap_lvl.SetPixel(i, j, Color.FromArgb(255, 255, 255));
                        }
                    }
                    break;
                case 5:
                    for (int i = 0; i < width; i++)
                    {
                        for (int j = 0; j < height; j++)
                        {
                            Color pixel1 = Bitmap_lvl.GetPixel(i, j);
                            if (levels(pixel1.R)[0] != 1 && levels(pixel1.R)[1] != 1 &&
                                levels(pixel1.R)[2] != 1 && levels(pixel1.R)[3] == 1)
                                Bitmap_lvl.SetPixel(i, j, Color.FromArgb(0, 0, 0));
                            else
                                Bitmap_lvl.SetPixel(i, j, Color.FromArgb(255, 255, 255));
                        }
                    }
                    break;
                case 4:
                    for (int i = 0; i < width; i++)
                    {
                        for (int j = 0; j < height; j++)
                        {
                            Color pixel1 = Bitmap_lvl.GetPixel(i, j);
                            if (levels(pixel1.R)[0] != 1 && levels(pixel1.R)[1] != 1 &&
                                levels(pixel1.R)[2] != 1 && levels(pixel1.R)[3] != 1 &&
                                levels(pixel1.R)[4] == 1)
                                Bitmap_lvl.SetPixel(i, j, Color.FromArgb(0, 0, 0));
                            else
                                Bitmap_lvl.SetPixel(i, j, Color.FromArgb(255, 255, 255));
                        }
                    }
                    break;
                case 3:
                    for (int i = 0; i < width; i++)
                    {
                        for (int j = 0; j < height; j++)
                        {
                            Color pixel1 = Bitmap_lvl.GetPixel(i, j);
                            if (levels(pixel1.R)[0] != 1 && levels(pixel1.R)[1] != 1 &&
                                levels(pixel1.R)[2] != 1 && levels(pixel1.R)[3] != 1 &&
                                levels(pixel1.R)[4] != 1 && levels(pixel1.R)[5] == 1)
                                Bitmap_lvl.SetPixel(i, j, Color.FromArgb(0, 0, 0));
                            else
                                Bitmap_lvl.SetPixel(i, j, Color.FromArgb(255, 255, 255));
                        }
                    }
                    break;
                case 2:
                    for (int i = 0; i < width; i++)
                    {
                        for (int j = 0; j < height; j++)
                        {
                            Color pixel1 = Bitmap_lvl.GetPixel(i, j);
                            if (levels(pixel1.R)[0] != 1 && levels(pixel1.R)[1] != 1 &&
                                levels(pixel1.R)[2] != 1 && levels(pixel1.R)[3] != 1 &&
                                levels(pixel1.R)[4] != 1 && levels(pixel1.R)[5] != 1 &&
                                levels(pixel1.R)[6] == 1)
                                Bitmap_lvl.SetPixel(i, j, Color.FromArgb(0, 0, 0));
                            else
                                Bitmap_lvl.SetPixel(i, j, Color.FromArgb(255, 255, 255));
                        }
                    }
                    break;
                case 1:
                    for (int i = 0; i < width; i++)
                    {
                        for (int j = 0; j < height; j++)
                        {
                            Color pixel1 = Bitmap_lvl.GetPixel(i, j);
                            if (levels(pixel1.R)[0] != 1 && levels(pixel1.R)[1] != 1 &&
                                levels(pixel1.R)[2] != 1 && levels(pixel1.R)[3] != 1 &&
                                levels(pixel1.R)[4] != 1 && levels(pixel1.R)[5] != 1 &&
                                levels(pixel1.R)[6] != 1 && levels(pixel1.R)[7] == 1)
                                Bitmap_lvl.SetPixel(i, j, Color.FromArgb(0, 0, 0));
                            else
                                Bitmap_lvl.SetPixel(i, j, Color.FromArgb(255, 255, 255));
                        }
                    }
                    break;
                default:
                    break;
            }
            pic_res.Image = Bitmap_lvl;
        }

        private int[] levels(int temp)
        {
            int temp1 = 0;
            List<int> s = new List<int>();
            while (temp > 0)
            {
                temp1 = temp % 2;
                temp = temp / 2;
                s.Add(temp1);
            }
            if (s.Count < 8)
            {
                int d = s.Count;
                for (int i = 0; i < 8 - d; i++)
                {
                    s.Add(0);
                }
            }

            int[] reverse = new int[s.Count];
            for (int i = s.Count - 1; i >= 0; i--)
            {
                reverse[s.Count - 1 - i] = s[i];
            }
            return reverse;
        }

        private void But_lin_Click(object sender, EventArgs e) // Кусочно-линейное преобразование
        {
            Bitmap Bitmap_lin = new Bitmap(pic.Image);
            int s; // значение результата степенного преобразования
            int height, width; // высота и ширина изображения

            height = Bitmap_lin.Height;
            width = Bitmap_lin.Width;
            chart1.Series[0].Points.Clear(); // Удаление точек на графике
            int r1 = 70; // Коэффициенты
            int s1 = 0;
            int r2 = 140;
            int s2 = 255;
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel = Bitmap_lin.GetPixel(i, j);
                    int x = pixel.R;

                    if ((x >= 0) && (x <= r1))
                    {
                        s = Convert.ToInt32((s1 / r1) * x);
                    }
                    else
                    {
                        if ((x > r1) && (x <= r2))
                        {
                            s = Convert.ToInt32(((s2 - s1) / (r2 - r1)) * (x - r1) + s1);
                        }
                        else
                        {
                            s = Convert.ToInt32(((255 - s2) / (255 - r2)) * (x - r2) + s2);
                        }
                    }
                    //chart1.Series[0].Points.AddXY(x, s);
                    Bitmap_lin.SetPixel(i, j, Color.FromArgb(s, s, s));
                }
            }

            for (int i = 0; i < 255; i++)
            {
                int x = i;

                if ((x >= 0) && (x <= r1))
                {
                    s = Convert.ToInt32((s1 / r1) * x);
                }
                else
                {
                    if ((x > r1) && (x <= r2))
                    {
                        s = Convert.ToInt32(((s2 - s1) / (r2 - r1)) * (x - r1) + s1);
                    }
                    else
                    {
                        s = Convert.ToInt32(((255 - s2) / (255 - r2)) * (x - r2) + s2);
                    }
                }
                chart1.Series[0].Points.AddXY(i, s);
            }
            pic_res.Image = Bitmap_lin;
        }

        private void But_pow040_Click(object sender, EventArgs e) // Степенное преобразование 0.4
        {
            Bitmap Bitmap_pow = new Bitmap(pic.Image);
            int s; // значение результата степенного преобразования
            int height, width; // высота и ширина изображения
            double C = 255; // const C

            height = Bitmap_pow.Height;
            width = Bitmap_pow.Width;
            chart1.Series[0].Points.Clear(); // Удаление точек на графике

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_pow.GetPixel(i, j);
                    s = Convert.ToInt32(C * Math.Pow(pixel1.R / 255.0, 0.4));
                    Bitmap_pow.SetPixel(i, j, Color.FromArgb(s, s, s));
                }
            }

            for (int i = 0; i < 255; i++)
            {
                s = Convert.ToInt32(C * Math.Pow(i / 255.0, 0.4));
                chart1.Series[0].Points.AddXY(i, s);
            }
            pic_res.Image = Bitmap_pow;
        }

        private void But_pow2_5_Click(object sender, EventArgs e) // Степенное преобразование 2.5
        {
            Bitmap Bitmap_pow = new Bitmap(pic.Image);
            int s; // значение результата степенного преобразования
            int height, width; // высота и ширина изображения
            double C = 255; // const C

            height = Bitmap_pow.Height;
            width = Bitmap_pow.Width;
            chart1.Series[0].Points.Clear(); // Удаление точек на графике

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_pow.GetPixel(i, j);
                    s = Convert.ToInt32(C * Math.Pow(pixel1.R / 255.0, 2.5));
                    Bitmap_pow.SetPixel(i, j, Color.FromArgb(s, s, s));
                }
            }

            for (int i = 0; i < 255; i++)
            {
                s = Convert.ToInt32(C * Math.Pow(i / 255.0, 2.5));
                chart1.Series[0].Points.AddXY(i, s);
            }
            pic_res.Image = Bitmap_pow;
        }

        private void But_log_Click(object sender, EventArgs e) // Логарифмическое преобразование
        {
            Bitmap Bitmap_log = new Bitmap(pic.Image);
            int s; // значение результата лог преобразования
            int height, width; // высота и ширина изображений

            height = Bitmap_log.Height;
            width = Bitmap_log.Width;
            chart1.Series[0].Points.Clear(); // Удаление точек на графике

            double C1 = 255 / (Math.Log10(1 + Convert.ToInt32(label_m.Text))); // Расчёт C

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_log.GetPixel(i, j);
                    s = Convert.ToInt32(C1 * Math.Log10(1 + pixel1.R));
                    Bitmap_log.SetPixel(i, j, Color.FromArgb(s, s, s));
                }
            }

            for(int i = 0; i < 255; i++)
            {
                s = Convert.ToInt32(C1 * Math.Log10(1 + i));
                chart1.Series[0].Points.AddXY(i, s);
            }
            pic_res.Image = Bitmap_log;
        }

        private void But_neg_Click(object sender, EventArgs e) //  преобразование изображения в негатив
        {
            Bitmap Bitmap_neg = new Bitmap(pic.Image);
            int x1; // значение каналов пикселя
            int height, width; // высота и ширина изображения

            height = Bitmap_neg.Height;
            width = Bitmap_neg.Width;
            chart1.Series[0].Points.Clear(); // Удаление точек на графике

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_neg.GetPixel(i, j);
                    // Расчет значений канала пикселя и перевод в негатив
                    x1 = 255 - Convert.ToInt32(0.299 * pixel1.R + 0.587 * pixel1.G + 0.114 * pixel1.B);
                    Bitmap_neg.SetPixel(i, j, Color.FromArgb(x1, x1, x1));

                    chart1.Series[0].Points.AddXY
                        (Convert.ToInt32(0.299 * pixel1.R + 0.587 * pixel1.G + 0.114 * pixel1.B), x1);
                }
            }
            pic_res.Image = Bitmap_neg;
        }

        private void But_gray_Click(object sender, EventArgs e) //  преобразование изображения в черно-белое
        {
            Bitmap Bitmap_gray = new Bitmap(pic.Image);
            int m = 0; // для дальнейшего расчета C
            int x1; // значение каналов пикселя
            int height, width; // высота и ширина изображений

            height = Bitmap_gray.Height;
            width = Bitmap_gray.Width;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_gray.GetPixel(i, j);
                    // Расчет значений канала пикселя
                    x1 = Convert.ToInt32(0.299 * pixel1.R + 0.587 * pixel1.G + 0.114 * pixel1.B);
                    Bitmap_gray.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                    if (x1 > m)
                    {
                        m = x1;
                    }
                }
            }
            pic_res.Image = Bitmap_gray;
            pic.Image = Bitmap_gray;

            // Открытие кнопок
            but_neg.Enabled = true;
            but_log.Enabled = true;
            but_pow2_5.Enabled = true;
            but_pow040.Enabled = true;
            but_lin.Enabled = true;
            but_level.Enabled = true;

            label_m.Text = Convert.ToString(m);
        }

        private void But_save_Click(object sender, EventArgs e) // Сохранение изображения
        {
            if (pic_res.Image != null) //если в pictureBox есть изображение
            {
                //создание диалогового окна "Сохранить как..", для сохранения изображения
                SaveFileDialog savedialog = new SaveFileDialog();
                savedialog.Title = "Сохранить картинку как...";
                //отображать ли предупреждение, если пользователь указывает имя уже существующего файла
                savedialog.OverwritePrompt = true;
                //отображать ли предупреждение, если пользователь указывает несуществующий путь
                savedialog.CheckPathExists = true;
                //список форматов файла, отображаемый в поле "Тип файла"
                savedialog.Filter = "Image Files(*.BMP)|*.BMP|Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";
                //отображается ли кнопка "Справка" в диалоговом окне
                savedialog.ShowHelp = true;
                if (savedialog.ShowDialog() == DialogResult.OK) //если в диалоговом окне нажата кнопка "ОК"
                {
                    try
                    {
                        pic_res.Image.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void But_open_Click(object sender, EventArgs e) // Загрузка изображения
        {
            Bitmap image; //Bitmap для открываемого изображения

            OpenFileDialog open_dialog = new OpenFileDialog(); //создание диалогового окна для выбора файла
            open_dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*"; //формат файла
            if (open_dialog.ShowDialog() == DialogResult.OK) //если в окне была нажата кнопка "ОК"
            {
                try
                {
                    image = new Bitmap(open_dialog.FileName);
                    this.pic.Size = image.Size;
                    pic.Image = image;
                    pic.Invalidate();
                    but_gray.Enabled = true;
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
