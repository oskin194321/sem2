﻿namespace tz_2_lab
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.but_open = new System.Windows.Forms.Button();
            this.pic = new System.Windows.Forms.PictureBox();
            this.pic_res = new System.Windows.Forms.PictureBox();
            this.but_save = new System.Windows.Forms.Button();
            this.but_calc = new System.Windows.Forms.Button();
            this.num_M = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_res)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_M)).BeginInit();
            this.SuspendLayout();
            // 
            // but_open
            // 
            this.but_open.Location = new System.Drawing.Point(12, 12);
            this.but_open.Name = "but_open";
            this.but_open.Size = new System.Drawing.Size(189, 30);
            this.but_open.TabIndex = 0;
            this.but_open.Text = "Загрузить";
            this.but_open.UseVisualStyleBackColor = true;
            // 
            // pic
            // 
            this.pic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic.Location = new System.Drawing.Point(12, 48);
            this.pic.Name = "pic";
            this.pic.Size = new System.Drawing.Size(400, 400);
            this.pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic.TabIndex = 1;
            this.pic.TabStop = false;
            // 
            // pic_res
            // 
            this.pic_res.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_res.Location = new System.Drawing.Point(418, 48);
            this.pic_res.Name = "pic_res";
            this.pic_res.Size = new System.Drawing.Size(400, 400);
            this.pic_res.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_res.TabIndex = 2;
            this.pic_res.TabStop = false;
            // 
            // but_save
            // 
            this.but_save.Location = new System.Drawing.Point(629, 12);
            this.but_save.Name = "but_save";
            this.but_save.Size = new System.Drawing.Size(189, 30);
            this.but_save.TabIndex = 3;
            this.but_save.Text = "Сохранить";
            this.but_save.UseVisualStyleBackColor = true;
            // 
            // but_calc
            // 
            this.but_calc.Location = new System.Drawing.Point(317, 12);
            this.but_calc.Name = "but_calc";
            this.but_calc.Size = new System.Drawing.Size(189, 30);
            this.but_calc.TabIndex = 4;
            this.but_calc.Text = "Интерполяция";
            this.but_calc.UseVisualStyleBackColor = true;
            // 
            // num_M
            // 
            this.num_M.Location = new System.Drawing.Point(234, 19);
            this.num_M.Name = "num_M";
            this.num_M.Size = new System.Drawing.Size(77, 22);
            this.num_M.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 464);
            this.Controls.Add(this.num_M);
            this.Controls.Add(this.but_calc);
            this.Controls.Add(this.but_save);
            this.Controls.Add(this.pic_res);
            this.Controls.Add(this.pic);
            this.Controls.Add(this.but_open);
            this.Name = "Form1";
            this.Text = "2 lab Оценка влияния параметров дискретизации на точность передачи изображения";
            ((System.ComponentModel.ISupportInitialize)(this.pic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_res)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_M)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button but_open;
        private System.Windows.Forms.PictureBox pic;
        private System.Windows.Forms.PictureBox pic_res;
        private System.Windows.Forms.Button but_save;
        private System.Windows.Forms.Button but_calc;
        private System.Windows.Forms.NumericUpDown num_M;
    }
}

