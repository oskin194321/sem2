﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tz_2_lab
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            but_calc.Click += But_calc_Click;
            but_open.Click += But_open_Click;
            but_save.Click += But_save_Click;
        }

        private void But_calc_Click(object sender, EventArgs e) // Интерполяция
        {            
            int width = pic.Image.Width; // Ширина исходного изображения
            int height = pic.Image.Height; // Высота
            int m_width = width * (int)num_M.Value; // Ширина нового изображения
            int m_height = height * (int)num_M.Value; // Высота

            Bitmap Pic = new Bitmap(pic.Image);
            Bitmap NewPic = new Bitmap(m_width, m_height);
            int k = 0, l;
            for (int i = 0; i < width; i++) // Интерполяция
            {
                l = 0;
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Pic.GetPixel(i, j);
                    for (int m = k; m < k + (int)num_M.Value; m++)
                    {
                        for (int n = l; n < l + (int)num_M.Value; n++)
                        {
                            NewPic.SetPixel(m, n, pixel1);
                        }
                    }
                    l = l + (int)num_M.Value;
                }
                k = k + (int)num_M.Value;
            }
            pic_res.Image = NewPic;
        }

        private void But_open_Click(object sender, EventArgs e) // Загрузка изображения
        {
            Bitmap image; //Bitmap для открываемого изображения
            OpenFileDialog open_dialog = new OpenFileDialog(); //создание диалогового окна для выбора файла
            open_dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*"; //формат файла
            if (open_dialog.ShowDialog() == DialogResult.OK) //если в окне была нажата кнопка "ОК"
            {
                try
                {
                    image = new Bitmap(open_dialog.FileName);
                    //this.pic.Size = image.Size;
                    pic.Image = image;
                    pic.Invalidate();
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void But_save_Click(object sender, EventArgs e) // Сохранение изображения
        {
            if (pic_res.Image != null) //если в pictureBox есть изображение
            {
                //создание диалогового окна "Сохранить как..", для сохранения изображения
                SaveFileDialog savedialog = new SaveFileDialog();
                savedialog.Title = "Сохранить картинку как...";
                //отображать ли предупреждение, если пользователь указывает имя уже существующего файла
                savedialog.OverwritePrompt = true;
                //отображать ли предупреждение, если пользователь указывает несуществующий путь
                savedialog.CheckPathExists = true;
                //список форматов файла, отображаемый в поле "Тип файла"
                savedialog.Filter = "Image Files(*.jpg)|*.jpg|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";
                //отображается ли кнопка "Справка" в диалоговом окне
                savedialog.ShowHelp = true;
                if (savedialog.ShowDialog() == DialogResult.OK) //если в диалоговом окне нажата кнопка "ОК"
                {
                    try
                    {
                        pic_res.Image.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}
