﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tz_4
{
    public partial class Form1 : Form
    {
        double[] sinusoid; // Значение точек синусоиды
        double[] quant; // Значение точек после квантования

        public Form1()
        {
            InitializeComponent();

            but_sin.Click += But_sin_Click;
            but_q.Click += But_q_Click;
            but_qn.Click += But_qn_Click;

            but_save_sin.Click += But_save_sin_Click;
            but_save_q.Click += But_save_q_Click;
            but_save_qn.Click += But_save_qn_Click;
        }

        private void But_q_Click(object sender, EventArgs e) // Квантование сигнала
        {
            Bitmap Bitmap = new Bitmap(300, 300);
            int x = 0; // Значение канала пикселя
            int height, width; // высота и ширина изображения
            double[] hist = new double[256]; // Массив для построения гистограммы

            height = Bitmap.Height;
            width = Bitmap.Width;

            quant = new double[width];

            int lvl = Convert.ToInt32(lvl_num.SelectedItem.ToString(), NumberFormatInfo.InvariantInfo); // Число уровней
            double step = 1 / Convert.ToDouble(lvl); // шаг квантования
            double[] mas_lvl = new double[lvl + 1];
            mas_lvl[0] = 0;
            for (int i = 1; i < mas_lvl.Length; i++) // Вычисление уровней квантования
            {
                mas_lvl[i] = mas_lvl[i - 1] + step;
            }
            double delta = 1;
            int index = 0;
            for (int i = 0; i < width; i++) // Вычисление линейного квантования
            {
                if (sinusoid[i] >= Convert.ToDouble(bottom.Text, NumberFormatInfo.InvariantInfo) 
                    && sinusoid[i] <= Convert.ToDouble(top.Text, NumberFormatInfo.InvariantInfo))
                {
                    delta = 1;
                    for (int j = 0; j < mas_lvl.Length; j++)
                    {
                        if (Math.Abs(sinusoid[i] - mas_lvl[j]) < delta)
                        {
                            delta = Math.Abs(sinusoid[i] - mas_lvl[j]);
                            index = j;
                        }
                    }
                    quant[i] = mas_lvl[index];
                }
                else
                {
                    quant[i] = sinusoid[i];
                }
            }

            for (int i = 0; i < width; i++) // Получение изображения
            {
                for (int j = 0; j < height; j++)
                {
                    x = Convert.ToInt32(quant[j]*255);
                    Bitmap.SetPixel(j, i, Color.FromArgb(x, x, x));
                    hist[x]++;
                }
            }
            pic_q.Image = Bitmap;
            calc_diff(pic_sin.Image, pic_q.Image);

            string tofile = "";
            for (int i = 0; i < hist.Length; i++)
            {
                hist[i] = hist[i] / (width * height);
                tofile += hist[i] + Environment.NewLine;
            }
            System.IO.File.WriteAllText("hist_q.txt", tofile);
        }

        private void But_qn_Click(object sender, EventArgs e)
        {
            Bitmap Bitmap = new Bitmap(300, 300);
            int x = 0; // Значение канала пикселя
            int height, width; // высота и ширина изображения

            height = Bitmap.Height;
            width = Bitmap.Width;

            double[] n_quant; // Значение точек после неравномерного квантования
            n_quant = new double[width];

            double[] n_lvl = new double[12] 
            { 0, 20.0/255.0, 40.0/255.0, 55.0/255.0, 92.0/255.0, 106.0/255.0, 120.0/255.0,
                134.0 /255.0, 195.0/255.0, 215.0/255.0, 232.0/255.0, 255.0/255.0}; // уровни квантования
            

            double delta = 1;
            int index = 0;
            for (int i = 0; i < width; i++)
            {

                if (quant[i] >= Convert.ToDouble(bottom.Text, NumberFormatInfo.InvariantInfo) 
                    && quant[i] <= Convert.ToDouble(top.Text, NumberFormatInfo.InvariantInfo))
                {
                    delta = 1;
                    for (int j = 0; j < n_lvl.Length; j++)
                    {
                        if (Math.Abs(sinusoid[i] - n_lvl[j]) < delta)
                        {
                            delta = Math.Abs(quant[i] - n_lvl[j]);
                            index = j;
                        }
                    }
                    n_quant[i] = n_lvl[index];
                }
                else
                {
                    n_quant[i] = quant[i];
                }

            }

            for (int i = 0; i < width; i++) // Получение изображения
            {
                for (int j = 0; j < height; j++)
                {
                    x = Convert.ToInt32(n_quant[j] * 255);
                    Bitmap.SetPixel(j, i, Color.FromArgb(x, x, x));
                }
            }
            pic_qn.Image = Bitmap;
            calc_diff(pic_q.Image, pic_qn.Image);
        }

        private void But_sin_Click(object sender, EventArgs e)
        {
            Bitmap Bitmap = new Bitmap(300, 300);
            int x = 0; // Значение канала пикселя
            int height, width; // высота и ширина изображения
            double fs; // Частота дискретизации

            height = Bitmap.Height;
            width = Bitmap.Width;
            sinusoid = new double[width];

            // Вычисление исходного сигнала с заданной частотой
            fs = Convert.ToDouble(fs_box.Text) / 2;
            // Создание сигнала в форме синусоиды
            sinusoid = Sinusoid(fs, 0, width);

            for (int i = 0; i < width; i++) // Получение изображения
            {
                for (int j = 0; j < height; j++)
                {
                    x = Convert.ToInt32(sinusoid[j]*255);
                    Bitmap.SetPixel(j, i, Color.FromArgb(x, x, x));
                }
            }
            pic_sin.Image = Bitmap;
        }

        double[] Sinusoid(double fs, double shift, int dots) // Вычисление синусоидального сигнала
        {
            double[] x = new double[dots];
            x[0] = 0;
            for (int i = 1; i < dots; i++)
            {
                x[i] = x[i - 1] + 1;
            }
            double[] y = new double[dots];
            for (int i = 0; i < dots; i++)
            {
                y[i] = 0.5 + 0.5 * Math.Sin(fs * x[i] + shift);
            }
            return y;
        }

        private void calc_diff(Image pic1, Image pic2) // Вычисление метрик полученных изображений
        {
            Bitmap Bitmap1 = new Bitmap(pic1);
            Bitmap Bitmap2 = new Bitmap(pic2);
            double result_mse = 0.0, result_psnr = 0.0, result_ssim = 0.0;

            result_mse = mse(Bitmap1, Bitmap2);
            result_psnr = psnr(result_mse);
            result_ssim = ssim(Bitmap1, Bitmap2);

            label_mse.Text = "MSE: " + Convert.ToString(result_mse);
            label_psnr.Text = "PSNR: " + Convert.ToString(result_psnr);
            label_ssim.Text = "SSIM: " + Convert.ToString(result_ssim);
        }

        private double mse(Bitmap Bitmap1, Bitmap Bitmap2) // Расчет показателя MSE
        {
            double calc_mse = 0.0;  // MSE
            int x1, x2; // значения каналов пикселя для 1 и 2 изображения
            int height, width; // высота и ширина изображений

            height = Bitmap1.Height;
            width = Bitmap1.Width;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap1.GetPixel(i, j);
                    Color pixel2 = Bitmap2.GetPixel(i, j);
                    // Расчет значений каналов пикселей
                    x1 = Convert.ToInt32(0.299 * pixel1.R + 0.587 * pixel1.G + 0.114 * pixel1.B);
                    x2 = Convert.ToInt32(0.299 * pixel2.R + 0.587 * pixel2.G + 0.114 * pixel2.B);

                    calc_mse += Math.Pow(x1 - x2, 2);
                }
            }

            calc_mse = calc_mse / (width * height);
            calc_mse = Math.Round(calc_mse, 5);
            return calc_mse;
        }

        private double ssim(Bitmap Bitmap1, Bitmap Bitmap2) // Расчет показателя SSIM
        {
            double calc_ssim = 0.0;

            int x = 0, y = 0; // значения каналов пикселя для 1 и 2 изображения
            double m_x = 0.0, m_y = 0.0, o_x = 0.0, o_y = 0.0, o_xy = 0.0; // Среднее значение и стандартное отклонение, ковариация
            double c1, c2; // Выравнивающие коэффициенты
            int height, width; // высота и ширина изображений

            height = Bitmap1.Height;
            width = Bitmap1.Width;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap1.GetPixel(i, j);
                    Color pixel2 = Bitmap2.GetPixel(i, j);
                    // Расчет значений каналов пикселей
                    x += Convert.ToInt32(0.299 * pixel1.R + 0.587 * pixel1.G + 0.114 * pixel1.B);
                    y += Convert.ToInt32(0.299 * pixel2.R + 0.587 * pixel2.G + 0.114 * pixel2.B);
                }
            }

            m_x = x / (height * width);
            m_y = y / (height * width);

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap1.GetPixel(i, j);
                    Color pixel2 = Bitmap2.GetPixel(i, j);
                    o_x += Math.Pow(0.299 * pixel1.R + 0.587 * pixel1.G + 0.114 * pixel1.B - m_x, 2);
                    o_y += Math.Pow(0.299 * pixel2.R + 0.587 * pixel2.G + 0.114 * pixel2.B - m_y, 2);
                    o_xy += (0.299 * pixel1.R + 0.587 * pixel1.G + 0.114 * pixel1.B - m_x) *
                        (0.299 * pixel2.R + 0.587 * pixel2.G + 0.114 * pixel2.B - m_y);
                }
            }

            o_x = o_x / (width * height);
            o_y = o_y / (width * height);
            o_xy = o_xy / (width * height);
            c1 = Math.Pow(0.01 * 255, 2);
            c2 = Math.Pow(0.03 * 255, 2);

            calc_ssim = ((2 * m_x * m_y + c1) * (2 * o_xy + c2)) /
                ((Math.Pow(m_x, 2) + Math.Pow(m_y, 2) + c1) * (o_x + o_y + c2));
            calc_ssim = Math.Round(calc_ssim, 5);
            return calc_ssim;
        }

        private double psnr(double mse) // Расчет показателя PSNR
        {
            double calc_psnr = 0.0;  // PSNR
            if (mse != 0.0)
            {
                calc_psnr = 20 * Math.Log10(255 / Math.Sqrt(mse));
                calc_psnr = Math.Round(calc_psnr, 5);
            }
            return calc_psnr;
        }

        private void save_image(Image pic) // Сохранение изображения
        {
            if (pic != null) //если в pictureBox есть изображение
            {
                //создание диалогового окна "Сохранить как..", для сохранения изображения
                SaveFileDialog savedialog = new SaveFileDialog();
                savedialog.Title = "Сохранить картинку как...";
                //отображать ли предупреждение, если пользователь указывает имя уже существующего файла
                savedialog.OverwritePrompt = true;
                //отображать ли предупреждение, если пользователь указывает несуществующий путь
                savedialog.CheckPathExists = true;
                //список форматов файла
                savedialog.Filter = "Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";
                //отображается ли кнопка "Справка" в диалоговом окне
                savedialog.ShowHelp = true;
                if (savedialog.ShowDialog() == DialogResult.OK) //если в диалоговом окне нажата кнопка "ОК"
                {
                    try
                    {
                        pic.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
        private void But_save_qn_Click(object sender, EventArgs e)
        {
            save_image(pic_qn.Image);
        }

        private void But_save_q_Click(object sender, EventArgs e)
        {
            save_image(pic_q.Image);
        }

        private void But_save_sin_Click(object sender, EventArgs e)
        {
            save_image(pic_sin.Image);
        }
    }
}
