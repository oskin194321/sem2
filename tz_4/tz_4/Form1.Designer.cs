﻿namespace tz_4
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pic_sin = new System.Windows.Forms.PictureBox();
            this.but_sin = new System.Windows.Forms.Button();
            this.but_save_sin = new System.Windows.Forms.Button();
            this.but_save_q = new System.Windows.Forms.Button();
            this.but_q = new System.Windows.Forms.Button();
            this.pic_q = new System.Windows.Forms.PictureBox();
            this.but_save_qn = new System.Windows.Forms.Button();
            this.but_qn = new System.Windows.Forms.Button();
            this.pic_qn = new System.Windows.Forms.PictureBox();
            this.fs_box = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lvl_num = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.bottom = new System.Windows.Forms.TextBox();
            this.top = new System.Windows.Forms.TextBox();
            this.label_mse = new System.Windows.Forms.Label();
            this.label_psnr = new System.Windows.Forms.Label();
            this.label_ssim = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pic_sin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_q)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_qn)).BeginInit();
            this.SuspendLayout();
            // 
            // pic_sin
            // 
            this.pic_sin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_sin.Location = new System.Drawing.Point(12, 12);
            this.pic_sin.Name = "pic_sin";
            this.pic_sin.Size = new System.Drawing.Size(300, 300);
            this.pic_sin.TabIndex = 0;
            this.pic_sin.TabStop = false;
            // 
            // but_sin
            // 
            this.but_sin.Location = new System.Drawing.Point(12, 439);
            this.but_sin.Name = "but_sin";
            this.but_sin.Size = new System.Drawing.Size(300, 30);
            this.but_sin.TabIndex = 1;
            this.but_sin.Text = "Получить синусоидальный сигнал";
            this.but_sin.UseVisualStyleBackColor = true;
            // 
            // but_save_sin
            // 
            this.but_save_sin.Location = new System.Drawing.Point(12, 475);
            this.but_save_sin.Name = "but_save_sin";
            this.but_save_sin.Size = new System.Drawing.Size(300, 30);
            this.but_save_sin.TabIndex = 2;
            this.but_save_sin.Text = "Сохранить изображение";
            this.but_save_sin.UseVisualStyleBackColor = true;
            // 
            // but_save_q
            // 
            this.but_save_q.Location = new System.Drawing.Point(400, 475);
            this.but_save_q.Name = "but_save_q";
            this.but_save_q.Size = new System.Drawing.Size(300, 30);
            this.but_save_q.TabIndex = 5;
            this.but_save_q.Text = "Сохранить изображение";
            this.but_save_q.UseVisualStyleBackColor = true;
            // 
            // but_q
            // 
            this.but_q.Location = new System.Drawing.Point(400, 439);
            this.but_q.Name = "but_q";
            this.but_q.Size = new System.Drawing.Size(300, 30);
            this.but_q.TabIndex = 4;
            this.but_q.Text = "Равномерное квантование";
            this.but_q.UseVisualStyleBackColor = true;
            // 
            // pic_q
            // 
            this.pic_q.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_q.Location = new System.Drawing.Point(400, 12);
            this.pic_q.Name = "pic_q";
            this.pic_q.Size = new System.Drawing.Size(300, 300);
            this.pic_q.TabIndex = 3;
            this.pic_q.TabStop = false;
            // 
            // but_save_qn
            // 
            this.but_save_qn.Location = new System.Drawing.Point(787, 475);
            this.but_save_qn.Name = "but_save_qn";
            this.but_save_qn.Size = new System.Drawing.Size(300, 30);
            this.but_save_qn.TabIndex = 8;
            this.but_save_qn.Text = "Сохранить изображение";
            this.but_save_qn.UseVisualStyleBackColor = true;
            // 
            // but_qn
            // 
            this.but_qn.Location = new System.Drawing.Point(787, 439);
            this.but_qn.Name = "but_qn";
            this.but_qn.Size = new System.Drawing.Size(300, 30);
            this.but_qn.TabIndex = 7;
            this.but_qn.Text = "Неравномерное квантование";
            this.but_qn.UseVisualStyleBackColor = true;
            // 
            // pic_qn
            // 
            this.pic_qn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_qn.Location = new System.Drawing.Point(787, 12);
            this.pic_qn.Name = "pic_qn";
            this.pic_qn.Size = new System.Drawing.Size(300, 300);
            this.pic_qn.TabIndex = 6;
            this.pic_qn.TabStop = false;
            // 
            // fs_box
            // 
            this.fs_box.Location = new System.Drawing.Point(185, 411);
            this.fs_box.Name = "fs_box";
            this.fs_box.Size = new System.Drawing.Size(127, 22);
            this.fs_box.TabIndex = 9;
            this.fs_box.Text = "503";
            this.fs_box.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 414);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(167, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Частота дискретизации";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(397, 414);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(195, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Число уровней квантования";
            // 
            // lvl_num
            // 
            this.lvl_num.FormattingEnabled = true;
            this.lvl_num.Items.AddRange(new object[] {
            "2",
            "8",
            "32",
            "64",
            "128",
            "256"});
            this.lvl_num.Location = new System.Drawing.Point(601, 411);
            this.lvl_num.Name = "lvl_num";
            this.lvl_num.Size = new System.Drawing.Size(99, 24);
            this.lvl_num.TabIndex = 12;
            this.lvl_num.Text = "2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(397, 324);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(228, 17);
            this.label3.TabIndex = 13;
            this.label3.Text = "Границы интервала квантования";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(397, 352);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 17);
            this.label4.TabIndex = 14;
            this.label4.Text = "Нижняя граница";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(397, 379);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 17);
            this.label5.TabIndex = 15;
            this.label5.Text = "Верхняя граница";
            // 
            // bottom
            // 
            this.bottom.Location = new System.Drawing.Point(601, 349);
            this.bottom.Name = "bottom";
            this.bottom.Size = new System.Drawing.Size(99, 22);
            this.bottom.TabIndex = 16;
            this.bottom.Text = "0";
            this.bottom.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // top
            // 
            this.top.Location = new System.Drawing.Point(601, 376);
            this.top.Name = "top";
            this.top.Size = new System.Drawing.Size(99, 22);
            this.top.TabIndex = 17;
            this.top.Text = "1";
            this.top.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label_mse
            // 
            this.label_mse.AutoSize = true;
            this.label_mse.Location = new System.Drawing.Point(461, 520);
            this.label_mse.Name = "label_mse";
            this.label_mse.Size = new System.Drawing.Size(37, 17);
            this.label_mse.TabIndex = 18;
            this.label_mse.Text = "MSE";
            // 
            // label_psnr
            // 
            this.label_psnr.AutoSize = true;
            this.label_psnr.Location = new System.Drawing.Point(461, 547);
            this.label_psnr.Name = "label_psnr";
            this.label_psnr.Size = new System.Drawing.Size(46, 17);
            this.label_psnr.TabIndex = 19;
            this.label_psnr.Text = "PSNR";
            // 
            // label_ssim
            // 
            this.label_ssim.AutoSize = true;
            this.label_ssim.Location = new System.Drawing.Point(461, 574);
            this.label_ssim.Name = "label_ssim";
            this.label_ssim.Size = new System.Drawing.Size(40, 17);
            this.label_ssim.TabIndex = 20;
            this.label_ssim.Text = "SSIM";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1101, 624);
            this.Controls.Add(this.label_ssim);
            this.Controls.Add(this.label_psnr);
            this.Controls.Add(this.label_mse);
            this.Controls.Add(this.top);
            this.Controls.Add(this.bottom);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lvl_num);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.fs_box);
            this.Controls.Add(this.but_save_qn);
            this.Controls.Add(this.but_qn);
            this.Controls.Add(this.pic_qn);
            this.Controls.Add(this.but_save_q);
            this.Controls.Add(this.but_q);
            this.Controls.Add(this.pic_q);
            this.Controls.Add(this.but_save_sin);
            this.Controls.Add(this.but_sin);
            this.Controls.Add(this.pic_sin);
            this.Name = "Form1";
            this.Text = "4 Выбор параметров квантования сигнала";
            ((System.ComponentModel.ISupportInitialize)(this.pic_sin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_q)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_qn)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pic_sin;
        private System.Windows.Forms.Button but_sin;
        private System.Windows.Forms.Button but_save_sin;
        private System.Windows.Forms.Button but_save_q;
        private System.Windows.Forms.Button but_q;
        private System.Windows.Forms.PictureBox pic_q;
        private System.Windows.Forms.Button but_save_qn;
        private System.Windows.Forms.Button but_qn;
        private System.Windows.Forms.PictureBox pic_qn;
        private System.Windows.Forms.TextBox fs_box;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox lvl_num;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox bottom;
        private System.Windows.Forms.TextBox top;
        private System.Windows.Forms.Label label_mse;
        private System.Windows.Forms.Label label_psnr;
        private System.Windows.Forms.Label label_ssim;
    }
}

