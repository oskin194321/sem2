﻿namespace oi_1_lab
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.but_open = new System.Windows.Forms.Button();
            this.but_log = new System.Windows.Forms.Button();
            this.but_pow = new System.Windows.Forms.Button();
            this.but_save_res = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_pow = new System.Windows.Forms.TextBox();
            this.but_norm = new System.Windows.Forms.Button();
            this.but_eq = new System.Windows.Forms.Button();
            this.pic = new System.Windows.Forms.PictureBox();
            this.pic_res = new System.Windows.Forms.PictureBox();
            this.but_save_gray = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label_min = new System.Windows.Forms.Label();
            this.label_max = new System.Windows.Forms.Label();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label_k = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_res)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // but_open
            // 
            this.but_open.Location = new System.Drawing.Point(13, 13);
            this.but_open.Name = "but_open";
            this.but_open.Size = new System.Drawing.Size(209, 30);
            this.but_open.TabIndex = 0;
            this.but_open.Text = "Загрузить";
            this.but_open.UseVisualStyleBackColor = true;
            // 
            // but_log
            // 
            this.but_log.Location = new System.Drawing.Point(12, 129);
            this.but_log.Name = "but_log";
            this.but_log.Size = new System.Drawing.Size(209, 30);
            this.but_log.TabIndex = 1;
            this.but_log.Text = "Логарифмическое";
            this.but_log.UseVisualStyleBackColor = true;
            // 
            // but_pow
            // 
            this.but_pow.Location = new System.Drawing.Point(13, 218);
            this.but_pow.Name = "but_pow";
            this.but_pow.Size = new System.Drawing.Size(209, 30);
            this.but_pow.TabIndex = 2;
            this.but_pow.Text = "Степенное";
            this.but_pow.UseVisualStyleBackColor = true;
            // 
            // but_save_res
            // 
            this.but_save_res.Location = new System.Drawing.Point(541, 502);
            this.but_save_res.Name = "but_save_res";
            this.but_save_res.Size = new System.Drawing.Size(307, 30);
            this.but_save_res.TabIndex = 3;
            this.but_save_res.Text = "Сохранить";
            this.but_save_res.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 193);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Степень:";
            // 
            // tb_pow
            // 
            this.tb_pow.Location = new System.Drawing.Point(120, 190);
            this.tb_pow.Name = "tb_pow";
            this.tb_pow.Size = new System.Drawing.Size(102, 22);
            this.tb_pow.TabIndex = 5;
            this.tb_pow.Text = "0,66";
            // 
            // but_norm
            // 
            this.but_norm.Location = new System.Drawing.Point(13, 289);
            this.but_norm.Name = "but_norm";
            this.but_norm.Size = new System.Drawing.Size(209, 30);
            this.but_norm.TabIndex = 6;
            this.but_norm.Text = "Нормализация";
            this.but_norm.UseVisualStyleBackColor = true;
            // 
            // but_eq
            // 
            this.but_eq.Location = new System.Drawing.Point(13, 325);
            this.but_eq.Name = "but_eq";
            this.but_eq.Size = new System.Drawing.Size(209, 30);
            this.but_eq.TabIndex = 7;
            this.but_eq.Text = "Эквализация";
            this.but_eq.UseVisualStyleBackColor = true;
            // 
            // pic
            // 
            this.pic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic.Location = new System.Drawing.Point(228, 13);
            this.pic.Name = "pic";
            this.pic.Size = new System.Drawing.Size(307, 473);
            this.pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic.TabIndex = 8;
            this.pic.TabStop = false;
            // 
            // pic_res
            // 
            this.pic_res.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_res.Location = new System.Drawing.Point(541, 13);
            this.pic_res.Name = "pic_res";
            this.pic_res.Size = new System.Drawing.Size(307, 473);
            this.pic_res.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_res.TabIndex = 9;
            this.pic_res.TabStop = false;
            // 
            // but_save_gray
            // 
            this.but_save_gray.Location = new System.Drawing.Point(228, 502);
            this.but_save_gray.Name = "but_save_gray";
            this.but_save_gray.Size = new System.Drawing.Size(307, 30);
            this.but_save_gray.TabIndex = 10;
            this.but_save_gray.Text = "Сохранить";
            this.but_save_gray.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Min";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "Max";
            // 
            // label_min
            // 
            this.label_min.AutoSize = true;
            this.label_min.Location = new System.Drawing.Point(69, 60);
            this.label_min.Name = "label_min";
            this.label_min.Size = new System.Drawing.Size(16, 17);
            this.label_min.TabIndex = 13;
            this.label_min.Text = "0";
            // 
            // label_max
            // 
            this.label_max.AutoSize = true;
            this.label_max.Location = new System.Drawing.Point(69, 93);
            this.label_max.Name = "label_max";
            this.label_max.Size = new System.Drawing.Size(32, 17);
            this.label_max.TabIndex = 14;
            this.label_max.Text = "255";
            // 
            // chart1
            // 
            chartArea1.AxisX.Maximum = 255D;
            chartArea1.AxisX.Minimum = 0D;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Location = new System.Drawing.Point(854, 108);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(403, 300);
            this.chart1.TabIndex = 15;
            this.chart1.Text = "chart1";
            // 
            // label_k
            // 
            this.label_k.AutoSize = true;
            this.label_k.Location = new System.Drawing.Point(994, 48);
            this.label_k.Name = "label_k";
            this.label_k.Size = new System.Drawing.Size(16, 17);
            this.label_k.TabIndex = 17;
            this.label_k.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(892, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 17);
            this.label5.TabIndex = 16;
            this.label5.Text = "Контраст К";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1263, 554);
            this.Controls.Add(this.label_k);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.label_max);
            this.Controls.Add(this.label_min);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.but_save_gray);
            this.Controls.Add(this.pic_res);
            this.Controls.Add(this.pic);
            this.Controls.Add(this.but_eq);
            this.Controls.Add(this.but_norm);
            this.Controls.Add(this.tb_pow);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.but_save_res);
            this.Controls.Add(this.but_pow);
            this.Controls.Add(this.but_log);
            this.Controls.Add(this.but_open);
            this.Name = "Form1";
            this.Text = "1 лаб Выбор параметров градационной коррекции на основе требований к конечному из" +
    "ображению";
            ((System.ComponentModel.ISupportInitialize)(this.pic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_res)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button but_open;
        private System.Windows.Forms.Button but_log;
        private System.Windows.Forms.Button but_pow;
        private System.Windows.Forms.Button but_save_res;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_pow;
        private System.Windows.Forms.Button but_norm;
        private System.Windows.Forms.Button but_eq;
        private System.Windows.Forms.PictureBox pic;
        private System.Windows.Forms.PictureBox pic_res;
        private System.Windows.Forms.Button but_save_gray;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label_min;
        private System.Windows.Forms.Label label_max;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Label label_k;
        private System.Windows.Forms.Label label5;
    }
}

