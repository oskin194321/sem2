﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oi_1_lab
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            but_log.Click += But_log_Click;
            but_pow.Click += But_pow_Click;
            but_norm.Click += But_norm_Click;
            but_eq.Click += But_eq_Click;

            but_open.Click += But_open_Click;
            but_save_gray.Click += But_save_gray_Click;
            but_save_res.Click += But_save_Click;
        }

        private void But_log_Click(object sender, EventArgs e)  // Логарифмическое преобразование
        {
            Bitmap Bitmap_log = new Bitmap(pic.Image);
            int s; // значение результата лог преобразования
            int height, width; // высота и ширина изображений

            height = Bitmap_log.Height;
            width = Bitmap_log.Width;

            int max = 0, min = 255; // для расчета контраста
            int[] hist = new int[256]; // Массив для подсчета количества пикселей с равным значением
            chart1.Series[0].Points.Clear(); // Удаление точек на графике

            double C1 = 255 / (Math.Log10(1 + Convert.ToInt32(label_max.Text))); // Расчёт C

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_log.GetPixel(i, j);
                    s = Convert.ToInt32(C1 * Math.Log10(1 + pixel1.R));
                    Bitmap_log.SetPixel(i, j, Color.FromArgb(s, s, s));
                    if (s > max)
                        max = s;
                    if (s < min)
                        min = s;
                    hist[s]++;
                }
            }

            for (int i = 0; i < 256; i++) // Построение гистограммы
                chart1.Series[0].Points.AddXY(i, hist[i]);
            label_k.Text = Convert.ToString(max - min); // Контраст
            pic_res.Image = Bitmap_log;
        }

        private void But_pow_Click(object sender, EventArgs e) // Степенное преобразование
        {
            Bitmap Bitmap_pow = new Bitmap(pic.Image);
            int s; // значение результата степенного преобразования
            int height, width; // высота и ширина изображения
            double pow = Convert.ToDouble(tb_pow.Text);
            double C = 255; // const C

            height = Bitmap_pow.Height;
            width = Bitmap_pow.Width;

            int max = 0, min = 255; // для расчета контраста
            int[] hist = new int[256]; // Массив для подсчета количества пикселей с равным значением
            chart1.Series[0].Points.Clear(); // Удаление точек на графике

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_pow.GetPixel(i, j);
                    s = Convert.ToInt32(C * Math.Pow(pixel1.R / 255.0, pow));
                    Bitmap_pow.SetPixel(i, j, Color.FromArgb(s, s, s));
                    if (s > max)
                        max = s;
                    if (s < min)
                        min = s;
                    hist[s]++;
                }
            }

            for (int i = 0; i < 256; i++) // Построение гистограммы
                chart1.Series[0].Points.AddXY(i, hist[i]);
            label_k.Text = Convert.ToString(max - min); // Контраст
            pic_res.Image = Bitmap_pow;
        }

        private void But_norm_Click(object sender, EventArgs e) // Нормализация
        {
            Bitmap Bitmap_norm = new Bitmap(pic.Image);
            int x1; // значение каналов пикселя
            int height, width; // высота и ширина изображения
            double a = 0.0, b = 0.0; // коэффициенты для расчета

            height = Bitmap_norm.Height;
            width = Bitmap_norm.Width;

            int max = 0, min = 255; // для расчета контраста
            int[] hist = new int[256]; // Массив для подсчета количества пикселей с равным значением
            chart1.Series[0].Points.Clear(); // Удаление точек на графике

            b = (255.0 - 0.0) / (Convert.ToDouble(label_max.Text) - Convert.ToDouble(label_min.Text));
            a = 0.0 - b * Convert.ToDouble(label_min.Text);

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_norm.GetPixel(i, j);
                    x1 = Convert.ToInt32(a + b * Convert.ToDouble(pixel1.R)); // Нормализация пикселя
                    Bitmap_norm.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                    if (x1 > max)
                        max = x1;
                    if (x1 < min)
                        min = x1;
                    hist[x1]++; // Увеличение количества пикселей с данным значением канала
                }
            }

            for (int i = 0; i < 256; i++) // Построение гистограммы
                chart1.Series[0].Points.AddXY(i, hist[i]);
            label_k.Text = Convert.ToString(max - min); // Контраст
            pic_res.Image = Bitmap_norm;
        }

        private void But_eq_Click(object sender, EventArgs e) // Эквализация
        {
            Bitmap Bitmap_eq = new Bitmap(pic.Image);
            int x1; // значение каналов пикселя
            int height, width; // высота и ширина изображения
            double[] hist_gray = new double[256];// Массив для значений черно-белого изображения
            double eq = 0.0; // Параметр эквализации

            height = Bitmap_eq.Height;
            width = Bitmap_eq.Width;

            int max = 0, min = 255; // для расчета контраста
            int[] hist = new int[256]; // Массив для подсчета количества пикселей с равным значением
            chart1.Series[0].Points.Clear(); // Удаление точек на графике

            for (int i = 0; i < width; i++) // Получение гистограммы черно-белого изображения
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_eq.GetPixel(i, j);
                    hist_gray[pixel1.R]++; // Увеличение количества пикселей с данным значением канала
                }
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_eq.GetPixel(i, j);
                    eq = 0.0;
                    for (int n = 0; n < pixel1.R; n++)
                    {
                        eq += hist_gray[n] / (width * height); // Вычисление параметра
                    }

                    x1 = Convert.ToInt32((hist_gray.Length - 1) * eq); // Расчет пикселя
                    Bitmap_eq.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                    if (x1 > max)
                        max = x1;
                    if (x1 < min)
                        min = x1;
                    hist[x1]++; // Увеличение количества пикселей с данным значением канала
                }
            }

            for (int i = 0; i < 256; i++) // Построение гистограммы
                chart1.Series[0].Points.AddXY(i, hist[i]);
            label_k.Text = Convert.ToString(max - min); // Контраст
            pic_res.Image = Bitmap_eq;
        }

        private void But_open_Click(object sender, EventArgs e) // Загрузка изображения
        {
            Bitmap image; //Bitmap для открываемого изображения
            OpenFileDialog open_dialog = new OpenFileDialog(); //создание диалогового окна для выбора файла
            open_dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*"; //формат файла
            if (open_dialog.ShowDialog() == DialogResult.OK) //если в окне была нажата кнопка "ОК"
            {
                try
                {
                    image = new Bitmap(open_dialog.FileName);
                    //this.pic.Size = image.Size;
                    pic.Image = image;
                    pic.Invalidate();
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            Bitmap Bitmap_gray = new Bitmap(pic.Image);
            int max = 0, min = 255; // для дальнейшего расчета 
            int x1; // значение каналов пикселя
            int height, width; // высота и ширина изображения

            height = Bitmap_gray.Height;
            width = Bitmap_gray.Width;

            int[] hist = new int[256]; // Массив для подсчета количества пикселей с равным значением
            chart1.Series[0].Points.Clear(); // Удаление точек на гистограмме

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_gray.GetPixel(i, j);
                    // Расчет значений канала пикселя
                    x1 = Convert.ToInt32(0.299 * pixel1.R + 0.587 * pixel1.G + 0.114 * pixel1.B);
                    Bitmap_gray.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                    if (x1 > max)
                        max = x1;
                    if (x1 < min)
                        min = x1;
                    hist[x1]++;
                }
            }
            pic.Image = Bitmap_gray;
            for(int i = 0; i < 256; i++) // Построение гистограммы
                chart1.Series[0].Points.AddXY(i, hist[i]);
            label_max.Text = Convert.ToString(max);
            label_min.Text = Convert.ToString(min);
            label_k.Text = Convert.ToString(max - min);
        }

        private void But_save_Click(object sender, EventArgs e) // Сохранение изображения
        {
            if (pic_res.Image != null) //если в pictureBox есть изображение
            {
                //создание диалогового окна "Сохранить как..", для сохранения изображения
                SaveFileDialog savedialog = new SaveFileDialog();
                savedialog.Title = "Сохранить картинку как...";
                //отображать ли предупреждение, если пользователь указывает имя уже существующего файла
                savedialog.OverwritePrompt = true;
                //отображать ли предупреждение, если пользователь указывает несуществующий путь
                savedialog.CheckPathExists = true;
                //список форматов файла, отображаемый в поле "Тип файла"
                savedialog.Filter = "Image Files(*.BMP)|*.BMP|Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";
                //отображается ли кнопка "Справка" в диалоговом окне
                savedialog.ShowHelp = true;
                if (savedialog.ShowDialog() == DialogResult.OK) //если в диалоговом окне нажата кнопка "ОК"
                {
                    try
                    {
                        pic_res.Image.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void But_save_gray_Click(object sender, EventArgs e) // Сохранение  черно-белого изображения
        {
            if (pic.Image != null) //если в pictureBox есть изображение
            {
                //создание диалогового окна "Сохранить как..", для сохранения изображения
                SaveFileDialog savedialog = new SaveFileDialog();
                savedialog.Title = "Сохранить картинку как...";
                //отображать ли предупреждение, если пользователь указывает имя уже существующего файла
                savedialog.OverwritePrompt = true;
                //отображать ли предупреждение, если пользователь указывает несуществующий путь
                savedialog.CheckPathExists = true;
                //список форматов файла, отображаемый в поле "Тип файла"
                savedialog.Filter = "Image Files(*.BMP)|*.BMP|Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";
                //отображается ли кнопка "Справка" в диалоговом окне
                savedialog.ShowHelp = true;
                if (savedialog.ShowDialog() == DialogResult.OK) //если в диалоговом окне нажата кнопка "ОК"
                {
                    try
                    {
                        pic.Image.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}
