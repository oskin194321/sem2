﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nn_2
{
    public class Neuron
    {
        Random rnd = new Random();

        int m = 784; // Количество пикселей 28х28=784
        int k = 10; // Количество нейронов = количество классов
        int h = 20; // Количество нейронов в скрытом слое
        int num_img = 100; // Количество изображений

        double[] enters; // Строка входов со значениями изображения
        double[,] W1, W2; // Веса 
        double[,] L_W1, L_W2; // Для применения алгоритма обратного распространения ошибки
        double[] Y_out_hide; // Вектор выходных значений нейронов скрытого слоя
        double[] Y_out_2; // Вектор выходных значений нейронов выходного слоя
        double[] δ_hide; // Вектор значений сигмоиды для параметров скрытого слоя
        double[] δ_2; // Вектор значений сигмоиды для параметров выходного слоя

        double speed; //коэффициент скорости обучения
        double[,] x; // Матрица X
        int[,] Y = new int[10, 10]; // Единичная матрица
        Form1 form;

        public Neuron(Form1 form, double tbox_speed)
        {
            this.form = form;
            speed = tbox_speed;

            enters = new double[m];
            Y_out_hide = new double[h];
            Y_out_2 = new double[k];
            δ_hide = new double[h];
            δ_2 = new double[k];

            x = new double[num_img, enters.Length + 1]; // количество изображений, число пикселей + маркер
            create_X_new(num_img, "train.csv");

            W1 = new double[m, h];
            W2 = new double[h, k];

            L_W1 = new double[m, h];
            L_W2 = new double[h, k];

            for (int i = 0; i < 10; i++) // Заполнение единичной матрицы
            {
                for (int j = 0; j < 10; j++)
                {
                    if (i == j)
                        Y[i, j] = 1;
                    else
                        Y[i, j] = 0;
                }
            }
        }

        public double[,] create_W1() // Инициализация матрицы весов W1
        {
            for (int i = 0; i < enters.Length; i++) // Заполнение матрицы весов W1
            {
                for (int j = 0; j < h; j++)
                    W1[i, j] = rnd.NextDouble() * 0.01 + 0.001;
            }
            return W1;
        }

        public double[,] create_W2() // Инициализация матрицы весов W2
        {
            for (int i = 0; i < h; i++) // Заполнение матрицы весов W2
            {
                for (int j = 0; j < k; j++)
                    W2[i, j] = rnd.NextDouble() * 0.01 + 0.001;
            }
            return W2;
        }

        public void create_X_new(int num, string FilePath) // Формирование матрицы "х"
        {
            string[] pixels = null; // Строка значений пикселей
            string[] train = File.ReadAllLines(FilePath); // Содержимое файла train.csv

            string[] label = null; // Строка значений пикселей
            string[] train_labels = File.ReadAllLines("train_labels.csv"); // Содержимое файла train.csv

            for (int i = 0; i < num; i++)
            {
                if (!String.IsNullOrEmpty(train[i]))
                {
                    pixels = train[i + 1].Split(','); // Разбиение строки на значения (id и пиксели)
                    label = train_labels[i + 1].Split(','); // Разбиение строки на значения (id и маркер)

                    for (int j = 0; j < m; j++)
                    {
                        x[i, j] = int.Parse(pixels[j + 1]);
                        if (x[i, j] <= 127)
                            x[i, j] = 0;
                        else
                            x[i, j] = 1;
                    }
                    x[i, m] = int.Parse(label[1]); // Сохранение маркера изображения
                }
            }
        }

        public void calc_out_hide_2() // Вычисление векторов выходных значений Y1 и Y2
        {
            for (int i = 0; i < h; i++) // Расчет выходных значений нейронов скрытого слоя
            {
                Y_out_hide[i] = 0;
                for (int j = 0; j < enters.Length; j++)
                    Y_out_hide[i] += enters[j] * W1[j, i];

                Y_out_hide[i] = 1.0 / (1.0 + Math.Pow(Math.E,-Y_out_hide[i]));
            }

            for (int i = 0; i < k; i++) // Расчет выходных значений нейронов выходного слоя
            {
                Y_out_2[i] = 0;
                for (int j = 0; j < Y_out_hide.Length; j++)
                    Y_out_2[i] += Y_out_hide[j] * W2[j, i];
            }
        }

        public void teach_W1W2() // Обучение
        {
            speed = Convert.ToDouble(form.tbox_speed.Text);
            form.datagrid_error.Rows.Clear(); // Удаление строк с отображением величины ошибки
            int num_error = 0;
            int num = 0; //кол-во итераций
            do
            {
                num_error = 0;
                for (int e = 0; e < x.GetLength(0) - 1; e++)
                {
                    for (int i = 0; i < enters.Length; i++)
                    {
                        enters[i] = x[e, i];
                    }
                    calc_out_hide_2();

                    double max = Y_out_2[0]; // Максимальное выходное значение
                    int number = 0;
                    for (int r = 1; r < 10; r++)
                    {                        
                        if (Y_out_2[r] > max)
                        {
                            max = Y_out_2[r];
                            number = r;
                        }
                    }
                    if (number != x[e, enters.Length]) // Сравнение цифры с маркером
                        num_error++;

                    for (int i = 0; i < k; i++)
                    {
                        Y_out_2[i] = 1.0 / (1.0 + Math.Pow(Math.E, -Y_out_2[i]));

                        δ_2[i] = (Y_out_2[i] - Convert.ToDouble(Y[(int)x[e, enters.Length], i])) * (Y_out_2[i] * (1 - Y_out_2[i])); // вычисление значений δ
                    }

                    for (int i = 0; i < k; i++) // Расчет L_W2
                    {
                        for (int j = 0; j < h; j++)
                        {
                            L_W2[j, i] = Y_out_hide[j] * δ_2[i];
                        }
                    }

                    double sum_δ2_w2;
                    for (int i = 0; i < h; i++) // Вычисление вектора значений δ для параметров скрытого слоя
                    {
                        sum_δ2_w2 = 0;
                        for (int j = 0; j < k; j++)
                            sum_δ2_w2 += δ_2[j] * W2[i, j];

                        δ_hide[i] = Y_out_hide[i] * (1 - Y_out_hide[i]) * sum_δ2_w2;
                    }

                    for (int i = 0; i < h; i++) // Расчет L_W1
                    {
                        for (int j = 0; j < m; j++)
                        {
                            L_W1[j, i] = enters[j] * δ_hide[i];
                        }
                    }
                    // Обновление весов
                    for (int i = 0; i < h; i++) // W1
                    {
                        for (int j = 0; j < m; j++)
                            W1[j, i] = W1[j, i] - speed * L_W1[j, i];
                    }

                    for (int i = 0; i < k; i++) // W2
                    {
                        for (int j = 0; j < h; j++)
                            W2[j, i] = W2[j, i] - speed * L_W2[j, i];
                    }
                }

                form.datagrid_error.Rows.Add();
                form.datagrid_error.Rows[num].HeaderCell.Value = Convert.ToString(num + 1); // Номер строки
                form.datagrid_error.Rows[num].Cells[0].Value = num_error;
                num += 1;
            } while (num_error != 0 && num < 150);
        }

        //public int recognize(string path)
        //{
        //    int number = 0; // Для результата распознавания
        //    Color[][] color;
        //    color = GetMatrixRGB(path);
        //    int count = 0; // Счетчик количества пикселей
        //    for (int i = 0; i < 28; i++)
        //    {
        //        for (int j = 0; j < 28; j++)
        //        {
        //            if (Convert.ToInt32(0.299 * color[i][j].R + 0.587 * color[i][j].G + 0.114 * color[i][j].B) > 127)
        //                enters[count] = 1;
        //            else
        //                enters[count] = 0;
        //            count++;
        //        }
        //    }
        //    calc_out_hide_2();
        //    double max = Y_out_2[0]; // Максимальное выходное значение
        //    int count_row = form.datagrid_out.Rows.Count - 1; // Номер строки для ввода
        //    form.datagrid_out.Rows.Add();
        //    form.datagrid_out.Rows[count_row].Cells[1].Value = Math.Round(max, 3); // Заполнение ячеек
        //    for (int i = 1; i < 10; i++)
        //    {
        //        form.datagrid_out.Rows[count_row].Cells[i + 1].Value = Math.Round(Y_out_2[i], 2); // Заполнение ячеек с выходными значениями
        //        if (Y_out_2[i] > max)
        //        {
        //            max = Y_out_2[i];
        //            number = i;
        //        }
        //    }
        //    form.datagrid_out.Rows[count_row].Cells[0].Value = number; // Ввод в ячейку распознанной цифры
        //    return number;
        //}

        public void test() // Режим распознавания
        {
            int number = 0; // Для результата распознавания

            string[] pixels = null; // Строка значений пикселей

            string[] label = null; // Строка значения маркера
            string[] train_labels = File.ReadAllLines("train_labels.csv");
            double accuracy = 0.0;

            string[] test = File.ReadAllLines("train.csv");

            for (int i = 0; i < 100; i++)
            {
                if (!String.IsNullOrEmpty(test[i]))
                {
                    pixels = test[i + 100].Split(','); // Разбиение строки на значения (id и пиксели)

                    label = train_labels[i + 100].Split(','); // Разбиение строки на значения (id и маркер)

                    for (int j = 0; j < m; j++)
                    {
                        enters[j] = int.Parse(pixels[j + 1]);
                        if (enters[j] <= 127)
                            enters[j] = 0;
                        else
                            enters[j] = 1;
                    }
                }
                calc_out_hide_2();

                //for(int i = 0; i < k; i++)
                //    Y_out_2[i] = 1.0 / (1.0 + Math.Pow(Math.E, -Y_out_2[i]));

                double max = Y_out_2[0]; // Максимальное выходное значение
                number = 0;
                int count_row = form.datagrid_out.Rows.Count - 1; // Номер строки для ввода
                for (int r = 1; r < 10; r++)
                {                   
                    if (Y_out_2[r] > max)
                    {
                        max = Y_out_2[r];
                        number = r;
                    }
                }
                if (number == int.Parse(label[1]))
                    accuracy++;
            }
            form.label_accur.Text = "Accuracy - " + accuracy / 100.0;
        }

        //private Color[][] GetMatrixRGB(string FilePathPic) // Формирование матрицы значений пикселей картинки
        //{
        //    Bitmap pic = new Bitmap(FilePathPic);

        //    int height = pic.Height;
        //    int width = pic.Width;

        //    Color[][] MatrixRGB = new Color[width][];
        //    for (int i = 0; i < width; i++)
        //    {
        //        MatrixRGB[i] = new Color[height];
        //        for (int j = 0; j < height; j++)
        //        {
        //            MatrixRGB[i][j] = pic.GetPixel(i, j);
        //        }
        //    }
        //    return MatrixRGB;
        //}

        public double[,] ret_W1()
        {
            return W1;
        }

        public double[,] ret_W2()
        {
            return W2;
        }

        public void write_test() // Формирование csv-файла для соревнования
        {
            int number = 0; // Для результата распознавания
            int id = 0;
            string[] pixels = null; // Строка значений пикселей
            string[] test = File.ReadAllLines("test.csv"); // Содержимое файла test.csv

            var csv = new StringBuilder();
            var first = "Id"; // id
            var second = "Label"; // Label
            var newLine = string.Format("{0},{1}", first, second);
            csv.AppendLine(newLine);
            for (int i = 1; i < test.Length; i++)
            {
                if (!String.IsNullOrEmpty(test[i]))
                {
                    pixels = test[i].Split(','); // Разбиение строки на значения (id и пиксели)

                    for (int j = 0; j < m; j++)
                    {
                        enters[j] = int.Parse(pixels[j + 1]);
                        if (enters[j] <= 127)
                            enters[j] = 0;
                        else
                            enters[j] = 1;
                    }
                }
                calc_out_hide_2();

                double max = Y_out_2[0]; // Максимальное выходное значение
                number = 0;
                for (int r = 1; r < 10; r++)
                {                    
                    if (Y_out_2[r] > max)
                    {
                        max = Y_out_2[r];
                        number = r;
                    }
                }

                id = int.Parse(pixels[0]); // id изображения

                first = Convert.ToString(id); // id
                second = Convert.ToString(number); // Label
                newLine = string.Format("{0},{1}", first, second);
                csv.AppendLine(newLine);
            }
            File.WriteAllText("result.csv", csv.ToString());
        }
    }
}
