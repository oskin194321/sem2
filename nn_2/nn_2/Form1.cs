﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nn_2
{
    public partial class Form1 : Form
    {
        Neuron network;
        string filePath; // Местонахождение изображения для проверки

        int m = 784; // Количество пикселей 28х28=784
        int k = 10; // Количество нейронов = количество классов
        int h = 20; // Количество нейронов в скрытом слое

        public Form1()
        {
            InitializeComponent();

            but_teach.Click += But_teach_Click;
            but_recognize.Click += But_recognize_Click;
            but_rec_train.Click += But_test_Click;
            but_test_csv.Click += But_test_csv_Click;
            but_choose_pic.Click += But_choose_pic_Click;
            but_w.Click += But_w_Click;
            // Создание экземпляра класса
            network = new Neuron(this, Convert.ToDouble(tbox_speed.Text));
        }

        private void But_test_csv_Click(object sender, EventArgs e)
        {
            network.write_test();
        }

        private void But_test_Click(object sender, EventArgs e)
        {
            network.test();
        }

        private void But_w_Click(object sender, EventArgs e)
        {
            double[,] W1 = new double[m, h]; // Матрица весов W1 для отображения на форме
            W1 = network.create_W1();
            double[,] W2 = new double[h, k]; // Матрица весов W2 для отображения на форме
            W2 = network.create_W2();

            show_w(W1, datagrid_W1);
            show_w(W2, datagrid_W2);
        }

        private void show_w(double[,] w, DataGridView dgv) // Отображение матрицы весов на форме
        {
            dgv.RowCount = w.GetLength(0); // указание числа строк
            dgv.ColumnCount = w.GetLength(1); // и столбцов для отображения на форме
            for (int i = 0; i < w.GetLength(0); i++)
            {
                dgv.Rows[i].HeaderCell.Value = Convert.ToString(i + 1); // Номер строки
                for (int j = 0; j < w.GetLength(1); j++)
                {
                    dgv.Rows[i].Cells[j].Value = Math.Round(w[i, j], 2); // Заполнение ячеек
                }
            }
        }

        private void But_teach_Click(object sender, EventArgs e) // Реализация режима обучения
        {
            Stopwatch timer = new Stopwatch(); // Отсчет времени
            timer.Start(); // Запуск отсчета времени
            network.teach_W1W2();
            timer.Stop(); // Остановка отсчета времени
            TimeSpan time = timer.Elapsed; // Преобразование полученного времени
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", time.Hours, time.Minutes, time.Seconds, time.Milliseconds);
            label_time.Text = "Время: " + elapsedTime; // Отображение времени на форме

            MessageBox.Show("Обучение завершено");

            double[,] W1 = new double[m, h]; // Матрица весов W1 для отображения на форме
            W1 = network.ret_W1();
            double[,] W2 = new double[h, k]; // Матрица весов W2 для отображения на форме
            W2 = network.ret_W2();

            show_w(W1, datagrid_W1);
            show_w(W2, datagrid_W2);
        }

        private void But_recognize_Click(object sender, EventArgs e) // Реализация распознавания
        {
            ///int number = network.recognize(filePath);
            //label_number.Text = "Число - " + number;
        }

        private void But_choose_pic_Click(object sender, EventArgs e) // Открытие картинки для распознавания
        {
            Bitmap image; //Bitmap для открываемого изображения
            OpenFileDialog open_dialog = new OpenFileDialog(); //создание диалогового окна для выбора файла
            open_dialog.Filter = "Image Files(*.BMP)|*.BMP|All files (*.*)|*.*"; //формат файла
            if (open_dialog.ShowDialog() == DialogResult.OK) //если в окне была нажата кнопка "ОК"
            {
                try
                {
                    image = new Bitmap(open_dialog.FileName);
                    pic.Image = image;
                    pic.Invalidate();
                    filePath = open_dialog.FileName;
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
