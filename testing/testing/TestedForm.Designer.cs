﻿namespace testing
{
    partial class TestedForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TestedForm));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.but_test4 = new System.Windows.Forms.Button();
            this.but_test3 = new System.Windows.Forms.Button();
            this.but_test2 = new System.Windows.Forms.Button();
            this.but_test1 = new System.Windows.Forms.Button();
            this.tabTest1 = new System.Windows.Forms.TabPage();
            this.label_test1 = new System.Windows.Forms.Label();
            this.but_res_test1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.but_answer1 = new System.Windows.Forms.Button();
            this.cb_test1_option = new System.Windows.Forms.ComboBox();
            this.cb_test1_quest = new System.Windows.Forms.ComboBox();
            this.but_to_menu = new System.Windows.Forms.Button();
            this.tabTest2 = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.but_answer2 = new System.Windows.Forms.Button();
            this.cb_test2_option = new System.Windows.Forms.ComboBox();
            this.cb_test2_quest = new System.Windows.Forms.ComboBox();
            this.but_to_menu2 = new System.Windows.Forms.Button();
            this.tabTest3 = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.but_answer3 = new System.Windows.Forms.Button();
            this.cb_test3_option = new System.Windows.Forms.ComboBox();
            this.cb_test3_quest = new System.Windows.Forms.ComboBox();
            this.but_to_menu3 = new System.Windows.Forms.Button();
            this.tabTest4 = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.but_answer4 = new System.Windows.Forms.Button();
            this.cb_test4_option = new System.Windows.Forms.ComboBox();
            this.cb_test4_quest = new System.Windows.Forms.ComboBox();
            this.but_to_menu4 = new System.Windows.Forms.Button();
            this.label_test2 = new System.Windows.Forms.Label();
            this.but_res_test2 = new System.Windows.Forms.Button();
            this.label_test3 = new System.Windows.Forms.Label();
            this.but_res_test3 = new System.Windows.Forms.Button();
            this.label_test4 = new System.Windows.Forms.Label();
            this.but_res_test4 = new System.Windows.Forms.Button();
            this.but_exit = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabTest1.SuspendLayout();
            this.tabTest2.SuspendLayout();
            this.tabTest3.SuspendLayout();
            this.tabTest4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabTest1);
            this.tabControl1.Controls.Add(this.tabTest2);
            this.tabControl1.Controls.Add(this.tabTest3);
            this.tabControl1.Controls.Add(this.tabTest4);
            this.tabControl1.Location = new System.Drawing.Point(12, 15);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1021, 507);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.but_exit);
            this.tabPage1.Controls.Add(this.but_test4);
            this.tabPage1.Controls.Add(this.but_test3);
            this.tabPage1.Controls.Add(this.but_test2);
            this.tabPage1.Controls.Add(this.but_test1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1013, 478);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Главное меню";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // but_test4
            // 
            this.but_test4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_test4.Location = new System.Drawing.Point(83, 317);
            this.but_test4.Name = "but_test4";
            this.but_test4.Size = new System.Drawing.Size(850, 80);
            this.but_test4.TabIndex = 3;
            this.but_test4.Text = "Тест 4 - \"Диагностика комплекса угрожаемого авторитета (КУА) у руководителей\"";
            this.but_test4.UseVisualStyleBackColor = true;
            // 
            // but_test3
            // 
            this.but_test3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_test3.Location = new System.Drawing.Point(83, 231);
            this.but_test3.Name = "but_test3";
            this.but_test3.Size = new System.Drawing.Size(850, 80);
            this.but_test3.TabIndex = 2;
            this.but_test3.Text = "Тест 3 - \"Диагностика уровня консервативности стиля организационного управления\"";
            this.but_test3.UseVisualStyleBackColor = true;
            // 
            // but_test2
            // 
            this.but_test2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_test2.Location = new System.Drawing.Point(83, 145);
            this.but_test2.Name = "but_test2";
            this.but_test2.Size = new System.Drawing.Size(850, 80);
            this.but_test2.TabIndex = 1;
            this.but_test2.Text = "Тест 2 - \"Экспресс-диагностика профессионально-деятельностных ограничений руковод" +
    "ителя\"";
            this.but_test2.UseVisualStyleBackColor = true;
            // 
            // but_test1
            // 
            this.but_test1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_test1.Location = new System.Drawing.Point(83, 59);
            this.but_test1.Name = "but_test1";
            this.but_test1.Size = new System.Drawing.Size(850, 80);
            this.but_test1.TabIndex = 0;
            this.but_test1.Text = "Тест 1 - \"Экспресс-диагностика барьеров в управленческой деятельности\"";
            this.but_test1.UseVisualStyleBackColor = true;
            // 
            // tabTest1
            // 
            this.tabTest1.Controls.Add(this.label_test1);
            this.tabTest1.Controls.Add(this.but_res_test1);
            this.tabTest1.Controls.Add(this.label2);
            this.tabTest1.Controls.Add(this.label1);
            this.tabTest1.Controls.Add(this.but_answer1);
            this.tabTest1.Controls.Add(this.cb_test1_option);
            this.tabTest1.Controls.Add(this.cb_test1_quest);
            this.tabTest1.Controls.Add(this.but_to_menu);
            this.tabTest1.Location = new System.Drawing.Point(4, 25);
            this.tabTest1.Name = "tabTest1";
            this.tabTest1.Padding = new System.Windows.Forms.Padding(3);
            this.tabTest1.Size = new System.Drawing.Size(1013, 478);
            this.tabTest1.TabIndex = 1;
            this.tabTest1.Text = "Тест 1";
            this.tabTest1.UseVisualStyleBackColor = true;
            // 
            // label_test1
            // 
            this.label_test1.AutoSize = true;
            this.label_test1.Location = new System.Drawing.Point(10, 228);
            this.label_test1.Name = "label_test1";
            this.label_test1.Size = new System.Drawing.Size(186, 17);
            this.label_test1.TabIndex = 7;
            this.label_test1.Text = "Результаты тестирования:";
            // 
            // but_res_test1
            // 
            this.but_res_test1.Location = new System.Drawing.Point(6, 163);
            this.but_res_test1.Name = "but_res_test1";
            this.but_res_test1.Size = new System.Drawing.Size(240, 30);
            this.but_res_test1.TabIndex = 6;
            this.but_res_test1.Text = "Узнать результаты теста";
            this.but_res_test1.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(711, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Ответ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Вопрос";
            // 
            // but_answer1
            // 
            this.but_answer1.Location = new System.Drawing.Point(871, 55);
            this.but_answer1.Name = "but_answer1";
            this.but_answer1.Size = new System.Drawing.Size(136, 30);
            this.but_answer1.TabIndex = 3;
            this.but_answer1.Text = "Ответить";
            this.but_answer1.UseVisualStyleBackColor = true;
            // 
            // cb_test1_option
            // 
            this.cb_test1_option.FormattingEnabled = true;
            this.cb_test1_option.Location = new System.Drawing.Point(714, 59);
            this.cb_test1_option.Name = "cb_test1_option";
            this.cb_test1_option.Size = new System.Drawing.Size(151, 24);
            this.cb_test1_option.TabIndex = 2;
            // 
            // cb_test1_quest
            // 
            this.cb_test1_quest.FormattingEnabled = true;
            this.cb_test1_quest.Location = new System.Drawing.Point(7, 59);
            this.cb_test1_quest.Name = "cb_test1_quest";
            this.cb_test1_quest.Size = new System.Drawing.Size(701, 24);
            this.cb_test1_quest.TabIndex = 1;
            // 
            // but_to_menu
            // 
            this.but_to_menu.Location = new System.Drawing.Point(871, 442);
            this.but_to_menu.Name = "but_to_menu";
            this.but_to_menu.Size = new System.Drawing.Size(136, 30);
            this.but_to_menu.TabIndex = 0;
            this.but_to_menu.Text = "В главное меню";
            this.but_to_menu.UseVisualStyleBackColor = true;
            // 
            // tabTest2
            // 
            this.tabTest2.Controls.Add(this.label_test2);
            this.tabTest2.Controls.Add(this.but_res_test2);
            this.tabTest2.Controls.Add(this.label3);
            this.tabTest2.Controls.Add(this.label4);
            this.tabTest2.Controls.Add(this.but_answer2);
            this.tabTest2.Controls.Add(this.cb_test2_option);
            this.tabTest2.Controls.Add(this.cb_test2_quest);
            this.tabTest2.Controls.Add(this.but_to_menu2);
            this.tabTest2.Location = new System.Drawing.Point(4, 25);
            this.tabTest2.Name = "tabTest2";
            this.tabTest2.Padding = new System.Windows.Forms.Padding(3);
            this.tabTest2.Size = new System.Drawing.Size(1013, 478);
            this.tabTest2.TabIndex = 2;
            this.tabTest2.Text = "Тест 2";
            this.tabTest2.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(710, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "Ответ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Вопрос";
            // 
            // but_answer2
            // 
            this.but_answer2.Location = new System.Drawing.Point(870, 55);
            this.but_answer2.Name = "but_answer2";
            this.but_answer2.Size = new System.Drawing.Size(136, 30);
            this.but_answer2.TabIndex = 8;
            this.but_answer2.Text = "Ответить";
            this.but_answer2.UseVisualStyleBackColor = true;
            // 
            // cb_test2_option
            // 
            this.cb_test2_option.FormattingEnabled = true;
            this.cb_test2_option.Location = new System.Drawing.Point(713, 59);
            this.cb_test2_option.Name = "cb_test2_option";
            this.cb_test2_option.Size = new System.Drawing.Size(151, 24);
            this.cb_test2_option.TabIndex = 7;
            // 
            // cb_test2_quest
            // 
            this.cb_test2_quest.FormattingEnabled = true;
            this.cb_test2_quest.Location = new System.Drawing.Point(6, 59);
            this.cb_test2_quest.Name = "cb_test2_quest";
            this.cb_test2_quest.Size = new System.Drawing.Size(701, 24);
            this.cb_test2_quest.TabIndex = 6;
            // 
            // but_to_menu2
            // 
            this.but_to_menu2.Location = new System.Drawing.Point(871, 442);
            this.but_to_menu2.Name = "but_to_menu2";
            this.but_to_menu2.Size = new System.Drawing.Size(136, 30);
            this.but_to_menu2.TabIndex = 1;
            this.but_to_menu2.Text = "В главное меню";
            this.but_to_menu2.UseVisualStyleBackColor = true;
            // 
            // tabTest3
            // 
            this.tabTest3.Controls.Add(this.label_test3);
            this.tabTest3.Controls.Add(this.but_res_test3);
            this.tabTest3.Controls.Add(this.label5);
            this.tabTest3.Controls.Add(this.label6);
            this.tabTest3.Controls.Add(this.but_answer3);
            this.tabTest3.Controls.Add(this.cb_test3_option);
            this.tabTest3.Controls.Add(this.cb_test3_quest);
            this.tabTest3.Controls.Add(this.but_to_menu3);
            this.tabTest3.Location = new System.Drawing.Point(4, 25);
            this.tabTest3.Name = "tabTest3";
            this.tabTest3.Padding = new System.Windows.Forms.Padding(3);
            this.tabTest3.Size = new System.Drawing.Size(1013, 478);
            this.tabTest3.TabIndex = 3;
            this.tabTest3.Text = "Тест 3";
            this.tabTest3.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(682, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 17);
            this.label5.TabIndex = 15;
            this.label5.Text = "Ответ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "Вопрос";
            // 
            // but_answer3
            // 
            this.but_answer3.Location = new System.Drawing.Point(869, 103);
            this.but_answer3.Name = "but_answer3";
            this.but_answer3.Size = new System.Drawing.Size(136, 30);
            this.but_answer3.TabIndex = 13;
            this.but_answer3.Text = "Ответить";
            this.but_answer3.UseVisualStyleBackColor = true;
            // 
            // cb_test3_option
            // 
            this.cb_test3_option.FormattingEnabled = true;
            this.cb_test3_option.Location = new System.Drawing.Point(736, 107);
            this.cb_test3_option.Name = "cb_test3_option";
            this.cb_test3_option.Size = new System.Drawing.Size(127, 24);
            this.cb_test3_option.TabIndex = 12;
            // 
            // cb_test3_quest
            // 
            this.cb_test3_quest.FormattingEnabled = true;
            this.cb_test3_quest.Location = new System.Drawing.Point(6, 58);
            this.cb_test3_quest.Name = "cb_test3_quest";
            this.cb_test3_quest.Size = new System.Drawing.Size(999, 24);
            this.cb_test3_quest.TabIndex = 11;
            // 
            // but_to_menu3
            // 
            this.but_to_menu3.Location = new System.Drawing.Point(871, 442);
            this.but_to_menu3.Name = "but_to_menu3";
            this.but_to_menu3.Size = new System.Drawing.Size(136, 30);
            this.but_to_menu3.TabIndex = 1;
            this.but_to_menu3.Text = "В главное меню";
            this.but_to_menu3.UseVisualStyleBackColor = true;
            // 
            // tabTest4
            // 
            this.tabTest4.Controls.Add(this.label_test4);
            this.tabTest4.Controls.Add(this.but_res_test4);
            this.tabTest4.Controls.Add(this.label7);
            this.tabTest4.Controls.Add(this.label8);
            this.tabTest4.Controls.Add(this.but_answer4);
            this.tabTest4.Controls.Add(this.cb_test4_option);
            this.tabTest4.Controls.Add(this.cb_test4_quest);
            this.tabTest4.Controls.Add(this.but_to_menu4);
            this.tabTest4.Location = new System.Drawing.Point(4, 25);
            this.tabTest4.Name = "tabTest4";
            this.tabTest4.Padding = new System.Windows.Forms.Padding(3);
            this.tabTest4.Size = new System.Drawing.Size(1013, 478);
            this.tabTest4.TabIndex = 4;
            this.tabTest4.Text = "Тест 4";
            this.tabTest4.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(641, 109);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 17);
            this.label7.TabIndex = 20;
            this.label7.Text = "Ответ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 17);
            this.label8.TabIndex = 19;
            this.label8.Text = "Вопрос";
            // 
            // but_answer4
            // 
            this.but_answer4.Location = new System.Drawing.Point(869, 102);
            this.but_answer4.Name = "but_answer4";
            this.but_answer4.Size = new System.Drawing.Size(136, 30);
            this.but_answer4.TabIndex = 18;
            this.but_answer4.Text = "Ответить";
            this.but_answer4.UseVisualStyleBackColor = true;
            // 
            // cb_test4_option
            // 
            this.cb_test4_option.FormattingEnabled = true;
            this.cb_test4_option.Location = new System.Drawing.Point(695, 106);
            this.cb_test4_option.Name = "cb_test4_option";
            this.cb_test4_option.Size = new System.Drawing.Size(168, 24);
            this.cb_test4_option.TabIndex = 17;
            // 
            // cb_test4_quest
            // 
            this.cb_test4_quest.FormattingEnabled = true;
            this.cb_test4_quest.Location = new System.Drawing.Point(6, 53);
            this.cb_test4_quest.Name = "cb_test4_quest";
            this.cb_test4_quest.Size = new System.Drawing.Size(999, 24);
            this.cb_test4_quest.TabIndex = 16;
            // 
            // but_to_menu4
            // 
            this.but_to_menu4.Location = new System.Drawing.Point(871, 442);
            this.but_to_menu4.Name = "but_to_menu4";
            this.but_to_menu4.Size = new System.Drawing.Size(136, 30);
            this.but_to_menu4.TabIndex = 1;
            this.but_to_menu4.Text = "В главное меню";
            this.but_to_menu4.UseVisualStyleBackColor = true;
            // 
            // label_test2
            // 
            this.label_test2.AutoSize = true;
            this.label_test2.Location = new System.Drawing.Point(10, 226);
            this.label_test2.Name = "label_test2";
            this.label_test2.Size = new System.Drawing.Size(289, 136);
            this.label_test2.TabIndex = 12;
            this.label_test2.Text = resources.GetString("label_test2.Text");
            // 
            // but_res_test2
            // 
            this.but_res_test2.Location = new System.Drawing.Point(6, 161);
            this.but_res_test2.Name = "but_res_test2";
            this.but_res_test2.Size = new System.Drawing.Size(240, 30);
            this.but_res_test2.TabIndex = 11;
            this.but_res_test2.Text = "Узнать результаты теста";
            this.but_res_test2.UseVisualStyleBackColor = true;
            // 
            // label_test3
            // 
            this.label_test3.AutoSize = true;
            this.label_test3.Location = new System.Drawing.Point(10, 259);
            this.label_test3.Name = "label_test3";
            this.label_test3.Size = new System.Drawing.Size(437, 68);
            this.label_test3.TabIndex = 17;
            this.label_test3.Text = "Результаты тестирования:\r\n\r\nУровень консервативности организационного стиля управ" +
    "ления\r\n\r\n";
            // 
            // but_res_test3
            // 
            this.but_res_test3.Location = new System.Drawing.Point(6, 194);
            this.but_res_test3.Name = "but_res_test3";
            this.but_res_test3.Size = new System.Drawing.Size(240, 30);
            this.but_res_test3.TabIndex = 16;
            this.but_res_test3.Text = "Узнать результаты теста";
            this.but_res_test3.UseVisualStyleBackColor = true;
            // 
            // label_test4
            // 
            this.label_test4.AutoSize = true;
            this.label_test4.Location = new System.Drawing.Point(10, 258);
            this.label_test4.Name = "label_test4";
            this.label_test4.Size = new System.Drawing.Size(437, 68);
            this.label_test4.TabIndex = 22;
            this.label_test4.Text = "Результаты тестирования:\r\n\r\nУровень консервативности организационного стиля управ" +
    "ления\r\n\r\n";
            // 
            // but_res_test4
            // 
            this.but_res_test4.Location = new System.Drawing.Point(6, 193);
            this.but_res_test4.Name = "but_res_test4";
            this.but_res_test4.Size = new System.Drawing.Size(240, 30);
            this.but_res_test4.TabIndex = 21;
            this.but_res_test4.Text = "Узнать результаты теста";
            this.but_res_test4.UseVisualStyleBackColor = true;
            // 
            // but_exit
            // 
            this.but_exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_exit.Location = new System.Drawing.Point(689, 419);
            this.but_exit.Name = "but_exit";
            this.but_exit.Size = new System.Drawing.Size(244, 40);
            this.but_exit.TabIndex = 4;
            this.but_exit.Text = "Выход";
            this.but_exit.UseVisualStyleBackColor = true;
            // 
            // TestedForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1045, 537);
            this.Controls.Add(this.tabControl1);
            this.Name = "TestedForm";
            this.Text = "Тестируемый";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabTest1.ResumeLayout(false);
            this.tabTest1.PerformLayout();
            this.tabTest2.ResumeLayout(false);
            this.tabTest2.PerformLayout();
            this.tabTest3.ResumeLayout(false);
            this.tabTest3.PerformLayout();
            this.tabTest4.ResumeLayout(false);
            this.tabTest4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button but_test4;
        private System.Windows.Forms.Button but_test3;
        private System.Windows.Forms.Button but_test2;
        private System.Windows.Forms.Button but_test1;
        private System.Windows.Forms.TabPage tabTest1;
        private System.Windows.Forms.Button but_to_menu;
        private System.Windows.Forms.TabPage tabTest2;
        private System.Windows.Forms.Button but_to_menu2;
        private System.Windows.Forms.TabPage tabTest3;
        private System.Windows.Forms.Button but_to_menu3;
        private System.Windows.Forms.TabPage tabTest4;
        private System.Windows.Forms.Button but_to_menu4;
        private System.Windows.Forms.Button but_answer1;
        private System.Windows.Forms.ComboBox cb_test1_option;
        private System.Windows.Forms.ComboBox cb_test1_quest;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button but_answer2;
        private System.Windows.Forms.ComboBox cb_test2_option;
        private System.Windows.Forms.ComboBox cb_test2_quest;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button but_answer3;
        private System.Windows.Forms.ComboBox cb_test3_option;
        private System.Windows.Forms.ComboBox cb_test3_quest;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button but_answer4;
        private System.Windows.Forms.ComboBox cb_test4_option;
        private System.Windows.Forms.ComboBox cb_test4_quest;
        private System.Windows.Forms.Button but_res_test1;
        private System.Windows.Forms.Label label_test1;
        private System.Windows.Forms.Label label_test2;
        private System.Windows.Forms.Button but_res_test2;
        private System.Windows.Forms.Label label_test3;
        private System.Windows.Forms.Button but_res_test3;
        private System.Windows.Forms.Label label_test4;
        private System.Windows.Forms.Button but_res_test4;
        private System.Windows.Forms.Button but_exit;
    }
}