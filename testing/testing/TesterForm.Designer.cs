﻿namespace testing
{
    partial class TesterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea19 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series46 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series47 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TesterForm));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea20 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series48 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series49 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series50 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.but_exit = new System.Windows.Forms.Button();
            this.but_click_res = new System.Windows.Forms.Button();
            this.but_users = new System.Windows.Forms.Button();
            this.but_test4 = new System.Windows.Forms.Button();
            this.but_test3 = new System.Windows.Forms.Button();
            this.but_test2 = new System.Windows.Forms.Button();
            this.but_test1 = new System.Windows.Forms.Button();
            this.tabTest1 = new System.Windows.Forms.TabPage();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dgv_test1 = new System.Windows.Forms.DataGridView();
            this.but_to_menu = new System.Windows.Forms.Button();
            this.tabTest2 = new System.Windows.Forms.TabPage();
            this.label12 = new System.Windows.Forms.Label();
            this.dgv_test2 = new System.Windows.Forms.DataGridView();
            this.label8 = new System.Windows.Forms.Label();
            this.but_to_menu2 = new System.Windows.Forms.Button();
            this.tabTest3 = new System.Windows.Forms.TabPage();
            this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label13 = new System.Windows.Forms.Label();
            this.dgv_test3 = new System.Windows.Forms.DataGridView();
            this.label9 = new System.Windows.Forms.Label();
            this.but_to_menu3 = new System.Windows.Forms.Button();
            this.tabTest4 = new System.Windows.Forms.TabPage();
            this.label14 = new System.Windows.Forms.Label();
            this.dgv_test4 = new System.Windows.Forms.DataGridView();
            this.label10 = new System.Windows.Forms.Label();
            this.but_to_menu4 = new System.Windows.Forms.Button();
            this.tabUsers = new System.Windows.Forms.TabPage();
            this.dgv_Users = new System.Windows.Forms.DataGridView();
            this.but_to_menu5 = new System.Windows.Forms.Button();
            this.tabResult = new System.Windows.Forms.TabPage();
            this.label_res = new System.Windows.Forms.Label();
            this.but_get_res = new System.Windows.Forms.Button();
            this.cb_tested = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.but_to_menu6 = new System.Windows.Forms.Button();
            this.testingDataSetRes1 = new testing.testingDataSetRes1();
            this.testingDataSetRes1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.testingDataSet1 = new testing.testingDataSet1();
            this.testingDataSet1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.testingDataSet3 = new testing.testingDataSet3();
            this.resultstest1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.results_test_1TableAdapter = new testing.testingDataSet3TableAdapters.Results_test_1TableAdapter();
            this.idtestedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res3DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res4DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.testingDataSet4 = new testing.testingDataSet4();
            this.usersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.usersTableAdapter = new testing.testingDataSet4TableAdapters.UsersTableAdapter();
            this.iduserDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.testingDataSet5 = new testing.testingDataSet5();
            this.resultstest2BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.results_test_2TableAdapter = new testing.testingDataSet5TableAdapters.Results_test_2TableAdapter();
            this.idtestedDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res1DataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res2DataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res3DataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res4DataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res5DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res6DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.testingDataSet6 = new testing.testingDataSet6();
            this.resultstest3BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.results_test_3TableAdapter = new testing.testingDataSet6TableAdapters.Results_test_3TableAdapter();
            this.idtestedDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resultDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.testingDataSet7 = new testing.testingDataSet7();
            this.resultstest4BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.results_test_4TableAdapter = new testing.testingDataSet7TableAdapters.Results_test_4TableAdapter();
            this.idtestedDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res1DataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res2DataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res3DataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res4DataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabTest1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_test1)).BeginInit();
            this.tabTest2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_test2)).BeginInit();
            this.tabTest3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_test3)).BeginInit();
            this.tabTest4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_test4)).BeginInit();
            this.tabUsers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Users)).BeginInit();
            this.tabResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSetRes1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSetRes1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultstest1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultstest2BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultstest3BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultstest4BindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabTest1);
            this.tabControl1.Controls.Add(this.tabTest2);
            this.tabControl1.Controls.Add(this.tabTest3);
            this.tabControl1.Controls.Add(this.tabTest4);
            this.tabControl1.Controls.Add(this.tabUsers);
            this.tabControl1.Controls.Add(this.tabResult);
            this.tabControl1.Location = new System.Drawing.Point(12, 15);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1021, 507);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.but_exit);
            this.tabPage1.Controls.Add(this.but_click_res);
            this.tabPage1.Controls.Add(this.but_users);
            this.tabPage1.Controls.Add(this.but_test4);
            this.tabPage1.Controls.Add(this.but_test3);
            this.tabPage1.Controls.Add(this.but_test2);
            this.tabPage1.Controls.Add(this.but_test1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1013, 478);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Главное меню";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // but_exit
            // 
            this.but_exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_exit.Location = new System.Drawing.Point(183, 427);
            this.but_exit.Name = "but_exit";
            this.but_exit.Size = new System.Drawing.Size(570, 45);
            this.but_exit.TabIndex = 6;
            this.but_exit.Text = "Выход";
            this.but_exit.UseVisualStyleBackColor = true;
            // 
            // but_click_res
            // 
            this.but_click_res.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_click_res.Location = new System.Drawing.Point(183, 337);
            this.but_click_res.Name = "but_click_res";
            this.but_click_res.Size = new System.Drawing.Size(570, 45);
            this.but_click_res.TabIndex = 5;
            this.but_click_res.Text = "Результаты тестируемого";
            this.but_click_res.UseVisualStyleBackColor = true;
            // 
            // but_users
            // 
            this.but_users.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_users.Location = new System.Drawing.Point(183, 286);
            this.but_users.Name = "but_users";
            this.but_users.Size = new System.Drawing.Size(570, 45);
            this.but_users.TabIndex = 4;
            this.but_users.Text = "Список пользователей";
            this.but_users.UseVisualStyleBackColor = true;
            // 
            // but_test4
            // 
            this.but_test4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_test4.Location = new System.Drawing.Point(183, 197);
            this.but_test4.Name = "but_test4";
            this.but_test4.Size = new System.Drawing.Size(570, 45);
            this.but_test4.TabIndex = 3;
            this.but_test4.Text = "Тест 4";
            this.but_test4.UseVisualStyleBackColor = true;
            // 
            // but_test3
            // 
            this.but_test3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_test3.Location = new System.Drawing.Point(183, 146);
            this.but_test3.Name = "but_test3";
            this.but_test3.Size = new System.Drawing.Size(570, 45);
            this.but_test3.TabIndex = 2;
            this.but_test3.Text = "Тест 3";
            this.but_test3.UseVisualStyleBackColor = true;
            // 
            // but_test2
            // 
            this.but_test2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_test2.Location = new System.Drawing.Point(183, 95);
            this.but_test2.Name = "but_test2";
            this.but_test2.Size = new System.Drawing.Size(570, 45);
            this.but_test2.TabIndex = 1;
            this.but_test2.Text = "Тест 2";
            this.but_test2.UseVisualStyleBackColor = true;
            // 
            // but_test1
            // 
            this.but_test1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_test1.Location = new System.Drawing.Point(183, 44);
            this.but_test1.Name = "but_test1";
            this.but_test1.Size = new System.Drawing.Size(570, 45);
            this.but_test1.TabIndex = 0;
            this.but_test1.Text = "Тест 1";
            this.but_test1.UseVisualStyleBackColor = true;
            // 
            // tabTest1
            // 
            this.tabTest1.Controls.Add(this.chart1);
            this.tabTest1.Controls.Add(this.label11);
            this.tabTest1.Controls.Add(this.label7);
            this.tabTest1.Controls.Add(this.dgv_test1);
            this.tabTest1.Controls.Add(this.but_to_menu);
            this.tabTest1.Location = new System.Drawing.Point(4, 25);
            this.tabTest1.Name = "tabTest1";
            this.tabTest1.Padding = new System.Windows.Forms.Padding(3);
            this.tabTest1.Size = new System.Drawing.Size(1013, 478);
            this.tabTest1.TabIndex = 1;
            this.tabTest1.Text = "Тест 1";
            this.tabTest1.UseVisualStyleBackColor = true;
            // 
            // chart1
            // 
            chartArea19.AxisX.Minimum = 0D;
            chartArea19.AxisX.Title = "Тестируемые";
            chartArea19.AxisY.Maximum = 60D;
            chartArea19.AxisY.Title = "Сумма баллов";
            chartArea19.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea19);
            this.chart1.Location = new System.Drawing.Point(629, 41);
            this.chart1.Name = "chart1";
            series46.ChartArea = "ChartArea1";
            series46.Name = "Series1";
            series47.BorderWidth = 2;
            series47.ChartArea = "ChartArea1";
            series47.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series47.Color = System.Drawing.Color.Red;
            series47.Name = "Series2";
            this.chart1.Series.Add(series46);
            this.chart1.Series.Add(series47);
            this.chart1.Size = new System.Drawing.Size(378, 300);
            this.chart1.TabIndex = 4;
            this.chart1.Text = "chart1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 336);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(580, 136);
            this.label11.TabIndex = 3;
            this.label11.Text = resources.GetString("label11.Text");
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(441, 17);
            this.label7.TabIndex = 2;
            this.label7.Text = "Экспресс-диагностика барьеров в управленческой деятельности";
            // 
            // dgv_test1
            // 
            this.dgv_test1.AutoGenerateColumns = false;
            this.dgv_test1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_test1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_test1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idtestedDataGridViewTextBoxColumn,
            this.res1DataGridViewTextBoxColumn,
            this.res2DataGridViewTextBoxColumn,
            this.res3DataGridViewTextBoxColumn,
            this.res4DataGridViewTextBoxColumn});
            this.dgv_test1.DataSource = this.resultstest1BindingSource;
            this.dgv_test1.Location = new System.Drawing.Point(6, 41);
            this.dgv_test1.Name = "dgv_test1";
            this.dgv_test1.RowHeadersWidth = 51;
            this.dgv_test1.RowTemplate.Height = 24;
            this.dgv_test1.Size = new System.Drawing.Size(597, 277);
            this.dgv_test1.TabIndex = 1;
            // 
            // but_to_menu
            // 
            this.but_to_menu.Location = new System.Drawing.Point(871, 442);
            this.but_to_menu.Name = "but_to_menu";
            this.but_to_menu.Size = new System.Drawing.Size(136, 30);
            this.but_to_menu.TabIndex = 0;
            this.but_to_menu.Text = "В главное меню";
            this.but_to_menu.UseVisualStyleBackColor = true;
            // 
            // tabTest2
            // 
            this.tabTest2.Controls.Add(this.label12);
            this.tabTest2.Controls.Add(this.dgv_test2);
            this.tabTest2.Controls.Add(this.label8);
            this.tabTest2.Controls.Add(this.but_to_menu2);
            this.tabTest2.Location = new System.Drawing.Point(4, 25);
            this.tabTest2.Name = "tabTest2";
            this.tabTest2.Padding = new System.Windows.Forms.Padding(3);
            this.tabTest2.Size = new System.Drawing.Size(1013, 478);
            this.tabTest2.TabIndex = 2;
            this.tabTest2.Text = "Тест 2";
            this.tabTest2.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 327);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(561, 136);
            this.label12.TabIndex = 5;
            this.label12.Text = resources.GetString("label12.Text");
            // 
            // dgv_test2
            // 
            this.dgv_test2.AutoGenerateColumns = false;
            this.dgv_test2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_test2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_test2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idtestedDataGridViewTextBoxColumn1,
            this.res1DataGridViewTextBoxColumn1,
            this.res2DataGridViewTextBoxColumn1,
            this.res3DataGridViewTextBoxColumn1,
            this.res4DataGridViewTextBoxColumn1,
            this.res5DataGridViewTextBoxColumn,
            this.res6DataGridViewTextBoxColumn});
            this.dgv_test2.DataSource = this.resultstest2BindingSource;
            this.dgv_test2.Location = new System.Drawing.Point(9, 37);
            this.dgv_test2.Name = "dgv_test2";
            this.dgv_test2.RowHeadersWidth = 51;
            this.dgv_test2.RowTemplate.Height = 24;
            this.dgv_test2.Size = new System.Drawing.Size(822, 275);
            this.dgv_test2.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(578, 17);
            this.label8.TabIndex = 3;
            this.label8.Text = "Экспресс-диагностика профессионально-деятельностных ограничений руководителя";
            // 
            // but_to_menu2
            // 
            this.but_to_menu2.Location = new System.Drawing.Point(871, 442);
            this.but_to_menu2.Name = "but_to_menu2";
            this.but_to_menu2.Size = new System.Drawing.Size(136, 30);
            this.but_to_menu2.TabIndex = 1;
            this.but_to_menu2.Text = "В главное меню";
            this.but_to_menu2.UseVisualStyleBackColor = true;
            // 
            // tabTest3
            // 
            this.tabTest3.Controls.Add(this.chart3);
            this.tabTest3.Controls.Add(this.label13);
            this.tabTest3.Controls.Add(this.dgv_test3);
            this.tabTest3.Controls.Add(this.label9);
            this.tabTest3.Controls.Add(this.but_to_menu3);
            this.tabTest3.Location = new System.Drawing.Point(4, 25);
            this.tabTest3.Name = "tabTest3";
            this.tabTest3.Padding = new System.Windows.Forms.Padding(3);
            this.tabTest3.Size = new System.Drawing.Size(1013, 478);
            this.tabTest3.TabIndex = 3;
            this.tabTest3.Text = "Тест 3";
            this.tabTest3.UseVisualStyleBackColor = true;
            // 
            // chart3
            // 
            chartArea20.AxisX.Minimum = 0D;
            chartArea20.AxisX.Title = "Тестируемые";
            chartArea20.AxisY.Maximum = 60D;
            chartArea20.AxisY.Title = "Сумма баллов";
            chartArea20.Name = "ChartArea1";
            this.chart3.ChartAreas.Add(chartArea20);
            this.chart3.Location = new System.Drawing.Point(467, 34);
            this.chart3.Name = "chart3";
            series48.ChartArea = "ChartArea1";
            series48.Name = "Series1";
            series49.BorderWidth = 2;
            series49.ChartArea = "ChartArea1";
            series49.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series49.Color = System.Drawing.Color.Orange;
            series49.Name = "Series2";
            series50.BorderWidth = 2;
            series50.ChartArea = "ChartArea1";
            series50.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series50.Color = System.Drawing.Color.Red;
            series50.Name = "Series3";
            this.chart3.Series.Add(series48);
            this.chart3.Series.Add(series49);
            this.chart3.Series.Add(series50);
            this.chart3.Size = new System.Drawing.Size(540, 300);
            this.chart3.TabIndex = 7;
            this.chart3.Text = "chart2";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 354);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(723, 102);
            this.label13.TabIndex = 6;
            this.label13.Text = resources.GetString("label13.Text");
            // 
            // dgv_test3
            // 
            this.dgv_test3.AutoGenerateColumns = false;
            this.dgv_test3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_test3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_test3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idtestedDataGridViewTextBoxColumn2,
            this.resultDataGridViewTextBoxColumn});
            this.dgv_test3.DataSource = this.resultstest3BindingSource;
            this.dgv_test3.Location = new System.Drawing.Point(9, 34);
            this.dgv_test3.Name = "dgv_test3";
            this.dgv_test3.RowHeadersWidth = 51;
            this.dgv_test3.RowTemplate.Height = 24;
            this.dgv_test3.Size = new System.Drawing.Size(394, 299);
            this.dgv_test3.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(517, 17);
            this.label9.TabIndex = 4;
            this.label9.Text = "Диагностика уровня консервативности стиля организационного управления";
            // 
            // but_to_menu3
            // 
            this.but_to_menu3.Location = new System.Drawing.Point(871, 442);
            this.but_to_menu3.Name = "but_to_menu3";
            this.but_to_menu3.Size = new System.Drawing.Size(136, 30);
            this.but_to_menu3.TabIndex = 1;
            this.but_to_menu3.Text = "В главное меню";
            this.but_to_menu3.UseVisualStyleBackColor = true;
            // 
            // tabTest4
            // 
            this.tabTest4.Controls.Add(this.label14);
            this.tabTest4.Controls.Add(this.dgv_test4);
            this.tabTest4.Controls.Add(this.label10);
            this.tabTest4.Controls.Add(this.but_to_menu4);
            this.tabTest4.Location = new System.Drawing.Point(4, 25);
            this.tabTest4.Name = "tabTest4";
            this.tabTest4.Padding = new System.Windows.Forms.Padding(3);
            this.tabTest4.Size = new System.Drawing.Size(1013, 478);
            this.tabTest4.TabIndex = 4;
            this.tabTest4.Text = "Тест 4";
            this.tabTest4.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 343);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(728, 119);
            this.label14.TabIndex = 7;
            this.label14.Text = resources.GetString("label14.Text");
            // 
            // dgv_test4
            // 
            this.dgv_test4.AutoGenerateColumns = false;
            this.dgv_test4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_test4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_test4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idtestedDataGridViewTextBoxColumn3,
            this.res1DataGridViewTextBoxColumn2,
            this.res2DataGridViewTextBoxColumn2,
            this.res3DataGridViewTextBoxColumn2,
            this.res4DataGridViewTextBoxColumn2});
            this.dgv_test4.DataSource = this.resultstest4BindingSource;
            this.dgv_test4.Location = new System.Drawing.Point(9, 35);
            this.dgv_test4.Name = "dgv_test4";
            this.dgv_test4.RowHeadersWidth = 51;
            this.dgv_test4.RowTemplate.Height = 24;
            this.dgv_test4.Size = new System.Drawing.Size(725, 290);
            this.dgv_test4.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(490, 17);
            this.label10.TabIndex = 5;
            this.label10.Text = "Диагностика комплекса угрожаемого авторитета (КУА) у руководителей";
            // 
            // but_to_menu4
            // 
            this.but_to_menu4.Location = new System.Drawing.Point(871, 442);
            this.but_to_menu4.Name = "but_to_menu4";
            this.but_to_menu4.Size = new System.Drawing.Size(136, 30);
            this.but_to_menu4.TabIndex = 1;
            this.but_to_menu4.Text = "В главное меню";
            this.but_to_menu4.UseVisualStyleBackColor = true;
            // 
            // tabUsers
            // 
            this.tabUsers.Controls.Add(this.dgv_Users);
            this.tabUsers.Controls.Add(this.but_to_menu5);
            this.tabUsers.Location = new System.Drawing.Point(4, 25);
            this.tabUsers.Name = "tabUsers";
            this.tabUsers.Padding = new System.Windows.Forms.Padding(3);
            this.tabUsers.Size = new System.Drawing.Size(1013, 478);
            this.tabUsers.TabIndex = 5;
            this.tabUsers.Text = "Пользователи";
            this.tabUsers.UseVisualStyleBackColor = true;
            // 
            // dgv_Users
            // 
            this.dgv_Users.AutoGenerateColumns = false;
            this.dgv_Users.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Users.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Users.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iduserDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn});
            this.dgv_Users.DataSource = this.usersBindingSource;
            this.dgv_Users.Location = new System.Drawing.Point(6, 6);
            this.dgv_Users.Name = "dgv_Users";
            this.dgv_Users.RowHeadersWidth = 51;
            this.dgv_Users.RowTemplate.Height = 24;
            this.dgv_Users.Size = new System.Drawing.Size(787, 406);
            this.dgv_Users.TabIndex = 3;
            // 
            // but_to_menu5
            // 
            this.but_to_menu5.Location = new System.Drawing.Point(828, 442);
            this.but_to_menu5.Name = "but_to_menu5";
            this.but_to_menu5.Size = new System.Drawing.Size(150, 30);
            this.but_to_menu5.TabIndex = 2;
            this.but_to_menu5.Text = "В главное меню";
            this.but_to_menu5.UseVisualStyleBackColor = true;
            // 
            // tabResult
            // 
            this.tabResult.Controls.Add(this.label_res);
            this.tabResult.Controls.Add(this.but_get_res);
            this.tabResult.Controls.Add(this.cb_tested);
            this.tabResult.Controls.Add(this.label6);
            this.tabResult.Controls.Add(this.but_to_menu6);
            this.tabResult.Location = new System.Drawing.Point(4, 25);
            this.tabResult.Name = "tabResult";
            this.tabResult.Padding = new System.Windows.Forms.Padding(3);
            this.tabResult.Size = new System.Drawing.Size(1013, 478);
            this.tabResult.TabIndex = 6;
            this.tabResult.Text = "Результаты тестируемого";
            this.tabResult.UseVisualStyleBackColor = true;
            // 
            // label_res
            // 
            this.label_res.AutoSize = true;
            this.label_res.Location = new System.Drawing.Point(6, 157);
            this.label_res.Name = "label_res";
            this.label_res.Size = new System.Drawing.Size(86, 17);
            this.label_res.TabIndex = 7;
            this.label_res.Text = "Результаты";
            // 
            // but_get_res
            // 
            this.but_get_res.Location = new System.Drawing.Point(9, 105);
            this.but_get_res.Name = "but_get_res";
            this.but_get_res.Size = new System.Drawing.Size(392, 30);
            this.but_get_res.TabIndex = 6;
            this.but_get_res.Text = "Посмотреть результаты";
            this.but_get_res.UseVisualStyleBackColor = true;
            // 
            // cb_tested
            // 
            this.cb_tested.FormattingEnabled = true;
            this.cb_tested.Location = new System.Drawing.Point(9, 64);
            this.cb_tested.Name = "cb_tested";
            this.cb_tested.Size = new System.Drawing.Size(392, 24);
            this.cb_tested.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(395, 17);
            this.label6.TabIndex = 4;
            this.label6.Text = "Выберите ФИО тестируемого для просмотра результатов";
            // 
            // but_to_menu6
            // 
            this.but_to_menu6.Location = new System.Drawing.Point(857, 442);
            this.but_to_menu6.Name = "but_to_menu6";
            this.but_to_menu6.Size = new System.Drawing.Size(150, 30);
            this.but_to_menu6.TabIndex = 3;
            this.but_to_menu6.Text = "В главное меню";
            this.but_to_menu6.UseVisualStyleBackColor = true;
            // 
            // testingDataSetRes1
            // 
            this.testingDataSetRes1.DataSetName = "testingDataSetRes1";
            this.testingDataSetRes1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // testingDataSetRes1BindingSource
            // 
            this.testingDataSetRes1BindingSource.DataSource = this.testingDataSetRes1;
            this.testingDataSetRes1BindingSource.Position = 0;
            // 
            // testingDataSet1
            // 
            this.testingDataSet1.DataSetName = "testingDataSet1";
            this.testingDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // testingDataSet1BindingSource
            // 
            this.testingDataSet1BindingSource.DataSource = this.testingDataSet1;
            this.testingDataSet1BindingSource.Position = 0;
            // 
            // testingDataSet3
            // 
            this.testingDataSet3.DataSetName = "testingDataSet3";
            this.testingDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // resultstest1BindingSource
            // 
            this.resultstest1BindingSource.DataMember = "Results_test_1";
            this.resultstest1BindingSource.DataSource = this.testingDataSet3;
            // 
            // results_test_1TableAdapter
            // 
            this.results_test_1TableAdapter.ClearBeforeFill = true;
            // 
            // idtestedDataGridViewTextBoxColumn
            // 
            this.idtestedDataGridViewTextBoxColumn.DataPropertyName = "id_tested";
            this.idtestedDataGridViewTextBoxColumn.HeaderText = "Тестируемый";
            this.idtestedDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.idtestedDataGridViewTextBoxColumn.Name = "idtestedDataGridViewTextBoxColumn";
            // 
            // res1DataGridViewTextBoxColumn
            // 
            this.res1DataGridViewTextBoxColumn.DataPropertyName = "res_1";
            this.res1DataGridViewTextBoxColumn.HeaderText = "1 - 3";
            this.res1DataGridViewTextBoxColumn.MinimumWidth = 6;
            this.res1DataGridViewTextBoxColumn.Name = "res1DataGridViewTextBoxColumn";
            // 
            // res2DataGridViewTextBoxColumn
            // 
            this.res2DataGridViewTextBoxColumn.DataPropertyName = "res_2";
            this.res2DataGridViewTextBoxColumn.HeaderText = "4 - 6";
            this.res2DataGridViewTextBoxColumn.MinimumWidth = 6;
            this.res2DataGridViewTextBoxColumn.Name = "res2DataGridViewTextBoxColumn";
            // 
            // res3DataGridViewTextBoxColumn
            // 
            this.res3DataGridViewTextBoxColumn.DataPropertyName = "res_3";
            this.res3DataGridViewTextBoxColumn.HeaderText = "7 - 9";
            this.res3DataGridViewTextBoxColumn.MinimumWidth = 6;
            this.res3DataGridViewTextBoxColumn.Name = "res3DataGridViewTextBoxColumn";
            // 
            // res4DataGridViewTextBoxColumn
            // 
            this.res4DataGridViewTextBoxColumn.DataPropertyName = "res_4";
            this.res4DataGridViewTextBoxColumn.HeaderText = "10 - 12";
            this.res4DataGridViewTextBoxColumn.MinimumWidth = 6;
            this.res4DataGridViewTextBoxColumn.Name = "res4DataGridViewTextBoxColumn";
            // 
            // testingDataSet4
            // 
            this.testingDataSet4.DataSetName = "testingDataSet4";
            this.testingDataSet4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // usersBindingSource
            // 
            this.usersBindingSource.DataMember = "Users";
            this.usersBindingSource.DataSource = this.testingDataSet4;
            // 
            // usersTableAdapter
            // 
            this.usersTableAdapter.ClearBeforeFill = true;
            // 
            // iduserDataGridViewTextBoxColumn
            // 
            this.iduserDataGridViewTextBoxColumn.DataPropertyName = "id_user";
            this.iduserDataGridViewTextBoxColumn.FillWeight = 85.5615F;
            this.iduserDataGridViewTextBoxColumn.HeaderText = "ID пользователя";
            this.iduserDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.iduserDataGridViewTextBoxColumn.Name = "iduserDataGridViewTextBoxColumn";
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.FillWeight = 114.4385F;
            this.nameDataGridViewTextBoxColumn.HeaderText = "ФИО";
            this.nameDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            // 
            // testingDataSet5
            // 
            this.testingDataSet5.DataSetName = "testingDataSet5";
            this.testingDataSet5.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // resultstest2BindingSource
            // 
            this.resultstest2BindingSource.DataMember = "Results_test_2";
            this.resultstest2BindingSource.DataSource = this.testingDataSet5;
            // 
            // results_test_2TableAdapter
            // 
            this.results_test_2TableAdapter.ClearBeforeFill = true;
            // 
            // idtestedDataGridViewTextBoxColumn1
            // 
            this.idtestedDataGridViewTextBoxColumn1.DataPropertyName = "id_tested";
            this.idtestedDataGridViewTextBoxColumn1.HeaderText = "Тестируемый";
            this.idtestedDataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.idtestedDataGridViewTextBoxColumn1.Name = "idtestedDataGridViewTextBoxColumn1";
            // 
            // res1DataGridViewTextBoxColumn1
            // 
            this.res1DataGridViewTextBoxColumn1.DataPropertyName = "res_1";
            this.res1DataGridViewTextBoxColumn1.HeaderText = "1";
            this.res1DataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.res1DataGridViewTextBoxColumn1.Name = "res1DataGridViewTextBoxColumn1";
            // 
            // res2DataGridViewTextBoxColumn1
            // 
            this.res2DataGridViewTextBoxColumn1.DataPropertyName = "res_2";
            this.res2DataGridViewTextBoxColumn1.HeaderText = "2";
            this.res2DataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.res2DataGridViewTextBoxColumn1.Name = "res2DataGridViewTextBoxColumn1";
            // 
            // res3DataGridViewTextBoxColumn1
            // 
            this.res3DataGridViewTextBoxColumn1.DataPropertyName = "res_3";
            this.res3DataGridViewTextBoxColumn1.HeaderText = "3";
            this.res3DataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.res3DataGridViewTextBoxColumn1.Name = "res3DataGridViewTextBoxColumn1";
            // 
            // res4DataGridViewTextBoxColumn1
            // 
            this.res4DataGridViewTextBoxColumn1.DataPropertyName = "res_4";
            this.res4DataGridViewTextBoxColumn1.HeaderText = "4";
            this.res4DataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.res4DataGridViewTextBoxColumn1.Name = "res4DataGridViewTextBoxColumn1";
            // 
            // res5DataGridViewTextBoxColumn
            // 
            this.res5DataGridViewTextBoxColumn.DataPropertyName = "res_5";
            this.res5DataGridViewTextBoxColumn.HeaderText = "5";
            this.res5DataGridViewTextBoxColumn.MinimumWidth = 6;
            this.res5DataGridViewTextBoxColumn.Name = "res5DataGridViewTextBoxColumn";
            // 
            // res6DataGridViewTextBoxColumn
            // 
            this.res6DataGridViewTextBoxColumn.DataPropertyName = "res_6";
            this.res6DataGridViewTextBoxColumn.HeaderText = "6";
            this.res6DataGridViewTextBoxColumn.MinimumWidth = 6;
            this.res6DataGridViewTextBoxColumn.Name = "res6DataGridViewTextBoxColumn";
            // 
            // testingDataSet6
            // 
            this.testingDataSet6.DataSetName = "testingDataSet6";
            this.testingDataSet6.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // resultstest3BindingSource
            // 
            this.resultstest3BindingSource.DataMember = "Results_test_3";
            this.resultstest3BindingSource.DataSource = this.testingDataSet6;
            // 
            // results_test_3TableAdapter
            // 
            this.results_test_3TableAdapter.ClearBeforeFill = true;
            // 
            // idtestedDataGridViewTextBoxColumn2
            // 
            this.idtestedDataGridViewTextBoxColumn2.DataPropertyName = "id_tested";
            this.idtestedDataGridViewTextBoxColumn2.HeaderText = "Тестируемый";
            this.idtestedDataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.idtestedDataGridViewTextBoxColumn2.Name = "idtestedDataGridViewTextBoxColumn2";
            // 
            // resultDataGridViewTextBoxColumn
            // 
            this.resultDataGridViewTextBoxColumn.DataPropertyName = "result";
            this.resultDataGridViewTextBoxColumn.HeaderText = "Сумма баллов";
            this.resultDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.resultDataGridViewTextBoxColumn.Name = "resultDataGridViewTextBoxColumn";
            // 
            // testingDataSet7
            // 
            this.testingDataSet7.DataSetName = "testingDataSet7";
            this.testingDataSet7.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // resultstest4BindingSource
            // 
            this.resultstest4BindingSource.DataMember = "Results_test_4";
            this.resultstest4BindingSource.DataSource = this.testingDataSet7;
            // 
            // results_test_4TableAdapter
            // 
            this.results_test_4TableAdapter.ClearBeforeFill = true;
            // 
            // idtestedDataGridViewTextBoxColumn3
            // 
            this.idtestedDataGridViewTextBoxColumn3.DataPropertyName = "id_tested";
            this.idtestedDataGridViewTextBoxColumn3.HeaderText = "Тестируемый";
            this.idtestedDataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.idtestedDataGridViewTextBoxColumn3.Name = "idtestedDataGridViewTextBoxColumn3";
            // 
            // res1DataGridViewTextBoxColumn2
            // 
            this.res1DataGridViewTextBoxColumn2.DataPropertyName = "res_1";
            this.res1DataGridViewTextBoxColumn2.HeaderText = "А";
            this.res1DataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.res1DataGridViewTextBoxColumn2.Name = "res1DataGridViewTextBoxColumn2";
            // 
            // res2DataGridViewTextBoxColumn2
            // 
            this.res2DataGridViewTextBoxColumn2.DataPropertyName = "res_2";
            this.res2DataGridViewTextBoxColumn2.HeaderText = "Б";
            this.res2DataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.res2DataGridViewTextBoxColumn2.Name = "res2DataGridViewTextBoxColumn2";
            // 
            // res3DataGridViewTextBoxColumn2
            // 
            this.res3DataGridViewTextBoxColumn2.DataPropertyName = "res_3";
            this.res3DataGridViewTextBoxColumn2.HeaderText = "В";
            this.res3DataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.res3DataGridViewTextBoxColumn2.Name = "res3DataGridViewTextBoxColumn2";
            // 
            // res4DataGridViewTextBoxColumn2
            // 
            this.res4DataGridViewTextBoxColumn2.DataPropertyName = "res_4";
            this.res4DataGridViewTextBoxColumn2.HeaderText = "Г";
            this.res4DataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.res4DataGridViewTextBoxColumn2.Name = "res4DataGridViewTextBoxColumn2";
            // 
            // TesterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1045, 537);
            this.Controls.Add(this.tabControl1);
            this.Name = "TesterForm";
            this.Text = "Тестирующий";
            this.Load += new System.EventHandler(this.TesterForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabTest1.ResumeLayout(false);
            this.tabTest1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_test1)).EndInit();
            this.tabTest2.ResumeLayout(false);
            this.tabTest2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_test2)).EndInit();
            this.tabTest3.ResumeLayout(false);
            this.tabTest3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_test3)).EndInit();
            this.tabTest4.ResumeLayout(false);
            this.tabTest4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_test4)).EndInit();
            this.tabUsers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Users)).EndInit();
            this.tabResult.ResumeLayout(false);
            this.tabResult.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSetRes1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSetRes1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultstest1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultstest2BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultstest3BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultstest4BindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button but_exit;
        private System.Windows.Forms.Button but_click_res;
        private System.Windows.Forms.Button but_users;
        private System.Windows.Forms.Button but_test4;
        private System.Windows.Forms.Button but_test3;
        private System.Windows.Forms.Button but_test2;
        private System.Windows.Forms.Button but_test1;
        private System.Windows.Forms.TabPage tabTest1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dgv_test1;
        private System.Windows.Forms.Button but_to_menu;
        private System.Windows.Forms.TabPage tabTest2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView dgv_test2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button but_to_menu2;
        private System.Windows.Forms.TabPage tabTest3;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridView dgv_test3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button but_to_menu3;
        private System.Windows.Forms.TabPage tabTest4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridView dgv_test4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button but_to_menu4;
        private System.Windows.Forms.TabPage tabUsers;
        private System.Windows.Forms.DataGridView dgv_Users;
        private System.Windows.Forms.Button but_to_menu5;
        private System.Windows.Forms.TabPage tabResult;
        private System.Windows.Forms.Label label_res;
        private System.Windows.Forms.Button but_get_res;
        private System.Windows.Forms.ComboBox cb_tested;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button but_to_menu6;
        private System.Windows.Forms.BindingSource testingDataSetRes1BindingSource;
        private testingDataSetRes1 testingDataSetRes1;
        private testingDataSet1 testingDataSet1;
        private System.Windows.Forms.BindingSource testingDataSet1BindingSource;
        private testingDataSet3 testingDataSet3;
        private System.Windows.Forms.BindingSource resultstest1BindingSource;
        private testingDataSet3TableAdapters.Results_test_1TableAdapter results_test_1TableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idtestedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn res1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn res2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn res3DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn res4DataGridViewTextBoxColumn;
        private testingDataSet4 testingDataSet4;
        private System.Windows.Forms.BindingSource usersBindingSource;
        private testingDataSet4TableAdapters.UsersTableAdapter usersTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn iduserDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private testingDataSet5 testingDataSet5;
        private System.Windows.Forms.BindingSource resultstest2BindingSource;
        private testingDataSet5TableAdapters.Results_test_2TableAdapter results_test_2TableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idtestedDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn res1DataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn res2DataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn res3DataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn res4DataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn res5DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn res6DataGridViewTextBoxColumn;
        private testingDataSet6 testingDataSet6;
        private System.Windows.Forms.BindingSource resultstest3BindingSource;
        private testingDataSet6TableAdapters.Results_test_3TableAdapter results_test_3TableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idtestedDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn resultDataGridViewTextBoxColumn;
        private testingDataSet7 testingDataSet7;
        private System.Windows.Forms.BindingSource resultstest4BindingSource;
        private testingDataSet7TableAdapters.Results_test_4TableAdapter results_test_4TableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idtestedDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn res1DataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn res2DataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn res3DataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn res4DataGridViewTextBoxColumn2;
    }
}