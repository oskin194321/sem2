﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace testing
{
    public partial class StartForm : Form
    {
        public string connectString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=testing.accdb;";
        private OleDbConnection myConnection;

        public StartForm()
        {
            InitializeComponent();
            
            myConnection = new OleDbConnection(connectString); // Создание экземпляра класса OleDbConnection
            myConnection.Open(); // Открытие соединения с БД

            but_check_in.Click += But_check_in_Click;
        }

        private void But_check_in_Click(object sender, EventArgs e)
        {
            string query = "SELECT id_user, access FROM Users WHERE login = \"" + tb_login.Text + "\" and password = \"" + tb_password.Text+ "\"";

            // создаем объект OleDbCommand для выполнения запроса к БД MS Access
            OleDbCommand command = new OleDbCommand(query, myConnection);

            // получаем объект OleDbDataReader для чтения табличного результата запроса SELECT
            OleDbDataReader reader = command.ExecuteReader();

            int count = 0; // счетчик количества записей с введенным логином и паролем
            int id_user = -1; // Для id пользователя
            int lvl_access = -1; // Для уровня доступа

            while (reader.Read())// Построчное считывания ответа от БД
            {
                count++;
                id_user = Convert.ToInt32(reader[0]); // запись id пользователя
                lvl_access = Convert.ToInt32(reader[1]); // запись уровня доступа к системе
            }
            reader.Close(); // закрытие OleDbDataReader

            if (lvl_access == 0) // Открытие формы Администратора
            {
                AdminForm form = new AdminForm(myConnection);
                form.Show();
            }

            if (lvl_access == 1) // Открытие формы Тестирующего
            {
                TesterForm form = new TesterForm(myConnection);
                form.Show();
            }

            if (lvl_access == 2) // Открытие формы Тестируемого
            {
                TestedForm form = new TestedForm(myConnection, id_user);
                form.Show();
            }

            if (lvl_access == -1)
                MessageBox.Show("Пользователь не найден");
        }

        private void StartForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            myConnection.Close(); // Закрытие соединения с БД
        }
    }
}
