﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace testing
{
    public partial class AdminForm : Form
    {
        private OleDbConnection myConnection;
        string query; // Для запроса к БД
        OleDbCommand command;
        OleDbDataReader reader;
        int[] id_tested;
        int count = 0;

        public AdminForm(OleDbConnection conn)
        {
            InitializeComponent();
            myConnection = conn; // Соединение с БД

            but_save_users.Click += But_save_users_Click;
            but_add_user.Click += But_add_user_Click;
            but_get_res.Click += But_get_res_Click;

            but_test1.Click += But_test1_Click;
            but_test2.Click += But_test2_Click;
            but_test3.Click += But_test3_Click;
            but_test4.Click += But_test4_Click;
            but_users.Click += But_users_Click;
            but_click_res.Click += But_click_res_Click;

            but_to_menu.Click += But_to_menu_Click;
            but_to_menu2.Click += But_to_menu_Click;
            but_to_menu3.Click += But_to_menu_Click;
            but_to_menu4.Click += But_to_menu_Click;
            but_to_menu5.Click += But_to_menu_Click;
            but_to_menu6.Click += But_to_menu_Click;

            but_exit.Click += But_exit_Click;
        }

        private void But_exit_Click(object sender, EventArgs e) // Выход из программы
        {
            this.Close();
        }

        private void But_click_res_Click(object sender, EventArgs e) // Переключение на вкладку "Результаты пользователя"
        {
            tabControl1.SelectedTab = tabControl1.TabPages["TabResult"];
        }

        private void But_add_user_Click(object sender, EventArgs e)
        {
            DataRow nRow = testingDataSet_Users.Tables[0].NewRow();
            int rc = dgv_Users.RowCount + 1;
            nRow[0] = rc;
            nRow[1] = tb_name.Text;
            nRow[2] = tb_login.Text;
            nRow[3] = tb_password.Text;
            nRow[4] = cb_access.SelectedIndex;
            testingDataSet_Users.Tables[0].Rows.Add(nRow);
            usersTableAdapter.Update(testingDataSet_Users.Users);
            testingDataSet_Users.Tables[0].AcceptChanges();
            dgv_Users.Refresh();
            tb_login.Text = "";
            tb_name.Text = "";
            tb_password.Text = "";
        }

        private void But_get_res_Click(object sender, EventArgs e) // Отображение результатов тестируемого
        {
            // Результаты Теста 1
            label_res.Text = "Тест 1. Экспресс-диагностика барьеров в управленческой деятельности:\n";
            query = "SELECT * FROM Results_test_1 WHERE id_tested = " + id_tested[cb_tested.SelectedIndex];
            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();
            count = 0; // счетчик количества записей
            int res1 = 0, res2 = 0, res3 = 0, res4 = 0, sum = 0, res5, res6;
            while (reader.Read())// Построчное считывания ответа от БД
            {
                res1 = (int)reader[2];
                res2 = (int)reader[3];
                res3 = (int)reader[4];
                res4 = (int)reader[5];
                count++;
            }
            reader.Close();
            if (count != 0)
            {
                if (res1 > 11 && res2 > 11 & res3 > 11 & res4 > 11)
                    label_res.Text += "\n - Выявлено наличие препятствий, мешающих успешно решать проблемы.";
                else
                    label_res.Text += "\n - Отсутствует наличие препятствий, мешающих успешно решать проблемы.";
                sum = res1 + res2 + res3 + res4;
                if (sum > 36)
                    label_res.Text += "\n - Есть основания говорить о несоответствии личностных особенностей требованиям данной профессии.";
                else
                    label_res.Text += "\n - Нет оснований говорить о несоответствии личностных особенностей требованиям данной профессии.";
            }
            else
                label_res.Text += " - Тест не пройден.";

            // Результаты Теста 2
            label_res.Text += "\n\nТест 2. Экспресс-диагностика профессионально-деятельностных ограничений руководителя:\n";
            query = "SELECT * FROM Results_test_2 WHERE id_tested = " + id_tested[cb_tested.SelectedIndex];
            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();
            res1 = 0; res2 = 0; res3 = 0; res4 = 0; res5 = 0; res6 = 0;
            count = 0; // счетчик количества записей
            while (reader.Read())// Построчное считывания ответа от БД
            {
                res1 = (int)reader[2];
                res2 = (int)reader[3];
                res3 = (int)reader[4];
                res4 = (int)reader[5];
                res5 = (int)reader[6];
                res6 = (int)reader[7];
                count++;
            }
            reader.Close();
            if (count != 0)
            {
                if (res1 + res2 + res3 >= res4 + res5 + res6)
                    label_res.Text += "\n - Умение управлять собой, наличие четких личных ценностей, способность к саморазвитию,\n " +
                        "неразвитость творческого подхода, неумение влиять на людей, отсутствие качеств наставника.";
                else
                    label_res.Text += "\n - Неумение управлять собой, рызмытость личных ценностей, остановка в саморазвитии,\n " +
                        "способность творчески решать проблемы, умение влиять на людей, умение обучать.";
            }
            else
                label_res.Text += " - Тест не пройден.";

            // Результаты Теста 3
            label_res.Text += "\n\nТест 3. Диагностика уровня консервативности стиля организационного управления:\n";
            query = "SELECT * FROM Results_test_3 WHERE id_tested = " + id_tested[cb_tested.SelectedIndex];
            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();
            sum = 0;
            while (reader.Read())// Построчное считывания ответа от БД
            {
                sum = (int)reader[2];                
                count++;
            }
            reader.Close();
            if (count != 0)
            {
                if (sum > 32)
                    label_res.Text += "\n - Высокий уровень.";
                else if (sum > 17)
                    label_res.Text += "\n - Средний уровень.";
                else
                    label_res.Text += "\n - Невысокий уровень.";
            }
            else
                label_res.Text += " - Тест не пройден.";

            // Результаты Теста 4
            label_res.Text += "\n\nТест 4. Диагностика комплекса угрожаемого авторитета (КУА) у руководителей:\n";
            query = "SELECT * FROM Results_test_4 WHERE id_tested = " + id_tested[cb_tested.SelectedIndex];
            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();
            res1 = 0; res2 = 0; res3 = 0; res4 = 0;
            count = 0; // счетчик количества записей
            while (reader.Read())// Построчное считывания ответа от БД
            {
                res1 = (int)reader[2];
                res2 = (int)reader[3];
                res3 = (int)reader[4];
                res4 = (int)reader[5];
                count++;
            }
            reader.Close();
            if (count != 0)
            {
                if (res1 > res2 && res1 > res3 && res1 > res4)
                    label_res.Text += "\n - Выявлено наличие комплекса угрожаемого авторитета (КУА).";
                else if (res2 > res3 && res2 > res4)
                    label_res.Text += "\n - По результатам можно судить о предрасположенности к наличию комплекса угрожаемого авторитета (КУА).";
                else if (res3 > res4)
                    label_res.Text += "\n - По результатам можно говорить о преобладании оборонительной позиции у руководителя по отношению к подчиненным.";
                else
                    label_res.Text += "\n - Выявлено отсутствие комплекса угрожаемого авторитета (КУА).";
            }
            else
                label_res.Text += " - Тест не пройден.";
        }

        private void load_tested() // Добавление списка тестируемых в подпункт с просмотром результатов
        {
            id_tested = new int[dgv_Users.RowCount];

            query = "SELECT id_user, name FROM Users WHERE access = 2";

            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();

            int count = 0; // счетчик количества записей
            while (reader.Read())// Построчное считывания ответа от БД
            {
                id_tested[count] = (int)reader[0];
                count++;
                cb_tested.Items.Add(reader[0] + " " + reader[1]);
            }
            reader.Close();
        }

        private void But_save_users_Click(object sender, EventArgs e) // Сохранение изменений в таблице Users
        {
            usersTableAdapter.Update(testingDataSet_Users);
        }

        private void But_users_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabControl1.TabPages["TabUsers"];
        }

        private void But_to_menu_Click(object sender, EventArgs e) // Возврат в главное меню
        {
            tabControl1.SelectedTab = tabControl1.TabPages["TabPage1"];
        }

        private void But_test4_Click(object sender, EventArgs e) // Переключение на вкладку "Тест 4"
        {
            tabControl1.SelectedTab = tabControl1.TabPages["TabTest4"];
        }

        private void But_test3_Click(object sender, EventArgs e) // Переключение на вкладку "Тест 3"
        {
            tabControl1.SelectedTab = tabControl1.TabPages["TabTest3"];
        }

        private void But_test2_Click(object sender, EventArgs e) // Переключение на вкладку "Тест 2"
        {
            tabControl1.SelectedTab = tabControl1.TabPages["TabTest2"];
        }

        private void But_test1_Click(object sender, EventArgs e) // Переключение на вкладку "Тест 1"
        {
            tabControl1.SelectedTab = tabControl1.TabPages["TabTest1"];
        }

        private void AdminForm_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "testingDataSet2.Results_test_4". При необходимости она может быть перемещена или удалена.
            this.results_test_4TableAdapter.Fill(this.testingDataSet2.Results_test_4);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "testingDataSet1.Results_test_3". При необходимости она может быть перемещена или удалена.
            this.results_test_3TableAdapter.Fill(this.testingDataSet1.Results_test_3);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "testingDataTest2.Results_test_2". При необходимости она может быть перемещена или удалена.
            this.results_test_2TableAdapter.Fill(this.testingDataTest2.Results_test_2);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "testingDataSetRes1.Results_test_1". При необходимости она может быть перемещена или удалена.
            this.results_test_1TableAdapter.Fill(this.testingDataSetRes1.Results_test_1);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "testingDataSet_Users.Users"
            this.usersTableAdapter.Fill(this.testingDataSet_Users.Users);
            load_tested();
            create_chart1();
        }

        private void create_chart1() // Создание гистограмм с результатами тестирования
        {
            // Тест 1
            query = "SELECT * FROM Results_test_1";
            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();
            count = 0; // счетчик количества записей
            int sum = 0;
            while (reader.Read())// Построчное считывания ответа от БД
            {
                sum = (int)reader[2] + (int)reader[3] + (int)reader[4] + (int)reader[5];
                count++;
                chart1.Series[0].Points.AddXY(count, sum);
            }
            reader.Close();
            chart1.Series[1].Points.AddXY(0, 36);
            chart1.Series[1].Points.AddXY(chart1.Series[0].Points.Count+1, 36);

            // Тест 3
            query = "SELECT * FROM Results_test_3";
            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();
            count = 0; // счетчик количества записей
            while (reader.Read())// Построчное считывания ответа от БД
            {
                count++;
                chart3.Series[0].Points.AddXY(count, reader[2]);
            }
            reader.Close();
            chart3.Series[1].Points.AddXY(0, 17);
            chart3.Series[1].Points.AddXY(chart3.Series[0].Points.Count + 1, 17);
            chart3.Series[2].Points.AddXY(0, 33);
            chart3.Series[2].Points.AddXY(chart3.Series[0].Points.Count + 1, 33);
        }

        private void dataGridView1_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e) // Подтверждение перед удалением строки
        {
            DialogResult dr = MessageBox.Show("Удалить запись?", "Удаление",
                MessageBoxButtons.OKCancel, MessageBoxIcon.Warning,
                MessageBoxDefaultButton.Button2);
            if (dr == DialogResult.Cancel)
                e.Cancel = true;
        }
    }
}