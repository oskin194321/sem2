﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace testing
{
    public partial class TestedForm : Form
    {
        OleDbConnection myConnection;
        string query; // Для запроса к БД
        OleDbCommand command;
        OleDbDataReader reader;
        int id_user;

        // Для хранения id вопросов
        int[] test1_id_quest=new int[12];
        int[] test2_id_quest = new int[42];
        int[] test3_id_quest = new int[22];
        int[] test4_id_quest = new int[15];
        // Для хранения id ответов
        int[] test1_id_option = new int[5];
        int[] test2_id_option = new int[2];
        int[] test3_id_option = new int[4];
        int[] test4_id_option = new int[4];

        public TestedForm(OleDbConnection conn, int id)
        {
            InitializeComponent();

            myConnection = conn;
            id_user = id; // id текущего пользователя

            show_questions();

            but_answer1.Click += But_answer1_Click;
            but_answer2.Click += But_answer2_Click;
            but_answer3.Click += But_answer3_Click;
            but_answer4.Click += But_answer4_Click;

            but_res_test1.Click += But_res_test1_Click;
            but_res_test2.Click += But_res_test2_Click;
            but_res_test3.Click += But_res_test3_Click;
            but_res_test4.Click += But_res_test4_Click;

            but_test1.Click += But_test1_Click;
            but_test2.Click += But_test2_Click;
            but_test3.Click += But_test3_Click;
            but_test4.Click += But_test4_Click;

            but_to_menu.Click += But_to_menu_Click;
            but_to_menu2.Click += But_to_menu_Click;
            but_to_menu3.Click += But_to_menu_Click;
            but_to_menu4.Click += But_to_menu_Click;

            but_exit.Click += But_exit_Click;
        }

        private void But_exit_Click(object sender, EventArgs e) // Выход из программы
        {
            this.Close();
        }

        private void But_res_test4_Click(object sender, EventArgs e) // Отображение результатов теста 4
        {
            int res1 = 0, res2 = 0, res3 = 0, res4 = 0;

            query = "SELECT * FROM Answers WHERE id_test = 4 and id_tested = " + id_user;
            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();

            int count = 0; // счетчик количества записей
            while (reader.Read())// Построчное считывания ответа от БД
            {
                if ((int)reader[4] == 12)
                    res1++;
                else if ((int)reader[4] == 13)
                    res2++;
                else if ((int)reader[4] == 14)
                    res3++;
                else if ((int)reader[4] == 15)
                    res4++;

                count++;
            }
            reader.Close();

            if (res1 > res2 && res1 > res3 && res1 > res4)
                label_test4.Text += "\n\nВыявлено наличие комплекса угрожаемого авторитета (КУА).";
            else if(res2 > res3 && res2 > res4)
                label_test4.Text += "\n\nПо результатам можно судить о предрасположенности к наличию комплекса угрожаемого авторитета (КУА).";
            else if(res3 > res4)
                label_test4.Text += "\n\nПо результатам можно говорить о преобладании оборонительной позиции у руководителя по отношению к подчиненным.";
            else
                label_test4.Text += "\n\nВыявлено отсутствие комплекса угрожаемого авторитета (КУА).";

            // Сохранение результата
            query = "SELECT * FROM Results_test_4 WHERE id_tested = " + id_user;

            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();

            count = 0; // счетчик количества записей
            while (reader.Read())// Построчное считывания ответа от БД
            {
                count++;
            }
            reader.Close();

            if (count == 0)
            {
                query = "INSERT INTO Results_test_4 (id_tested, res_1, res_2, res_3, res_4) " +
                "VALUES (" + id_user + ", " + res1 + ", " + res2 + ", " + res3 + ", " + res4 + ")";

                command = new OleDbCommand(query, myConnection);
                command.ExecuteNonQuery();
            }
            else
                MessageBox.Show("Результат получен");
        }

        private void But_res_test3_Click(object sender, EventArgs e) // Отображение результатов теста 3
        {
            int sum = 0;

            query = "SELECT * FROM Answers WHERE id_test = 3 and id_tested = " + id_user;
            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();

            int count = 0; // счетчик количества записей
            while (reader.Read())// Построчное считывания ответа от БД
            {
                if ((int)reader[4] == 8)
                    sum += -2;
                else if ((int)reader[4] == 9)
                    sum += -1;
                else if ((int)reader[4] == 10)
                    sum += 1;
                else if ((int)reader[4] == 11)
                    sum += 2;

                count++;
            }
            reader.Close();

            if (sum > 32)
                label_test3.Text += "\n\n - Высокий уровень -";
            else if (sum > 17)
                label_test3.Text += "\n\n - Средний уровень -";
            else
                label_test3.Text += "\n\n - Невысокий уровень -";

            // Сохранение результата
            query = "SELECT * FROM Results_test_3 WHERE id_tested = " + id_user;
            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();

            count = 0; // счетчик количества записей
            while (reader.Read())// Построчное считывания ответа от БД
            {
                count++;
            }
            reader.Close();

            if (count == 0)
            {
                query = "INSERT INTO Results_test_3 (id_tested, result) VALUES (" + id_user + ", " + sum + ")";
                command = new OleDbCommand(query, myConnection);
                command.ExecuteNonQuery();
            }
            else
                MessageBox.Show("Результат получен");
        }

        private void But_res_test2_Click(object sender, EventArgs e) // Отображение результатов теста 2
        {
            int res1 = 0, res2 = 0, res3 = 0, res4 = 0, res5 = 0, res6 = 0;

            query = "SELECT * FROM Answers WHERE id_test = 2 and id_tested = " + id_user;

            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();

            int count = 0; // счетчик количества записей
            while (reader.Read())// Построчное считывания ответа от БД
            {
                int id_q = (int)reader[3];

                if (id_q == 13 || id_q == 19 || id_q == 25 || id_q == 31 || id_q == 37 || id_q == 43 || id_q == 49)
                {
                    if((int)reader[4]==6)
                        res1++;
                }
                else if (id_q == 14 || id_q == 20 || id_q == 26 || id_q == 32 || id_q == 38 || id_q == 44 || id_q == 50)
                {
                    if ((int)reader[4] == 6)
                        res2++;
                }
                else if (id_q == 15 || id_q == 21 || id_q == 27 || id_q == 33 || id_q == 39 || id_q == 45 || id_q == 51)
                {
                    if ((int)reader[4] == 6)
                        res3++;
                }
                else if (id_q == 16 || id_q == 22 || id_q == 28 || id_q == 34 || id_q == 40 || id_q == 46 || id_q == 52)
                {
                    if ((int)reader[4] == 6)
                        res4++;
                }
                else if (id_q == 17 || id_q == 23 || id_q == 29 || id_q == 35 || id_q == 41 || id_q == 47 || id_q == 53)
                {
                    if ((int)reader[4] == 6)
                        res5++;
                }
                else if (id_q == 18 || id_q == 24 || id_q == 30 || id_q == 36 || id_q == 42 || id_q == 48 || id_q == 54)
                {
                    if ((int)reader[4] == 6)
                        res6++;
                }
                count++;
            }
            reader.Close();

            // Сохранение результата
            query = "SELECT * FROM Results_test_2 WHERE id_tested = " + id_user;

            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();

            count = 0; // счетчик количества записей
            while (reader.Read())// Построчное считывания ответа от БД
            {
                count++;
            }
            reader.Close();

            if (count == 0)
            {
                query = "INSERT INTO Results_test_2 (id_tested, res_1, res_2, res_3, res_4, res_5, res_6) " +
                "VALUES (" + id_user + ", " + res1 + ", " + res2 + ", " + res3 + ", " + res4 + ", " + res5 + ", " + res6 + ")";

                command = new OleDbCommand(query, myConnection);
                command.ExecuteNonQuery();
            }
            else
                MessageBox.Show("Результат получен");
        }

        private void But_res_test1_Click(object sender, EventArgs e) // Отображение результатов теста 1
        {
            int res1 = 0, res2 = 0, res3 = 0, res4 = 0, sum = 0;

            query = "SELECT * FROM Answers WHERE id_test = 1 and id_tested = " + id_user;

            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();

            int count = 0; // счетчик количества записей
            while (reader.Read())// Построчное считывания ответа от БД
            {
                if ((int)reader[3] < 4)
                    res1 += (int)reader[4];
                else if((int)reader[3] < 7)
                    res2 += (int)reader[4];
                else if ((int)reader[3] < 10)
                    res3 += (int)reader[4];
                else if ((int)reader[3] < 13)
                    res4 += (int)reader[4];

                count++;
            }
            reader.Close();

            if (res1 > 11 && res2 > 11 & res3 > 11 & res4 > 11)
                label_test1.Text += "\n\nВыявлено наличие препятствий, мешающих успешно решать проблемы.";
            else
                label_test1.Text += "\n\nОтсутствует наличие препятствий, мешающих успешно решать проблемы.";

            sum = res1 + res2 + res3 + res4;
            if(sum > 36)
                label_test1.Text += "\n\nЕсть основания говорить о несоответствии личностных особенностей требованиям данной профессии.";
            else
                label_test1.Text += "\n\nНет оснований говорить о несоответствии личностных особенностей требованиям данной профессии.";

            // Сохранение результата
            query = "SELECT * FROM Results_test_1 WHERE id_tested = " + id_user;

            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();

            count = 0; // счетчик количества записей
            while (reader.Read())// Построчное считывания ответа от БД
            {
                count++;
            }
            reader.Close();

            if (count == 0)
            {
                query = "INSERT INTO Results_test_1 (id_tested, res_1, res_2, res_3, res_4) " +
                "VALUES (" + id_user + ", " + res1 + ", " + res2 + ", " + res3 + ", " + res4 + ")";

                command = new OleDbCommand(query, myConnection);
                command.ExecuteNonQuery();
            }
            else
                MessageBox.Show("Результат получен");
        }

        private void But_answer4_Click(object sender, EventArgs e) // Сохранение ответа на вопрос из теста 4
        {
            if (cb_test4_quest.Text != "" && cb_test4_option.Text != "") // Проверка наличия выбранного вопроса и ответа
            {
                query = "SELECT * FROM Answers WHERE id_test = 4 and id_question = " +
                    test4_id_quest[cb_test4_quest.SelectedIndex] + " and id_tested = " + id_user;

                command = new OleDbCommand(query, myConnection);
                reader = command.ExecuteReader();

                int count = 0; // счетчик количества записей
                while (reader.Read())// Построчное считывания ответа от БД
                {
                    count++;
                }
                reader.Close();

                if (count == 0)
                {
                    query = "INSERT INTO Answers (id_tested, id_test, id_question, id_answer_option) " +
                    "VALUES (" + id_user + ", 4, " + test4_id_quest[cb_test4_quest.SelectedIndex] + ", "
                    + test4_id_option[cb_test4_option.SelectedIndex] + ")";

                    command = new OleDbCommand(query, myConnection);
                    command.ExecuteNonQuery();
                }
                else
                    MessageBox.Show("Ответ на данный вопрос уже дан");
            }
        }

        private void But_answer3_Click(object sender, EventArgs e) // Сохранение ответа на вопрос из теста 3
        {
            if (cb_test3_quest.Text != "" && cb_test3_option.Text != "") // Проверка наличия выбранного вопроса и ответа
            {
                query = "SELECT * FROM Answers WHERE id_test = 3 and id_question = " +
                    test3_id_quest[cb_test3_quest.SelectedIndex] + " and id_tested = " + id_user;

                command = new OleDbCommand(query, myConnection);
                reader = command.ExecuteReader();

                int count = 0; // счетчик количества записей
                while (reader.Read())// Построчное считывания ответа от БД
                {
                    count++;
                }
                reader.Close();

                if (count == 0)
                {
                    query = "INSERT INTO Answers (id_tested, id_test, id_question, id_answer_option) " +
                    "VALUES (" + id_user + ", 3, " + test3_id_quest[cb_test3_quest.SelectedIndex] + ", "
                    + test3_id_option[cb_test3_option.SelectedIndex] + ")";

                    command = new OleDbCommand(query, myConnection);
                    command.ExecuteNonQuery();
                }
                else
                    MessageBox.Show("Ответ на данный вопрос уже дан");
            }
        }

        private void But_answer2_Click(object sender, EventArgs e) // Сохранение ответа на вопрос из теста 2
        {
            if (cb_test2_quest.Text != "" && cb_test2_option.Text != "") // Проверка наличия выбранного вопроса и ответа
            {
                query = "SELECT * FROM Answers WHERE id_test = 2 and id_question = " +
                    test2_id_quest[cb_test2_quest.SelectedIndex] + " and id_tested = " + id_user;

                command = new OleDbCommand(query, myConnection);
                reader = command.ExecuteReader();

                int count = 0; // счетчик количества записей
                while (reader.Read())// Построчное считывания ответа от БД
                {
                    count++;
                }
                reader.Close();

                if (count == 0)
                {
                    query = "INSERT INTO Answers (id_tested, id_test, id_question, id_answer_option) " +
                    "VALUES (" + id_user + ", 2, " + test2_id_quest[cb_test2_quest.SelectedIndex] + ", "
                    + test2_id_option[cb_test2_option.SelectedIndex] + ")";

                    command = new OleDbCommand(query, myConnection);
                    command.ExecuteNonQuery();
                }
                else
                    MessageBox.Show("Ответ на данный вопрос уже дан");
            }
        }

        private void But_answer1_Click(object sender, EventArgs e) // Сохранение ответа на вопрос из теста 1
        {
            if (cb_test1_quest.Text != "" && cb_test1_option.Text != "") // Проверка наличия выбранного вопроса и ответа
            {
                query = "SELECT * FROM Answers WHERE id_test = 1 and id_question = " +
                    test1_id_quest[cb_test1_quest.SelectedIndex] + " and id_tested = " + id_user;

                command = new OleDbCommand(query, myConnection);
                reader = command.ExecuteReader();

                int count = 0; // счетчик количества записей
                while (reader.Read())// Построчное считывания ответа от БД
                {
                    count++;
                }
                reader.Close();

                if (count == 0)
                {
                    query = "INSERT INTO Answers (id_tested, id_test, id_question, id_answer_option) " +
                    "VALUES (" + id_user + ", 1, " + test1_id_quest[cb_test1_quest.SelectedIndex] + ", "
                    + test1_id_option[cb_test1_option.SelectedIndex] + ")";

                    command = new OleDbCommand(query, myConnection);
                    command.ExecuteNonQuery();
                }
                else
                    MessageBox.Show("Ответ на данный вопрос уже дан");
            }
        }

        private void show_questions() // Отображение вопросов и ответов
        {
            // Тест 1
            // Считывание вопросов и отображение в ComboBox
            query = "SELECT id_question, num_question, question FROM Questions WHERE id_test = 1";

            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();

            int count = 0; // счетчик количества записей
            while (reader.Read())// Построчное считывания ответа от БД
            {
                test1_id_quest[count] = (int)reader[0];
                count++;
                cb_test1_quest.Items.Add(reader[1] + ". " + reader[2]);
            }
            reader.Close();

            // Считывание вариантов ответов и отображение в ComboBox
            query = "SELECT id_answer_option, answer FROM Answer_options WHERE id_test = 1";

            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();

            count = 0; // счетчик количества записей
            while (reader.Read())// Построчное считывания ответа от БД
            {
                test1_id_option[count] = (int)reader[0];
                count++;
                cb_test1_option.Items.Add(reader[1]);
            }
            reader.Close();

            // Тест 2
            // Считывание вопросов и отображение в ComboBox
            query = "SELECT id_question, num_question, question FROM Questions WHERE id_test = 2";

            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();

            count = 0; // счетчик количества записей
            while (reader.Read())// Построчное считывания ответа от БД
            {
                test2_id_quest[count] = (int)reader[0];
                count++;
                cb_test2_quest.Items.Add(reader[1] + ". " + reader[2]);
            }
            reader.Close();

            // Считывание вариантов ответов и отображение в ComboBox
            query = "SELECT id_answer_option, answer FROM Answer_options WHERE id_test = 2";

            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();

            count = 0; // счетчик количества записей
            while (reader.Read())// Построчное считывания ответа от БД
            {
                test2_id_option[count] = (int)reader[0];
                count++;
                cb_test2_option.Items.Add(reader[1]);
            }
            reader.Close();

            // Тест 3
            // Считывание вопросов и отображение в ComboBox
            query = "SELECT id_question, num_question, question FROM Questions WHERE id_test = 3";

            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();

            count = 0; // счетчик количества записей
            while (reader.Read())// Построчное считывания ответа от БД
            {
                test3_id_quest[count] = (int)reader[0];
                count++;
                cb_test3_quest.Items.Add(reader[1] + ". " + reader[2]);
            }
            reader.Close();

            // Считывание вариантов ответов и отображение в ComboBox
            query = "SELECT id_answer_option, answer FROM Answer_options WHERE id_test = 3";

            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();

            count = 0; // счетчик количества записей
            while (reader.Read())// Построчное считывания ответа от БД
            {
                test3_id_option[count] = (int)reader[0];
                count++;
                cb_test3_option.Items.Add(reader[1]);
            }
            reader.Close();

            // Тест 4
            // Считывание вопросов и отображение в ComboBox
            query = "SELECT id_question, num_question, question FROM Questions WHERE id_test = 4";

            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();

            count = 0; // счетчик количества записей
            while (reader.Read())// Построчное считывания ответа от БД
            {
                test4_id_quest[count] = (int)reader[0];
                count++;
                cb_test4_quest.Items.Add(reader[1] + ". " + reader[2]);
            }
            reader.Close();

            // Считывание вариантов ответов и отображение в ComboBox
            query = "SELECT id_answer_option, answer FROM Answer_options WHERE id_test = 4";

            command = new OleDbCommand(query, myConnection);
            reader = command.ExecuteReader();

            count = 0; // счетчик количества записей
            while (reader.Read())// Построчное считывания ответа от БД
            {
                test4_id_option[count] = (int)reader[0];
                count++;
                cb_test4_option.Items.Add(reader[1]);
            }
            reader.Close();
        }

        private void But_to_menu_Click(object sender, EventArgs e) // Возврат в главное меню
        {
            tabControl1.SelectedTab = tabControl1.TabPages["TabPage1"];
        }

        private void But_test4_Click(object sender, EventArgs e) // Переключение на вкладку "Тест 4"
        {
            tabControl1.SelectedTab = tabControl1.TabPages["TabTest4"];
        }

        private void But_test3_Click(object sender, EventArgs e) // Переключение на вкладку "Тест 3"
        {
            tabControl1.SelectedTab = tabControl1.TabPages["TabTest3"];
        }

        private void But_test2_Click(object sender, EventArgs e) // Переключение на вкладку "Тест 2"
        {
            tabControl1.SelectedTab = tabControl1.TabPages["TabTest2"];
        }

        private void But_test1_Click(object sender, EventArgs e) // Переключение на вкладку "Тест 1"
        {
            tabControl1.SelectedTab = tabControl1.TabPages["TabTest1"];
        }
    }
}
