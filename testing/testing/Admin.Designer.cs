﻿namespace testing
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminForm));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.but_test1 = new System.Windows.Forms.Button();
            this.tabTest1 = new System.Windows.Forms.TabPage();
            this.tabTest2 = new System.Windows.Forms.TabPage();
            this.tabTest3 = new System.Windows.Forms.TabPage();
            this.tabTest4 = new System.Windows.Forms.TabPage();
            this.but_test2 = new System.Windows.Forms.Button();
            this.but_test3 = new System.Windows.Forms.Button();
            this.but_test4 = new System.Windows.Forms.Button();
            this.but_to_menu = new System.Windows.Forms.Button();
            this.but_to_menu2 = new System.Windows.Forms.Button();
            this.but_to_menu3 = new System.Windows.Forms.Button();
            this.but_to_menu4 = new System.Windows.Forms.Button();
            this.tabUsers = new System.Windows.Forms.TabPage();
            this.but_users = new System.Windows.Forms.Button();
            this.but_to_menu5 = new System.Windows.Forms.Button();
            this.dgv_Users = new System.Windows.Forms.DataGridView();
            this.testingDataSet_Users = new testing.testingDataSet_Users();
            this.usersTableAdapter = new testing.testingDataSet_UsersTableAdapters.UsersTableAdapter();
            this.but_save_users = new System.Windows.Forms.Button();
            this.usersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.but_add_user = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_name = new System.Windows.Forms.TextBox();
            this.tb_login = new System.Windows.Forms.TextBox();
            this.tb_password = new System.Windows.Forms.TextBox();
            this.cb_access = new System.Windows.Forms.ComboBox();
            this.testingDataSet = new testing.testingDataSet();
            this.testingDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.iduserDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.loginDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.passwordDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accessDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.but_click_res = new System.Windows.Forms.Button();
            this.tabResult = new System.Windows.Forms.TabPage();
            this.but_to_menu6 = new System.Windows.Forms.Button();
            this.but_exit = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.cb_tested = new System.Windows.Forms.ComboBox();
            this.but_get_res = new System.Windows.Forms.Button();
            this.label_res = new System.Windows.Forms.Label();
            this.dgv_test1 = new System.Windows.Forms.DataGridView();
            this.testingDataSetBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.testingDataSetRes1 = new testing.testingDataSetRes1();
            this.resultstest1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.results_test_1TableAdapter = new testing.testingDataSetRes1TableAdapters.Results_test_1TableAdapter();
            this.idtestedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res3DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res4DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.dgv_test2 = new System.Windows.Forms.DataGridView();
            this.testingDataTest2 = new testing.testingDataTest2();
            this.resultstest2BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.results_test_2TableAdapter = new testing.testingDataTest2TableAdapters.Results_test_2TableAdapter();
            this.idtestedDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res1DataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res2DataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res3DataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res4DataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res5DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res6DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label12 = new System.Windows.Forms.Label();
            this.dgv_test3 = new System.Windows.Forms.DataGridView();
            this.testingDataSet1 = new testing.testingDataSet1();
            this.resultstest3BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.results_test_3TableAdapter = new testing.testingDataSet1TableAdapters.Results_test_3TableAdapter();
            this.idtestedDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.resultDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label13 = new System.Windows.Forms.Label();
            this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.dgv_test4 = new System.Windows.Forms.DataGridView();
            this.testingDataSet2 = new testing.testingDataSet2();
            this.resultstest4BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.results_test_4TableAdapter = new testing.testingDataSet2TableAdapters.Results_test_4TableAdapter();
            this.idtestedDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res1DataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res2DataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res3DataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.res4DataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label14 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabTest1.SuspendLayout();
            this.tabTest2.SuspendLayout();
            this.tabTest3.SuspendLayout();
            this.tabTest4.SuspendLayout();
            this.tabUsers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Users)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet_Users)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSetBindingSource)).BeginInit();
            this.tabResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_test1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSetBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSetRes1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultstest1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_test2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataTest2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultstest2BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_test3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultstest3BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_test4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultstest4BindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabTest1);
            this.tabControl1.Controls.Add(this.tabTest2);
            this.tabControl1.Controls.Add(this.tabTest3);
            this.tabControl1.Controls.Add(this.tabTest4);
            this.tabControl1.Controls.Add(this.tabUsers);
            this.tabControl1.Controls.Add(this.tabResult);
            this.tabControl1.Location = new System.Drawing.Point(12, 21);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1021, 507);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.but_exit);
            this.tabPage1.Controls.Add(this.but_click_res);
            this.tabPage1.Controls.Add(this.but_users);
            this.tabPage1.Controls.Add(this.but_test4);
            this.tabPage1.Controls.Add(this.but_test3);
            this.tabPage1.Controls.Add(this.but_test2);
            this.tabPage1.Controls.Add(this.but_test1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1013, 478);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Главное меню";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // but_test1
            // 
            this.but_test1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_test1.Location = new System.Drawing.Point(183, 44);
            this.but_test1.Name = "but_test1";
            this.but_test1.Size = new System.Drawing.Size(570, 45);
            this.but_test1.TabIndex = 0;
            this.but_test1.Text = "Тест 1";
            this.but_test1.UseVisualStyleBackColor = true;
            // 
            // tabTest1
            // 
            this.tabTest1.Controls.Add(this.chart1);
            this.tabTest1.Controls.Add(this.label11);
            this.tabTest1.Controls.Add(this.label7);
            this.tabTest1.Controls.Add(this.dgv_test1);
            this.tabTest1.Controls.Add(this.but_to_menu);
            this.tabTest1.Location = new System.Drawing.Point(4, 25);
            this.tabTest1.Name = "tabTest1";
            this.tabTest1.Padding = new System.Windows.Forms.Padding(3);
            this.tabTest1.Size = new System.Drawing.Size(1013, 478);
            this.tabTest1.TabIndex = 1;
            this.tabTest1.Text = "Тест 1";
            this.tabTest1.UseVisualStyleBackColor = true;
            // 
            // tabTest2
            // 
            this.tabTest2.Controls.Add(this.label12);
            this.tabTest2.Controls.Add(this.dgv_test2);
            this.tabTest2.Controls.Add(this.label8);
            this.tabTest2.Controls.Add(this.but_to_menu2);
            this.tabTest2.Location = new System.Drawing.Point(4, 25);
            this.tabTest2.Name = "tabTest2";
            this.tabTest2.Padding = new System.Windows.Forms.Padding(3);
            this.tabTest2.Size = new System.Drawing.Size(1013, 478);
            this.tabTest2.TabIndex = 2;
            this.tabTest2.Text = "Тест 2";
            this.tabTest2.UseVisualStyleBackColor = true;
            // 
            // tabTest3
            // 
            this.tabTest3.Controls.Add(this.chart3);
            this.tabTest3.Controls.Add(this.label13);
            this.tabTest3.Controls.Add(this.dgv_test3);
            this.tabTest3.Controls.Add(this.label9);
            this.tabTest3.Controls.Add(this.but_to_menu3);
            this.tabTest3.Location = new System.Drawing.Point(4, 25);
            this.tabTest3.Name = "tabTest3";
            this.tabTest3.Padding = new System.Windows.Forms.Padding(3);
            this.tabTest3.Size = new System.Drawing.Size(1013, 478);
            this.tabTest3.TabIndex = 3;
            this.tabTest3.Text = "Тест 3";
            this.tabTest3.UseVisualStyleBackColor = true;
            // 
            // tabTest4
            // 
            this.tabTest4.Controls.Add(this.label14);
            this.tabTest4.Controls.Add(this.dgv_test4);
            this.tabTest4.Controls.Add(this.label10);
            this.tabTest4.Controls.Add(this.but_to_menu4);
            this.tabTest4.Location = new System.Drawing.Point(4, 25);
            this.tabTest4.Name = "tabTest4";
            this.tabTest4.Padding = new System.Windows.Forms.Padding(3);
            this.tabTest4.Size = new System.Drawing.Size(1013, 478);
            this.tabTest4.TabIndex = 4;
            this.tabTest4.Text = "Тест 4";
            this.tabTest4.UseVisualStyleBackColor = true;
            // 
            // but_test2
            // 
            this.but_test2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_test2.Location = new System.Drawing.Point(183, 95);
            this.but_test2.Name = "but_test2";
            this.but_test2.Size = new System.Drawing.Size(570, 45);
            this.but_test2.TabIndex = 1;
            this.but_test2.Text = "Тест 2";
            this.but_test2.UseVisualStyleBackColor = true;
            // 
            // but_test3
            // 
            this.but_test3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_test3.Location = new System.Drawing.Point(183, 146);
            this.but_test3.Name = "but_test3";
            this.but_test3.Size = new System.Drawing.Size(570, 45);
            this.but_test3.TabIndex = 2;
            this.but_test3.Text = "Тест 3";
            this.but_test3.UseVisualStyleBackColor = true;
            // 
            // but_test4
            // 
            this.but_test4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_test4.Location = new System.Drawing.Point(183, 197);
            this.but_test4.Name = "but_test4";
            this.but_test4.Size = new System.Drawing.Size(570, 45);
            this.but_test4.TabIndex = 3;
            this.but_test4.Text = "Тест 4";
            this.but_test4.UseVisualStyleBackColor = true;
            // 
            // but_to_menu
            // 
            this.but_to_menu.Location = new System.Drawing.Point(871, 442);
            this.but_to_menu.Name = "but_to_menu";
            this.but_to_menu.Size = new System.Drawing.Size(136, 30);
            this.but_to_menu.TabIndex = 0;
            this.but_to_menu.Text = "В главное меню";
            this.but_to_menu.UseVisualStyleBackColor = true;
            // 
            // but_to_menu2
            // 
            this.but_to_menu2.Location = new System.Drawing.Point(871, 442);
            this.but_to_menu2.Name = "but_to_menu2";
            this.but_to_menu2.Size = new System.Drawing.Size(136, 30);
            this.but_to_menu2.TabIndex = 1;
            this.but_to_menu2.Text = "В главное меню";
            this.but_to_menu2.UseVisualStyleBackColor = true;
            // 
            // but_to_menu3
            // 
            this.but_to_menu3.Location = new System.Drawing.Point(871, 442);
            this.but_to_menu3.Name = "but_to_menu3";
            this.but_to_menu3.Size = new System.Drawing.Size(136, 30);
            this.but_to_menu3.TabIndex = 1;
            this.but_to_menu3.Text = "В главное меню";
            this.but_to_menu3.UseVisualStyleBackColor = true;
            // 
            // but_to_menu4
            // 
            this.but_to_menu4.Location = new System.Drawing.Point(871, 442);
            this.but_to_menu4.Name = "but_to_menu4";
            this.but_to_menu4.Size = new System.Drawing.Size(136, 30);
            this.but_to_menu4.TabIndex = 1;
            this.but_to_menu4.Text = "В главное меню";
            this.but_to_menu4.UseVisualStyleBackColor = true;
            // 
            // tabUsers
            // 
            this.tabUsers.Controls.Add(this.cb_access);
            this.tabUsers.Controls.Add(this.tb_password);
            this.tabUsers.Controls.Add(this.tb_login);
            this.tabUsers.Controls.Add(this.tb_name);
            this.tabUsers.Controls.Add(this.label5);
            this.tabUsers.Controls.Add(this.label4);
            this.tabUsers.Controls.Add(this.label3);
            this.tabUsers.Controls.Add(this.label2);
            this.tabUsers.Controls.Add(this.label1);
            this.tabUsers.Controls.Add(this.but_add_user);
            this.tabUsers.Controls.Add(this.but_save_users);
            this.tabUsers.Controls.Add(this.dgv_Users);
            this.tabUsers.Controls.Add(this.but_to_menu5);
            this.tabUsers.Location = new System.Drawing.Point(4, 25);
            this.tabUsers.Name = "tabUsers";
            this.tabUsers.Padding = new System.Windows.Forms.Padding(3);
            this.tabUsers.Size = new System.Drawing.Size(1013, 478);
            this.tabUsers.TabIndex = 5;
            this.tabUsers.Text = "Пользователи";
            this.tabUsers.UseVisualStyleBackColor = true;
            // 
            // but_users
            // 
            this.but_users.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_users.Location = new System.Drawing.Point(183, 286);
            this.but_users.Name = "but_users";
            this.but_users.Size = new System.Drawing.Size(570, 45);
            this.but_users.TabIndex = 4;
            this.but_users.Text = "Список пользователей";
            this.but_users.UseVisualStyleBackColor = true;
            // 
            // but_to_menu5
            // 
            this.but_to_menu5.Location = new System.Drawing.Point(828, 442);
            this.but_to_menu5.Name = "but_to_menu5";
            this.but_to_menu5.Size = new System.Drawing.Size(150, 30);
            this.but_to_menu5.TabIndex = 2;
            this.but_to_menu5.Text = "В главное меню";
            this.but_to_menu5.UseVisualStyleBackColor = true;
            // 
            // dgv_Users
            // 
            this.dgv_Users.AutoGenerateColumns = false;
            this.dgv_Users.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Users.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Users.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iduserDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.loginDataGridViewTextBoxColumn,
            this.passwordDataGridViewTextBoxColumn,
            this.accessDataGridViewTextBoxColumn});
            this.dgv_Users.DataSource = this.usersBindingSource;
            this.dgv_Users.Location = new System.Drawing.Point(6, 6);
            this.dgv_Users.Name = "dgv_Users";
            this.dgv_Users.RowHeadersWidth = 51;
            this.dgv_Users.RowTemplate.Height = 24;
            this.dgv_Users.Size = new System.Drawing.Size(705, 406);
            this.dgv_Users.TabIndex = 3;
            this.dgv_Users.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridView1_UserDeletingRow);
            // 
            // testingDataSet_Users
            // 
            this.testingDataSet_Users.DataSetName = "testingDataSet_Users";
            this.testingDataSet_Users.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // usersTableAdapter
            // 
            this.usersTableAdapter.ClearBeforeFill = true;
            // 
            // but_save_users
            // 
            this.but_save_users.Location = new System.Drawing.Point(575, 442);
            this.but_save_users.Name = "but_save_users";
            this.but_save_users.Size = new System.Drawing.Size(136, 30);
            this.but_save_users.TabIndex = 4;
            this.but_save_users.Text = "Сохранить";
            this.but_save_users.UseVisualStyleBackColor = true;
            // 
            // usersBindingSource
            // 
            this.usersBindingSource.DataMember = "Users";
            this.usersBindingSource.DataSource = this.testingDataSet_Users;
            // 
            // but_add_user
            // 
            this.but_add_user.Location = new System.Drawing.Point(828, 293);
            this.but_add_user.Name = "but_add_user";
            this.but_add_user.Size = new System.Drawing.Size(150, 30);
            this.but_add_user.TabIndex = 5;
            this.but_add_user.Text = "Добавить запись";
            this.but_add_user.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(777, 114);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(171, 29);
            this.label1.TabIndex = 6;
            this.label1.Text = "Новая запись";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(746, 163);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "ФИО:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(746, 191);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Логин:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(746, 219);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Пароль:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(746, 247);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Доступ:";
            // 
            // tb_name
            // 
            this.tb_name.Location = new System.Drawing.Point(828, 160);
            this.tb_name.Name = "tb_name";
            this.tb_name.Size = new System.Drawing.Size(150, 22);
            this.tb_name.TabIndex = 11;
            // 
            // tb_login
            // 
            this.tb_login.Location = new System.Drawing.Point(828, 188);
            this.tb_login.Name = "tb_login";
            this.tb_login.Size = new System.Drawing.Size(150, 22);
            this.tb_login.TabIndex = 12;
            // 
            // tb_password
            // 
            this.tb_password.Location = new System.Drawing.Point(828, 216);
            this.tb_password.Name = "tb_password";
            this.tb_password.Size = new System.Drawing.Size(150, 22);
            this.tb_password.TabIndex = 13;
            // 
            // cb_access
            // 
            this.cb_access.FormattingEnabled = true;
            this.cb_access.Items.AddRange(new object[] {
            "0 - Администратор",
            "1 - Тестирующий",
            "2 - Тестируемый"});
            this.cb_access.Location = new System.Drawing.Point(828, 244);
            this.cb_access.Name = "cb_access";
            this.cb_access.Size = new System.Drawing.Size(150, 24);
            this.cb_access.TabIndex = 14;
            // 
            // testingDataSet
            // 
            this.testingDataSet.DataSetName = "testingDataSet";
            this.testingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // testingDataSetBindingSource
            // 
            this.testingDataSetBindingSource.DataSource = this.testingDataSet;
            this.testingDataSetBindingSource.Position = 0;
            // 
            // iduserDataGridViewTextBoxColumn
            // 
            this.iduserDataGridViewTextBoxColumn.DataPropertyName = "id_user";
            this.iduserDataGridViewTextBoxColumn.HeaderText = "id_user";
            this.iduserDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.iduserDataGridViewTextBoxColumn.Name = "iduserDataGridViewTextBoxColumn";
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            // 
            // loginDataGridViewTextBoxColumn
            // 
            this.loginDataGridViewTextBoxColumn.DataPropertyName = "login";
            this.loginDataGridViewTextBoxColumn.HeaderText = "login";
            this.loginDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.loginDataGridViewTextBoxColumn.Name = "loginDataGridViewTextBoxColumn";
            // 
            // passwordDataGridViewTextBoxColumn
            // 
            this.passwordDataGridViewTextBoxColumn.DataPropertyName = "password";
            this.passwordDataGridViewTextBoxColumn.HeaderText = "password";
            this.passwordDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.passwordDataGridViewTextBoxColumn.Name = "passwordDataGridViewTextBoxColumn";
            // 
            // accessDataGridViewTextBoxColumn
            // 
            this.accessDataGridViewTextBoxColumn.DataPropertyName = "Access";
            this.accessDataGridViewTextBoxColumn.HeaderText = "Access";
            this.accessDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.accessDataGridViewTextBoxColumn.Name = "accessDataGridViewTextBoxColumn";
            // 
            // but_click_res
            // 
            this.but_click_res.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_click_res.Location = new System.Drawing.Point(183, 337);
            this.but_click_res.Name = "but_click_res";
            this.but_click_res.Size = new System.Drawing.Size(570, 45);
            this.but_click_res.TabIndex = 5;
            this.but_click_res.Text = "Результаты тестируемого";
            this.but_click_res.UseVisualStyleBackColor = true;
            // 
            // tabResult
            // 
            this.tabResult.Controls.Add(this.label_res);
            this.tabResult.Controls.Add(this.but_get_res);
            this.tabResult.Controls.Add(this.cb_tested);
            this.tabResult.Controls.Add(this.label6);
            this.tabResult.Controls.Add(this.but_to_menu6);
            this.tabResult.Location = new System.Drawing.Point(4, 25);
            this.tabResult.Name = "tabResult";
            this.tabResult.Padding = new System.Windows.Forms.Padding(3);
            this.tabResult.Size = new System.Drawing.Size(1013, 478);
            this.tabResult.TabIndex = 6;
            this.tabResult.Text = "Результаты тестируемого";
            this.tabResult.UseVisualStyleBackColor = true;
            // 
            // but_to_menu6
            // 
            this.but_to_menu6.Location = new System.Drawing.Point(857, 442);
            this.but_to_menu6.Name = "but_to_menu6";
            this.but_to_menu6.Size = new System.Drawing.Size(150, 30);
            this.but_to_menu6.TabIndex = 3;
            this.but_to_menu6.Text = "В главное меню";
            this.but_to_menu6.UseVisualStyleBackColor = true;
            // 
            // but_exit
            // 
            this.but_exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.but_exit.Location = new System.Drawing.Point(183, 427);
            this.but_exit.Name = "but_exit";
            this.but_exit.Size = new System.Drawing.Size(570, 45);
            this.but_exit.TabIndex = 6;
            this.but_exit.Text = "Выход";
            this.but_exit.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(395, 17);
            this.label6.TabIndex = 4;
            this.label6.Text = "Выберите ФИО тестируемого для просмотра результатов";
            // 
            // cb_tested
            // 
            this.cb_tested.FormattingEnabled = true;
            this.cb_tested.Location = new System.Drawing.Point(9, 64);
            this.cb_tested.Name = "cb_tested";
            this.cb_tested.Size = new System.Drawing.Size(392, 24);
            this.cb_tested.TabIndex = 5;
            // 
            // but_get_res
            // 
            this.but_get_res.Location = new System.Drawing.Point(9, 105);
            this.but_get_res.Name = "but_get_res";
            this.but_get_res.Size = new System.Drawing.Size(392, 30);
            this.but_get_res.TabIndex = 6;
            this.but_get_res.Text = "Посмотреть результаты";
            this.but_get_res.UseVisualStyleBackColor = true;
            // 
            // label_res
            // 
            this.label_res.AutoSize = true;
            this.label_res.Location = new System.Drawing.Point(6, 157);
            this.label_res.Name = "label_res";
            this.label_res.Size = new System.Drawing.Size(86, 17);
            this.label_res.TabIndex = 7;
            this.label_res.Text = "Результаты";
            // 
            // dgv_test1
            // 
            this.dgv_test1.AutoGenerateColumns = false;
            this.dgv_test1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_test1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_test1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idtestedDataGridViewTextBoxColumn,
            this.res1DataGridViewTextBoxColumn,
            this.res2DataGridViewTextBoxColumn,
            this.res3DataGridViewTextBoxColumn,
            this.res4DataGridViewTextBoxColumn});
            this.dgv_test1.DataSource = this.resultstest1BindingSource;
            this.dgv_test1.Location = new System.Drawing.Point(6, 41);
            this.dgv_test1.Name = "dgv_test1";
            this.dgv_test1.RowHeadersWidth = 51;
            this.dgv_test1.RowTemplate.Height = 24;
            this.dgv_test1.Size = new System.Drawing.Size(597, 277);
            this.dgv_test1.TabIndex = 1;
            // 
            // testingDataSetBindingSource1
            // 
            this.testingDataSetBindingSource1.DataSource = this.testingDataSet;
            this.testingDataSetBindingSource1.Position = 0;
            // 
            // testingDataSetRes1
            // 
            this.testingDataSetRes1.DataSetName = "testingDataSetRes1";
            this.testingDataSetRes1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // resultstest1BindingSource
            // 
            this.resultstest1BindingSource.DataMember = "Results_test_1";
            this.resultstest1BindingSource.DataSource = this.testingDataSetRes1;
            // 
            // results_test_1TableAdapter
            // 
            this.results_test_1TableAdapter.ClearBeforeFill = true;
            // 
            // idtestedDataGridViewTextBoxColumn
            // 
            this.idtestedDataGridViewTextBoxColumn.DataPropertyName = "id_tested";
            this.idtestedDataGridViewTextBoxColumn.HeaderText = "Тестируемый";
            this.idtestedDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.idtestedDataGridViewTextBoxColumn.Name = "idtestedDataGridViewTextBoxColumn";
            // 
            // res1DataGridViewTextBoxColumn
            // 
            this.res1DataGridViewTextBoxColumn.DataPropertyName = "res_1";
            this.res1DataGridViewTextBoxColumn.HeaderText = "1 - 3";
            this.res1DataGridViewTextBoxColumn.MinimumWidth = 6;
            this.res1DataGridViewTextBoxColumn.Name = "res1DataGridViewTextBoxColumn";
            // 
            // res2DataGridViewTextBoxColumn
            // 
            this.res2DataGridViewTextBoxColumn.DataPropertyName = "res_2";
            this.res2DataGridViewTextBoxColumn.HeaderText = "4 - 6";
            this.res2DataGridViewTextBoxColumn.MinimumWidth = 6;
            this.res2DataGridViewTextBoxColumn.Name = "res2DataGridViewTextBoxColumn";
            // 
            // res3DataGridViewTextBoxColumn
            // 
            this.res3DataGridViewTextBoxColumn.DataPropertyName = "res_3";
            this.res3DataGridViewTextBoxColumn.HeaderText = "7 - 9";
            this.res3DataGridViewTextBoxColumn.MinimumWidth = 6;
            this.res3DataGridViewTextBoxColumn.Name = "res3DataGridViewTextBoxColumn";
            // 
            // res4DataGridViewTextBoxColumn
            // 
            this.res4DataGridViewTextBoxColumn.DataPropertyName = "res_4";
            this.res4DataGridViewTextBoxColumn.HeaderText = "10 - 12";
            this.res4DataGridViewTextBoxColumn.MinimumWidth = 6;
            this.res4DataGridViewTextBoxColumn.Name = "res4DataGridViewTextBoxColumn";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(441, 17);
            this.label7.TabIndex = 2;
            this.label7.Text = "Экспресс-диагностика барьеров в управленческой деятельности";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(578, 17);
            this.label8.TabIndex = 3;
            this.label8.Text = "Экспресс-диагностика профессионально-деятельностных ограничений руководителя";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(517, 17);
            this.label9.TabIndex = 4;
            this.label9.Text = "Диагностика уровня консервативности стиля организационного управления";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(490, 17);
            this.label10.TabIndex = 5;
            this.label10.Text = "Диагностика комплекса угрожаемого авторитета (КУА) у руководителей";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 336);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(580, 136);
            this.label11.TabIndex = 3;
            this.label11.Text = resources.GetString("label11.Text");
            // 
            // chart1
            // 
            chartArea3.AxisX.Minimum = 0D;
            chartArea3.AxisX.Title = "Тестируемые";
            chartArea3.AxisY.Maximum = 60D;
            chartArea3.AxisY.Title = "Сумма баллов";
            chartArea3.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea3);
            this.chart1.Location = new System.Drawing.Point(629, 41);
            this.chart1.Name = "chart1";
            series6.ChartArea = "ChartArea1";
            series6.Name = "Series1";
            series7.BorderWidth = 2;
            series7.ChartArea = "ChartArea1";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series7.Color = System.Drawing.Color.Red;
            series7.Name = "Series2";
            this.chart1.Series.Add(series6);
            this.chart1.Series.Add(series7);
            this.chart1.Size = new System.Drawing.Size(378, 300);
            this.chart1.TabIndex = 4;
            this.chart1.Text = "chart1";
            // 
            // dgv_test2
            // 
            this.dgv_test2.AutoGenerateColumns = false;
            this.dgv_test2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_test2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_test2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idtestedDataGridViewTextBoxColumn1,
            this.res1DataGridViewTextBoxColumn1,
            this.res2DataGridViewTextBoxColumn1,
            this.res3DataGridViewTextBoxColumn1,
            this.res4DataGridViewTextBoxColumn1,
            this.res5DataGridViewTextBoxColumn,
            this.res6DataGridViewTextBoxColumn});
            this.dgv_test2.DataSource = this.resultstest2BindingSource;
            this.dgv_test2.Location = new System.Drawing.Point(9, 37);
            this.dgv_test2.Name = "dgv_test2";
            this.dgv_test2.RowHeadersWidth = 51;
            this.dgv_test2.RowTemplate.Height = 24;
            this.dgv_test2.Size = new System.Drawing.Size(822, 275);
            this.dgv_test2.TabIndex = 4;
            // 
            // testingDataTest2
            // 
            this.testingDataTest2.DataSetName = "testingDataTest2";
            this.testingDataTest2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // resultstest2BindingSource
            // 
            this.resultstest2BindingSource.DataMember = "Results_test_2";
            this.resultstest2BindingSource.DataSource = this.testingDataTest2;
            // 
            // results_test_2TableAdapter
            // 
            this.results_test_2TableAdapter.ClearBeforeFill = true;
            // 
            // idtestedDataGridViewTextBoxColumn1
            // 
            this.idtestedDataGridViewTextBoxColumn1.DataPropertyName = "id_tested";
            this.idtestedDataGridViewTextBoxColumn1.HeaderText = "Тестируемый";
            this.idtestedDataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.idtestedDataGridViewTextBoxColumn1.Name = "idtestedDataGridViewTextBoxColumn1";
            // 
            // res1DataGridViewTextBoxColumn1
            // 
            this.res1DataGridViewTextBoxColumn1.DataPropertyName = "res_1";
            this.res1DataGridViewTextBoxColumn1.HeaderText = "1";
            this.res1DataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.res1DataGridViewTextBoxColumn1.Name = "res1DataGridViewTextBoxColumn1";
            // 
            // res2DataGridViewTextBoxColumn1
            // 
            this.res2DataGridViewTextBoxColumn1.DataPropertyName = "res_2";
            this.res2DataGridViewTextBoxColumn1.HeaderText = "2";
            this.res2DataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.res2DataGridViewTextBoxColumn1.Name = "res2DataGridViewTextBoxColumn1";
            // 
            // res3DataGridViewTextBoxColumn1
            // 
            this.res3DataGridViewTextBoxColumn1.DataPropertyName = "res_3";
            this.res3DataGridViewTextBoxColumn1.HeaderText = "3";
            this.res3DataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.res3DataGridViewTextBoxColumn1.Name = "res3DataGridViewTextBoxColumn1";
            // 
            // res4DataGridViewTextBoxColumn1
            // 
            this.res4DataGridViewTextBoxColumn1.DataPropertyName = "res_4";
            this.res4DataGridViewTextBoxColumn1.HeaderText = "4";
            this.res4DataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.res4DataGridViewTextBoxColumn1.Name = "res4DataGridViewTextBoxColumn1";
            // 
            // res5DataGridViewTextBoxColumn
            // 
            this.res5DataGridViewTextBoxColumn.DataPropertyName = "res_5";
            this.res5DataGridViewTextBoxColumn.HeaderText = "5";
            this.res5DataGridViewTextBoxColumn.MinimumWidth = 6;
            this.res5DataGridViewTextBoxColumn.Name = "res5DataGridViewTextBoxColumn";
            // 
            // res6DataGridViewTextBoxColumn
            // 
            this.res6DataGridViewTextBoxColumn.DataPropertyName = "res_6";
            this.res6DataGridViewTextBoxColumn.HeaderText = "6";
            this.res6DataGridViewTextBoxColumn.MinimumWidth = 6;
            this.res6DataGridViewTextBoxColumn.Name = "res6DataGridViewTextBoxColumn";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 327);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(561, 136);
            this.label12.TabIndex = 5;
            this.label12.Text = resources.GetString("label12.Text");
            // 
            // dgv_test3
            // 
            this.dgv_test3.AutoGenerateColumns = false;
            this.dgv_test3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_test3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_test3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idtestedDataGridViewTextBoxColumn2,
            this.resultDataGridViewTextBoxColumn});
            this.dgv_test3.DataSource = this.resultstest3BindingSource;
            this.dgv_test3.Location = new System.Drawing.Point(9, 34);
            this.dgv_test3.Name = "dgv_test3";
            this.dgv_test3.RowHeadersWidth = 51;
            this.dgv_test3.RowTemplate.Height = 24;
            this.dgv_test3.Size = new System.Drawing.Size(394, 299);
            this.dgv_test3.TabIndex = 5;
            // 
            // testingDataSet1
            // 
            this.testingDataSet1.DataSetName = "testingDataSet1";
            this.testingDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // resultstest3BindingSource
            // 
            this.resultstest3BindingSource.DataMember = "Results_test_3";
            this.resultstest3BindingSource.DataSource = this.testingDataSet1;
            // 
            // results_test_3TableAdapter
            // 
            this.results_test_3TableAdapter.ClearBeforeFill = true;
            // 
            // idtestedDataGridViewTextBoxColumn2
            // 
            this.idtestedDataGridViewTextBoxColumn2.DataPropertyName = "id_tested";
            this.idtestedDataGridViewTextBoxColumn2.HeaderText = "Тестируемый";
            this.idtestedDataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.idtestedDataGridViewTextBoxColumn2.Name = "idtestedDataGridViewTextBoxColumn2";
            // 
            // resultDataGridViewTextBoxColumn
            // 
            this.resultDataGridViewTextBoxColumn.DataPropertyName = "result";
            this.resultDataGridViewTextBoxColumn.HeaderText = "Сумма баллов";
            this.resultDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.resultDataGridViewTextBoxColumn.Name = "resultDataGridViewTextBoxColumn";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 354);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(723, 102);
            this.label13.TabIndex = 6;
            this.label13.Text = resources.GetString("label13.Text");
            // 
            // chart3
            // 
            chartArea4.AxisX.Minimum = 0D;
            chartArea4.AxisX.Title = "Тестируемые";
            chartArea4.AxisY.Maximum = 60D;
            chartArea4.AxisY.Title = "Сумма баллов";
            chartArea4.Name = "ChartArea1";
            this.chart3.ChartAreas.Add(chartArea4);
            this.chart3.Location = new System.Drawing.Point(467, 34);
            this.chart3.Name = "chart3";
            series8.ChartArea = "ChartArea1";
            series8.Name = "Series1";
            series9.BorderWidth = 2;
            series9.ChartArea = "ChartArea1";
            series9.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series9.Color = System.Drawing.Color.Orange;
            series9.Name = "Series2";
            series10.BorderWidth = 2;
            series10.ChartArea = "ChartArea1";
            series10.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series10.Color = System.Drawing.Color.Red;
            series10.Name = "Series3";
            this.chart3.Series.Add(series8);
            this.chart3.Series.Add(series9);
            this.chart3.Series.Add(series10);
            this.chart3.Size = new System.Drawing.Size(540, 300);
            this.chart3.TabIndex = 7;
            this.chart3.Text = "chart2";
            // 
            // dgv_test4
            // 
            this.dgv_test4.AutoGenerateColumns = false;
            this.dgv_test4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_test4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_test4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idtestedDataGridViewTextBoxColumn3,
            this.res1DataGridViewTextBoxColumn2,
            this.res2DataGridViewTextBoxColumn2,
            this.res3DataGridViewTextBoxColumn2,
            this.res4DataGridViewTextBoxColumn2});
            this.dgv_test4.DataSource = this.resultstest4BindingSource;
            this.dgv_test4.Location = new System.Drawing.Point(9, 35);
            this.dgv_test4.Name = "dgv_test4";
            this.dgv_test4.RowHeadersWidth = 51;
            this.dgv_test4.RowTemplate.Height = 24;
            this.dgv_test4.Size = new System.Drawing.Size(725, 290);
            this.dgv_test4.TabIndex = 6;
            // 
            // testingDataSet2
            // 
            this.testingDataSet2.DataSetName = "testingDataSet2";
            this.testingDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // resultstest4BindingSource
            // 
            this.resultstest4BindingSource.DataMember = "Results_test_4";
            this.resultstest4BindingSource.DataSource = this.testingDataSet2;
            // 
            // results_test_4TableAdapter
            // 
            this.results_test_4TableAdapter.ClearBeforeFill = true;
            // 
            // idtestedDataGridViewTextBoxColumn3
            // 
            this.idtestedDataGridViewTextBoxColumn3.DataPropertyName = "id_tested";
            this.idtestedDataGridViewTextBoxColumn3.HeaderText = "Тестируемый";
            this.idtestedDataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.idtestedDataGridViewTextBoxColumn3.Name = "idtestedDataGridViewTextBoxColumn3";
            // 
            // res1DataGridViewTextBoxColumn2
            // 
            this.res1DataGridViewTextBoxColumn2.DataPropertyName = "res_1";
            this.res1DataGridViewTextBoxColumn2.HeaderText = "А";
            this.res1DataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.res1DataGridViewTextBoxColumn2.Name = "res1DataGridViewTextBoxColumn2";
            // 
            // res2DataGridViewTextBoxColumn2
            // 
            this.res2DataGridViewTextBoxColumn2.DataPropertyName = "res_2";
            this.res2DataGridViewTextBoxColumn2.HeaderText = "Б";
            this.res2DataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.res2DataGridViewTextBoxColumn2.Name = "res2DataGridViewTextBoxColumn2";
            // 
            // res3DataGridViewTextBoxColumn2
            // 
            this.res3DataGridViewTextBoxColumn2.DataPropertyName = "res_3";
            this.res3DataGridViewTextBoxColumn2.HeaderText = "В";
            this.res3DataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.res3DataGridViewTextBoxColumn2.Name = "res3DataGridViewTextBoxColumn2";
            // 
            // res4DataGridViewTextBoxColumn2
            // 
            this.res4DataGridViewTextBoxColumn2.DataPropertyName = "res_4";
            this.res4DataGridViewTextBoxColumn2.HeaderText = "Г";
            this.res4DataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.res4DataGridViewTextBoxColumn2.Name = "res4DataGridViewTextBoxColumn2";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 343);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(728, 119);
            this.label14.TabIndex = 7;
            this.label14.Text = resources.GetString("label14.Text");
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1045, 537);
            this.Controls.Add(this.tabControl1);
            this.Name = "AdminForm";
            this.Text = "Администратор";
            this.Load += new System.EventHandler(this.AdminForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabTest1.ResumeLayout(false);
            this.tabTest1.PerformLayout();
            this.tabTest2.ResumeLayout(false);
            this.tabTest2.PerformLayout();
            this.tabTest3.ResumeLayout(false);
            this.tabTest3.PerformLayout();
            this.tabTest4.ResumeLayout(false);
            this.tabTest4.PerformLayout();
            this.tabUsers.ResumeLayout(false);
            this.tabUsers.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Users)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet_Users)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSetBindingSource)).EndInit();
            this.tabResult.ResumeLayout(false);
            this.tabResult.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_test1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSetBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSetRes1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultstest1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_test2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataTest2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultstest2BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_test3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultstest3BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_test4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testingDataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultstest4BindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button but_test1;
        private System.Windows.Forms.TabPage tabTest1;
        private System.Windows.Forms.TabPage tabTest2;
        private System.Windows.Forms.TabPage tabTest3;
        private System.Windows.Forms.TabPage tabTest4;
        private System.Windows.Forms.Button but_test4;
        private System.Windows.Forms.Button but_test3;
        private System.Windows.Forms.Button but_test2;
        private System.Windows.Forms.Button but_to_menu;
        private System.Windows.Forms.Button but_to_menu2;
        private System.Windows.Forms.Button but_to_menu3;
        private System.Windows.Forms.Button but_to_menu4;
        private System.Windows.Forms.TabPage tabUsers;
        private System.Windows.Forms.Button but_users;
        private System.Windows.Forms.Button but_to_menu5;
        private System.Windows.Forms.DataGridView dgv_Users;
        private testingDataSet_Users testingDataSet_Users;
        private testingDataSet_UsersTableAdapters.UsersTableAdapter usersTableAdapter;
        private System.Windows.Forms.Button but_save_users;
        private System.Windows.Forms.BindingSource usersBindingSource;
        private System.Windows.Forms.Button but_add_user;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_password;
        private System.Windows.Forms.TextBox tb_login;
        private System.Windows.Forms.TextBox tb_name;
        private System.Windows.Forms.ComboBox cb_access;
        private System.Windows.Forms.DataGridViewTextBoxColumn iduserDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn loginDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn passwordDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn accessDataGridViewTextBoxColumn;
        private testingDataSet testingDataSet;
        private System.Windows.Forms.BindingSource testingDataSetBindingSource;
        private System.Windows.Forms.Button but_click_res;
        private System.Windows.Forms.TabPage tabResult;
        private System.Windows.Forms.Button but_to_menu6;
        private System.Windows.Forms.Button but_exit;
        private System.Windows.Forms.ComboBox cb_tested;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button but_get_res;
        private System.Windows.Forms.Label label_res;
        private System.Windows.Forms.DataGridView dgv_test1;
        private System.Windows.Forms.BindingSource testingDataSetBindingSource1;
        private testingDataSetRes1 testingDataSetRes1;
        private System.Windows.Forms.BindingSource resultstest1BindingSource;
        private testingDataSetRes1TableAdapters.Results_test_1TableAdapter results_test_1TableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idtestedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn res1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn res2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn res3DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn res4DataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DataGridView dgv_test2;
        private testingDataTest2 testingDataTest2;
        private System.Windows.Forms.BindingSource resultstest2BindingSource;
        private testingDataTest2TableAdapters.Results_test_2TableAdapter results_test_2TableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idtestedDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn res1DataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn res2DataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn res3DataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn res4DataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn res5DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn res6DataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView dgv_test3;
        private testingDataSet1 testingDataSet1;
        private System.Windows.Forms.BindingSource resultstest3BindingSource;
        private testingDataSet1TableAdapters.Results_test_3TableAdapter results_test_3TableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idtestedDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn resultDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart3;
        private System.Windows.Forms.DataGridView dgv_test4;
        private testingDataSet2 testingDataSet2;
        private System.Windows.Forms.BindingSource resultstest4BindingSource;
        private testingDataSet2TableAdapters.Results_test_4TableAdapter results_test_4TableAdapter;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridViewTextBoxColumn idtestedDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn res1DataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn res2DataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn res3DataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn res4DataGridViewTextBoxColumn2;
    }
}