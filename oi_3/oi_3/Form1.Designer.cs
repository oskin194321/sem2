﻿namespace oi_3
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pic = new System.Windows.Forms.PictureBox();
            this.pic_res = new System.Windows.Forms.PictureBox();
            this.but_open = new System.Windows.Forms.Button();
            this.but_save = new System.Windows.Forms.Button();
            this.but_gray = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label_max = new System.Windows.Forms.Label();
            this.label_min = new System.Windows.Forms.Label();
            this.but_norm = new System.Windows.Forms.Button();
            this.but_eq = new System.Windows.Forms.Button();
            this.but_func = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_res)).BeginInit();
            this.SuspendLayout();
            // 
            // pic
            // 
            this.pic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic.Location = new System.Drawing.Point(13, 13);
            this.pic.Name = "pic";
            this.pic.Size = new System.Drawing.Size(500, 500);
            this.pic.TabIndex = 0;
            this.pic.TabStop = false;
            // 
            // pic_res
            // 
            this.pic_res.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_res.Location = new System.Drawing.Point(752, 12);
            this.pic_res.Name = "pic_res";
            this.pic_res.Size = new System.Drawing.Size(500, 500);
            this.pic_res.TabIndex = 1;
            this.pic_res.TabStop = false;
            // 
            // but_open
            // 
            this.but_open.Location = new System.Drawing.Point(13, 554);
            this.but_open.Name = "but_open";
            this.but_open.Size = new System.Drawing.Size(500, 30);
            this.but_open.TabIndex = 2;
            this.but_open.Text = "Загрузить изображение";
            this.but_open.UseVisualStyleBackColor = true;
            // 
            // but_save
            // 
            this.but_save.Location = new System.Drawing.Point(752, 554);
            this.but_save.Name = "but_save";
            this.but_save.Size = new System.Drawing.Size(500, 30);
            this.but_save.TabIndex = 3;
            this.but_save.Text = "Сохранить изображение";
            this.but_save.UseVisualStyleBackColor = true;
            // 
            // but_gray
            // 
            this.but_gray.Location = new System.Drawing.Point(529, 91);
            this.but_gray.Name = "but_gray";
            this.but_gray.Size = new System.Drawing.Size(206, 30);
            this.but_gray.TabIndex = 4;
            this.but_gray.Text = "Черно-белое";
            this.but_gray.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(601, 531);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "max:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(601, 554);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "min:";
            // 
            // label_max
            // 
            this.label_max.AutoSize = true;
            this.label_max.Location = new System.Drawing.Point(644, 531);
            this.label_max.Name = "label_max";
            this.label_max.Size = new System.Drawing.Size(16, 17);
            this.label_max.TabIndex = 7;
            this.label_max.Text = "?";
            // 
            // label_min
            // 
            this.label_min.AutoSize = true;
            this.label_min.Location = new System.Drawing.Point(644, 554);
            this.label_min.Name = "label_min";
            this.label_min.Size = new System.Drawing.Size(16, 17);
            this.label_min.TabIndex = 8;
            this.label_min.Text = "?";
            // 
            // but_norm
            // 
            this.but_norm.Enabled = false;
            this.but_norm.Location = new System.Drawing.Point(529, 127);
            this.but_norm.Name = "but_norm";
            this.but_norm.Size = new System.Drawing.Size(206, 30);
            this.but_norm.TabIndex = 9;
            this.but_norm.Text = "Нормализация";
            this.but_norm.UseVisualStyleBackColor = true;
            // 
            // but_eq
            // 
            this.but_eq.Enabled = false;
            this.but_eq.Location = new System.Drawing.Point(529, 163);
            this.but_eq.Name = "but_eq";
            this.but_eq.Size = new System.Drawing.Size(206, 30);
            this.but_eq.TabIndex = 10;
            this.but_eq.Text = "Эквализация";
            this.but_eq.UseVisualStyleBackColor = true;
            // 
            // but_func
            // 
            this.but_func.Enabled = false;
            this.but_func.Location = new System.Drawing.Point(529, 199);
            this.but_func.Name = "but_func";
            this.but_func.Size = new System.Drawing.Size(206, 60);
            this.but_func.TabIndex = 11;
            this.but_func.Text = "Преобразование по функции";
            this.but_func.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 635);
            this.Controls.Add(this.but_func);
            this.Controls.Add(this.but_eq);
            this.Controls.Add(this.but_norm);
            this.Controls.Add(this.label_min);
            this.Controls.Add(this.label_max);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.but_gray);
            this.Controls.Add(this.but_save);
            this.Controls.Add(this.but_open);
            this.Controls.Add(this.pic_res);
            this.Controls.Add(this.pic);
            this.Name = "Form1";
            this.Text = "3 Применение гистограммных методов коррекции";
            ((System.ComponentModel.ISupportInitialize)(this.pic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_res)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pic;
        private System.Windows.Forms.PictureBox pic_res;
        private System.Windows.Forms.Button but_open;
        private System.Windows.Forms.Button but_save;
        private System.Windows.Forms.Button but_gray;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_max;
        private System.Windows.Forms.Label label_min;
        private System.Windows.Forms.Button but_norm;
        private System.Windows.Forms.Button but_eq;
        private System.Windows.Forms.Button but_func;
    }
}

