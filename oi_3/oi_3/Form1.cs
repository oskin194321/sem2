﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oi_3
{
    public partial class Form1 : Form
    {
        Bitmap Bitmap_for_eq;

        public Form1()
        {
            InitializeComponent();

            but_save.Click += But_save_Click;
            but_open.Click += But_open_Click;

            but_gray.Click += But_gray_Click;
            but_norm.Click += But_norm_Click;
            but_eq.Click += But_eq_Click;
            but_func.Click += But_func_Click;
        }

        private void But_eq_Click(object sender, EventArgs e) // Эквализация гистограммы
        {
            Bitmap Bitmap_eq = new Bitmap(pic.Image);
            int x1; // значение каналов пикселя
            int height, width; // высота и ширина изображения
            
            double[] hist_gray = new double[256];// Массив для значений черно-белого изображения
            double[] hist_eq = new double[256]; // Массив для построения гистограммы
            double eq = 0.0; // Параметр эквализации

            height = Bitmap_eq.Height;
            width = Bitmap_eq.Width;

            for (int i = 0; i < width; i++) // Получение гистограммы черно-белого изображения
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_eq.GetPixel(i, j);
                    hist_gray[pixel1.R]++; // Увеличение количества пикселей с данным значением канала
                }
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_eq.GetPixel(i, j);
                    eq = 0.0;
                    for(int n = 0; n < pixel1.R; n++)
                    {
                        eq += hist_gray[n] / (width * height); // Вычисление параметра
                    }

                    x1 = Convert.ToInt32((hist_gray.Length - 1) * eq); // Расчет пикселя
                    Bitmap_eq.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                    hist_eq[x1]++; // Увеличение количества пикселей с данным значением канала
                }
            }
            pic_res.Image = Bitmap_eq;
            Bitmap_for_eq = Bitmap_eq; // для дальнейшего расчета по заданной функции
            // Сохранение данных для гистограммы
            string tofile = "";
            for (int i = 0; i < hist_eq.Length; i++)
            {
                hist_eq[i] = hist_eq[i] / (width * height);
                tofile += hist_eq[i] + Environment.NewLine;
            }
            System.IO.File.WriteAllText("eq.txt", tofile);
        }

        private void But_norm_Click(object sender, EventArgs e) // Нормализация гистограммы
        {
            Bitmap Bitmap_norm = new Bitmap(pic.Image);
            int x1; // значение каналов пикселя
            int height, width; // высота и ширина изображения
            double[] hist_norm = new double[256]; // Массив для построения гистограммы
            double a = 0.0, b = 0.0; // коэффициенты для расчета

            height = Bitmap_norm.Height;
            width = Bitmap_norm.Width;

            b = (255.0 - 0.0) / (Convert.ToDouble(label_max.Text) - Convert.ToDouble(label_min.Text));
            a = 0.0 - b * Convert.ToDouble(label_min.Text);

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_norm.GetPixel(i, j);
                    x1 = Convert.ToInt32(a + b * Convert.ToDouble(pixel1.R)); // Нормализация пикселя
                    Bitmap_norm.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                    hist_norm[x1]++; // Увеличение количества пикселей с данным значением канала
                }
            }
            pic_res.Image = Bitmap_norm;
            // Сохранение данных для гистограммы
            string tofile = "";
            for (int i = 0; i < hist_norm.Length; i++)
            {
                hist_norm[i] = hist_norm[i] / (width * height);
                tofile += hist_norm[i] + Environment.NewLine;
            }
            System.IO.File.WriteAllText("norm.txt", tofile);
        }

        private void But_func_Click(object sender, EventArgs e) // Преобразование гистограммы по заданной функции
        {
            Bitmap Bitmap_func = new Bitmap(pic.Image);
            int x1; // значение каналов пикселя
            int height, width; // высота и ширина изображения
            double[] hist_func = new double[256]; // Массив для построения гистограммы

            height = Bitmap_func.Height;
            width = Bitmap_func.Width;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_func.GetPixel(i, j);
                    Color pixel_eq = Bitmap_for_eq.GetPixel(i, j);
                    // Расчет значений канала пикселя
                    x1 = Convert.ToInt32(Math.Pow(pixel_eq.R * 255 * 255, 0.33)); // расчет по заданной функции
                    Bitmap_func.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                    hist_func[x1]++; // Увеличение количества пикселей с данным значением канала
                }
            }
            pic_res.Image = Bitmap_func;
            // Сохранение данных для гистограммы
            string tofile = "";
            for (int i = 0; i < hist_func.Length; i++)
            {
                hist_func[i] = hist_func[i] / (width * height);
                tofile += hist_func[i] + Environment.NewLine;
            }
            System.IO.File.WriteAllText("func.txt", tofile);
        }

        private void But_gray_Click(object sender, EventArgs e) // Преобразование в черно-белое изображение
        {
            Bitmap Bitmap_gray = new Bitmap(pic.Image);
            int max = 0, min = 255; // для дальнейшего расчета 
            int x1; // значение каналов пикселя
            int height, width; // высота и ширина изображения
            double[] hist_gray = new double[256]; // Массив для построения гистограммы

            height = Bitmap_gray.Height;
            width = Bitmap_gray.Width;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_gray.GetPixel(i, j);
                    // Расчет значений канала пикселя
                    x1 = Convert.ToInt32(0.299 * pixel1.R + 0.587 * pixel1.G + 0.114 * pixel1.B);
                    Bitmap_gray.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                    hist_gray[x1]++; // Увеличение количества пикселей с данным значением канала
                    if (x1 > max)
                    {
                        max = x1;
                    }
                    if (x1 < min)
                    {
                        min = x1;
                    }
                }
            }
            pic_res.Image = Bitmap_gray;
            pic.Image = Bitmap_gray;
            // Сохранение данных для гистограммы
            string tofile = "";
            for(int i = 0; i < hist_gray.Length; i++)
            {
                hist_gray[i] = hist_gray[i] / (width * height);
                tofile += hist_gray[i] + Environment.NewLine;
            }
            System.IO.File.WriteAllText("gray.txt", tofile);

            // Открытие кнопок
            but_norm.Enabled = true;
            but_eq.Enabled = true;
            but_func.Enabled = true;

            label_max.Text = Convert.ToString(max);
            label_min.Text = Convert.ToString(min);
        }

        private void But_open_Click(object sender, EventArgs e) // Открытие изображения
        {
            Bitmap image; //Bitmap для открываемого изображения

            OpenFileDialog open_dialog = new OpenFileDialog(); //создание диалогового окна для выбора файла
            open_dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*"; //формат файла
            if (open_dialog.ShowDialog() == DialogResult.OK) //если в окне была нажата кнопка "ОК"
            {
                try
                {
                    image = new Bitmap(open_dialog.FileName);
                    this.pic.Size = image.Size;
                    pic.Image = image;
                    pic.Invalidate();
                    but_gray.Enabled = true;
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void But_save_Click(object sender, EventArgs e) // Сохранение изображения
        {
            if (pic_res.Image != null) //если в pictureBox есть изображение
            {
                //создание диалогового окна "Сохранить как..", для сохранения изображения
                SaveFileDialog savedialog = new SaveFileDialog();
                savedialog.Title = "Сохранить картинку как...";
                //отображать ли предупреждение, если пользователь указывает имя уже существующего файла
                savedialog.OverwritePrompt = true;
                //отображать ли предупреждение, если пользователь указывает несуществующий путь
                savedialog.CheckPathExists = true;
                //список форматов файла, отображаемый в поле "Тип файла"
                savedialog.Filter = "Image Files(*.BMP)|*.BMP|Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";
                //отображается ли кнопка "Справка" в диалоговом окне
                savedialog.ShowHelp = true;
                if (savedialog.ShowDialog() == DialogResult.OK) //если в диалоговом окне нажата кнопка "ОК"
                {
                    try
                    {
                        pic_res.Image.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}
