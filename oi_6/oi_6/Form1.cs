﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oi_6
{
    public partial class Form1 : Form
    {
        Bitmap image; // Bitmap для загружаемого изображения

        int[,] array_calc; // Массив для вычислений

        public Form1()
        {
            InitializeComponent();
            // Загрузка изображения
            but_open.Click += But_open_Click;
            // Обработка изображений
            but_porog.Click += But_porog_Click;
            but_contour.Click += But_contour_Click;
            but_cluster.Click += But_cluster_Click;
            // Сохранение изображений
            save_porog.Click += Save_porog_Click;
            save_contour.Click += Save_contour_Click;
            save_cluster.Click += Save_cluster_Click;
        }

        private void But_porog_Click(object sender, EventArgs e) // Пороговая обработка
        {
            int parts = Convert.ToInt32(parts_porog.Text);
            Bitmap Bitmap_pic = new Bitmap(pic.Image); // Исходное изображение
            int height, width; // высота и ширина изображений
            height = image.Height;
            width = image.Width;            
            int[,] array_pic = new int[width, height];
            int[,] array_pic2 = new int[width, height];
            for (int i = 0; i < width; i++) // Заполнение массива значениями канала пикселя
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_pic.GetPixel(i, j);
                    array_pic[i, j] = pixel1.R;
                    array_pic2[i, j] = pixel1.R;
                }
            }
            int[,] array_calc = new int[width, height];
            if (type_porog.SelectedIndex == 0) // Простая обработка
            {
                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < height; j++)
                    {
                        if (array_pic[i, j] > num_porog.Value)
                            array_calc[i, j] = 255;
                        else
                            array_calc[i, j] = 0;
                    }
                }
            }
            else // Адаптивная обработка
            {
                int width_A = width; // Ширина для адаптивной обработки
                int height_A = height; // Высота для адаптивной обработки
                if (width_A % 2 == 1 || height_A % 2 == 1) // Приведение изображения к четному количеству пикселей по сторонам
                {
                    if (width_A % 2 == 1)
                        width_A++;
                    if (height_A % 2 == 1)
                        height_A++;
                    array_pic2 = new int[width_A, height_A];
                    for (int i = 0; i < width_A; i++)
                    {
                        for (int j = 0; j < height_A; j++)
                        {
                            if (j == height_A - 1)
                            {
                                if (i == width_A - 1)
                                    array_pic2[i, j] = array_pic[i - 2, j - 2];
                                else
                                    array_pic2[i, j] = array_pic[i, j - 2];
                            }
                            else
                            {
                                if (i == width_A - 1)
                                    array_pic2[i, j] = array_pic[i - 2, j];
                                else
                                    array_pic2[i, j] = array_pic[i, j];
                            }
                        }
                    }
                    array_calc = new int[width_A, height_A];
                }
                int i_start = 0;
                int j_start = 0;
                int i_step = width_A / Convert.ToInt32(Math.Round(Math.Sqrt(parts)));
                int j_step = height_A / Convert.ToInt32(Math.Round(Math.Sqrt(parts)));
                int i_end = i_step;
                int j_end = j_step;
                List<double> t;
                double prev_t;
                double g_1;
                double g_2;
                double count_g1;
                double count_g2;
                for (int n = 0; n < Math.Sqrt(parts); n++) // Вычисление адаптивной пороговой обработки
                {
                    j_start = 0;
                    j_end = j_step;
                    for (int l = 0; l < Math.Sqrt(parts); l++)
                    {
                        t = new List<double>();
                        t.Add(0);
                        t.Add((double)num_porog.Value);
                        prev_t = 0;
                        while (t.Last() - prev_t > Convert.ToInt32(delta_porog.Text)) // Вычисление порога
                        {
                            g_1 = 0;
                            g_2 = 0;
                            count_g1 = 0;
                            count_g2 = 0;
                            for (int i = i_start; i < i_end; i++)
                            {
                                for (int j = j_start; j < j_end; j++)
                                {
                                    if (array_pic2[i, j] < t.Last())
                                    {
                                        g_1 += array_pic2[i, j];
                                        count_g1++;
                                    }
                                    else
                                    {
                                        g_2 += array_pic2[i, j];
                                        count_g2++;
                                    }
                                }
                            }
                            g_1 = g_1 / count_g1;
                            g_2 = g_2 / count_g2;
                            prev_t = t.Last();
                            t.Add(0.5 * (g_1 + g_2));
                        }
                        for (int i = i_start; i < i_end; i++) // Заполнение части изображения
                        {
                            for (int j = j_start; j < j_end; j++)
                            {
                                if (array_pic2[i, j] > t.Last())
                                    array_calc[i, j] = 255;
                                else
                                    array_calc[i, j] = 0;
                            }
                        }
                        j_start = j_start + j_step;
                        j_end = j_end + j_step;
                    }
                    i_start = i_start + i_step;
                    i_end = i_end + i_step;
                }
            }
            Bitmap porog = new Bitmap(width, height);
            for (int i = 0; i < width; i++) // Формирование изображения
            {
                for (int j = 0; j < height; j++)
                {
                    int x1 = array_calc[i, j];
                    porog.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                }
            }
            pic_porog.Image = porog;
        }

        private void But_contour_Click(object sender, EventArgs e) // Выделение контуров
        {
            Bitmap Bitmap_pic = new Bitmap(pic.Image); // Исходное изображение
            int height, width; // высота и ширина изображений
            height = image.Height;
            width = image.Width;
            int[,] array_pic = new int[width, height];
            for (int i = 0; i < width; i++) // Заполнение массива значениями канала пикселя
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_pic.GetPixel(i, j);
                    array_pic[i, j] = pixel1.R;
                }
            }
            Bitmap contour;

            double square = 0; // процент занимаемой площади Sk
            double mean = 0; // Для вычисления Sk

            //int[,] array_calc; // Массив для вычислений
            switch (type_contour.SelectedIndex)
            {
                case 0: // Оператор Робертса
                    array_calc = new int[width - 1, height - 1];
                    if (dir_contour.SelectedIndex == 0) // Горизонтальные
                    {
                        for (int i = 0; i < width - 1; i++)
                        {
                            for (int j = 0; j < height - 1; j++)
                            {
                                array_calc[i, j] = -array_pic[i, j] + array_pic[i + 1, j + 1];
                                if (array_calc[i, j] < 0)
                                    array_calc[i, j] = 0;

                                if (array_calc[i, j] != 0) // Для вычисления площади
                                    mean += array_calc[i, j];
                            }
                        }
                    }
                    else // Вертикальные
                    {
                        for (int i = 0; i < width - 1; i++)
                        {
                            for (int j = 0; j < height - 1; j++)
                            {
                                array_calc[i, j] = -array_pic[i + 1, j] + array_pic[i, j + 1];
                                if (array_calc[i, j] < 0)
                                    array_calc[i, j] = 0;

                                if (array_calc[i, j] != 0) // Для вычисления площади
                                    mean += array_calc[i, j];
                            }
                        }
                    }
                    contour = new Bitmap(width - 1, height - 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                    for (int i = 0; i < width - 1; i++) // Формирование изображения
                    {
                        for (int j = 0; j < height - 1; j++)
                        {
                            int x1 = array_calc[i, j];
                            contour.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                        }
                    }
                    pic_contour.Image = contour;
                    break;
                case 1: // Оператор Превитта
                    array_calc = new int[width - 2, height - 2];
                    if (dir_contour.SelectedIndex == 0) // Горизонтальные
                    {
                        for (int i = 1; i < width - 2; i++)
                        {
                            for (int j = 1; j < height - 2; j++)
                            {
                                array_calc[i, j] = -array_pic[i - 1, j - 1] - array_pic[i, j - 1] - array_pic[i + 1, j - 1]
                                                + array_pic[i - 1, j + 1] + array_pic[i, j + 1] + array_pic[i + 1, j + 1];
                                if (array_calc[i, j] < 0)
                                    array_calc[i, j] = 0;
                                if (array_calc[i, j] > 255)
                                    array_calc[i, j] = 255;

                                if (array_calc[i, j] != 0) // Для вычисления площади
                                    mean += array_calc[i, j];
                            }
                        }
                    }
                    else // Вертикальные
                    {
                        for (int i = 1; i < width - 2; i++)
                        {
                            for (int j = 1; j < height - 2; j++)
                            {
                                array_calc[i, j] = -array_pic[i - 1, j - 1] - array_pic[i - 1, j] - array_pic[i - 1, j + 1]
                                                + array_pic[i + 1, j - 1] + array_pic[i + 1, j] + array_pic[i + 1, j + 1];
                                if (array_calc[i, j] < 0)
                                    array_calc[i, j] = 0;
                                if (array_calc[i, j] > 255)
                                    array_calc[i, j] = 255;

                                if (array_calc[i, j] != 0) // Для вычисления площади
                                    mean += array_calc[i, j];
                            }
                        }
                    }
                    contour = new Bitmap(width - 2, height - 2);
                    for (int i = 0; i < width - 2; i++) // Формирование изображения
                    {
                        for (int j = 0; j < height - 2; j++)
                        {
                            int x1 = array_calc[i, j];
                            contour.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                        }
                    }
                    pic_contour.Image = contour;
                    break;
                case 2: // Оператор Собела
                    array_calc = new int[width - 2, height - 2];
                    if (dir_contour.SelectedIndex == 0) // Горизонтальные
                    {
                        for (int i = 1; i < width - 2; i++)
                        {
                            for (int j = 1; j < height - 2; j++)
                            {
                                array_calc[i, j] = -array_pic[i - 1, j - 1] - 2 * array_pic[i, j - 1] - array_pic[i + 1, j - 1]
                                                + array_pic[i - 1, j + 1] + 2 * array_pic[i, j + 1] + array_pic[i + 1, j + 1];
                                if (array_calc[i, j] < 0)
                                    array_calc[i, j] = 0;
                                if (array_calc[i, j] > 255)
                                    array_calc[i, j] = 255;

                                if (array_calc[i, j] != 0) // Для вычисления площади
                                    mean += array_calc[i, j];
                            }
                        }
                    }
                    else // Вертикальные
                    {
                        for (int i = 1; i < width - 2; i++)
                        {
                            for (int j = 1; j < height - 2; j++)
                            {
                                array_calc[i, j] = -array_pic[i - 1, j - 1] - 2 * array_pic[i - 1, j] - array_pic[i - 1, j + 1]
                                                + array_pic[i + 1, j - 1] + 2 * array_pic[i + 1, j] + array_pic[i + 1, j + 1];
                                if (array_calc[i, j] < 0)
                                    array_calc[i, j] = 0;
                                if (array_calc[i, j] > 255)
                                    array_calc[i, j] = 255;

                                if (array_calc[i, j] != 0) // Для вычисления площади
                                    mean += array_calc[i, j];
                            }
                        }
                    }
                    contour = new Bitmap(width - 2, height - 2);
                    for (int i = 0; i < width - 2; i++) // Вывод изображения
                    {
                        for (int j = 0; j < height - 2; j++)
                        {
                            int x1 = array_calc[i, j];
                            contour.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                        }
                    }
                    pic_contour.Image = contour;
                    break;
                case 3: // Лапласиан Гауссиан
                    array_calc = new int[width - 4, height - 4];
                    for (int i = 2; i < width - 4; i++)
                    {
                        for (int j = 2; j < height - 4; j++)
                        {
                            array_calc[i, j] = -array_pic[i, j - 2] - array_pic[i - 1, j - 1] - 2 * array_pic[i, j - 1] 
                                - array_pic[i + 1, j - 1] - array_pic[i - 2, j] - 2 * array_pic[i - 1, j] 
                                + 16 * array_pic[i, j] - 2 * array_pic[i + 1, j] - array_pic[i + 2, j] - array_pic[i - 1, j + 1] 
                                - 2 * array_pic[i, j + 1] - array_pic[i + 1, j + 1] - array_pic[i, j + 2];
                            if (array_calc[i, j] < 0)
                                array_calc[i, j] = 0;
                            if (array_calc[i, j] > 255)
                                array_calc[i, j] = 255;

                            if (array_calc[i, j] != 0) // Для вычисления площади
                                mean += array_calc[i, j];
                        }
                    }
                    contour = new Bitmap(width - 4, height - 4);
                    for (int i = 0; i < width - 4; i++) // Вывод изображения
                    {
                        for (int j = 0; j < height - 4; j++)
                        {
                            int x1 = array_calc[i, j];
                            contour.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                        }
                    }
                    pic_contour.Image = contour;
                    break;
            }

            mean = mean / array_calc.Length;
            foreach (double x in array_calc) // Вычисление площади Sk
            {
                if (x >= mean) square++;
            }
            square = Math.Round((square / array_calc.Length) * 100, 1);
            label_sk.Text = "Площадь: " + square.ToString() + " %";
        }

        private void But_cluster_Click(object sender, EventArgs e) // Кластеризация
        {
            Bitmap Bitmap_pic = new Bitmap(pic.Image); // Исходное изображение
            int height, width; // высота и ширина изображений
            height = image.Height;
            width = image.Width;
            int[,] array_pic = new int[width, height];
            for (int i = 0; i < width; i++) // Заполнение массива значениями канала пикселя
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_pic.GetPixel(i, j);
                    array_pic[i, j] = pixel1.R;
                }
            }
            int delta = (int)num_cluster.Value;
            int x;
            int y;
            Bitmap cluster;
            switch (type_cluster.SelectedIndex)
            {
                case 0: // Метод выращивания областей
                    Random rand = new Random();
                    for (int r = 0; r < 50; r++)
                    {
                        x = rand.Next(0, width);
                        y = rand.Next(0, height);
                        for (int i = 0; i < width; i++)
                        {
                            for (int j = 0; j < height; j++)
                            {
                                if (Math.Abs(array_pic[i, j] - array_pic[x, y]) <= delta)
                                    array_pic[i, j] = array_pic[x, y];
                            }
                        }
                    }
                    break;
                case 1: // Метод разделения и слияния областей
                    for (int i = 0; i < width - 1; i++)
                    {
                        for (int j = 0; j < height - 1; j++)
                        {
                            if (Math.Abs(array_pic[i, j] - (array_pic[i + 1, j] + array_pic[i, j + 1] + array_pic[i + 1, j + 1]) / 3) <= delta)
                            {
                                array_pic[i + 1, j] = array_pic[i, j];
                                array_pic[i, j + 1] = array_pic[i, j];
                                array_pic[i + 1, j + 1] = array_pic[i, j];
                            }
                        }
                    }
                    break;
                case 2: // Метод водораздела
                    bool grad;
                    int[,] array_temp = new int[width, height];
                    for (int i = 1; i < width - 1; i++)
                    {
                        for (int j = 1; j < height - 1; j++)
                        {
                            x = i;
                            y = j;
                            grad = true;
                            while (grad && x > 0 && y > 0 && x < width - 1 && y < height - 1)
                            {
                                if (array_pic[x, y] > array_pic[x + 1, y])
                                    x++;
                                else
                                {
                                    if (array_pic[x, y] > array_pic[x - 1, y])
                                        x--;
                                    else
                                    {
                                        if (array_pic[x, y] > array_pic[x, y + 1])
                                            y++;
                                        else
                                        {
                                            if (array_pic[x, y] > array_pic[x, y - 1])
                                                y--;
                                            else
                                                grad = !grad;
                                        }
                                    }
                                }
                                array_temp[i, j] = array_pic[x, y];
                            }
                        }
                    }
                    array_pic = array_temp;
                    break;
            }
            cluster = new Bitmap(width, height);
            int x1 = 0; // Значение каналов пикселя
            for (int i = 0; i < width; i++) // Формирование изображения
            {
                for (int j = 0; j < height; j++)
                {
                    x1 = Convert.ToInt32(array_pic[i, j]);
                    cluster.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                }
            }
            pic_cluster.Image = cluster;
        }

        private void But_open_Click(object sender, EventArgs e) // Загрузка изображения
        {
            OpenFileDialog open_dialog = new OpenFileDialog(); //создание диалогового окна для выбора файла
            open_dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*"; //формат файла
            if (open_dialog.ShowDialog() == DialogResult.OK) //если в окне была нажата кнопка "ОК"
            {
                try
                {
                    image = new Bitmap(open_dialog.FileName);
                    //this.pic.Size = image.Size;
                    pic.Image = image;
                    pic.Invalidate();
                    but_porog.Enabled = true;
                    but_contour.Enabled = true;
                    but_cluster.Enabled = true;
                    save_porog.Enabled = true;
                    save_contour.Enabled = true;
                    save_cluster.Enabled = true;                    
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Bitmap Bitmap_gray = new Bitmap(pic.Image);
                int x1; // значение каналов пикселя
                int height, width; // высота и ширина изображений
                int[] hist_func = new int[256]; // Массив для построения гистограммы
                chart1.Series[0].Points.Clear(); // Удаление точек на графике
                
                height = Bitmap_gray.Height;
                width = Bitmap_gray.Width;

                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < height; j++)
                    {
                        Color pixel1 = Bitmap_gray.GetPixel(i, j);
                        // Расчет значений канала пикселя
                        x1 = Convert.ToInt32(0.299 * pixel1.R + 0.587 * pixel1.G + 0.114 * pixel1.B);
                        Bitmap_gray.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                        hist_func[x1]++; // Увеличение количества пикселей с данным значением канала
                    }
                }
                pic.Image = Bitmap_gray;
                for (int i = 0; i < hist_func.Length; i++) // Построение гистограммы
                {
                    chart1.Series[0].Points.AddXY(i, hist_func[i]);
                }
                // Сохранение изображения в градациях серого
                if (pic != null) //если в pictureBox есть изображение
                {
                    //создание диалогового окна "Сохранить как..", для сохранения изображения
                    SaveFileDialog savedialog = new SaveFileDialog();
                    savedialog.Title = "Сохранить картинку как...";
                    //отображать ли предупреждение, если пользователь указывает имя уже существующего файла
                    savedialog.OverwritePrompt = true;
                    //отображать ли предупреждение, если пользователь указывает несуществующий путь
                    savedialog.CheckPathExists = true;
                    //список форматов файла
                    savedialog.Filter = "Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";
                    //отображается ли кнопка "Справка" в диалоговом окне
                    savedialog.ShowHelp = true;
                    if (savedialog.ShowDialog() == DialogResult.OK) //если в диалоговом окне нажата кнопка "ОК"
                    {
                        try
                        {
                            pic.Image.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                        }
                        catch
                        {
                            MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
        }

        private void Save_porog_Click(object sender, EventArgs e) // Сохранение после пороговой обработки
        {
            if (pic_porog != null) //если в pictureBox есть изображение
            {
                //создание диалогового окна "Сохранить как..", для сохранения изображения
                SaveFileDialog savedialog = new SaveFileDialog();
                savedialog.Title = "Сохранить картинку как...";
                //отображать ли предупреждение, если пользователь указывает имя уже существующего файла
                savedialog.OverwritePrompt = true;
                //отображать ли предупреждение, если пользователь указывает несуществующий путь
                savedialog.CheckPathExists = true;
                //список форматов файла
                savedialog.Filter = "Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";
                //отображается ли кнопка "Справка" в диалоговом окне
                savedialog.ShowHelp = true;
                if (savedialog.ShowDialog() == DialogResult.OK) //если в диалоговом окне нажата кнопка "ОК"
                {
                    try
                    {
                        pic_porog.Image.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void Save_contour_Click(object sender, EventArgs e) // Сохранение после выделение контуров
        {
            if (pic_contour != null) //если в pictureBox есть изображение
            {
                //создание диалогового окна "Сохранить как..", для сохранения изображения
                SaveFileDialog savedialog = new SaveFileDialog();
                savedialog.Title = "Сохранить картинку как...";
                //отображать ли предупреждение, если пользователь указывает имя уже существующего файла
                savedialog.OverwritePrompt = true;
                //отображать ли предупреждение, если пользователь указывает несуществующий путь
                savedialog.CheckPathExists = true;
                //список форматов файла
                savedialog.Filter = "Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";
                //отображается ли кнопка "Справка" в диалоговом окне
                savedialog.ShowHelp = true;
                if (savedialog.ShowDialog() == DialogResult.OK) //если в диалоговом окне нажата кнопка "ОК"
                {
                    try
                    {
                        pic_contour.Image.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void Save_cluster_Click(object sender, EventArgs e) // Сохранение после кластеризации
        {
            if (pic_cluster != null) //если в pictureBox есть изображение
            {
                //создание диалогового окна "Сохранить как..", для сохранения изображения
                SaveFileDialog savedialog = new SaveFileDialog();
                savedialog.Title = "Сохранить картинку как...";
                //отображать ли предупреждение, если пользователь указывает имя уже существующего файла
                savedialog.OverwritePrompt = true;
                //отображать ли предупреждение, если пользователь указывает несуществующий путь
                savedialog.CheckPathExists = true;
                //список форматов файла
                savedialog.Filter = "Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";
                //отображается ли кнопка "Справка" в диалоговом окне
                savedialog.ShowHelp = true;
                if (savedialog.ShowDialog() == DialogResult.OK) //если в диалоговом окне нажата кнопка "ОК"
                {
                    try
                    {
                        pic_cluster.Image.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}
