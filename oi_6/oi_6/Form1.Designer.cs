﻿namespace oi_6
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.save_contour = new System.Windows.Forms.Button();
            this.save_cluster = new System.Windows.Forms.Button();
            this.pic_cluster = new System.Windows.Forms.PictureBox();
            this.type_cluster = new System.Windows.Forms.ComboBox();
            this.num_cluster = new System.Windows.Forms.NumericUpDown();
            this.but_cluster = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.but_contour = new System.Windows.Forms.Button();
            this.type_contour = new System.Windows.Forms.ComboBox();
            this.dir_contour = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.parts_porog = new System.Windows.Forms.ComboBox();
            this.save_porog = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.type_porog = new System.Windows.Forms.ComboBox();
            this.delta_porog = new System.Windows.Forms.TextBox();
            this.num_porog = new System.Windows.Forms.NumericUpDown();
            this.but_porog = new System.Windows.Forms.Button();
            this.but_open = new System.Windows.Forms.Button();
            this.pic_porog = new System.Windows.Forms.PictureBox();
            this.pic = new System.Windows.Forms.PictureBox();
            this.pic_contour = new System.Windows.Forms.PictureBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label_sk = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pic_cluster)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_cluster)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_porog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_porog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_contour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // save_contour
            // 
            this.save_contour.Enabled = false;
            this.save_contour.Location = new System.Drawing.Point(404, 447);
            this.save_contour.Margin = new System.Windows.Forms.Padding(4);
            this.save_contour.Name = "save_contour";
            this.save_contour.Size = new System.Drawing.Size(160, 43);
            this.save_contour.TabIndex = 47;
            this.save_contour.Text = "Сохранение";
            this.save_contour.UseVisualStyleBackColor = true;
            // 
            // save_cluster
            // 
            this.save_cluster.Enabled = false;
            this.save_cluster.Location = new System.Drawing.Point(848, 397);
            this.save_cluster.Margin = new System.Windows.Forms.Padding(4);
            this.save_cluster.Name = "save_cluster";
            this.save_cluster.Size = new System.Drawing.Size(100, 28);
            this.save_cluster.TabIndex = 46;
            this.save_cluster.Text = "Сохранить";
            this.save_cluster.UseVisualStyleBackColor = true;
            // 
            // pic_cluster
            // 
            this.pic_cluster.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_cluster.Location = new System.Drawing.Point(574, 280);
            this.pic_cluster.Margin = new System.Windows.Forms.Padding(4);
            this.pic_cluster.Name = "pic_cluster";
            this.pic_cluster.Size = new System.Drawing.Size(267, 244);
            this.pic_cluster.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_cluster.TabIndex = 45;
            this.pic_cluster.TabStop = false;
            // 
            // type_cluster
            // 
            this.type_cluster.FormattingEnabled = true;
            this.type_cluster.Items.AddRange(new object[] {
            "Выращиваиние области",
            "Разделение и слияние",
            "Водораздел"});
            this.type_cluster.Location = new System.Drawing.Point(848, 363);
            this.type_cluster.Margin = new System.Windows.Forms.Padding(4);
            this.type_cluster.Name = "type_cluster";
            this.type_cluster.Size = new System.Drawing.Size(160, 24);
            this.type_cluster.TabIndex = 44;
            // 
            // num_cluster
            // 
            this.num_cluster.Location = new System.Drawing.Point(848, 331);
            this.num_cluster.Margin = new System.Windows.Forms.Padding(4);
            this.num_cluster.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.num_cluster.Name = "num_cluster";
            this.num_cluster.Size = new System.Drawing.Size(160, 22);
            this.num_cluster.TabIndex = 43;
            this.num_cluster.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // but_cluster
            // 
            this.but_cluster.Enabled = false;
            this.but_cluster.Location = new System.Drawing.Point(848, 281);
            this.but_cluster.Margin = new System.Windows.Forms.Padding(4);
            this.but_cluster.Name = "but_cluster";
            this.but_cluster.Size = new System.Drawing.Size(160, 43);
            this.but_cluster.TabIndex = 42;
            this.but_cluster.Text = "Кластеризация";
            this.but_cluster.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(404, 394);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 17);
            this.label6.TabIndex = 41;
            this.label6.Text = "Тип выделения";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(404, 345);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 17);
            this.label5.TabIndex = 40;
            this.label5.Text = "Направление";
            // 
            // but_contour
            // 
            this.but_contour.Enabled = false;
            this.but_contour.Location = new System.Drawing.Point(406, 298);
            this.but_contour.Margin = new System.Windows.Forms.Padding(4);
            this.but_contour.Name = "but_contour";
            this.but_contour.Size = new System.Drawing.Size(160, 43);
            this.but_contour.TabIndex = 39;
            this.but_contour.Text = "Выделение контуров";
            this.but_contour.UseVisualStyleBackColor = true;
            // 
            // type_contour
            // 
            this.type_contour.FormattingEnabled = true;
            this.type_contour.Items.AddRange(new object[] {
            "Робертс",
            "Превитт",
            "Собел",
            "Лапласиан гауссиана"});
            this.type_contour.Location = new System.Drawing.Point(406, 365);
            this.type_contour.Margin = new System.Windows.Forms.Padding(4);
            this.type_contour.Name = "type_contour";
            this.type_contour.Size = new System.Drawing.Size(160, 24);
            this.type_contour.TabIndex = 38;
            // 
            // dir_contour
            // 
            this.dir_contour.FormattingEnabled = true;
            this.dir_contour.Items.AddRange(new object[] {
            "Горизонтальные",
            "Вертикальные"});
            this.dir_contour.Location = new System.Drawing.Point(406, 414);
            this.dir_contour.Margin = new System.Windows.Forms.Padding(4);
            this.dir_contour.Name = "dir_contour";
            this.dir_contour.Size = new System.Drawing.Size(160, 24);
            this.dir_contour.TabIndex = 37;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(404, 223);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 17);
            this.label4.TabIndex = 36;
            this.label4.Text = "Части изображения";
            // 
            // parts_porog
            // 
            this.parts_porog.FormattingEnabled = true;
            this.parts_porog.Items.AddRange(new object[] {
            "4",
            "16",
            "1"});
            this.parts_porog.Location = new System.Drawing.Point(406, 243);
            this.parts_porog.Margin = new System.Windows.Forms.Padding(4);
            this.parts_porog.Name = "parts_porog";
            this.parts_porog.Size = new System.Drawing.Size(160, 24);
            this.parts_porog.TabIndex = 35;
            // 
            // save_porog
            // 
            this.save_porog.Enabled = false;
            this.save_porog.Location = new System.Drawing.Point(848, 31);
            this.save_porog.Margin = new System.Windows.Forms.Padding(4);
            this.save_porog.Name = "save_porog";
            this.save_porog.Size = new System.Drawing.Size(100, 28);
            this.save_porog.TabIndex = 34;
            this.save_porog.Text = "Сохранить";
            this.save_porog.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(404, 175);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 17);
            this.label3.TabIndex = 33;
            this.label3.Text = "Дельта порога";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(404, 127);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 17);
            this.label2.TabIndex = 32;
            this.label2.Text = "Порог";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(404, 78);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 17);
            this.label1.TabIndex = 31;
            this.label1.Text = "Тип обработки";
            // 
            // type_porog
            // 
            this.type_porog.FormattingEnabled = true;
            this.type_porog.Items.AddRange(new object[] {
            "Простая",
            "Адаптивная"});
            this.type_porog.Location = new System.Drawing.Point(406, 97);
            this.type_porog.Margin = new System.Windows.Forms.Padding(4);
            this.type_porog.Name = "type_porog";
            this.type_porog.Size = new System.Drawing.Size(160, 24);
            this.type_porog.TabIndex = 30;
            // 
            // delta_porog
            // 
            this.delta_porog.Location = new System.Drawing.Point(406, 195);
            this.delta_porog.Margin = new System.Windows.Forms.Padding(4);
            this.delta_porog.Name = "delta_porog";
            this.delta_porog.Size = new System.Drawing.Size(132, 22);
            this.delta_porog.TabIndex = 29;
            this.delta_porog.Text = "5";
            // 
            // num_porog
            // 
            this.num_porog.Location = new System.Drawing.Point(406, 147);
            this.num_porog.Margin = new System.Windows.Forms.Padding(4);
            this.num_porog.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.num_porog.Name = "num_porog";
            this.num_porog.Size = new System.Drawing.Size(160, 22);
            this.num_porog.TabIndex = 28;
            this.num_porog.Value = new decimal(new int[] {
            128,
            0,
            0,
            0});
            // 
            // but_porog
            // 
            this.but_porog.Enabled = false;
            this.but_porog.Location = new System.Drawing.Point(406, 31);
            this.but_porog.Margin = new System.Windows.Forms.Padding(4);
            this.but_porog.Name = "but_porog";
            this.but_porog.Size = new System.Drawing.Size(160, 43);
            this.but_porog.TabIndex = 25;
            this.but_porog.Text = "Пороговая обработка";
            this.but_porog.UseVisualStyleBackColor = true;
            // 
            // but_open
            // 
            this.but_open.Location = new System.Drawing.Point(20, 31);
            this.but_open.Margin = new System.Windows.Forms.Padding(4);
            this.but_open.Name = "but_open";
            this.but_open.Size = new System.Drawing.Size(100, 28);
            this.but_open.TabIndex = 23;
            this.but_open.Text = "Загрузить";
            this.but_open.UseVisualStyleBackColor = true;
            // 
            // pic_porog
            // 
            this.pic_porog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_porog.Location = new System.Drawing.Point(574, 31);
            this.pic_porog.Margin = new System.Windows.Forms.Padding(4);
            this.pic_porog.Name = "pic_porog";
            this.pic_porog.Size = new System.Drawing.Size(267, 244);
            this.pic_porog.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_porog.TabIndex = 48;
            this.pic_porog.TabStop = false;
            // 
            // pic
            // 
            this.pic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic.Location = new System.Drawing.Point(128, 31);
            this.pic.Margin = new System.Windows.Forms.Padding(4);
            this.pic.Name = "pic";
            this.pic.Size = new System.Drawing.Size(267, 244);
            this.pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic.TabIndex = 49;
            this.pic.TabStop = false;
            // 
            // pic_contour
            // 
            this.pic_contour.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_contour.Location = new System.Drawing.Point(128, 280);
            this.pic_contour.Margin = new System.Windows.Forms.Padding(4);
            this.pic_contour.Name = "pic_contour";
            this.pic_contour.Size = new System.Drawing.Size(267, 244);
            this.pic_contour.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_contour.TabIndex = 50;
            this.pic_contour.TabStop = false;
            // 
            // chart1
            // 
            chartArea1.AxisX.Maximum = 255D;
            chartArea1.AxisX.Minimum = 0D;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Location = new System.Drawing.Point(848, 66);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(364, 209);
            this.chart1.TabIndex = 51;
            this.chart1.Text = "chart1";
            // 
            // label_sk
            // 
            this.label_sk.AutoSize = true;
            this.label_sk.Location = new System.Drawing.Point(20, 345);
            this.label_sk.Name = "label_sk";
            this.label_sk.Size = new System.Drawing.Size(24, 17);
            this.label_sk.TabIndex = 52;
            this.label_sk.Text = "Sk";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1224, 554);
            this.Controls.Add(this.label_sk);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.pic_contour);
            this.Controls.Add(this.pic);
            this.Controls.Add(this.pic_porog);
            this.Controls.Add(this.save_contour);
            this.Controls.Add(this.save_cluster);
            this.Controls.Add(this.pic_cluster);
            this.Controls.Add(this.type_cluster);
            this.Controls.Add(this.num_cluster);
            this.Controls.Add(this.but_cluster);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.but_contour);
            this.Controls.Add(this.type_contour);
            this.Controls.Add(this.dir_contour);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.parts_porog);
            this.Controls.Add(this.save_porog);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.type_porog);
            this.Controls.Add(this.delta_porog);
            this.Controls.Add(this.num_porog);
            this.Controls.Add(this.but_porog);
            this.Controls.Add(this.but_open);
            this.Name = "Form1";
            this.Text = "6 Сегментация изображений";
            ((System.ComponentModel.ISupportInitialize)(this.pic_cluster)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_cluster)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_porog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_porog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_contour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button save_contour;
        private System.Windows.Forms.Button save_cluster;
        private System.Windows.Forms.PictureBox pic_cluster;
        private System.Windows.Forms.ComboBox type_cluster;
        private System.Windows.Forms.NumericUpDown num_cluster;
        private System.Windows.Forms.Button but_cluster;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button but_contour;
        private System.Windows.Forms.ComboBox type_contour;
        private System.Windows.Forms.ComboBox dir_contour;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox parts_porog;
        private System.Windows.Forms.Button save_porog;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox type_porog;
        private System.Windows.Forms.TextBox delta_porog;
        private System.Windows.Forms.NumericUpDown num_porog;
        private System.Windows.Forms.Button but_porog;
        private System.Windows.Forms.Button but_open;
        private System.Windows.Forms.PictureBox pic_porog;
        private System.Windows.Forms.PictureBox pic;
        private System.Windows.Forms.PictureBox pic_contour;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Label label_sk;
    }
}

