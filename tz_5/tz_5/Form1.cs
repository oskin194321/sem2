﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tz_5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            but_alg.Click += But_alg_Click;

            but_open.Click += But_open_Click;
            but_save.Click += But_save_Click;
        }

        private void But_alg_Click(object sender, EventArgs e) // Реализация алгоритма дебайеризации
        {
            Bitmap Bmp_gray = gray(pic.Image); // Получение черно-белого изображения
            Bitmap Bmp_deb; // Результат преобразования

            int width = pic.Image.Width; // Ширина изображения
            int height = pic.Image.Height; // Высота изображения

            if (cb_alg.SelectedIndex == 0) // SuperPixel
            {
                if (width % 2 == 1 || height % 2 == 1) // Проверка пикселей на четность и приведение к четности
                {
                    if (width % 2 == 1)
                        width++;
                    if (height % 2 == 1)
                        height++;
                    Bmp_gray = new Bitmap(Bmp_gray, width, height);
                }
                Bmp_deb = new Bitmap(width / 2, height / 2);
                for (int i = 0; i < width; i++) // Преобразование SuperPixel
                {
                    if (i % 2 == 0)
                    {
                        for (int j = 0; j < height; j++)
                        {
                            if (j % 2 == 0)
                            {
                                int i_j = Bmp_gray.GetPixel(i, j).R;
                                int i1_j = Bmp_gray.GetPixel(i + 1, j).R;
                                int i_j1 = Bmp_gray.GetPixel(i, j + 1).R;
                                int i1_j1 = Bmp_gray.GetPixel(i + 1, j + 1).R;
                                switch (filter.SelectedIndex) // Выбранный тип фильтра
                                {
                                    case 0: // RGGB
                                        Bmp_deb.SetPixel(i / 2, j / 2, Color.FromArgb(i_j, (i1_j + i_j1) / 2, i1_j1));
                                        break;
                                    case 1: // BGGR
                                        Bmp_deb.SetPixel(i / 2, j / 2, Color.FromArgb(i1_j1, (i1_j + i_j1) / 2, i_j));
                                        break;
                                    case 2: // GBRG
                                        Bmp_deb.SetPixel(i / 2, j / 2, Color.FromArgb(i_j1, (i_j + i1_j1) / 2, i1_j));
                                        break;
                                    case 3: // GRBG
                                        Bmp_deb.SetPixel(i / 2, j / 2, Color.FromArgb(i1_j, (i_j + i1_j1) / 2, i_j1));
                                        break;
                                }
                            }
                        }
                    }
                }
                pic_res.Image = Bmp_deb;
            }
            else // 1. Билинейная интерполяция
            {
                Bmp_deb = new Bitmap(width, height);
                int[,] frame3;
                frame3 = new int[width + 2, height + 2];
                for (int i = 0; i < width + 2; i++) // Добавление рамки для вычислений
                {
                    for (int j = 0; j < height + 2; j++)
                    {
                        if (i == 0)
                        {
                            if (j == 0)
                                frame3[i, j] = Bmp_gray.GetPixel(i + 1, j + 1).R;
                            else
                            {
                                if (j == height + 1)
                                    frame3[i, j] = Bmp_gray.GetPixel(i + 1, j - 3).R;
                                else
                                    frame3[i, j] = Bmp_gray.GetPixel(i + 1, j - 1).R;
                            }
                        }
                        else
                        {
                            if (i == width + 1)
                            {
                                if (j == 0)
                                    frame3[i, j] = Bmp_gray.GetPixel(i - 3, j + 1).R;
                                else
                                {
                                    if (j == height + 1)
                                        frame3[i, j] = Bmp_gray.GetPixel(i - 4, j - 4).R;
                                    else
                                        frame3[i, j] = Bmp_gray.GetPixel(i - 3, j - 1).R;
                                }
                            }
                            else
                            {
                                if (j == 0)
                                    frame3[i, j] = Bmp_gray.GetPixel(i - 1, j + 1).R;
                                else
                                {
                                    if (j == height + 1)
                                        frame3[i, j] = Bmp_gray.GetPixel(i - 1, j - 3).R;
                                    else
                                        frame3[i, j] = Bmp_gray.GetPixel(i - 1, j - 1).R;
                                }
                            }
                        }
                    }
                }
                Color col = new Color();
                for (int i = 1; i < width + 1; i++) // Преобразование Bilinear
                {
                    for (int j = 1; j < height + 1; j++)
                    {
                        switch (filter.SelectedIndex) // Типы фильтров
                        {
                            case 0: // RGGB
                                if (i % 2 == 0)
                                {
                                    if (j % 2 == 0)
                                    {
                                        col = Color.FromArgb((frame3[i - 1, j - 1] + frame3[i - 1, j + 1] + frame3[i + 1, j - 1] + frame3[i + 1, j + 1]) / 4,
                                    (frame3[i + 1, j] + frame3[i, j + 1] + frame3[i - 1, j] + frame3[i, j - 1]) / 4,
                                    frame3[i, j]); //Blue
                                    }
                                    else
                                    {
                                        col = Color.FromArgb((frame3[i - 1, j] + frame3[i + 1, j]) / 2,
                                    frame3[i, j],
                                    (frame3[i, j - 1] + frame3[i, j + 1]) / 2); // Green 1
                                    }
                                }
                                else
                                {
                                    if (j % 2 == 0)
                                    {
                                        col = Color.FromArgb((frame3[i, j - 1] + frame3[i, j + 1]) / 2,
                                    frame3[i, j],
                                    (frame3[i - 1, j] + frame3[i + 1, j]) / 2); // Green 2
                                    }
                                    else
                                    {
                                        col = Color.FromArgb(frame3[i, j],
                                    (frame3[i + 1, j] + frame3[i, j + 1] + frame3[i - 1, j] + frame3[i, j - 1]) / 4,
                                    (frame3[i - 1, j - 1] + frame3[i - 1, j + 1] + frame3[i + 1, j - 1] + frame3[i + 1, j + 1]) / 4); // Red
                                    }
                                }
                                break;
                            case 1: // BGGR
                                if (i % 2 == 0)
                                {
                                    if (j % 2 == 0)
                                    {
                                        col = Color.FromArgb(frame3[i, j],
                                    (frame3[i + 1, j] + frame3[i, j + 1] + frame3[i - 1, j] + frame3[i, j - 1]) / 4,
                                    (frame3[i - 1, j - 1] + frame3[i - 1, j + 1] + frame3[i + 1, j - 1] + frame3[i + 1, j + 1]) / 4); // Red
                                    }
                                    else
                                    {
                                        col = Color.FromArgb((frame3[i, j - 1] + frame3[i, j + 1]) / 2,
                                    frame3[i, j],
                                    (frame3[i - 1, j] + frame3[i + 1, j]) / 2); // Green 2
                                    }
                                }
                                else
                                {
                                    if (j % 2 == 0)
                                    {
                                        col = Color.FromArgb((frame3[i - 1, j] + frame3[i + 1, j]) / 2,
                                    frame3[i, j],
                                    (frame3[i, j - 1] + frame3[i, j + 1]) / 2); // Green 1
                                    }
                                    else
                                    {
                                        col = Color.FromArgb((frame3[i - 1, j - 1] + frame3[i - 1, j + 1] + frame3[i + 1, j - 1] + frame3[i + 1, j + 1]) / 4,
                                    (frame3[i + 1, j] + frame3[i, j + 1] + frame3[i - 1, j] + frame3[i, j - 1]) / 4,
                                    frame3[i, j]); //Blue
                                    }
                                }
                                break;
                            case 2: // GBRG
                                if (i % 2 == 0)
                                {
                                    if (j % 2 == 0)
                                    {
                                        col = Color.FromArgb((frame3[i - 1, j] + frame3[i + 1, j]) / 2,
                                    frame3[i, j],
                                    (frame3[i, j - 1] + frame3[i, j + 1]) / 2); // Green 1
                                    }
                                    else
                                    {
                                        col = Color.FromArgb((frame3[i - 1, j - 1] + frame3[i - 1, j + 1] + frame3[i + 1, j - 1] + frame3[i + 1, j + 1]) / 4,
                                    (frame3[i + 1, j] + frame3[i, j + 1] + frame3[i - 1, j] + frame3[i, j - 1]) / 4,
                                    frame3[i, j]); //Blue
                                    }
                                }
                                else
                                {
                                    if (j % 2 == 0)
                                    {
                                        col = Color.FromArgb(frame3[i, j],
                                    (frame3[i + 1, j] + frame3[i, j + 1] + frame3[i - 1, j] + frame3[i, j - 1]) / 4,
                                    (frame3[i - 1, j - 1] + frame3[i - 1, j + 1] + frame3[i + 1, j - 1] + frame3[i + 1, j + 1]) / 4); // Red
                                    }
                                    else
                                    {
                                        col = Color.FromArgb((frame3[i, j - 1] + frame3[i, j + 1]) / 2,
                                    frame3[i, j],
                                    (frame3[i - 1, j] + frame3[i + 1, j]) / 2); // Green 2
                                    }
                                }
                                break;
                            case 3: // GRBG
                                if (i % 2 == 0)
                                {
                                    if (j % 2 == 0)
                                    {
                                        col = Color.FromArgb((frame3[i, j - 1] + frame3[i, j + 1]) / 2,
                                    frame3[i, j],
                                    (frame3[i - 1, j] + frame3[i + 1, j]) / 2); // Green 2

                                    }
                                    else
                                    {
                                        col = Color.FromArgb(frame3[i, j],
                                    (frame3[i + 1, j] + frame3[i, j + 1] + frame3[i - 1, j] + frame3[i, j - 1]) / 4,
                                    (frame3[i - 1, j - 1] + frame3[i - 1, j + 1] + frame3[i + 1, j - 1] + frame3[i + 1, j + 1]) / 4); // Red

                                    }
                                }
                                else
                                {
                                    if (j % 2 == 0)
                                    {
                                        col = Color.FromArgb((frame3[i - 1, j - 1] + frame3[i - 1, j + 1] + frame3[i + 1, j - 1] + frame3[i + 1, j + 1]) / 4,
                                    (frame3[i + 1, j] + frame3[i, j + 1] + frame3[i - 1, j] + frame3[i, j - 1]) / 4,
                                    frame3[i, j]); //Blue
                                    }
                                    else
                                    {
                                        col = Color.FromArgb((frame3[i - 1, j] + frame3[i + 1, j]) / 2,
                                    frame3[i, j],
                                    (frame3[i, j - 1] + frame3[i, j + 1]) / 2); // Green 1
                                    }
                                }
                                break;
                        }
                        Bmp_deb.SetPixel(i - 1, j - 1, col);
                    }
                }
                pic_res.Image = Bmp_deb;
            }
        }

        private Bitmap gray(Image pic_open) // Преобразование в черно-белое
        {
            Bitmap Bitmap_gray = new Bitmap(pic_open);
            int x1; // значение каналов пикселя
            int height, width; // высота и ширина изображения

            height = Bitmap_gray.Height;
            width = Bitmap_gray.Width;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_gray.GetPixel(i, j);
                    // Расчет значений канала пикселя
                    x1 = Convert.ToInt32(0.299 * pixel1.R + 0.587 * pixel1.G + 0.114 * pixel1.B);
                    Bitmap_gray.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                }
            }
            return Bitmap_gray;
        }

        private void But_open_Click(object sender, EventArgs e) // Открытие изображения
        {
            Bitmap image; // Bitmap для открываемого изображения

            OpenFileDialog open_dialog = new OpenFileDialog(); //создание диалогового окна для выбора файла
            open_dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*"; //формат файла
            if (open_dialog.ShowDialog() == DialogResult.OK) //если в окне была нажата кнопка "ОК"
            {
                try
                {
                    image = new Bitmap(open_dialog.FileName);
                    pic.Image = image;
                    pic.Invalidate();
                    but_alg.Enabled = true;
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void But_save_Click(object sender, EventArgs e) // Сохранение изображения
        {
            if (pic_res != null) //если в pictureBox есть изображение
            {
                //создание диалогового окна "Сохранить как..", для сохранения изображения
                SaveFileDialog savedialog = new SaveFileDialog();
                savedialog.Title = "Сохранить картинку как...";
                //отображать ли предупреждение, если пользователь указывает имя уже существующего файла
                savedialog.OverwritePrompt = true;
                //отображать ли предупреждение, если пользователь указывает несуществующий путь
                savedialog.CheckPathExists = true;
                //список форматов файла
                savedialog.Filter = "Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";
                //отображается ли кнопка "Справка" в диалоговом окне
                savedialog.ShowHelp = true;
                if (savedialog.ShowDialog() == DialogResult.OK) //если в диалоговом окне нажата кнопка "ОК"
                {
                    try
                    {
                        pic_res.Image.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}