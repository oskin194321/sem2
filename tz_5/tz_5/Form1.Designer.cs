﻿namespace tz_5
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pic = new System.Windows.Forms.PictureBox();
            this.pic_res = new System.Windows.Forms.PictureBox();
            this.but_open = new System.Windows.Forms.Button();
            this.but_save = new System.Windows.Forms.Button();
            this.but_alg = new System.Windows.Forms.Button();
            this.filter = new System.Windows.Forms.ComboBox();
            this.cb_alg = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_res)).BeginInit();
            this.SuspendLayout();
            // 
            // pic
            // 
            this.pic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic.Location = new System.Drawing.Point(12, 75);
            this.pic.Name = "pic";
            this.pic.Size = new System.Drawing.Size(600, 600);
            this.pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic.TabIndex = 0;
            this.pic.TabStop = false;
            // 
            // pic_res
            // 
            this.pic_res.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_res.Location = new System.Drawing.Point(618, 75);
            this.pic_res.Name = "pic_res";
            this.pic_res.Size = new System.Drawing.Size(600, 600);
            this.pic_res.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_res.TabIndex = 1;
            this.pic_res.TabStop = false;
            // 
            // but_open
            // 
            this.but_open.Location = new System.Drawing.Point(12, 23);
            this.but_open.Name = "but_open";
            this.but_open.Size = new System.Drawing.Size(200, 30);
            this.but_open.TabIndex = 2;
            this.but_open.Text = "Загрузить изображение";
            this.but_open.UseVisualStyleBackColor = true;
            // 
            // but_save
            // 
            this.but_save.Location = new System.Drawing.Point(1016, 23);
            this.but_save.Name = "but_save";
            this.but_save.Size = new System.Drawing.Size(200, 30);
            this.but_save.TabIndex = 3;
            this.but_save.Text = "Сохранить изображение";
            this.but_save.UseVisualStyleBackColor = true;
            // 
            // but_alg
            // 
            this.but_alg.Enabled = false;
            this.but_alg.Location = new System.Drawing.Point(618, 23);
            this.but_alg.Name = "but_alg";
            this.but_alg.Size = new System.Drawing.Size(200, 30);
            this.but_alg.TabIndex = 4;
            this.but_alg.Text = "Применить алгоритм";
            this.but_alg.UseVisualStyleBackColor = true;
            // 
            // filter
            // 
            this.filter.FormattingEnabled = true;
            this.filter.Items.AddRange(new object[] {
            "RGGB",
            "BGGR",
            "GBRG",
            "GRBG"});
            this.filter.Location = new System.Drawing.Point(287, 27);
            this.filter.Name = "filter";
            this.filter.Size = new System.Drawing.Size(121, 24);
            this.filter.TabIndex = 5;
            // 
            // cb_alg
            // 
            this.cb_alg.FormattingEnabled = true;
            this.cb_alg.Items.AddRange(new object[] {
            "SuperPixel",
            "Bilinear"});
            this.cb_alg.Location = new System.Drawing.Point(414, 27);
            this.cb_alg.Name = "cb_alg";
            this.cb_alg.Size = new System.Drawing.Size(121, 24);
            this.cb_alg.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(287, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "Фильтр";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(411, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "Алгоритм";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1228, 704);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cb_alg);
            this.Controls.Add(this.filter);
            this.Controls.Add(this.but_alg);
            this.Controls.Add(this.but_save);
            this.Controls.Add(this.but_open);
            this.Controls.Add(this.pic_res);
            this.Controls.Add(this.pic);
            this.Name = "Form1";
            this.Text = "5 Изучение алгоритмов дебайеризации";
            ((System.ComponentModel.ISupportInitialize)(this.pic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_res)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pic;
        private System.Windows.Forms.PictureBox pic_res;
        private System.Windows.Forms.Button but_open;
        private System.Windows.Forms.Button but_save;
        private System.Windows.Forms.Button but_alg;
        private System.Windows.Forms.ComboBox filter;
        private System.Windows.Forms.ComboBox cb_alg;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

