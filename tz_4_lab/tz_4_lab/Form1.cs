﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tz_4_lab
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            but_open.Click += But_open_Click;
            but_calc.Click += But_calc_Click;
        }

        private void But_calc_Click(object sender, EventArgs e) // Вычисление усредненных координат RGB
        {
            Bitmap image = new Bitmap(pic.Image);
            int width = image.Width; // Ширина поля тест-объекта
            int height = image.Height; // Высота поля тест-объекта

            // Усредненные значения каналов RGB
            double R = 0, G = 0, B = 0;
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    R += image.GetPixel(i, j).R;
                    G += image.GetPixel(i, j).G;
                    B += image.GetPixel(i, j).B;
                }
            }

            // Подсчет и отображение на форме усредненных значений каналов RGB
            R = Math.Round(R / (width * height), 2);
            G = Math.Round(G / (width * height), 2);
            B = Math.Round(B / (width * height), 2);
            label_R.Text = Convert.ToString(R);
            label_G.Text = Convert.ToString(G);
            label_B.Text = Convert.ToString(B);
        }

        private void But_open_Click(object sender, EventArgs e)
        {
            Bitmap image; //Bitmap для открываемого изображения
            OpenFileDialog open_dialog = new OpenFileDialog(); //создание диалогового окна для выбора файла
            open_dialog.Filter = "Image Files(*.TIF;*.TIFF;*.BMP;*.JPG;*.GIF;*.PNG)|*.TIF;*.TIFF;*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*"; //формат файла
            if (open_dialog.ShowDialog() == DialogResult.OK) //если в окне была нажата кнопка "ОК"
            {
                try
                {
                    image = new Bitmap(open_dialog.FileName);
                    pic.Image = image;
                    pic.Invalidate();
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
