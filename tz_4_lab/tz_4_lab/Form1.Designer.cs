﻿namespace tz_4_lab
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.but_open = new System.Windows.Forms.Button();
            this.but_calc = new System.Windows.Forms.Button();
            this.pic = new System.Windows.Forms.PictureBox();
            this.label_R = new System.Windows.Forms.Label();
            this.label_G = new System.Windows.Forms.Label();
            this.label_B = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).BeginInit();
            this.SuspendLayout();
            // 
            // but_open
            // 
            this.but_open.Location = new System.Drawing.Point(217, 116);
            this.but_open.Name = "but_open";
            this.but_open.Size = new System.Drawing.Size(125, 30);
            this.but_open.TabIndex = 0;
            this.but_open.Text = "Загрузить";
            this.but_open.UseVisualStyleBackColor = true;
            // 
            // but_calc
            // 
            this.but_calc.Location = new System.Drawing.Point(389, 116);
            this.but_calc.Name = "but_calc";
            this.but_calc.Size = new System.Drawing.Size(125, 30);
            this.but_calc.TabIndex = 1;
            this.but_calc.Text = "Рассчитать";
            this.but_calc.UseVisualStyleBackColor = true;
            // 
            // pic
            // 
            this.pic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic.Location = new System.Drawing.Point(217, 152);
            this.pic.Name = "pic";
            this.pic.Size = new System.Drawing.Size(125, 125);
            this.pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic.TabIndex = 2;
            this.pic.TabStop = false;
            // 
            // label_R
            // 
            this.label_R.AutoSize = true;
            this.label_R.Location = new System.Drawing.Point(386, 180);
            this.label_R.Name = "label_R";
            this.label_R.Size = new System.Drawing.Size(18, 17);
            this.label_R.TabIndex = 3;
            this.label_R.Text = "R";
            // 
            // label_G
            // 
            this.label_G.AutoSize = true;
            this.label_G.Location = new System.Drawing.Point(386, 211);
            this.label_G.Name = "label_G";
            this.label_G.Size = new System.Drawing.Size(19, 17);
            this.label_G.TabIndex = 4;
            this.label_G.Text = "G";
            // 
            // label_B
            // 
            this.label_B.AutoSize = true;
            this.label_B.Location = new System.Drawing.Point(386, 244);
            this.label_B.Name = "label_B";
            this.label_B.Size = new System.Drawing.Size(17, 17);
            this.label_B.TabIndex = 5;
            this.label_B.Text = "B";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 410);
            this.Controls.Add(this.label_B);
            this.Controls.Add(this.label_G);
            this.Controls.Add(this.label_R);
            this.Controls.Add(this.pic);
            this.Controls.Add(this.but_calc);
            this.Controls.Add(this.but_open);
            this.Name = "Form1";
            this.Text = "4 tz Average RGB";
            ((System.ComponentModel.ISupportInitialize)(this.pic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button but_open;
        private System.Windows.Forms.Button but_calc;
        private System.Windows.Forms.PictureBox pic;
        private System.Windows.Forms.Label label_R;
        private System.Windows.Forms.Label label_G;
        private System.Windows.Forms.Label label_B;
    }
}

