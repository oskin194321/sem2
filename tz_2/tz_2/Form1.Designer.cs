﻿namespace tz_2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pic1 = new System.Windows.Forms.PictureBox();
            this.pic2 = new System.Windows.Forms.PictureBox();
            this.but_choose1 = new System.Windows.Forms.Button();
            this.but_choose2 = new System.Windows.Forms.Button();
            this.but_compare = new System.Windows.Forms.Button();
            this.label_mse = new System.Windows.Forms.Label();
            this.label_psnr = new System.Windows.Forms.Label();
            this.label_ssim = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.pic1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pic1
            // 
            this.pic1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic1.Location = new System.Drawing.Point(12, 12);
            this.pic1.Name = "pic1";
            this.pic1.Size = new System.Drawing.Size(525, 525);
            this.pic1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic1.TabIndex = 0;
            this.pic1.TabStop = false;
            // 
            // pic2
            // 
            this.pic2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic2.Location = new System.Drawing.Point(555, 12);
            this.pic2.Name = "pic2";
            this.pic2.Size = new System.Drawing.Size(525, 525);
            this.pic2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic2.TabIndex = 1;
            this.pic2.TabStop = false;
            // 
            // but_choose1
            // 
            this.but_choose1.Location = new System.Drawing.Point(12, 549);
            this.but_choose1.Name = "but_choose1";
            this.but_choose1.Size = new System.Drawing.Size(525, 36);
            this.but_choose1.TabIndex = 2;
            this.but_choose1.Text = "Выбрать 1 изображение";
            this.but_choose1.UseVisualStyleBackColor = true;
            // 
            // but_choose2
            // 
            this.but_choose2.Location = new System.Drawing.Point(555, 549);
            this.but_choose2.Name = "but_choose2";
            this.but_choose2.Size = new System.Drawing.Size(525, 36);
            this.but_choose2.TabIndex = 3;
            this.but_choose2.Text = "Выбрать 2 изображение";
            this.but_choose2.UseVisualStyleBackColor = true;
            // 
            // but_compare
            // 
            this.but_compare.Location = new System.Drawing.Point(1105, 383);
            this.but_compare.Name = "but_compare";
            this.but_compare.Size = new System.Drawing.Size(211, 36);
            this.but_compare.TabIndex = 4;
            this.but_compare.Text = "Сравнить";
            this.but_compare.UseVisualStyleBackColor = true;
            // 
            // label_mse
            // 
            this.label_mse.AutoSize = true;
            this.label_mse.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_mse.Location = new System.Drawing.Point(42, 66);
            this.label_mse.Name = "label_mse";
            this.label_mse.Size = new System.Drawing.Size(55, 20);
            this.label_mse.TabIndex = 5;
            this.label_mse.Text = "MSE: ";
            // 
            // label_psnr
            // 
            this.label_psnr.AutoSize = true;
            this.label_psnr.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_psnr.Location = new System.Drawing.Point(42, 108);
            this.label_psnr.Name = "label_psnr";
            this.label_psnr.Size = new System.Drawing.Size(65, 20);
            this.label_psnr.TabIndex = 6;
            this.label_psnr.Text = "PSNR: ";
            // 
            // label_ssim
            // 
            this.label_ssim.AutoSize = true;
            this.label_ssim.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_ssim.Location = new System.Drawing.Point(42, 156);
            this.label_ssim.Name = "label_ssim";
            this.label_ssim.Size = new System.Drawing.Size(59, 20);
            this.label_ssim.TabIndex = 7;
            this.label_ssim.Text = "SSIM: ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label_mse);
            this.groupBox1.Controls.Add(this.label_ssim);
            this.groupBox1.Controls.Add(this.label_psnr);
            this.groupBox1.Location = new System.Drawing.Point(1105, 127);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(211, 250);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Показатели";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1328, 661);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.but_compare);
            this.Controls.Add(this.but_choose2);
            this.Controls.Add(this.but_choose1);
            this.Controls.Add(this.pic2);
            this.Controls.Add(this.pic1);
            this.Name = "Form1";
            this.Text = "Техническое зрение 2";
            ((System.ComponentModel.ISupportInitialize)(this.pic1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pic1;
        private System.Windows.Forms.PictureBox pic2;
        private System.Windows.Forms.Button but_choose1;
        private System.Windows.Forms.Button but_choose2;
        private System.Windows.Forms.Button but_compare;
        private System.Windows.Forms.Label label_mse;
        private System.Windows.Forms.Label label_psnr;
        private System.Windows.Forms.Label label_ssim;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}

