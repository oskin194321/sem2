﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tz_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            but_choose1.Click += But_choose1_Click;
            but_choose2.Click += But_choose2_Click;
            but_compare.Click += But_compare_Click;
        }

        private void But_compare_Click(object sender, EventArgs e)
        {
            Bitmap Bitmap1 = new Bitmap(pic1.Image);
            Bitmap Bitmap2 = new Bitmap(pic2.Image);
            double result_mse = 0.0, result_psnr = 0.0, result_ssim = 0.0;

            result_mse = mse(Bitmap1, Bitmap2);            
            result_psnr = psnr(result_mse);
            result_ssim = ssim(Bitmap1, Bitmap2);

            label_mse.Text = "MSE: "+ Convert.ToString(result_mse);
            label_psnr.Text = "PSNR: " + Convert.ToString(result_psnr);
            label_ssim.Text = "SSIM: " + Convert.ToString(result_ssim);
        }

        private double mse(Bitmap Bitmap1, Bitmap Bitmap2) // Расчет показателя MSE
        {
            double calc_mse = 0.0;  // MSE
            int x1, x2; // значения каналов пикселя для 1 и 2 изображения
            int height, width; // высота и ширина изображений

            height = Bitmap1.Height;
            width = Bitmap1.Width;

            for(int i = 0; i < width; i++)
            {
                for(int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap1.GetPixel(i, j);
                    Color pixel2 = Bitmap2.GetPixel(i, j);
                    // Расчет значений каналов пикселей
                    x1 = Convert.ToInt32(0.299 * pixel1.R + 0.587 * pixel1.G + 0.114 * pixel1.B); 
                    x2 = Convert.ToInt32(0.299 * pixel2.R + 0.587 * pixel2.G + 0.114 * pixel2.B);

                    calc_mse += Math.Pow(x1 - x2, 2);
                }
            }

            calc_mse = calc_mse / (width * height);
            calc_mse = Math.Round(calc_mse, 5);
            return calc_mse;
        }

        private double ssim(Bitmap Bitmap1, Bitmap Bitmap2) // Расчет показателя SSIM
        {
            double calc_ssim = 0.0;

            int x = 0, y = 0; // значения каналов пикселя для 1 и 2 изображения
            double m_x = 0.0, m_y = 0.0, o_x = 0.0, o_y = 0.0, o_xy = 0.0; // Среднее значение и стандартное отклонение, ковариация
            double c1, c2; // Выравнивающие коэффициенты
            int height, width; // высота и ширина изображений

            height = Bitmap1.Height;
            width = Bitmap1.Width;

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap1.GetPixel(i, j);
                    Color pixel2 = Bitmap2.GetPixel(i, j);
                    // Расчет значений каналов пикселей
                    x += Convert.ToInt32(0.299 * pixel1.R + 0.587 * pixel1.G + 0.114 * pixel1.B);
                    y += Convert.ToInt32(0.299 * pixel2.R + 0.587 * pixel2.G + 0.114 * pixel2.B);
                }
            }

            m_x = x / (height * width);
            m_y = y / (height * width);

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap1.GetPixel(i, j);
                    Color pixel2 = Bitmap2.GetPixel(i, j);
                    o_x += Math.Pow(0.299 * pixel1.R + 0.587 * pixel1.G + 0.114 * pixel1.B - m_x, 2);
                    o_y += Math.Pow(0.299 * pixel2.R + 0.587 * pixel2.G + 0.114 * pixel2.B - m_y, 2);
                    o_xy += (0.299 * pixel1.R + 0.587 * pixel1.G + 0.114 * pixel1.B - m_x)*
                        (0.299 * pixel2.R + 0.587 * pixel2.G + 0.114 * pixel2.B - m_y);
                }
            }

            o_x = o_x / (width * height);
            o_y = o_y / (width * height);
            o_xy = o_xy / (width * height);
            c1 = Math.Pow(0.01 * 255, 2);
            c2 = Math.Pow(0.03 * 255, 2);

            calc_ssim = ((2 * m_x * m_y + c1) * (2 * o_xy + c2)) / 
                ((Math.Pow(m_x, 2) + Math.Pow(m_y, 2) + c1) * (o_x + o_y + c2));
            calc_ssim = Math.Round(calc_ssim, 5);
            return calc_ssim;
        }

        private double psnr(double mse) // Расчет показателя PSNR
        {
            double calc_psnr = 0.0;  // PSNR
            if (mse != 0.0)
            {
                calc_psnr = 20 * Math.Log10(255 / Math.Sqrt(mse));
                calc_psnr = Math.Round(calc_psnr, 5);
            }
            return calc_psnr;
        }

        private void But_choose2_Click(object sender, EventArgs e)
        {
            Bitmap image; //Bitmap для открываемого изображения

            OpenFileDialog open_dialog = new OpenFileDialog(); //создание диалогового окна для выбора файла
            open_dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*"; //формат файла
            if (open_dialog.ShowDialog() == DialogResult.OK) //если в окне была нажата кнопка "ОК"
            {
                try
                {
                    image = new Bitmap(open_dialog.FileName);
                    pic2.Image = image;
                    pic2.Invalidate();
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void But_choose1_Click(object sender, EventArgs e)
        {
            Bitmap image; //Bitmap для открываемого изображения

            OpenFileDialog open_dialog = new OpenFileDialog(); //создание диалогового окна для выбора файла
            open_dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*"; //формат файла
            if (open_dialog.ShowDialog() == DialogResult.OK) //если в окне была нажата кнопка "ОК"
            {
                try
                {
                    image = new Bitmap(open_dialog.FileName);
                    pic1.Image = image;
                    pic1.Invalidate();
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
