﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ga_1
{
    public partial class Form1 : Form
    {
        int[,] matrix;

        public Form1()
        {
            InitializeComponent();
            calc_button.Click += calc_Click;
        }

        private void calc_Click(object sender, EventArgs e)
        {
            textBox.Text = null;
            int size = datagridmatrix.RowCount;
            if (datagridmatrix.RowCount != value_matrix.Value)
            {
                size = (int)value_matrix.Value; // количество узлов графа
                Random rnd = new Random();
                matrix = new int[size, size];
                for (int i = 0; i < matrix.GetLength(0); i++) // заполнение массива случайными значениями
                {
                    for (int j = 0; j < matrix.GetLength(1); j++)
                    {
                        if (i != j)
                        {
                            if (matrix[i, j] != null)
                            {
                                matrix[i, j] = rnd.Next(1, 9);
                                matrix[j, i] = matrix[i, j];
                            }
                        }
                        else
                        {
                            matrix[i, j] = 0;
                        }
                    }
                }
                show_matrix(matrix);
            }

            int size_matrix = size - 1; // размер матрицы
            int number_chrom = (int)value_chrom.Value; // число хромосом
            int chance_mut = (int)value_chance.Value; // вероятность мутации    
            int evols = (int)value_evols.Value; // число эволюций  
            int[,] chromosomes;
            chromosomes = generate_chrom(number_chrom, size_matrix); // создание хромосом

            for (int q = 0; q < evols; q++) // расчет поколений
            { 
                sum(chromosomes, number_chrom, size_matrix, matrix);
                sort_by_sum(number_chrom, chromosomes, size_matrix + 1, size_matrix);
                percent(chromosomes, number_chrom, size_matrix + 2);

                if (radio_cycle.Checked == true)
                {
                    int average=0; // средняя длина пути
                    int min=0; // длина оптимального пути
                    textBox.Text += "---------------- " + (q + 1) + " ----------------" + Environment.NewLine;
                    for (int i = 0; i < number_chrom; i++) // вывод информации на каждом этапе (циклический)
                    {
                        average += chromosomes[i, size_matrix + 1];
                        if (i == 0)
                            min = chromosomes[i, size_matrix + 1];
                        if(min > chromosomes[i, size_matrix + 1])
                            min = chromosomes[i, size_matrix + 1];
                    }
                    textBox.Text += Environment.NewLine + "Средняя длина пути - " + average / value_chrom.Value + Environment.NewLine;
                    textBox.Text += "Длина оптимального пути - " + min + Environment.NewLine + Environment.NewLine;
                }

                if (radio_step.Checked == true || q==value_evols.Value-1)
                {
                    if (radio_cycle.Checked == true)
                        textBox.Text += "---------- Итоговый набор хромосом ----------" + Environment.NewLine;
                    else
                        textBox.Text += "---------------- " + (q + 1) + " ----------------" + Environment.NewLine;

                    for (int i = 0; i < number_chrom; i++) // вывод на textBox хромосом, суммы и шанса мутации
                    {
                        for (int j = 0; j < size_matrix + 3; j++)
                        {
                            textBox.Text += chromosomes[i, j].ToString() + " ";
                        }
                        textBox.Text += Environment.NewLine;
                    }
                    textBox.Text += "-----------------------------------" + Environment.NewLine;
                }

                int[,] chromosomes_new;
                chromosomes_new = evolution(number_chrom, size_matrix, chromosomes, size_matrix + 2, chance_mut); // эволюция предыдущих хромосом
                chromosomes = chromosomes_new;

                if (radio_step.Checked == true)
                {
                    for (int m = 0; m < number_chrom; m++) // вывод на textBox хромосом после эволюции
                    {
                        for (int k = 0; k < size_matrix + 1; k++)
                        {
                            textBox.Text += chromosomes[m, k].ToString() + " ";
                        }
                        textBox.Text += Environment.NewLine;
                    }
                    //textBox.Text += "-----------------------------------" + Environment.NewLine;
                }
            }
        }

        public void show_matrix(int[,] matrix) // заполнение матрицы на форме
        {
            datagridmatrix.RowCount = matrix.GetLength(0); // указание числа строк
            datagridmatrix.ColumnCount = matrix.GetLength(1); // и столбцов для отображения на форме
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    datagridmatrix.Rows[i].Cells[j].Value = matrix[i, j];
                }
            }
        }

        public static int[,] generate_chrom(int number_chrom, int size_matrix) // создание хромосом
        { 
            int[,] chromosomes;
            chromosomes = new int[number_chrom,size_matrix + 3]; // с учетом полей для суммы и процента
            Random rand = new Random();
            for (int i = 0; i < number_chrom; i++)
            { 
                chromosomes[i,0] = 0;
                chromosomes[i,size_matrix] = size_matrix;
            }
            for (int j = 0; j < number_chrom; j++) // заполнение хромосомы случайными числами в пределах размера матрицы
            {
                for (int k = 1; k < size_matrix; k++)
                {
                    chromosomes[j,k] = rand.Next(size_matrix + 1);
                }
            }
            return chromosomes;
        }


        public void sum(int[,] chromosomes, int number_chrom, int size_matrix, int[,] matrix) // функция расчета суммы пути
        { 
            int sum;
            for (int i = 0; i < number_chrom; i++)
            {
                sum = 0;
                for (int j = 0; j < size_matrix; j++)
                {
                    sum = sum + matrix[chromosomes[i,j],chromosomes[i,j + 1]]; // расчет суммы из исходной матрицы по номерам в хромосомах
                }
                chromosomes[i,size_matrix + 1] = sum;
            }
        }

        public void sort_by_sum(int number_chrom, int[,] chromosomes, int sum_pos, int size_matrix) // Сортировка по длине пути
        { 
            for (int i = 0; i < number_chrom; i++)
            {
                for (int j = 0; j < number_chrom - 1; j++)
                {
                    if (chromosomes[j,sum_pos] > chromosomes[j + 1,sum_pos])
                    {
                        for (int k = 0; k < size_matrix + 2; k++)
                        {
                            int temp = chromosomes[j,k];
                            chromosomes[j,k] = chromosomes[j + 1,k];
                            chromosomes[j + 1,k] = temp;
                        }
                    }
                }
            }
        }

        public void percent(int[,] chromosomes, int number_chrom, int percent_column) // расчет процента вероятности мутации
        { 
            double percent = 0.0;
            for (int i = 0; i < number_chrom; i++)
            {
                percent = 100 / (2.5 * (i + 1));
                chromosomes[i,percent_column] = (int)percent; 
            }
        }

        public int[,] evolution(int number_chrom, int size_matrix, int[,] chromosomes, int percent_column, int chance_mut)
        { 
            int[,] chromosomes_new;
            chromosomes_new = new int[number_chrom,size_matrix + 3]; // учитывая поля для суммы и процента
            Random rand = new Random();

            int length_1 = size_matrix / 2 + 1; // Размер первой части для эволюции

            for (int i = 0; i < number_chrom; i++) // эволюция первой части хромосомы
            { 
                int rnd = rand.Next(100 - 1); // шанс сохранения генов предка
                int percent = chromosomes[0,percent_column]; // нарастающий шанс сохранения гена
                int j = 0; // номер хромосомы для скрещивания
                int procentMutation = rand.Next(100 - 1); // шанс мутации

                if (procentMutation > chance_mut)
                {
                    while (rnd > percent) // поиск хромосомы для эволюции
                    { 
                        j++;
                        percent = percent + chromosomes[j,percent_column];
                    }

                    for (int n = 0; n < length_1; n++) // запись генов из старой хромосомы в новую
                    {
                        chromosomes_new[i,n] = chromosomes[j,n];
                    }
                }
                else
                {
                    for (int n = 1; n < length_1; n++) // заполнение хромосомы случайными числами при срабатывании мутации
                    { 
                        chromosomes_new[i,n] = rand.Next(size_matrix + 1);
                    }
                    chromosomes_new[i,0] = 0;
                }
            }

            for (int m = 0; m < number_chrom; m++) // эволюция второй части хромосомы
            { 
                int rnd1 = rand.Next(100 - 1);
                int procent1 = chromosomes[0,percent_column];
                int k = 0;
                int procentMutation = rand.Next(100 - 1);

                if (procentMutation > chance_mut)
                {
                    while (rnd1 > procent1)
                    {
                        k++;
                        procent1 = procent1 + chromosomes[k,percent_column];
                    }

                    for (int f = length_1; f <= size_matrix; f++)
                    {
                        chromosomes_new[m,f] = chromosomes[k,f];
                    }
                }
                else
                {
                    for (int f = length_1; f <= size_matrix - 1; f++)
                    {
                        chromosomes_new[m,f] = rand.Next(size_matrix + 1);
                    }
                    chromosomes_new[m,size_matrix] = size_matrix;
                }
            }
            return chromosomes_new;
        }
    }
}