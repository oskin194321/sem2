﻿namespace ga_1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.calc_button = new System.Windows.Forms.Button();
            this.datagridmatrix = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radio_cycle = new System.Windows.Forms.RadioButton();
            this.radio_step = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.value_chance = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.value_evols = new System.Windows.Forms.NumericUpDown();
            this.value_chrom = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.value_matrix = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.datagridmatrix)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.value_chance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.value_evols)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.value_chrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.value_matrix)).BeginInit();
            this.SuspendLayout();
            // 
            // calc_button
            // 
            this.calc_button.Location = new System.Drawing.Point(40, 248);
            this.calc_button.Name = "calc_button";
            this.calc_button.Size = new System.Drawing.Size(118, 46);
            this.calc_button.TabIndex = 0;
            this.calc_button.Text = "Расчет";
            this.calc_button.UseVisualStyleBackColor = true;
            // 
            // datagridmatrix
            // 
            this.datagridmatrix.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.datagridmatrix.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.datagridmatrix.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datagridmatrix.Location = new System.Drawing.Point(569, 51);
            this.datagridmatrix.Name = "datagridmatrix";
            this.datagridmatrix.RowHeadersWidth = 51;
            this.datagridmatrix.RowTemplate.Height = 24;
            this.datagridmatrix.Size = new System.Drawing.Size(508, 488);
            this.datagridmatrix.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1091, 574);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.value_matrix);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.textBox);
            this.tabPage1.Controls.Add(this.datagridmatrix);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1083, 545);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "GA-1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radio_cycle);
            this.groupBox2.Controls.Add(this.radio_step);
            this.groupBox2.Location = new System.Drawing.Point(6, 16);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 81);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Режим работы";
            // 
            // radio_cycle
            // 
            this.radio_cycle.AutoSize = true;
            this.radio_cycle.Location = new System.Drawing.Point(40, 49);
            this.radio_cycle.Name = "radio_cycle";
            this.radio_cycle.Size = new System.Drawing.Size(117, 21);
            this.radio_cycle.TabIndex = 1;
            this.radio_cycle.Text = "Циклический";
            this.radio_cycle.UseVisualStyleBackColor = true;
            // 
            // radio_step
            // 
            this.radio_step.AutoSize = true;
            this.radio_step.Checked = true;
            this.radio_step.Location = new System.Drawing.Point(40, 21);
            this.radio_step.Name = "radio_step";
            this.radio_step.Size = new System.Drawing.Size(104, 21);
            this.radio_step.TabIndex = 0;
            this.radio_step.TabStop = true;
            this.radio_step.Text = "Пошаговый";
            this.radio_step.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.calc_button);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.value_chance);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.value_evols);
            this.groupBox1.Controls.Add(this.value_chrom);
            this.groupBox1.Location = new System.Drawing.Point(6, 122);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 313);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Настройка работы";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Размер популяции";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(161, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Количество поколений";
            // 
            // value_chance
            // 
            this.value_chance.Location = new System.Drawing.Point(40, 203);
            this.value_chance.Name = "value_chance";
            this.value_chance.Size = new System.Drawing.Size(118, 22);
            this.value_chance.TabIndex = 9;
            this.value_chance.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 172);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Шанс мутации";
            // 
            // value_evols
            // 
            this.value_evols.Location = new System.Drawing.Point(40, 132);
            this.value_evols.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.value_evols.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.value_evols.Name = "value_evols";
            this.value_evols.Size = new System.Drawing.Size(118, 22);
            this.value_evols.TabIndex = 8;
            this.value_evols.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // value_chrom
            // 
            this.value_chrom.Location = new System.Drawing.Point(40, 66);
            this.value_chrom.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.value_chrom.Name = "value_chrom";
            this.value_chrom.Size = new System.Drawing.Size(118, 22);
            this.value_chrom.TabIndex = 7;
            this.value_chrom.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(30, 457);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(171, 17);
            this.label6.TabIndex = 12;
            this.label6.Text = "Количество узлов графа";
            // 
            // value_matrix
            // 
            this.value_matrix.Location = new System.Drawing.Point(40, 487);
            this.value_matrix.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.value_matrix.Minimum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.value_matrix.Name = "value_matrix";
            this.value_matrix.Size = new System.Drawing.Size(118, 22);
            this.value_matrix.TabIndex = 11;
            this.value_matrix.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(755, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Исходная матрица";
            // 
            // textBox
            // 
            this.textBox.Location = new System.Drawing.Point(212, 6);
            this.textBox.Multiline = true;
            this.textBox.Name = "textBox";
            this.textBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox.Size = new System.Drawing.Size(351, 533);
            this.textBox.TabIndex = 2;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1083, 545);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(836, 589);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(267, 17);
            this.label5.TabIndex = 3;
            this.label5.Text = "Оськин С. А. oskin-sergey@mail.ru 2020";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1115, 611);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "GA-1";
            ((System.ComponentModel.ISupportInitialize)(this.datagridmatrix)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.value_chance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.value_evols)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.value_chrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.value_matrix)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button calc_button;
        private System.Windows.Forms.DataGridView datagridmatrix;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox textBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown value_chrom;
        private System.Windows.Forms.NumericUpDown value_evols;
        private System.Windows.Forms.NumericUpDown value_chance;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown value_matrix;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radio_cycle;
        private System.Windows.Forms.RadioButton radio_step;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}

