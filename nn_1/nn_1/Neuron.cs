﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Services;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Schema;

namespace nn_1
{
    public class Neuron
    {
        Random rnd = new Random();

        double[] enters = new double[784];

        double[] res_out = new double[10]; // Результаты перемножения х и w с применением активационной функции
        double[,] w; // Матрица W
        double porog; // Значение порога активационной функции
        double speed; //коэффициент скорости обучения
        double[,] x; // Матрица X
        int[,] y = new int[10, 10]; // Единичная матрица
        int[] error_vector = new int[10]; // Вектор ошибок
        Form1 form;

        public Neuron(Form1 form, double tbox_activation, double tbox_speed)
        {
            this.form = form;
            porog = tbox_activation;
            speed = tbox_speed;

            int count = 0; // Счетчик количества изображений
            x = new double[100, enters.Length + 1]; // 100 изображений, число пикселей + маркер

            for (int i = 0; i < 10; i++) // Формирование матрицы "х"
            {
                for (int j = 0; j < 10; j++)
                {
                    create_X(x, count, i, "numbers/" + i + "/" + j + ".bmp");
                    count++;
                }
            }

            w = new double[enters.Length, 10];

            for (int i = 0; i < 10; i++) // Заполнение единичной матрицы
            {
                for (int j = 0; j < 10; j++)
                {
                    if (i == j)
                        y[i, j] = 1;
                    else
                        y[i, j] = 0;
                }
            }
        }

        public double[,] create_W()
        {
            for (int i = 0; i < enters.Length; i++) // Заполнение матрицы весов
            {
                for (int j = 0; j < 10; j++)
                {
                    w[i, j] = rnd.NextDouble() * 0.2 + 0.1;
                }
            }
            return w;
        }

        public void create_X(double[,] x, int row, int marker, string FilePathPic) // Формирование матрицы "х"
        {
            Color[][] color;
            color = GetMatrixRGB(FilePathPic);
            int count = 0; // Счетчик количества пикселей
            for (int i = 0; i < 28; i++)
            {
                for (int j = 0; j < 28; j++)
                {
                    if (color[i][j] != Color.FromArgb(255, 0, 0, 0))
                    {
                        x[row, count] = 1;
                    }
                    else
                        x[row, count] = 0;
                    count++;
                }
            }
            x[row, enters.Length] = marker;
        }

        public double[] calc_out() // Вычисление результата перемножения х и w
        {
            for(int i = 0; i < 10; i++)
            {
                res_out[i] = 0;
                
                for(int j = 0; j < enters.Length; j++)
                {
                    res_out[i] += enters[j] * w[j, i];
                }
            }
            return res_out;
        }

        public double[,] teach() // Обучение
        {
            form.datagrid_error.Rows.Clear(); // Удаление строк с отображением величины ошибки
            double gError = 0.0; // Величина ошибки
            int num = 0; //кол-во итераций
            do
            {
                gError = 0.0;
                for (int e = 0; e < x.GetLength(0) - 1; e++)
                {
                    for (int i = 0; i < enters.Length; i++)
                    {
                        enters[i] = x[e, i];
                    }

                    calc_out();

                    for (int i = 0; i < 10; i++)
                    {
                        if (res_out[i] > porog)
                            res_out[i] = 1;
                        else
                            res_out[i] = 0;

                        error_vector[i] = y[(int)x[e, enters.Length], i] - (int)res_out[i];
                        gError += Math.Abs(error_vector[i]);
                    }

                    for (int i = 0; i < enters.Length; i++) // Обновление матрицы весов
                    {
                        for (int j = 0; j < 10; j++)
                        {
                            w[i, j] += speed * error_vector[j] * enters[i];
                        }
                    }
                }

                form.datagrid_error.Rows.Add();
                form.datagrid_error.Rows[num].HeaderCell.Value = Convert.ToString(num + 1); // Номер строки
                form.datagrid_error.Rows[num].Cells[0].Value = Math.Round(gError, 2); // Заполнение ячеек

                num += 1;

            } while (gError != 0);

            return w;
        }

        public int recognize(string path)
        {
            int number = 0; // Для результата распознавания
            Color[][] color;
            color = GetMatrixRGB(path);
            int count = 0; // Счетчик количества пикселей
            for (int i = 0; i < 28; i++)
            {
                for (int j = 0; j < 28; j++)
                {
                    if (color[i][j] != Color.FromArgb(255, 0, 0, 0))
                    {
                        enters[count] = 1;
                    }
                    else
                        enters[count] = 0;
                    count++;
                }
            }

            calc_out();
            double max = res_out[0]; // Максимальное выходное значение
            int count_row = form.datagrid_out.Rows.Count-1; // Номер строки для ввода
            form.datagrid_out.Rows.Add();
            form.datagrid_out.Rows[count_row].Cells[1].Value = Math.Round(max, 2); // Заполнение ячеек
            for (int i = 1; i < 10; i++)
            {
                form.datagrid_out.Rows[count_row].Cells[i+1].Value = Math.Round(res_out[i], 2); // Заполнение ячеек с выходными значениями
                if (res_out[i] > max)
                {
                    max = res_out[i];
                    number = i;
                }                
            }
            form.datagrid_out.Rows[count_row].Cells[0].Value = number; // Ввод в ячейку распознанной цифры
            return number;
        }

        private Color[][] GetMatrixRGB(string FilePathPic) // Формирование матрицы значений пикселей картинки
        {
            Bitmap pic = new Bitmap(FilePathPic);

            int height = pic.Height;
            int width = pic.Width;

            Color[][] MatrixRGB = new Color[width][];
            for (int i = 0; i < width; i++)
            {
                MatrixRGB[i] = new Color[height];
                for (int j = 0; j < height; j++)
                {
                    MatrixRGB[i][j] = pic.GetPixel(i, j);
                }
            }
            return MatrixRGB;
        }
    }
}