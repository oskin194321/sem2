﻿namespace tz_3
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pic = new System.Windows.Forms.PictureBox();
            this.but_save = new System.Windows.Forms.Button();
            this.but_calc = new System.Windows.Forms.Button();
            this.k = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.fs_box = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.shift_yes = new System.Windows.Forms.RadioButton();
            this.no = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).BeginInit();
            this.SuspendLayout();
            // 
            // pic
            // 
            this.pic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic.Location = new System.Drawing.Point(361, 12);
            this.pic.Name = "pic";
            this.pic.Size = new System.Drawing.Size(400, 400);
            this.pic.TabIndex = 0;
            this.pic.TabStop = false;
            // 
            // but_save
            // 
            this.but_save.Location = new System.Drawing.Point(361, 418);
            this.but_save.Name = "but_save";
            this.but_save.Size = new System.Drawing.Size(400, 30);
            this.but_save.TabIndex = 1;
            this.but_save.Text = "Сохранить";
            this.but_save.UseVisualStyleBackColor = true;
            // 
            // but_calc
            // 
            this.but_calc.Location = new System.Drawing.Point(69, 267);
            this.but_calc.Name = "but_calc";
            this.but_calc.Size = new System.Drawing.Size(219, 30);
            this.but_calc.TabIndex = 2;
            this.but_calc.Text = "Получить изображение";
            this.but_calc.UseVisualStyleBackColor = true;
            // 
            // k
            // 
            this.k.FormatString = "N2";
            this.k.FormattingEnabled = true;
            this.k.Items.AddRange(new object[] {
            "0.2",
            "0.33",
            "0.5",
            "1",
            "1.41",
            "2",
            "3"});
            this.k.Location = new System.Drawing.Point(69, 178);
            this.k.Name = "k";
            this.k.Size = new System.Drawing.Size(219, 24);
            this.k.TabIndex = 3;
            this.k.Text = "0.2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(69, 143);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Коэффициент";
            // 
            // fs_box
            // 
            this.fs_box.Location = new System.Drawing.Point(69, 100);
            this.fs_box.Name = "fs_box";
            this.fs_box.Size = new System.Drawing.Size(219, 22);
            this.fs_box.TabIndex = 5;
            this.fs_box.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(167, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Частота дискретизации";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(69, 227);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Сдвиг по фазе";
            // 
            // shift_yes
            // 
            this.shift_yes.AutoSize = true;
            this.shift_yes.Location = new System.Drawing.Point(178, 225);
            this.shift_yes.Name = "shift_yes";
            this.shift_yes.Size = new System.Drawing.Size(48, 21);
            this.shift_yes.TabIndex = 8;
            this.shift_yes.TabStop = true;
            this.shift_yes.Text = "Да";
            this.shift_yes.UseVisualStyleBackColor = true;
            // 
            // no
            // 
            this.no.AutoSize = true;
            this.no.Location = new System.Drawing.Point(234, 225);
            this.no.Name = "no";
            this.no.Size = new System.Drawing.Size(54, 21);
            this.no.TabIndex = 9;
            this.no.TabStop = true;
            this.no.Text = "Нет";
            this.no.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(773, 455);
            this.Controls.Add(this.no);
            this.Controls.Add(this.shift_yes);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.fs_box);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.k);
            this.Controls.Add(this.but_calc);
            this.Controls.Add(this.but_save);
            this.Controls.Add(this.pic);
            this.Name = "Form1";
            this.Text = "3 Выбор параметров дискретизации сигнала";
            ((System.ComponentModel.ISupportInitialize)(this.pic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pic;
        private System.Windows.Forms.Button but_save;
        private System.Windows.Forms.Button but_calc;
        private System.Windows.Forms.ComboBox k;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox fs_box;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton shift_yes;
        private System.Windows.Forms.RadioButton no;
    }
}

