﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tz_3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            but_calc.Click += But_calc_Click;
            but_save.Click += But_save_Click;
        }

        // Получение изображения дискретизированного сигнала
        private void But_calc_Click(object sender, EventArgs e)
        {
            Bitmap Bitmap = new Bitmap(400, 400);
            int x = 0; // Значение канала пикселя
            int height, width; // высота и ширина изображения
            double[] sinusoid; // Значение точек синусоиды на заданном промежутке
            double shift = 0; // Сдвиг по фазе
            double fs; // Частота дискретизации


            height = Bitmap.Height;
            width = Bitmap.Width;
            sinusoid = new double[width];

            if (shift_yes.Checked == true) // Определение сдвига
                shift = Math.PI;
            else
                shift = 0;

            // Вычисление исходного сигнала с заданной частотой
            fs = Convert.ToDouble(fs_box.Text) * Convert.ToDouble(k.SelectedItem.ToString(), NumberFormatInfo.InvariantInfo);
            // Создание сигнала в форме синусоиды
            sinusoid = Sinusoid(fs, shift, width);

            for (int i = 0; i < width; i++) // Получение изображения
            {
                for (int j = 0; j < height; j++)
                {
                    x = Convert.ToInt32(sinusoid[j]);
                    Bitmap.SetPixel(i, j, Color.FromArgb(x, x, x));
                }
            }
            pic.Image = Bitmap;

        }

        // Вычисление точек синусоиды
        double[] Sinusoid(double fs, double shift, int dots)
        {
            double[] x = new double[dots];
            x[0] = 0;
            for (int i = 1; i < dots; i++)
            {
                x[i] = x[i - 1] + 1;
            }
            double[] y = new double[dots];
            for (int i = 0; i < dots; i++)
            {
                y[i] = (0.5 + 0.5 * Math.Sin(fs * x[i] + shift)) * 255;
            }
            return y;
        }

        // Сохранение полученного изображения
        private void But_save_Click(object sender, EventArgs e)
        {
            if (pic.Image != null) //если в pictureBox есть изображение
            {
                //создание диалогового окна "Сохранить как..", для сохранения изображения
                SaveFileDialog savedialog = new SaveFileDialog();
                savedialog.Title = "Сохранить картинку как...";
                //отображать ли предупреждение, если пользователь указывает имя уже существующего файла
                savedialog.OverwritePrompt = true;
                //отображать ли предупреждение, если пользователь указывает несуществующий путь
                savedialog.CheckPathExists = true;
                //список форматов файла
                savedialog.Filter = "Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";
                //отображается ли кнопка "Справка" в диалоговом окне
                savedialog.ShowHelp = true;
                if (savedialog.ShowDialog() == DialogResult.OK) //если в диалоговом окне нажата кнопка "ОК"
                {
                    try
                    {
                        pic.Image.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}
