﻿namespace oi_5
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pic = new System.Windows.Forms.PictureBox();
            this.but_open = new System.Windows.Forms.Button();
            this.but_spec = new System.Windows.Forms.Button();
            this.pic_spec = new System.Windows.Forms.PictureBox();
            this.pic_renew = new System.Windows.Forms.PictureBox();
            this.but_save_spec = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.filter = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.slice = new System.Windows.Forms.NumericUpDown();
            this.but_filter = new System.Windows.Forms.Button();
            this.but_save_filter = new System.Windows.Forms.Button();
            this.pic_filter = new System.Windows.Forms.PictureBox();
            this.pic_image = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_renew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_filter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_image)).BeginInit();
            this.SuspendLayout();
            // 
            // pic
            // 
            this.pic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic.Location = new System.Drawing.Point(330, 12);
            this.pic.Name = "pic";
            this.pic.Size = new System.Drawing.Size(300, 300);
            this.pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic.TabIndex = 0;
            this.pic.TabStop = false;
            // 
            // but_open
            // 
            this.but_open.Location = new System.Drawing.Point(12, 45);
            this.but_open.Name = "but_open";
            this.but_open.Size = new System.Drawing.Size(300, 30);
            this.but_open.TabIndex = 1;
            this.but_open.Text = "Загрузить изображение";
            this.but_open.UseVisualStyleBackColor = true;
            // 
            // but_spec
            // 
            this.but_spec.Location = new System.Drawing.Point(12, 135);
            this.but_spec.Name = "but_spec";
            this.but_spec.Size = new System.Drawing.Size(300, 30);
            this.but_spec.TabIndex = 2;
            this.but_spec.Text = "Получение спектра и восст. изображения";
            this.but_spec.UseVisualStyleBackColor = true;
            // 
            // pic_spec
            // 
            this.pic_spec.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_spec.Location = new System.Drawing.Point(636, 12);
            this.pic_spec.Name = "pic_spec";
            this.pic_spec.Size = new System.Drawing.Size(300, 300);
            this.pic_spec.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_spec.TabIndex = 3;
            this.pic_spec.TabStop = false;
            // 
            // pic_renew
            // 
            this.pic_renew.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_renew.Location = new System.Drawing.Point(942, 12);
            this.pic_renew.Name = "pic_renew";
            this.pic_renew.Size = new System.Drawing.Size(300, 300);
            this.pic_renew.TabIndex = 4;
            this.pic_renew.TabStop = false;
            // 
            // but_save_spec
            // 
            this.but_save_spec.Location = new System.Drawing.Point(12, 171);
            this.but_save_spec.Name = "but_save_spec";
            this.but_save_spec.Size = new System.Drawing.Size(300, 30);
            this.but_save_spec.TabIndex = 5;
            this.but_save_spec.Text = "Сохранить спектр и восст. изображение";
            this.but_save_spec.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 352);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Фильтр";
            // 
            // filter
            // 
            this.filter.FormattingEnabled = true;
            this.filter.Items.AddRange(new object[] {
            "Идеальный низкочастотный фильтр",
            "Низкочастотный фильтр Гаусса",
            "Идеальный высокочастотный фильтр",
            "Высокочастотный фильтр Гаусса"});
            this.filter.Location = new System.Drawing.Point(16, 373);
            this.filter.Name = "filter";
            this.filter.Size = new System.Drawing.Size(296, 24);
            this.filter.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 406);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "Срез";
            // 
            // slice
            // 
            this.slice.Location = new System.Drawing.Point(59, 404);
            this.slice.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.slice.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.slice.Name = "slice";
            this.slice.Size = new System.Drawing.Size(92, 22);
            this.slice.TabIndex = 9;
            this.slice.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // but_filter
            // 
            this.but_filter.Location = new System.Drawing.Point(12, 432);
            this.but_filter.Name = "but_filter";
            this.but_filter.Size = new System.Drawing.Size(300, 30);
            this.but_filter.TabIndex = 10;
            this.but_filter.Text = "Применение фильтра";
            this.but_filter.UseVisualStyleBackColor = true;
            // 
            // but_save_filter
            // 
            this.but_save_filter.Location = new System.Drawing.Point(12, 468);
            this.but_save_filter.Name = "but_save_filter";
            this.but_save_filter.Size = new System.Drawing.Size(300, 30);
            this.but_save_filter.TabIndex = 11;
            this.but_save_filter.Text = "Сохранить спектр фильтра и изобажение";
            this.but_save_filter.UseVisualStyleBackColor = true;
            // 
            // pic_filter
            // 
            this.pic_filter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_filter.Location = new System.Drawing.Point(330, 318);
            this.pic_filter.Name = "pic_filter";
            this.pic_filter.Size = new System.Drawing.Size(300, 300);
            this.pic_filter.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_filter.TabIndex = 12;
            this.pic_filter.TabStop = false;
            // 
            // pic_image
            // 
            this.pic_image.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_image.Location = new System.Drawing.Point(636, 318);
            this.pic_image.Name = "pic_image";
            this.pic_image.Size = new System.Drawing.Size(300, 300);
            this.pic_image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_image.TabIndex = 13;
            this.pic_image.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1258, 660);
            this.Controls.Add(this.pic_image);
            this.Controls.Add(this.pic_filter);
            this.Controls.Add(this.but_save_filter);
            this.Controls.Add(this.but_filter);
            this.Controls.Add(this.slice);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.filter);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.but_save_spec);
            this.Controls.Add(this.pic_renew);
            this.Controls.Add(this.pic_spec);
            this.Controls.Add(this.but_spec);
            this.Controls.Add(this.but_open);
            this.Controls.Add(this.pic);
            this.Name = "Form1";
            this.Text = "5 Применение частотных фильтров размытия и повышения резкости";
            ((System.ComponentModel.ISupportInitialize)(this.pic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_spec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_renew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_filter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_image)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pic;
        private System.Windows.Forms.Button but_open;
        private System.Windows.Forms.Button but_spec;
        private System.Windows.Forms.PictureBox pic_spec;
        private System.Windows.Forms.PictureBox pic_renew;
        private System.Windows.Forms.Button but_save_spec;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox filter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown slice;
        private System.Windows.Forms.Button but_filter;
        private System.Windows.Forms.Button but_save_filter;
        private System.Windows.Forms.PictureBox pic_filter;
        private System.Windows.Forms.PictureBox pic_image;
    }
}

