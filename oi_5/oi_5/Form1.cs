﻿using AForge.Imaging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oi_5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            but_spec.Click += But_spec_Click;
            but_filter.Click += But_filter_Click;
            but_open.Click += But_open_Click;
            but_save_spec.Click += But_save_spec_Click;
            but_save_filter.Click += But_save_filter_Click;
        }        

        private void But_filter_Click(object sender, EventArgs e) // Применение фильтров к изображению
        {
            int width = pic.Width;
            int height = pic.Height;

            double d0 = Convert.ToDouble(slice.Value.ToString()); // Частота среза
            double d; // Переменная функции фильтра
            double h; // Значение функции фильтра

            ComplexImage c_img_filter; // Создание Complex изображения
            var gray = new AForge.Imaging.Filters.Grayscale
                (0.2125, 0.7154, 0.0721).Apply(new Bitmap(pic.Image)); // преобразование в черно-белое
            c_img_filter = ComplexImage.FromBitmap(gray); // Получение Complex изображения из gray
            c_img_filter.ForwardFourierTransform(); // Прямое преобразование Фурье

            switch (filter.SelectedIndex) // Выбранный фильтр на форме
            {
                case 0: // Идеальный низкочастотный фильтр
                    for (int i = 0; i < width; i++)
                    {
                        for (int j = 0; j < height; j++)
                        {
                            d = Math.Sqrt(Math.Pow(i - (width / 2), 2) + Math.Pow(j - (height / 2), 2));
                            if (d > d0)
                                c_img_filter.Data[i, j] = c_img_filter.Data[i, j] * 0;
                        }
                    }
                    break;
                case 1: // Низкочастотный фильтр Гаусса
                    for (int i = 0; i < width; i++)
                    {
                        for (int j = 0; j < height; j++)
                        {
                            d = Math.Sqrt(Math.Pow(i - (width / 2), 2) + Math.Pow(j - (height / 2), 2));
                            h = Math.Exp(-Math.Pow(d, 2) / (2 * Math.Pow(d0, 2)));
                            c_img_filter.Data[i, j] = c_img_filter.Data[i, j] * h;
                        }
                    }
                    break;
                case 2: // Идеальный высокочастотный фильтр
                    for (int i = 0; i < width; i++)
                    {
                        for (int j = 0; j < height; j++)
                        {
                            d = Math.Sqrt(Math.Pow(i - (width / 2), 2) + Math.Pow(j - (height / 2), 2));
                            if (d <= d0)
                                c_img_filter.Data[i, j] = c_img_filter.Data[i, j] * 0;
                        }
                    }
                    break;
                case 3: // Высокочастотный фильтр Гаусса
                    for (int i = 0; i < width; i++)
                    {
                        for (int j = 0; j < height; j++)
                        {
                            d = Math.Sqrt(Math.Pow(i - (width / 2), 2) + Math.Pow(j - (height / 2), 2));
                            h = 1 - Math.Exp(-Math.Pow(d, 2) / (2 * Math.Pow(d0, 2)));
                            c_img_filter.Data[i, j] = c_img_filter.Data[i, j] * h;
                        }
                    }
                    break;
            }
            pic_filter.Image = c_img_filter.ToBitmap(); // Вывод фильтра спектра на форму
            c_img_filter.BackwardFourierTransform(); // Обратное преобразование Фурье
            pic_image.Image = c_img_filter.ToBitmap(); // Вывод полученного изображения на форму
        }

        private void But_spec_Click(object sender, EventArgs e) // Получение двумерного спектра и восстановленного изображения
        {
            ComplexImage c_img_spec; // Создание Complex изображения
            var gray = new AForge.Imaging.Filters.Grayscale
                (0.2125, 0.7154, 0.0721).Apply(new Bitmap(pic.Image)); // преобразование в черно-белое
            c_img_spec = ComplexImage.FromBitmap(gray); // Получение Complex изображения из gray
            c_img_spec.ForwardFourierTransform(); // Прямое преобразование Фурье
            pic_spec.Image = c_img_spec.ToBitmap(); // Вывод спектра на форму
            c_img_spec.BackwardFourierTransform(); // Обратное преобразование Фурье
            pic_renew.Image = c_img_spec.ToBitmap(); // Вывод восст. изображения на форму
        }

        private void But_open_Click(object sender, EventArgs e) // Загрузка изображения
        {
            Bitmap image; //Bitmap для открываемого изображения
            OpenFileDialog open_dialog = new OpenFileDialog(); //создание диалогового окна для выбора файла
            open_dialog.Filter = "Image Files(*.TIF;*.TIFF;*.BMP;*.JPG;*.GIF;*.PNG)|*.TIF;*.TIFF;*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*"; //формат файла
            if (open_dialog.ShowDialog() == DialogResult.OK) //если в окне была нажата кнопка "ОК"
            {
                try
                {
                    image = new Bitmap(open_dialog.FileName);
                    //this.pic.Size = image.Size;
                    pic.Image = image;
                    pic.Invalidate();
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void But_save_filter_Click(object sender, EventArgs e) // Сохранение фильтра и изображения
        {
            if (pic_image.Image != null) //если в pictureBox есть изображение
            {
                //создание диалогового окна "Сохранить как..", для сохранения изображения
                SaveFileDialog savedialog = new SaveFileDialog();
                savedialog.Title = "Сохранить картинку как...";
                //отображать ли предупреждение, если пользователь указывает имя уже существующего файла
                savedialog.OverwritePrompt = true;
                //отображать ли предупреждение, если пользователь указывает несуществующий путь
                savedialog.CheckPathExists = true;
                //список форматов файла, отображаемый в поле "Тип файла"
                savedialog.Filter = "Image Files(*.BMP)|*.BMP|Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";
                //отображается ли кнопка "Справка" в диалоговом окне
                savedialog.ShowHelp = true;
                if (savedialog.ShowDialog() == DialogResult.OK) //если в диалоговом окне нажата кнопка "ОК"
                {
                    try
                    {
                        pic_image.Image.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void But_save_spec_Click(object sender, EventArgs e) // Сохранение спектра и восст.
        {
            if (pic_spec.Image != null) //если в pictureBox есть изображение
            {
                //создание диалогового окна "Сохранить как..", для сохранения изображения
                SaveFileDialog savedialog = new SaveFileDialog();
                savedialog.Title = "Сохранить картинку как...";
                //отображать ли предупреждение, если пользователь указывает имя уже существующего файла
                savedialog.OverwritePrompt = true;
                //отображать ли предупреждение, если пользователь указывает несуществующий путь
                savedialog.CheckPathExists = true;
                //список форматов файла, отображаемый в поле "Тип файла"
                savedialog.Filter = "Image Files(*.BMP)|*.BMP|Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";
                //отображается ли кнопка "Справка" в диалоговом окне
                savedialog.ShowHelp = true;
                if (savedialog.ShowDialog() == DialogResult.OK) //если в диалоговом окне нажата кнопка "ОК"
                {
                    try
                    {
                        pic_spec.Image.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }

            if (pic_renew.Image != null) //если в pictureBox есть изображение
            {
                //создание диалогового окна "Сохранить как..", для сохранения изображения
                SaveFileDialog savedialog = new SaveFileDialog();
                savedialog.Title = "Сохранить картинку как...";
                //отображать ли предупреждение, если пользователь указывает имя уже существующего файла
                savedialog.OverwritePrompt = true;
                //отображать ли предупреждение, если пользователь указывает несуществующий путь
                savedialog.CheckPathExists = true;
                //список форматов файла, отображаемый в поле "Тип файла"
                savedialog.Filter = "Image Files(*.BMP)|*.BMP|Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";
                //отображается ли кнопка "Справка" в диалоговом окне
                savedialog.ShowHelp = true;
                if (savedialog.ShowDialog() == DialogResult.OK) //если в диалоговом окне нажата кнопка "ОК"
                {
                    try
                    {
                        pic_renew.Image.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}