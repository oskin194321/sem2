﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tz_3_lab
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            but_dithering.Click += But_dithering_Click;
            but_open.Click += But_open_Click;
            but_save.Click += But_save_Click;
        }

        private void But_dithering_Click(object sender, EventArgs e)
        {
            Bitmap picgray = new Bitmap(pic_gray.Image); // Bitmap изображения в градациях серого
            int width = pic_gray.Image.Width; // Ширина исходного изображения
            int height = pic_gray.Image.Height; // Высота

            Bitmap res = new Bitmap(width, height); // Bitmap для результата
            Random rnd = new Random();
            int[,] Dith = new int[width, height]; // Массив значений светлоты
            switch (cb_dith.SelectedIndex)
            {
                case 0: // Случайный дизеринг
                    for (int i = 0; i < width; i++)
                    {
                        for (int j = 0; j < height; j++)
                        {
                            Dith[i, j] = rnd.Next(0, 256);
                            if (picgray.GetPixel(i, j).R > Dith[i, j])
                                Dith[i, j] = 255;
                            else
                                Dith[i, j] = 0;
                        }
                    }
                    break;
                case 1: // Упорядоченный дизеринг
                    double average; // Среднее значение светлоты
                    // Значения пороговой матрицы
                    double m0 = 255.0 * 0.25;
                    double m1 = 255.0 * 0.5;
                    double m2 = 255.0 * 0.75;
                    double m3 = 255.0;
                    for (int i = 0; i < width - 1; i += 2)
                    {
                        for (int j = 0; j < height - 1; j += 2)
                        {
                            average = (picgray.GetPixel(i, j).R + picgray.GetPixel(i + 1, j).R + 
                                picgray.GetPixel(i, j + 1).R + picgray.GetPixel(i + 1, j + 1).R) / 4;

                            if (average >= m3)
                                Dith[i, j + 1] = 255;
                            else
                                Dith[i, j + 1] = 0;

                            if (average >= m2)
                                Dith[i + 1, j] = 255;
                            else
                                Dith[i + 1, j] = 0;

                            if (average >= m1)
                                Dith[i + 1, j + 1] = 255;
                            else
                                Dith[i + 1, j + 1] = 0;

                            if (average >= m0)
                                Dith[i, j] = 255;
                            else
                                Dith[i, j] = 0;
                        }
                    }
                    break;
            }
            for (int i = 0; i < width; i++) // Отображение изображения на форме
            {
                for (int j = 0; j < height; j++)
                    res.SetPixel(i, j, Color.FromArgb(Dith[i, j], Dith[i, j], Dith[i, j]));
            }
            pic_res.Image = res;
        }

        private void But_open_Click(object sender, EventArgs e) // Open
        {
            Bitmap image;
            OpenFileDialog open_dialog = new OpenFileDialog(); //создание диалогового окна для выбора файла
            open_dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*"; //формат файла
            if (open_dialog.ShowDialog() == DialogResult.OK) //если в окне была нажата кнопка "ОК"
            {
                try
                {
                    image = new Bitmap(open_dialog.FileName);
                    pic_gray.Image = image;
                    pic_gray.Invalidate();
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Bitmap Bitmap_gray = new Bitmap(pic_gray.Image);
                int x1; // значение каналов пикселя
                int height, width; // высота и ширина изображений

                height = Bitmap_gray.Height;
                width = Bitmap_gray.Width;

                for (int i = 0; i < width; i++)
                {
                    for (int j = 0; j < height; j++)
                    {
                        Color pixel1 = Bitmap_gray.GetPixel(i, j);
                        // Расчет значений канала пикселя
                        x1 = Convert.ToInt32(0.299 * pixel1.R + 0.587 * pixel1.G + 0.114 * pixel1.B);
                        Bitmap_gray.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                    }
                }
                pic_gray.Image = Bitmap_gray;
                // Сохранение изображения в градациях серого
                if (pic_gray != null) //если в pictureBox есть изображение
                {
                    //создание диалогового окна "Сохранить как..", для сохранения изображения
                    SaveFileDialog savedialog = new SaveFileDialog();
                    savedialog.Title = "Сохранить картинку как...";
                    //отображать ли предупреждение, если пользователь указывает имя уже существующего файла
                    savedialog.OverwritePrompt = true;
                    //отображать ли предупреждение, если пользователь указывает несуществующий путь
                    savedialog.CheckPathExists = true;
                    //список форматов файла
                    savedialog.Filter = "Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";
                    //отображается ли кнопка "Справка" в диалоговом окне
                    savedialog.ShowHelp = true;
                    if (savedialog.ShowDialog() == DialogResult.OK) //если в диалоговом окне нажата кнопка "ОК"
                    {
                        try
                        {
                            pic_gray.Image.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                        }
                        catch
                        {
                            MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
        }

        private void But_save_Click(object sender, EventArgs e) // Save
        {
            
        }
    }
}
