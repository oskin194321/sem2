﻿namespace tz_3_lab
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.but_open = new System.Windows.Forms.Button();
            this.pic_gray = new System.Windows.Forms.PictureBox();
            this.pic_res = new System.Windows.Forms.PictureBox();
            this.but_dithering = new System.Windows.Forms.Button();
            this.but_save = new System.Windows.Forms.Button();
            this.cb_dith = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pic_gray)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_res)).BeginInit();
            this.SuspendLayout();
            // 
            // but_open
            // 
            this.but_open.Location = new System.Drawing.Point(12, 12);
            this.but_open.Name = "but_open";
            this.but_open.Size = new System.Drawing.Size(100, 30);
            this.but_open.TabIndex = 0;
            this.but_open.Text = "Загрузить";
            this.but_open.UseVisualStyleBackColor = true;
            // 
            // pic_gray
            // 
            this.pic_gray.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_gray.Location = new System.Drawing.Point(12, 48);
            this.pic_gray.Name = "pic_gray";
            this.pic_gray.Size = new System.Drawing.Size(350, 350);
            this.pic_gray.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_gray.TabIndex = 1;
            this.pic_gray.TabStop = false;
            // 
            // pic_res
            // 
            this.pic_res.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_res.Location = new System.Drawing.Point(368, 48);
            this.pic_res.Name = "pic_res";
            this.pic_res.Size = new System.Drawing.Size(350, 350);
            this.pic_res.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_res.TabIndex = 2;
            this.pic_res.TabStop = false;
            // 
            // but_dithering
            // 
            this.but_dithering.Location = new System.Drawing.Point(368, 12);
            this.but_dithering.Name = "but_dithering";
            this.but_dithering.Size = new System.Drawing.Size(100, 30);
            this.but_dithering.TabIndex = 3;
            this.but_dithering.Text = "Дизеринг";
            this.but_dithering.UseVisualStyleBackColor = true;
            // 
            // but_save
            // 
            this.but_save.Location = new System.Drawing.Point(618, 12);
            this.but_save.Name = "but_save";
            this.but_save.Size = new System.Drawing.Size(100, 30);
            this.but_save.TabIndex = 4;
            this.but_save.Text = "Сохранить";
            this.but_save.UseVisualStyleBackColor = true;
            // 
            // cb_dith
            // 
            this.cb_dith.FormattingEnabled = true;
            this.cb_dith.Items.AddRange(new object[] {
            "Случайный",
            "Упорядоченный"});
            this.cb_dith.Location = new System.Drawing.Point(177, 16);
            this.cb_dith.Name = "cb_dith";
            this.cb_dith.Size = new System.Drawing.Size(185, 24);
            this.cb_dith.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(735, 417);
            this.Controls.Add(this.cb_dith);
            this.Controls.Add(this.but_save);
            this.Controls.Add(this.but_dithering);
            this.Controls.Add(this.pic_res);
            this.Controls.Add(this.pic_gray);
            this.Controls.Add(this.but_open);
            this.Name = "Form1";
            this.Text = "3 лаб Дизеринг";
            ((System.ComponentModel.ISupportInitialize)(this.pic_gray)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_res)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button but_open;
        private System.Windows.Forms.PictureBox pic_gray;
        private System.Windows.Forms.PictureBox pic_res;
        private System.Windows.Forms.Button but_dithering;
        private System.Windows.Forms.Button but_save;
        private System.Windows.Forms.ComboBox cb_dith;
    }
}

