﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tz_6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            but_open.Click += But_open_Click;
            but_save.Click += But_save_Click;
        }

        private void But_open_Click(object sender, EventArgs e) // Открытие изображения
        {
            Bitmap image; //Bitmap для открываемого изображения
            OpenFileDialog open_dialog = new OpenFileDialog(); //создание диалогового окна для выбора файла
            open_dialog.Filter = "Image Files(*.JPEG;*.TIF;*.TIFF;*.BMP;*.JPG;*.GIF;*.PNG)|*.JPEG;*.TIF;*.TIFF;*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*"; //формат файла
            Stopwatch timer = new Stopwatch(); // Отсчет времени
            if (open_dialog.ShowDialog() == DialogResult.OK) //если в окне была нажата кнопка "ОК"
            {
                try
                {
                    timer.Start(); // Запуск отсчета времени
                    image = new Bitmap(open_dialog.FileName);
                    this.pic.Size = image.Size;
                    pic.Image = image;
                    pic.Invalidate();
                    timer.Stop(); // Остановка отсчета времени
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            TimeSpan time = timer.Elapsed; // Преобразование полученного времени
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", time.Hours, time.Minutes, time.Seconds, time.Milliseconds);
            time_read.Text = elapsedTime; // Отображение времени на форме
        }

        private void But_save_Click(object sender, EventArgs e) // Сохранение изображения
        {
            if (pic.Image != null) //если в pictureBox есть изображение
            {
                //создание диалогового окна "Сохранить как..", для сохранения изображения
                SaveFileDialog savedialog = new SaveFileDialog();
                savedialog.Title = "Сохранить картинку как...";
                //отображать ли предупреждение, если пользователь указывает имя уже существующего файла
                savedialog.OverwritePrompt = true;
                //отображать ли предупреждение, если пользователь указывает несуществующий путь
                savedialog.CheckPathExists = true;
                //список форматов файла, отображаемый в поле "Тип файла"
                savedialog.Filter = "Image Files(*.JPEG)|*.JPEG|Image Files(*.BMP)|*.BMP|Image Files(*.TIFF)|*.TIFF|Image Files(*.GIF)|*.GIF|All files (*.*)|*.*";
                //отображается ли кнопка "Справка" в диалоговом окне
                savedialog.ShowHelp = true;
                Stopwatch timer = new Stopwatch(); // Отсчет времени
                if (savedialog.ShowDialog() == DialogResult.OK) //если в диалоговом окне нажата кнопка "ОК"
                {
                    try
                    {
                        timer.Start(); // Запуск отсчета времени
                        pic.Image.Save(savedialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                        timer.Stop(); // Остановка отсчета времени
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                TimeSpan time = timer.Elapsed; // Преобразование полученного времени
                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", time.Hours, time.Minutes, time.Seconds, time.Milliseconds);
                time_write.Text = elapsedTime; // Отображение времени на форме
            }
        }
    }
}
