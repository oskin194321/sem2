﻿namespace tz_6
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pic = new System.Windows.Forms.PictureBox();
            this.but_open = new System.Windows.Forms.Button();
            this.but_save = new System.Windows.Forms.Button();
            this.time_read = new System.Windows.Forms.Label();
            this.time_write = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).BeginInit();
            this.SuspendLayout();
            // 
            // pic
            // 
            this.pic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic.Location = new System.Drawing.Point(12, 48);
            this.pic.Name = "pic";
            this.pic.Size = new System.Drawing.Size(1000, 532);
            this.pic.TabIndex = 0;
            this.pic.TabStop = false;
            // 
            // but_open
            // 
            this.but_open.Location = new System.Drawing.Point(12, 12);
            this.but_open.Name = "but_open";
            this.but_open.Size = new System.Drawing.Size(330, 30);
            this.but_open.TabIndex = 1;
            this.but_open.Text = "Загрузить изображение";
            this.but_open.UseVisualStyleBackColor = true;
            // 
            // but_save
            // 
            this.but_save.Location = new System.Drawing.Point(348, 12);
            this.but_save.Name = "but_save";
            this.but_save.Size = new System.Drawing.Size(330, 30);
            this.but_save.TabIndex = 2;
            this.but_save.Text = "Сохранить изображение";
            this.but_save.UseVisualStyleBackColor = true;
            // 
            // time_read
            // 
            this.time_read.AutoSize = true;
            this.time_read.Location = new System.Drawing.Point(684, 19);
            this.time_read.Name = "time_read";
            this.time_read.Size = new System.Drawing.Size(109, 17);
            this.time_read.TabIndex = 3;
            this.time_read.Text = "Время чтения: ";
            // 
            // time_write
            // 
            this.time_write.AutoSize = true;
            this.time_write.Location = new System.Drawing.Point(845, 19);
            this.time_write.Name = "time_write";
            this.time_write.Size = new System.Drawing.Size(108, 17);
            this.time_write.TabIndex = 4;
            this.time_write.Text = "Время записи: ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1026, 592);
            this.Controls.Add(this.time_write);
            this.Controls.Add(this.time_read);
            this.Controls.Add(this.but_save);
            this.Controls.Add(this.but_open);
            this.Controls.Add(this.pic);
            this.Name = "Form1";
            this.Text = "6 Изучения алгоритмов сжатия и восстановления данных без потерь и с потерями";
            ((System.ComponentModel.ISupportInitialize)(this.pic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pic;
        private System.Windows.Forms.Button but_open;
        private System.Windows.Forms.Button but_save;
        private System.Windows.Forms.Label time_read;
        private System.Windows.Forms.Label time_write;
    }
}

