﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ga_2
{
    class Neuron
    {
        Random rnd = new Random();

        double[] enters = new double[784];

        double[] res_out = new double[10]; // Результаты перемножения х и w с применением активационной функции
        double[,] w; // Матрица W
        double porog; // Значение порога активационной функции
        double mut_value; // Вероятность мутации
        double[,] x; // Матрица X
        int[,] y = new int[10, 10]; // Единичная матрица
        int[] error_vector = new int[10]; // Вектор ошибок

        int num_chrom; // Количество хромосом
        double[][,] chromosomes; // 10 массивов хромосом для каждого класса

        Form1 form;

        public Neuron(Form1 form, double tbox_activation, double tbox_mut)
        {
            this.form = form;
            porog = tbox_activation;
            mut_value = tbox_mut;

            num_chrom = 100;
            chromosomes = new double[10][,];

            int count = 0; // Счетчик количества изображений
            x = new double[100, enters.Length + 1]; // 100 изображений, число пикселей + маркер

            for (int i = 0; i < 10; i++) // Формирование матрицы "х"
            {
                for (int j = 0; j < 10; j++)
                {
                    create_X(x, count, i, "numbers/" + i + "/" + j + ".bmp");
                    count++;
                }
            }

            w = new double[enters.Length, 10];

            for (int i = 0; i < 10; i++) // Заполнение единичной матрицы
            {
                for (int j = 0; j < 10; j++)
                {
                    if (i == j)
                        y[i, j] = 1;
                    else
                        y[i, j] = 0;
                }
            }
        }

        public double[,] create_W()
        {
            for (int k = 0; k < 10; k++)
            {
                chromosomes[k] = new double[num_chrom, enters.Length + 2]; // кол-во хромосом, кол-во входов + кол-во ошибок + шанс скрещивания
                for (int i = 0; i < num_chrom; i++) // Заполнение матрицы весов
                {
                    for (int j = 0; j < enters.Length; j++)
                    {
                        //chromosomes[k][i, j] = rnd.Next(-1000, 1000);
                        chromosomes[k][i, j] = rnd.NextDouble() * (-0.3) + 0.15;
                    }
                }
            }
            return w;
        }

        public void create_X(double[,] x, int row, int marker, string FilePathPic) // Формирование матрицы "х"
        {
            Color[][] color;
            color = GetMatrixRGB(FilePathPic);
            int count = 0; // Счетчик количества пикселей
            for (int i = 0; i < 28; i++)
            {
                for (int j = 0; j < 28; j++)
                {
                    if (color[i][j].R > 127)
                    {
                        x[row, count] = 1;
                    }
                    else
                        x[row, count] = 0;
                    count++;
                }
            }
            x[row, enters.Length] = marker;
        }

        public double[] calc_out() // Вычисление результата перемножения х и w
        {
            for (int i = 0; i < 10; i++)
            {
                res_out[i] = 0;

                for (int j = 0; j < enters.Length; j++)
                {
                    res_out[i] += enters[j] * w[j, i];
                }
            }
            return res_out;
        }

        public double[,] teach() // Обучение
        {
            mut_value = Convert.ToDouble(form.tbox_mut.Text);
            porog = Convert.ToDouble(form.tbox_activation.Text);
            form.datagrid_error.Rows.Clear(); // Удаление строк с отображением величины ошибки
            double gError = 0.0; // Величина ошибки
            int num = 0; //кол-во итераций
            do
            {
                gError = 0.0;

                for (int ch = 0; ch < num_chrom; ch++) // Вычисление кол-ва ошибок для каждой хромосомы каждого класса
                {

                    for (int i = 0; i < enters.Length; i++) // Заполнение матрицы весов с ch-хромосомами из матрицы каждого класса
                    {
                        for (int j = 0; j < 10; j++)
                        {
                            w[i, j] = chromosomes[j][ch, i];
                        }
                    }

                    // Вычисление выходных значений для каждого изображения и подсчет кол-ва ошибок ch-хромосом каждого класса
                    for (int e = 0; e < x.GetLength(0); e++)
                    {
                        for (int i = 0; i < enters.Length; i++)
                        {
                            enters[i] = x[e, i];
                        }

                        calc_out();

                        for (int i = 0; i < 10; i++)
                        {
                            if (res_out[i] > porog)
                                res_out[i] = 1;
                            else
                                res_out[i] = 0;

                            error_vector[i] = y[(int)x[e, enters.Length], i] - (int)res_out[i];
                            //if(ch == 0)
                               // gError += Math.Abs(error_vector[i]);

                            //if(Math.Abs(error_vector[i]) == 1)
                                chromosomes[i][ch, enters.Length] += Math.Abs(error_vector[i]);
                        }
                    }
                }

                sort_chrom(); // Сортировка хромосом

                //-----------------------Для величины ошибки--------------------------
                for (int i = 0; i < enters.Length; i++) // Заполнение матрицы весов с ch-хромосомами из матрицы каждого класса
                {
                    for (int j = 0; j < 10; j++)
                        w[i, j] = chromosomes[j][0, i];
                }
                // Вычисление выходных значений для каждого изображения и подсчет кол-ва ошибок лучших каждого класса
                for (int e = 0; e < x.GetLength(0); e++)
                {
                    for (int i = 0; i < enters.Length; i++)
                        enters[i] = x[e, i];
                    calc_out();
                    for (int i = 0; i < 10; i++)
                    {
                        if (res_out[i] > porog)
                            res_out[i] = 1;
                        else
                            res_out[i] = 0;
                        error_vector[i] = y[(int)x[e, enters.Length], i] - (int)res_out[i];
                        gError += Math.Abs(error_vector[i]);
                    }
                }
                //------------------------------------------------

                if (num<14)
                    evolution(); // Эволюция хромосом

                if (num == 29)
                    MessageBox.Show("");

                form.datagrid_error.Rows.Add();
                form.datagrid_error.Rows[num].HeaderCell.Value = Convert.ToString(num + 1); // Номер строки
                form.datagrid_error.Rows[num].Cells[0].Value = Math.Round(gError, 2); // Заполнение ячеек

                num += 1;

            } while (num < 15);
            //} while (gError != 0);
            
            for (int i = 0; i < enters.Length; i++) // Сохранение финальной матрицы весов из лучших хромосом каждого класса
            {
                for (int j = 0; j < 10; j++)
                {
                    w[i, j] = chromosomes[j][0, i];
                }
            }

            return w;
        }

        private void sort_chrom() // Сортировка хромосом по возрастанию кол-ва ошибок и назначение шанса скрещивания
        {
            double[] temp;

            for (int m = 0; m < 10; m++) // Сортировка хромосом каждого класса
            {
                temp = new double[enters.Length + 2];

                for (int i = 0; i < num_chrom; i++)
                    for (int j = 0; j < num_chrom - 1; j++)
                        if (chromosomes[m][j, enters.Length] > chromosomes[m][j + 1, enters.Length])
                        {
                            for (int k = 0; k < enters.Length + 1; k++)
                                temp[k] = chromosomes[m][j, k];
                            for (int k = 0; k < enters.Length + 1; k++)
                                chromosomes[m][j, k] = chromosomes[m][j + 1, k];
                            for (int k = 0; k < enters.Length + 1; k++)
                                chromosomes[m][j + 1, k] = temp[k];
                        }

                for (int i = 0; i < num_chrom; i++) // Назначение шанса скрещивания
                    chromosomes[m][i, enters.Length + 1] = 100.0 / (2.5 * (i + 1.0));
            }
        }

        private void evolution() // Эволюция хромосом каждого класса
        {
            double[,] temp_chrom; // Временный массив хромосом одного класса
            int rnd_percent; // Случайное число для выбора хромосомы для скрещивания
            double sum_percent; // Накопительная сумма процента для выбора хромосомы для скрещивания
            int count; // Номер выбранной хромосомы для скрещивания
            double rnd_mutation; // Случайное число для срабатывания скрещивания или мутации

            double mutation = Convert.ToDouble(form.tbox_mut.Text); // Заданная на форме вероятность мутации

            for (int m = 0; m < 10; m++) // Эволюция каждого класса хромосом
            {
                temp_chrom = new double[num_chrom, enters.Length + 2];

                for (int i = 0; i < num_chrom; i++)
                {
                    for (int j = 0; j < enters.Length; j++)
                    {
                        rnd_mutation = rnd.NextDouble();
                        if (rnd_mutation > mutation) // Скрещивание (шанс мутации не сработал)
                        {
                            rnd_percent = rnd.Next(1, 100);
                            sum_percent = 0;
                            count = 0;
                            do
                            {
                                sum_percent += chromosomes[m][count, enters.Length + 1];
                                count++;
                            }
                            while (rnd_percent > sum_percent);
                            count--;
                            temp_chrom[i, j] = chromosomes[m][count, j];
                        }
                        else // Шанс мутации сработал
                            temp_chrom[i, j] = rnd.NextDouble() * (-2) + 1;
                    }
                }
                chromosomes[m] = temp_chrom; // Сохранение эволюционированного массива хромосом m-класса
            }
        }

        public int recognize(string path)
        {
            int number = 0; // Для результата распознавания
            Color[][] color;
            color = GetMatrixRGB(path);
            int count = 0; // Счетчик количества пикселей
            for (int i = 0; i < 28; i++)
            {
                for (int j = 0; j < 28; j++)
                {
                    if (color[i][j].R > 127)
                    {
                        enters[count] = 1;
                    }
                    else
                        enters[count] = 0;
                    count++;
                }
            }

            calc_out();
            double max = res_out[0]; // Максимальное выходное значение
            int count_row = form.datagrid_out.Rows.Count - 1; // Номер строки для ввода
            form.datagrid_out.Rows.Add();
            form.datagrid_out.Rows[count_row].Cells[1].Value = Math.Round(max, 2); // Заполнение ячеек
            for (int i = 1; i < 10; i++)
            {
                form.datagrid_out.Rows[count_row].Cells[i + 1].Value = Math.Round(res_out[i], 2); // Заполнение ячеек с выходными значениями
                if (res_out[i] > max)
                {
                    max = res_out[i];
                    number = i;
                }
            }
            form.datagrid_out.Rows[count_row].Cells[0].Value = number; // Ввод в ячейку распознанной цифры
            return number;
        }

        private Color[][] GetMatrixRGB(string FilePathPic) // Формирование матрицы значений пикселей картинки
        {
            Bitmap pic = new Bitmap(FilePathPic);
            int height = pic.Height;
            int width = pic.Width;

            Color[][] MatrixRGB = new Color[width][];
            for (int i = 0; i < width; i++)
            {
                MatrixRGB[i] = new Color[height];
                for (int j = 0; j < height; j++)
                    MatrixRGB[i][j] = pic.GetPixel(i, j);
            }
            return MatrixRGB;
        }
    }
}
