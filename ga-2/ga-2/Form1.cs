﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ga_2
{
    public partial class Form1 : Form
    {
        Neuron network;
        string filePath; // Местонахождение изображения для проверки

        public Form1()
        {
            InitializeComponent();

            but_teach.Click += But_teach_Click;
            but_recognize.Click += But_recognize_Click;
            but_choose_pic.Click += But_choose_pic_Click;
            but_w.Click += But_w_Click;
            // Создание экземпляра класса
            network = new Neuron(this, Convert.ToDouble(tbox_activation.Text), Convert.ToDouble(tbox_mut.Text));
        }

        private void But_w_Click(object sender, EventArgs e)
        {
            //double[,] w = new double[784, 10]; // Матрица весов для отображения на форме
            //w = network.create_W();
            //show_w(w);
            datagrid_w.Rows.Clear();
            network.create_W();
            MessageBox.Show("Веса сформированы");
        }

        private void show_w(double[,] w) // Отображение матрицы весов на форме
        {
            datagrid_w.RowCount = w.GetLength(0); // указание числа строк
            datagrid_w.ColumnCount = w.GetLength(1); // и столбцов для отображения на форме
            for (int i = 0; i < w.GetLength(0); i++)
            {
                datagrid_w.Rows[i].HeaderCell.Value = Convert.ToString(i + 1); // Номер строки
                for (int j = 0; j < w.GetLength(1); j++)
                {
                    datagrid_w.Rows[i].Cells[j].Value = Math.Round(w[i, j], 2); // Заполнение ячеек
                }
            }
        }

        private void But_teach_Click(object sender, EventArgs e) // Реализация режима обучения
        {
            double[,] w = new double[784, 10]; // Матрица весов для отображения на форме
            w = network.teach(); // Обучение
            MessageBox.Show("Обучение завершено");
            show_w(w);
        }

        private void But_recognize_Click(object sender, EventArgs e) // Реализация распознавания
        {
            int number = network.recognize(filePath);
            label_number.Text = "Число - " + number;

        }

        private void But_choose_pic_Click(object sender, EventArgs e) // Открытие картинки для распознавания
        {
            Bitmap image; //Bitmap для открываемого изображения
            OpenFileDialog open_dialog = new OpenFileDialog(); //создание диалогового окна для выбора файла
            open_dialog.Filter = "Image Files(*.BMP)|*.BMP|All files (*.*)|*.*"; //формат файла
            if (open_dialog.ShowDialog() == DialogResult.OK) //если в окне была нажата кнопка "ОК"
            {
                try
                {
                    image = new Bitmap(open_dialog.FileName);
                    pic.Image = image;
                    pic.Invalidate();
                    filePath = open_dialog.FileName;
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
