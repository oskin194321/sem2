﻿namespace oi_1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.L = new System.Windows.Forms.NumericUpDown();
            this.A = new System.Windows.Forms.NumericUpDown();
            this.B = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lch_to_lab = new System.Windows.Forms.Button();
            this.lab_to_lch = new System.Windows.Forms.Button();
            this.L_lch = new System.Windows.Forms.NumericUpDown();
            this.H_lch = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.C_lch = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rgb_to_lab = new System.Windows.Forms.Button();
            this.lab_to_rgb = new System.Windows.Forms.Button();
            this.r_rgb = new System.Windows.Forms.NumericUpDown();
            this.b_rgb = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.g_rgb = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.hsv_to_lab = new System.Windows.Forms.Button();
            this.rgb_to_hsv = new System.Windows.Forms.Button();
            this.h_hsv = new System.Windows.Forms.NumericUpDown();
            this.v_hsv = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.s_hsv = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.hsl_to_lab = new System.Windows.Forms.Button();
            this.rgb_to_hsl = new System.Windows.Forms.Button();
            this.h_hsl = new System.Windows.Forms.NumericUpDown();
            this.l_hsl = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.s_hsl = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lab_color = new System.Windows.Forms.PictureBox();
            this.lch_color = new System.Windows.Forms.PictureBox();
            this.rgb_color = new System.Windows.Forms.PictureBox();
            this.hsv_color = new System.Windows.Forms.PictureBox();
            this.hsl_color = new System.Windows.Forms.PictureBox();
            this.calc_colors = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.from_rgb = new System.Windows.Forms.PictureBox();
            this.calc_to_lab = new System.Windows.Forms.Button();
            this.from_hsl = new System.Windows.Forms.PictureBox();
            this.from_lch = new System.Windows.Forms.PictureBox();
            this.from_hsv = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.L)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.A)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.B)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L_lch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.H_lch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.C_lch)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.r_rgb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.b_rgb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.g_rgb)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.h_hsv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_hsv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.s_hsv)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.h_hsl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.l_hsl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.s_hsl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lab_color)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lch_color)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgb_color)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hsv_color)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hsl_color)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.from_rgb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.from_hsl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.from_lch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.from_hsv)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "L";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "A";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "B";
            // 
            // L
            // 
            this.L.Location = new System.Drawing.Point(62, 21);
            this.L.Name = "L";
            this.L.Size = new System.Drawing.Size(85, 22);
            this.L.TabIndex = 4;
            // 
            // A
            // 
            this.A.Location = new System.Drawing.Point(62, 61);
            this.A.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.A.Minimum = new decimal(new int[] {
            128,
            0,
            0,
            -2147483648});
            this.A.Name = "A";
            this.A.Size = new System.Drawing.Size(85, 22);
            this.A.TabIndex = 5;
            // 
            // B
            // 
            this.B.Location = new System.Drawing.Point(62, 107);
            this.B.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.B.Minimum = new decimal(new int[] {
            128,
            0,
            0,
            -2147483648});
            this.B.Name = "B";
            this.B.Size = new System.Drawing.Size(85, 22);
            this.B.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.L);
            this.groupBox1.Controls.Add(this.B);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.A);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(170, 231);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "LAB";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lch_to_lab);
            this.groupBox2.Controls.Add(this.lab_to_lch);
            this.groupBox2.Controls.Add(this.L_lch);
            this.groupBox2.Controls.Add(this.H_lch);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.C_lch);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(213, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(170, 231);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "LCH";
            // 
            // lch_to_lab
            // 
            this.lch_to_lab.Location = new System.Drawing.Point(6, 184);
            this.lch_to_lab.Name = "lch_to_lab";
            this.lch_to_lab.Size = new System.Drawing.Size(158, 32);
            this.lch_to_lab.TabIndex = 8;
            this.lch_to_lab.Text = "LCH to LAB";
            this.lch_to_lab.UseVisualStyleBackColor = true;
            // 
            // lab_to_lch
            // 
            this.lab_to_lch.Location = new System.Drawing.Point(6, 146);
            this.lab_to_lch.Name = "lab_to_lch";
            this.lab_to_lch.Size = new System.Drawing.Size(158, 32);
            this.lab_to_lch.TabIndex = 7;
            this.lab_to_lch.Text = "LAB to LCH";
            this.lab_to_lch.UseVisualStyleBackColor = true;
            // 
            // L_lch
            // 
            this.L_lch.Location = new System.Drawing.Point(62, 21);
            this.L_lch.Name = "L_lch";
            this.L_lch.Size = new System.Drawing.Size(85, 22);
            this.L_lch.TabIndex = 4;
            // 
            // H_lch
            // 
            this.H_lch.Location = new System.Drawing.Point(62, 107);
            this.H_lch.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.H_lch.Name = "H_lch";
            this.H_lch.Size = new System.Drawing.Size(85, 22);
            this.H_lch.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "L";
            // 
            // C_lch
            // 
            this.C_lch.Location = new System.Drawing.Point(62, 61);
            this.C_lch.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.C_lch.Minimum = new decimal(new int[] {
            128,
            0,
            0,
            -2147483648});
            this.C_lch.Name = "C_lch";
            this.C_lch.Size = new System.Drawing.Size(85, 22);
            this.C_lch.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 17);
            this.label5.TabIndex = 2;
            this.label5.Text = "C";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 109);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(18, 17);
            this.label6.TabIndex = 3;
            this.label6.Text = "H";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rgb_to_lab);
            this.groupBox3.Controls.Add(this.lab_to_rgb);
            this.groupBox3.Controls.Add(this.r_rgb);
            this.groupBox3.Controls.Add(this.b_rgb);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.g_rgb);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Location = new System.Drawing.Point(414, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(170, 231);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "RGB";
            // 
            // rgb_to_lab
            // 
            this.rgb_to_lab.Location = new System.Drawing.Point(6, 184);
            this.rgb_to_lab.Name = "rgb_to_lab";
            this.rgb_to_lab.Size = new System.Drawing.Size(158, 32);
            this.rgb_to_lab.TabIndex = 8;
            this.rgb_to_lab.Text = "RGB to LAB";
            this.rgb_to_lab.UseVisualStyleBackColor = true;
            // 
            // lab_to_rgb
            // 
            this.lab_to_rgb.Location = new System.Drawing.Point(6, 146);
            this.lab_to_rgb.Name = "lab_to_rgb";
            this.lab_to_rgb.Size = new System.Drawing.Size(158, 32);
            this.lab_to_rgb.TabIndex = 7;
            this.lab_to_rgb.Text = "LAB to RGB";
            this.lab_to_rgb.UseVisualStyleBackColor = true;
            // 
            // r_rgb
            // 
            this.r_rgb.Location = new System.Drawing.Point(62, 21);
            this.r_rgb.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.r_rgb.Name = "r_rgb";
            this.r_rgb.Size = new System.Drawing.Size(85, 22);
            this.r_rgb.TabIndex = 4;
            // 
            // b_rgb
            // 
            this.b_rgb.Location = new System.Drawing.Point(62, 107);
            this.b_rgb.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.b_rgb.Name = "b_rgb";
            this.b_rgb.Size = new System.Drawing.Size(85, 22);
            this.b_rgb.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "R";
            // 
            // g_rgb
            // 
            this.g_rgb.Location = new System.Drawing.Point(62, 61);
            this.g_rgb.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.g_rgb.Name = "g_rgb";
            this.g_rgb.Size = new System.Drawing.Size(85, 22);
            this.g_rgb.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(22, 63);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(19, 17);
            this.label8.TabIndex = 2;
            this.label8.Text = "G";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(23, 109);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 17);
            this.label9.TabIndex = 3;
            this.label9.Text = "B";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.hsv_to_lab);
            this.groupBox4.Controls.Add(this.rgb_to_hsv);
            this.groupBox4.Controls.Add(this.h_hsv);
            this.groupBox4.Controls.Add(this.v_hsv);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.s_hsv);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Location = new System.Drawing.Point(616, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(170, 231);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "HSV (HSB)";
            // 
            // hsv_to_lab
            // 
            this.hsv_to_lab.Location = new System.Drawing.Point(6, 184);
            this.hsv_to_lab.Name = "hsv_to_lab";
            this.hsv_to_lab.Size = new System.Drawing.Size(158, 32);
            this.hsv_to_lab.TabIndex = 8;
            this.hsv_to_lab.Text = "HSV (HSB) to LAB";
            this.hsv_to_lab.UseVisualStyleBackColor = true;
            // 
            // rgb_to_hsv
            // 
            this.rgb_to_hsv.Location = new System.Drawing.Point(6, 146);
            this.rgb_to_hsv.Name = "rgb_to_hsv";
            this.rgb_to_hsv.Size = new System.Drawing.Size(158, 32);
            this.rgb_to_hsv.TabIndex = 7;
            this.rgb_to_hsv.Text = "RGB to HSV (HSB)";
            this.rgb_to_hsv.UseVisualStyleBackColor = true;
            // 
            // h_hsv
            // 
            this.h_hsv.Location = new System.Drawing.Point(62, 21);
            this.h_hsv.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.h_hsv.Name = "h_hsv";
            this.h_hsv.Size = new System.Drawing.Size(85, 22);
            this.h_hsv.TabIndex = 4;
            // 
            // v_hsv
            // 
            this.v_hsv.Location = new System.Drawing.Point(62, 107);
            this.v_hsv.Name = "v_hsv";
            this.v_hsv.Size = new System.Drawing.Size(85, 22);
            this.v_hsv.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(23, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(18, 17);
            this.label10.TabIndex = 1;
            this.label10.Text = "H";
            // 
            // s_hsv
            // 
            this.s_hsv.Location = new System.Drawing.Point(62, 61);
            this.s_hsv.Name = "s_hsv";
            this.s_hsv.Size = new System.Drawing.Size(85, 22);
            this.s_hsv.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(22, 63);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 17);
            this.label11.TabIndex = 2;
            this.label11.Text = "S";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(23, 109);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(30, 17);
            this.label12.TabIndex = 3;
            this.label12.Text = "V/B";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.hsl_to_lab);
            this.groupBox5.Controls.Add(this.rgb_to_hsl);
            this.groupBox5.Controls.Add(this.h_hsl);
            this.groupBox5.Controls.Add(this.l_hsl);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.s_hsl);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Location = new System.Drawing.Point(817, 12);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(170, 231);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "HSL (HSI)";
            // 
            // hsl_to_lab
            // 
            this.hsl_to_lab.Location = new System.Drawing.Point(6, 184);
            this.hsl_to_lab.Name = "hsl_to_lab";
            this.hsl_to_lab.Size = new System.Drawing.Size(158, 32);
            this.hsl_to_lab.TabIndex = 8;
            this.hsl_to_lab.Text = "HSL (HSI) to LAB";
            this.hsl_to_lab.UseVisualStyleBackColor = true;
            // 
            // rgb_to_hsl
            // 
            this.rgb_to_hsl.Location = new System.Drawing.Point(6, 146);
            this.rgb_to_hsl.Name = "rgb_to_hsl";
            this.rgb_to_hsl.Size = new System.Drawing.Size(158, 32);
            this.rgb_to_hsl.TabIndex = 7;
            this.rgb_to_hsl.Text = "RGB to HSL (HSI)";
            this.rgb_to_hsl.UseVisualStyleBackColor = true;
            // 
            // h_hsl
            // 
            this.h_hsl.Location = new System.Drawing.Point(62, 21);
            this.h_hsl.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.h_hsl.Name = "h_hsl";
            this.h_hsl.Size = new System.Drawing.Size(85, 22);
            this.h_hsl.TabIndex = 4;
            // 
            // l_hsl
            // 
            this.l_hsl.Location = new System.Drawing.Point(62, 107);
            this.l_hsl.Name = "l_hsl";
            this.l_hsl.Size = new System.Drawing.Size(85, 22);
            this.l_hsl.TabIndex = 6;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(23, 23);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(18, 17);
            this.label13.TabIndex = 1;
            this.label13.Text = "H";
            // 
            // s_hsl
            // 
            this.s_hsl.Location = new System.Drawing.Point(62, 61);
            this.s_hsl.Name = "s_hsl";
            this.s_hsl.Size = new System.Drawing.Size(85, 22);
            this.s_hsl.TabIndex = 5;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(22, 63);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 17);
            this.label14.TabIndex = 2;
            this.label14.Text = "S";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(23, 109);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(23, 17);
            this.label15.TabIndex = 3;
            this.label15.Text = "L/I";
            // 
            // lab_color
            // 
            this.lab_color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lab_color.Location = new System.Drawing.Point(6, 21);
            this.lab_color.Name = "lab_color";
            this.lab_color.Size = new System.Drawing.Size(158, 70);
            this.lab_color.TabIndex = 13;
            this.lab_color.TabStop = false;
            // 
            // lch_color
            // 
            this.lch_color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lch_color.Location = new System.Drawing.Point(207, 21);
            this.lch_color.Name = "lch_color";
            this.lch_color.Size = new System.Drawing.Size(158, 70);
            this.lch_color.TabIndex = 14;
            this.lch_color.TabStop = false;
            // 
            // rgb_color
            // 
            this.rgb_color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rgb_color.Location = new System.Drawing.Point(408, 21);
            this.rgb_color.Name = "rgb_color";
            this.rgb_color.Size = new System.Drawing.Size(158, 70);
            this.rgb_color.TabIndex = 15;
            this.rgb_color.TabStop = false;
            // 
            // hsv_color
            // 
            this.hsv_color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hsv_color.Location = new System.Drawing.Point(610, 21);
            this.hsv_color.Name = "hsv_color";
            this.hsv_color.Size = new System.Drawing.Size(158, 70);
            this.hsv_color.TabIndex = 16;
            this.hsv_color.TabStop = false;
            // 
            // hsl_color
            // 
            this.hsl_color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.hsl_color.Location = new System.Drawing.Point(811, 21);
            this.hsl_color.Name = "hsl_color";
            this.hsl_color.Size = new System.Drawing.Size(158, 70);
            this.hsl_color.TabIndex = 17;
            this.hsl_color.TabStop = false;
            // 
            // calc_colors
            // 
            this.calc_colors.Location = new System.Drawing.Point(6, 112);
            this.calc_colors.Name = "calc_colors";
            this.calc_colors.Size = new System.Drawing.Size(963, 32);
            this.calc_colors.TabIndex = 18;
            this.calc_colors.Text = "Отобразить цвета";
            this.calc_colors.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.rgb_color);
            this.groupBox6.Controls.Add(this.calc_colors);
            this.groupBox6.Controls.Add(this.lab_color);
            this.groupBox6.Controls.Add(this.hsl_color);
            this.groupBox6.Controls.Add(this.lch_color);
            this.groupBox6.Controls.Add(this.hsv_color);
            this.groupBox6.Location = new System.Drawing.Point(12, 268);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(975, 161);
            this.groupBox6.TabIndex = 19;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Разные цветовые пространства";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.from_rgb);
            this.groupBox7.Controls.Add(this.calc_to_lab);
            this.groupBox7.Controls.Add(this.from_hsl);
            this.groupBox7.Controls.Add(this.from_lch);
            this.groupBox7.Controls.Add(this.from_hsv);
            this.groupBox7.Location = new System.Drawing.Point(12, 446);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(975, 161);
            this.groupBox7.TabIndex = 20;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Обратное преобразование в Lab";
            // 
            // from_rgb
            // 
            this.from_rgb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.from_rgb.Location = new System.Drawing.Point(408, 21);
            this.from_rgb.Name = "from_rgb";
            this.from_rgb.Size = new System.Drawing.Size(158, 70);
            this.from_rgb.TabIndex = 15;
            this.from_rgb.TabStop = false;
            // 
            // calc_to_lab
            // 
            this.calc_to_lab.Location = new System.Drawing.Point(207, 112);
            this.calc_to_lab.Name = "calc_to_lab";
            this.calc_to_lab.Size = new System.Drawing.Size(762, 32);
            this.calc_to_lab.TabIndex = 18;
            this.calc_to_lab.Text = "Отобразить цвета после обратного преобразования в Lab";
            this.calc_to_lab.UseVisualStyleBackColor = true;
            // 
            // from_hsl
            // 
            this.from_hsl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.from_hsl.Location = new System.Drawing.Point(811, 21);
            this.from_hsl.Name = "from_hsl";
            this.from_hsl.Size = new System.Drawing.Size(158, 70);
            this.from_hsl.TabIndex = 17;
            this.from_hsl.TabStop = false;
            // 
            // from_lch
            // 
            this.from_lch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.from_lch.Location = new System.Drawing.Point(207, 21);
            this.from_lch.Name = "from_lch";
            this.from_lch.Size = new System.Drawing.Size(158, 70);
            this.from_lch.TabIndex = 14;
            this.from_lch.TabStop = false;
            // 
            // from_hsv
            // 
            this.from_hsv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.from_hsv.Location = new System.Drawing.Point(610, 21);
            this.from_hsv.Name = "from_hsv";
            this.from_hsv.Size = new System.Drawing.Size(158, 70);
            this.from_hsv.TabIndex = 16;
            this.from_hsv.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 619);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "1_Обработка_изображения";
            ((System.ComponentModel.ISupportInitialize)(this.L)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.A)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.B)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.L_lch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.H_lch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.C_lch)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.r_rgb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.b_rgb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.g_rgb)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.h_hsv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.v_hsv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.s_hsv)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.h_hsl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.l_hsl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.s_hsl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lab_color)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lch_color)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgb_color)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hsv_color)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hsl_color)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.from_rgb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.from_hsl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.from_lch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.from_hsv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown L;
        private System.Windows.Forms.NumericUpDown A;
        private System.Windows.Forms.NumericUpDown B;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button lch_to_lab;
        private System.Windows.Forms.Button lab_to_lch;
        private System.Windows.Forms.NumericUpDown L_lch;
        private System.Windows.Forms.NumericUpDown H_lch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown C_lch;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button rgb_to_lab;
        private System.Windows.Forms.Button lab_to_rgb;
        private System.Windows.Forms.NumericUpDown r_rgb;
        private System.Windows.Forms.NumericUpDown b_rgb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown g_rgb;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button hsv_to_lab;
        private System.Windows.Forms.Button rgb_to_hsv;
        private System.Windows.Forms.NumericUpDown h_hsv;
        private System.Windows.Forms.NumericUpDown v_hsv;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown s_hsv;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button hsl_to_lab;
        private System.Windows.Forms.Button rgb_to_hsl;
        private System.Windows.Forms.NumericUpDown h_hsl;
        private System.Windows.Forms.NumericUpDown l_hsl;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown s_hsl;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox lab_color;
        private System.Windows.Forms.PictureBox lch_color;
        private System.Windows.Forms.PictureBox rgb_color;
        private System.Windows.Forms.PictureBox hsv_color;
        private System.Windows.Forms.PictureBox hsl_color;
        private System.Windows.Forms.Button calc_colors;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.PictureBox from_rgb;
        private System.Windows.Forms.Button calc_to_lab;
        private System.Windows.Forms.PictureBox from_hsl;
        private System.Windows.Forms.PictureBox from_lch;
        private System.Windows.Forms.PictureBox from_hsv;
    }
}

