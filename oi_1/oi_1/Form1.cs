﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oi_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            lab_to_lch.Click += Lab_to_lch_Click;
            lch_to_lab.Click += Lch_to_lab_Click;

            lab_to_rgb.Click += Lab_to_rgb_Click;
            rgb_to_lab.Click += Rgb_to_lab_Click;

            rgb_to_hsv.Click += Rgb_to_hsv_Click;
            hsv_to_lab.Click += Hsv_to_lab_Click;

            rgb_to_hsl.Click += Rgb_to_hsl_Click;
            hsl_to_lab.Click += Hsl_to_lab_Click;

            calc_colors.Click += Calc_colors_Click;
            calc_to_lab.Click += Calc_to_lab_Click;
        }

        private void Calc_to_lab_Click(object sender, EventArgs e)
        {
            int[] rgb = new int[3];
            //----------- LCH to Lab ------------
            rgb = func_lch_to_lab((int)L_lch.Value, (int)C_lch.Value, (int)H_lch.Value);

            double[] deltaE = new double[3];
            deltaE = calc_E((int)L.Value, (int)A.Value, (int)B.Value, rgb[0], rgb[1], rgb[2]);

            rgb = func_lab_to_rgb((int)rgb[0], (int)rgb[1], (int)rgb[2]);
            from_lch.BackColor = Color.FromArgb(rgb[0], rgb[1], rgb[2]);            
            //----------- RGB to Lab ------------
            rgb = func_rgb_to_lab((int)r_rgb.Value, (int)g_rgb.Value, (int)b_rgb.Value);
            //double dE = calc_E((int)L.Value, (int)A.Value, (int)B.Value, rgb[0], rgb[1], rgb[2]);
            rgb = func_lab_to_rgb((int)rgb[0], (int)rgb[1], (int)rgb[2]);
            from_rgb.BackColor = Color.FromArgb(rgb[0], rgb[1], rgb[2]);
            //----------- HSV to Lab ------------
            rgb = func_hsv_to_rgb((int)h_hsv.Value, (int)s_hsv.Value, (int)v_hsv.Value);
            rgb = func_rgb_to_lab((int)rgb[0], (int)rgb[1], (int)rgb[2]);
            rgb = func_lab_to_rgb((int)rgb[0], (int)rgb[1], (int)rgb[2]);
            from_hsv.BackColor = Color.FromArgb(rgb[0], rgb[1], rgb[2]);
            //----------- HSL to Lab ------------
            rgb = func_hsl_to_rgb((int)h_hsl.Value, (int)s_hsl.Value, (int)l_hsl.Value);
            rgb = func_rgb_to_lab((int)rgb[0], (int)rgb[1], (int)rgb[2]);
            //double dE = calc_E((int)L.Value, (int)A.Value, (int)B.Value, rgb[0], rgb[1], rgb[2]);
            rgb = func_lab_to_rgb((int)rgb[0], (int)rgb[1], (int)rgb[2]);
            from_hsl.BackColor = Color.FromArgb(rgb[0], rgb[1], rgb[2]);
        }

        private void Calc_colors_Click(object sender, EventArgs e) // Вывод цветов на форму
        {
            int[] rgb = new int[3];
            //----------- Lab ------------
            rgb = func_lab_to_rgb((int)L.Value, (int)A.Value, (int)B.Value); 
            lab_color.BackColor = Color.FromArgb(rgb[0], rgb[1], rgb[2]);
            //----------- LCH ------------
            rgb = func_lch_to_lab((int)L_lch.Value, (int)C_lch.Value, (int)H_lch.Value);
            rgb = func_lab_to_rgb((int)rgb[0], (int)rgb[1], (int)rgb[2]);
            lch_color.BackColor = Color.FromArgb(rgb[0], rgb[1], rgb[2]);
            //----------- RGB ------------
            rgb_color.BackColor = Color.FromArgb((int)r_rgb.Value, (int)g_rgb.Value, (int)b_rgb.Value);
            //----------- HSV ------------
            rgb = func_hsv_to_rgb((int)h_hsv.Value, (int)s_hsv.Value, (int)v_hsv.Value);
            hsv_color.BackColor = Color.FromArgb(rgb[0], rgb[1], rgb[2]);
            //----------- HSL ------------
            rgb = func_hsl_to_rgb((int)h_hsl.Value, (int)s_hsl.Value, (int)l_hsl.Value);
            hsl_color.BackColor = Color.FromArgb(rgb[0], rgb[1], rgb[2]);
        }

        public double[] calc_E(int L, int a, int b, int L1, int a1, int b1) // Расчет цветового различия
        {            
            //----------- Расчет dE ------------
            double dE;

            dE = Math.Sqrt(Math.Pow(((double)L - (double)L1), 2) + Math.Pow(((double)a - (double)a1), 2) + Math.Pow(((double)b - (double)b1), 2));
            //----------- Расчет dE94 ----------
            double dE94, dL, dC, dH, C1, C2, d_a, d_b, K_L, S_L, K_C, S_C, K_H, S_H;    
            
            dL = (double)L - (double)L1;
            C1 = Math.Sqrt(Math.Pow((double)a, 2) + Math.Pow((double)b, 2));
            C2 = Math.Sqrt(Math.Pow((double)a1, 2) + Math.Pow((double)b1, 2));
            dC = C1 - C2;
            d_a = a - a1;
            d_b = b - b1;
            dH = Math.Sqrt(Math.Pow((double)d_a, 2) + Math.Pow((double)d_b, 2) - Math.Pow((double)dC, 2));
            K_L = 2.0;
            S_L = 1.0;
            K_C = 1.0;
            S_C = 1.0 + 0.048 * C1;
            K_H = 1.0;
            S_H = 1.0 + 0.014 * C1;

            dE94 = Math.Sqrt(Math.Pow(dL / (K_L * S_L), 2) + Math.Pow(dC / (K_C * S_C), 2) + Math.Pow(dH / (K_H * S_H), 2));
            //----------- Расчет dE00 ----------
            double k_L = 1.0, k_C = 1.0, k_H = 1.0;
            double deg360InRad = deg2Rad(360.0);
            double deg180InRad = deg2Rad(180.0);
            double pow25To7 = 6103515625.0; // pow(25, 7)

            double barC = (C1 + C2) / 2.0;
            double G = 0.5 * (1 - Math.Sqrt(Math.Pow(barC, 7) / (Math.Pow(barC, 7) + pow25To7)));
            double a1Prime = (1.0 + G) * (double)a;
            double a2Prime = (1.0 + G) * (double)a1;
            double CPrime1 = Math.Sqrt((a1Prime * a1Prime) + Math.Pow((double)b,2));
            double CPrime2 = Math.Sqrt((a2Prime * a2Prime) + Math.Pow((double)b1, 2));
            double hPrime1;
            if (b == 0 && a1Prime == 0)
                hPrime1 = 0.0;
            else
                hPrime1 = Math.Atan2((double)b, a1Prime);

            if (hPrime1 < 0)
                hPrime1 += deg360InRad;
            double hPrime2;
            if ((double)b == 0 && a2Prime == 0)
                hPrime2 = 0.0;
            else
                hPrime2 = Math.Atan2((double)b1, a2Prime);

            if (hPrime2 < 0)
                hPrime2 += deg360InRad;

            double deltaLPrime = (double)L1 - (double)L;
            double deltaCPrime = CPrime2 - CPrime1;
            double deltahPrime;
            double CPrimeProduct = CPrime1 * CPrime2;
            if (CPrimeProduct == 0)
                deltahPrime = 0;
            else
            {
                deltahPrime = hPrime2 - hPrime1;
                if (deltahPrime < -deg180InRad)
                    deltahPrime += deg360InRad;
                else if (deltahPrime > deg180InRad)
                    deltahPrime -= deg360InRad;
            }
            double deltaHPrime = 2.0 * Math.Sqrt(CPrimeProduct) *
                Math.Sin(deltahPrime / 2.0);

            double barLPrime = ((double)L + (double)L1) / 2.0;
            double barCPrime = (CPrime1 + CPrime2) / 2.0;
            double barhPrime, hPrimeSum = hPrime1 + hPrime2;
            if (CPrime1 * CPrime2 == 0)
            {
                barhPrime = hPrimeSum;
            }
            else
            {
                if (Math.Abs(hPrime1 - hPrime2) <= deg180InRad) 
                    barhPrime = hPrimeSum / 2.0;
                else
                {
                    if (hPrimeSum < deg360InRad)
                        barhPrime = (hPrimeSum + deg360InRad) / 2.0;
                    else
                        barhPrime = (hPrimeSum - deg360InRad) / 2.0;
                }
            }
            double T = 1.0 - (0.17 * Math.Cos(barhPrime - deg2Rad(30.0))) +
                (0.24 * Math.Cos(2.0 * barhPrime)) +
                (0.32 * Math.Cos((3.0 * barhPrime) + deg2Rad(6.0))) -
                (0.20 * Math.Cos((4.0 * barhPrime) - deg2Rad(63.0)));
            double deltaTheta = deg2Rad(30.0) *
                Math.Exp(-Math.Pow((barhPrime - deg2Rad(275.0)) / deg2Rad(25.0), 2.0));
            double R_C = 2.0 * Math.Sqrt(Math.Pow(barCPrime, 7.0) /
                (Math.Pow(barCPrime, 7.0) + pow25To7));
            S_L = 1.0 + ((0.015 * Math.Pow(barLPrime - 50.0, 2.0)) /
                Math.Sqrt(20 + Math.Pow(barLPrime - 50.0, 2.0)));
            S_C = 1.0 + (0.045 * barCPrime);
            S_H = 1.0 + (0.015 * barCPrime * T);
            double R_T = (-Math.Sin(2.0 * deltaTheta)) * R_C;

            double dE00 = Math.Sqrt(
                Math.Pow(deltaLPrime / (k_L * S_L), 2.0) +
                Math.Pow(deltaCPrime / (k_C * S_C), 2.0) +
                Math.Pow(deltaHPrime / (k_H * S_H), 2.0) +
                (R_T * (deltaCPrime / (k_C * S_C)) * (deltaHPrime / (k_H * S_H))));
            //----------- Возврат значений ----------
            double[] deltaE = new double[3];
            deltaE[0] = dE;
            deltaE[1] = dE94;
            deltaE[2] = dE00;
            return deltaE;
        }

        private double deg2Rad(double deg)
        {
            return (deg * (Math.PI / 180.0));
        }

        private void Hsl_to_lab_Click(object sender, EventArgs e) // HSL to Lab
        {
            int[] lab = new int[3];
            lab = func_hsl_to_rgb((int)h_hsl.Value, (int)s_hsl.Value, (int)l_hsl.Value); // HSL to RGB

            lab = func_rgb_to_lab(lab[0], lab[1], lab[2]); // RGB to Lab

            L.Value = lab[0];
            A.Value = lab[1];
            B.Value = lab[2];
        }

        private void Hsv_to_lab_Click(object sender, EventArgs e) // HSV to Lab
        {
            int[] lab = new int[3];
            lab = func_hsv_to_rgb((int)h_hsv.Value, (int)s_hsv.Value, (int)v_hsv.Value); // HSV to RGB

            lab = func_rgb_to_lab(lab[0], lab[1], lab[2]); // RGB to Lab

            L.Value = lab[0];
            A.Value = lab[1];
            B.Value = lab[2];
        }

        private void Rgb_to_hsl_Click(object sender, EventArgs e)
        {
            int[] hsl = new int[3];
            hsl = func_rgb_to_hsl((int)r_rgb.Value, (int)g_rgb.Value, (int)b_rgb.Value);

            h_hsl.Value = hsl[0];
            s_hsl.Value = hsl[1];
            l_hsl.Value = hsl[2];
        }

        private void Rgb_to_hsv_Click(object sender, EventArgs e)
        {
            int[] hsv = new int[3];
            hsv = func_rgb_to_hsv((int)r_rgb.Value, (int)g_rgb.Value, (int)b_rgb.Value);

            h_hsv.Value = hsv[0];
            s_hsv.Value = hsv[1];
            v_hsv.Value = hsv[2];            
        }

        private void Rgb_to_lab_Click(object sender, EventArgs e)
        {
            int[] lab = new int[3];
            lab = func_rgb_to_lab((int)r_rgb.Value, (int)g_rgb.Value, (int)b_rgb.Value);

            L.Value = lab[0];
            A.Value = lab[1];
            B.Value = lab[2];
        }

        private void Lab_to_rgb_Click(object sender, EventArgs e)
        {
            int[] rgb = new int[3];
            rgb = func_lab_to_rgb((int)L.Value, (int)A.Value, (int)B.Value);

            r_rgb.Value = rgb[0];
            g_rgb.Value = rgb[1];
            b_rgb.Value = rgb[2];
        }

        private void Lch_to_lab_Click(object sender, EventArgs e)
        {
            int[] lab = new int[3];
            lab = func_lch_to_lab((int)L_lch.Value, (int)C_lch.Value, (int)H_lch.Value);
            L.Value = lab[0];
            A.Value = lab[1];
            B.Value = lab[2];
        }

        private void Lab_to_lch_Click(object sender, EventArgs e)
        {
            int[] lch = new int[3];
            lch = func_lab_to_lch((int)L.Value, (int)A.Value, (int)B.Value);
            L_lch.Value = lch[0];
            C_lch.Value = lch[1];
            H_lch.Value = lch[2];
        }

        private int[] func_lch_to_lab(int L, int C, int H) // функция преобразования LCH to Lab
        {
            int[] temp = new int[3]; // массив для рассчитываемых L, a, b

            temp[0] = L; // L
            temp[1] = Convert.ToInt32(Math.Cos((double)H * Math.PI / 180) * (double)C); // a
            temp[2] = Convert.ToInt32(Math.Sin((double)H * Math.PI / 180) * (double)C); // b

            return temp;
        }

        private int[] func_lab_to_lch(int L, int a, int b) // функция преобразования Lab to LCH
        {
            int[] temp = new int[3]; // массив для рассчитываемых L, C, H

            temp[0] = L; // L
            temp[1] = (int)Math.Sqrt(Math.Pow((double)a, 2) + Math.Pow((double)b, 2)); // C
            
            double h = Math.Atan2((double)b, (double)a);
            if (h > 0)
            {
                h = (h / Math.PI) * 180;
            }
            else
            {
                h = 360 - (Math.Abs(h) / Math.PI) * 180;
            }
            temp[2] = (int)h; // H

            return temp;
        }

        private int[] func_rgb_to_hsv(int R, int G, int B) // функция преобразования RGB to HSV
        {
            int[] hsv = new int[3]; // массив для рассчитываемых H, S, V

            double r, g, b, min, max, delta_max;
            double[] temp = new double[3];

            r = (double)R / 255;
            g = (double)G / 255;
            b = (double)B / 255;

            temp[0] = r;
            temp[1] = g;
            temp[2] = b;

            min = temp.Min();
            max = temp.Max();
            delta_max = max - min;

            hsv[2] = Convert.ToInt32(max * 100); // V/B

            if (delta_max == 0)
            {
                hsv[0] = 0; // H
                hsv[1] = 0; // S
            }
            else
            {
                hsv[1] = Convert.ToInt32((delta_max / max) * 100); // S
                double del_r, del_g, del_b, h_temp = 0;
                del_r = (((max - r) / 6) + (delta_max / 2)) / delta_max;
                del_g = (((max - g) / 6) + (delta_max / 2)) / delta_max;
                del_b = (((max - b) / 6) + (delta_max / 2)) / delta_max;

                if (r == max)
                    h_temp = del_b - del_g;
                else if (g == max)
                    h_temp = 0.333 + del_r - del_b;
                else if (b == max)
                    h_temp = 0.667 + del_g - del_r;

                if (h_temp < 0)
                    h_temp += 1;
                if (h_temp > 1)
                    h_temp -= 1;

                hsv[0] = Convert.ToInt32(h_temp * 360); // H
            }
            return hsv;
        }

        private int[] func_rgb_to_hsl(int R, int G, int B) // функция преобразования RGB to HSL
        {
            int[] hsl = new int[3]; // массив для рассчитываемых H, S, L

            double r, g, b, min, max, delta_max;
            double[] temp = new double[3];

            r = (double)R / 255;
            g = (double)G / 255;
            b = (double)B / 255;

            temp[0] = r;
            temp[1] = g;
            temp[2] = b;

            min = temp.Min();
            max = temp.Max();
            delta_max = max - min;

            hsl[2] = Convert.ToInt32(((max + min) / 2) * 100); // L

            if (delta_max == 0)
            {
                hsl[0] = 0; // H
                hsl[1] = 0; // S
            }
            else
            {
                if(((max + min) / 2) < 0.5)
                {
                    hsl[1] = Convert.ToInt32((delta_max / (max + min)) * 100); // S
                }
                else
                    hsl[1] = Convert.ToInt32((delta_max / (2 - max - min)) * 100); // S

                double del_r, del_g, del_b, h_temp = 0;
                del_r = (((max - r) / 6) + (delta_max / 2)) / delta_max;
                del_g = (((max - g) / 6) + (delta_max / 2)) / delta_max;
                del_b = (((max - b) / 6) + (delta_max / 2)) / delta_max;

                if (r == max)
                    h_temp = del_b - del_g;
                else if (g == max)
                    h_temp = 0.333 + del_r - del_b;
                else if (b == max)
                    h_temp = 0.667 + del_g - del_r;

                if (h_temp < 0)
                    h_temp += 1;
                if (h_temp > 1)
                    h_temp -= 1;

                hsl[0] = Convert.ToInt32(h_temp * 360); // H
            }
            return hsl;
        }

        public int[] func_lab_to_rgb(int L_lab, int a_lab, int b_lab) // функция преобразования Lab to RGB
        {
            int[] rgb = new int[3]; // массив для рассчитываемых R, G, B
            double x, y, z, var_r, var_g, var_b;
            //----------- Lab to XYZ ------------
            y = (L_lab + 16.0) / 116.0;
            x = (a_lab / 500.0) + y;
            z = y - (b_lab / 200.0);

            if (Math.Pow(y, 3) > 0.008856)
                y = Math.Pow(y, 3);
            else
                y = (y - 0.13793) / 7.787;

            if (Math.Pow(x, 3) > 0.008856)
                x = Math.Pow(x, 3);
            else
                x = (x - 0.13793) / 7.787;

            if (Math.Pow(z, 3) > 0.008856)
                z = Math.Pow(z, 3);
            else
                z = (z - 0.13793) / 7.787;

            x = x * 95.047;
            y = y * 100;
            z = z * 108.883;
            //----------- XYZ to RGB ------------
            x = x / 100;
            y = y / 100;
            z = z / 100;

            var_r = x * 3.2406 + y * (-1.5372) + z * (-0.4986);
            var_g = x * (-0.9689) + y * 1.8758 + z * 0.0415;
            var_b = x * 0.0557 + y * (-0.2040) + z * 1.0570;

            if (var_r > 0.0031308)
                var_r = 1.055 * Math.Pow(var_r, 0.417) - 0.055;
            else
                var_r = 12.92 * var_r;

            if (var_g > 0.0031308)
                var_g = 1.055 * Math.Pow(var_g, 0.417) - 0.055;
            else
                var_g = 12.92 * var_g;

            if (var_b > 0.0031308)
                var_b = 1.055 * Math.Pow(var_b, 0.417) - 0.055;
            else
                var_b = 12.92 * var_b;

            rgb[0] = Convert.ToInt32(var_r * 255); // R
            rgb[1] = Convert.ToInt32(var_g * 255); // G
            rgb[2] = Convert.ToInt32(var_b * 255); // B

            for(int i = 0; i < 3; i++)
            {
                if (rgb[i] > 255)
                    rgb[i] = 255;

                if (rgb[i] < 0)
                    rgb[i] = 0;
            }
            return rgb;
        }

        public int[] func_rgb_to_lab(int r_rgb, int g_rgb, int b_rgb) // функция преобразования RGB to Lab
        {
            int[] lab = new int[3]; // массив для рассчитываемых L, a, b

            double var_r, var_g, var_b, x, y, z;
            //----------- RGB to XYZ ------------
            var_r = r_rgb / 255.0;
            var_g = g_rgb / 255.0;
            var_b = b_rgb / 255.0;

            if (var_r > 0.04045)
                var_r = Math.Pow((var_r + 0.055) / 1.055, 2.4);
            else
                var_r = var_r / 12.92;

            if (var_g > 0.04045)
                var_g = Math.Pow((var_g + 0.055) / 1.055, 2.4);
            else
                var_g = var_g / 12.92;

            if (var_b > 0.04045)
                var_b = Math.Pow((var_b + 0.055) / 1.055, 2.4);
            else
                var_b = var_b / 12.92;

            var_r = var_r * 100;
            var_g = var_g * 100;
            var_b = var_b * 100;

            x = var_r * 0.4124 + var_g * 0.3576 + var_b * 0.1805;
            y = var_r * 0.2126 + var_g * 0.7152 + var_b * 0.0722;
            z = var_r * 0.0193 + var_g * 0.1192 + var_b * 0.9505;
            //----------- XYZ to Lab ------------
            double var_x, var_y, var_z;

            var_x = x / 95.047;
            var_y = y / 100.0;
            var_z = z / 108.883;

            if (var_x > 0.008856)
                var_x = Math.Pow(var_x, 0.3333);
            else
                var_x = 7.787 * var_x + 0.1379;

            if (var_y > 0.008856)
                var_y = Math.Pow(var_y, 0.3333);
            else
                var_y = 7.787 * var_y + 0.1379;

            if (var_z > 0.008856)
                var_z = Math.Pow(var_z, 0.3333);
            else
                var_z = 7.787 * var_z + 0.1379;

            lab[0] = Convert.ToInt32((116 * var_y) - 16);
            lab[1] = Convert.ToInt32(500 * (var_x - var_y));
            lab[2] = Convert.ToInt32(200 * (var_y - var_z));

            return lab;
        }

        public int[] func_hsv_to_rgb(int h_hsv, int s_hsv, int v_hsv) // функция преобразования HSV to RGB
        {
            int[] rgb = new int[3]; // массив для рассчитываемых R, G, B
            double H, S, V, var_r, var_g, var_b, var_h, var_i, var_1, var_2, var_3;

            H = (double)h_hsv / 360.0;
            S = (double)s_hsv / 100.0;
            V = (double)v_hsv / 100.0;

            if (S == 0)
            {
                rgb[0] = Convert.ToInt32(V * 255);
                rgb[1] = Convert.ToInt32(V * 255);
                rgb[2] = Convert.ToInt32(V * 255);
            }
            else
            {
                var_h = H * 6;
                if (var_h == 6)
                    var_h = 0;

                var_i = (int)var_h;

                var_1 = V * (1 - S);
                var_2 = V * (1 - S * (var_h - var_i));
                var_3 = V * (1 - S * (1 - (var_h - var_i)));

                if (var_i == 0)
                {
                    var_r = V; var_g = var_3; var_b = var_1;
                }
                else if (var_i == 1)
                {
                    var_r = var_2; var_g = V; var_b = var_1;
                }
                else if (var_i == 2)
                {
                    var_r = var_1; var_g = V; var_b = var_3;
                }
                else if (var_i == 3)
                {
                    var_r = var_1; var_g = var_2; var_b = V;
                }
                else if (var_i == 4)
                {
                    var_r = var_3; var_g = var_1; var_b = V;
                }
                else
                {
                    var_r = V; var_g = var_1; var_b = var_2;
                }

                rgb[0] = Convert.ToInt32(var_r * 255);
                rgb[1] = Convert.ToInt32(var_g * 255);
                rgb[2] = Convert.ToInt32(var_b * 255);
            }

            return rgb;
        }

        public int[] func_hsl_to_rgb(int h_hsl, int s_hsl, int l_hsl) // функция преобразования HSL to RGB
        {
            int[] rgb = new int[3]; // массив для рассчитываемых R, G, B
            double H, S, L, var_r, var_g, var_b, var_1, var_2;

            H = (double)h_hsl / 360.0;
            S = (double)s_hsl / 100.0;
            L = (double)l_hsl / 100.0;

            if (S == 0)
            {
                rgb[0] = Convert.ToInt32(L * 255);
                rgb[1] = Convert.ToInt32(L * 255);
                rgb[2] = Convert.ToInt32(L * 255);
            }
            else
            {
                if (L < 0.5)
                    var_2 = L * (1 + S);
                else
                    var_2 = (L + S) - (S * L);

                var_1 = 2 * L - var_2;

                rgb[0] = Convert.ToInt32(255 * Hue_to_RGB(var_1, var_2, H + 0.3333));
                rgb[1] = Convert.ToInt32(255 * Hue_to_RGB(var_1, var_2, H));
                rgb[2] = Convert.ToInt32(255 * Hue_to_RGB(var_1, var_2, H - 0.3333));

            }

            return rgb;
        }

        public double Hue_to_RGB(double v1, double v2, double vH)
        {
            if (vH < 0)
                vH += 1;
            if (vH > 1)
                vH -= 1;

            if ((6 * vH) < 1)
                return (v1 + (v2 - v1) * 6 * vH);
            if ((2 * vH) < 1)
                return (v2);
            if ((3 * vH) < 2)
                return (v1 + (v2 - v1) * (0.6667 - vH) * 6);

            return v1;
        }
    }
}
