﻿using AForge.Imaging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace tz_1_lab
{
    public partial class Form1 : Form
    {
        double[] str;

        public Form1()
        {
            InitializeComponent();
            but_open.Click += But_open_Click;
            but_rotate.Click += But_rotate_Click;
            but_MTF.Click += But_MTF_Click;
            but_lens.Click += But_lens_Click;
        }

        private void But_rotate_Click(object sender, EventArgs e) // Поворот изображения
        {
            Bitmap Bitmap_rotate = new Bitmap(pic.Image);
            int x1; // значение каналов пикселя
            int height, width; // высота и ширина изображения

            height = Bitmap_rotate.Height;
            width = Bitmap_rotate.Width;
            double[,] pic_gray = new double[width, height];
            str = new double[width];

            for (int i = 0; i < width; i++) // Преобразование в черно-белое
            {
                for (int j = 0; j < height; j++)
                {
                    Color pixel1 = Bitmap_rotate.GetPixel(i, j);
                    // Расчет значений канала пикселя
                    x1 = Convert.ToInt32(0.299 * pixel1.R + 0.587 * pixel1.G + 0.114 * pixel1.B);
                    pic_gray[i, j] = x1;
                    Bitmap_rotate.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                }
            }

            Bitmap_rotate = RotateImage(Bitmap_rotate, Convert.ToInt32(num_angle.Value.ToString())); // Поворот изображения
            Color[,] colorMatrix = new Color[width, height];
            for (int i = 0; i < width; i++) // Усреднение столбцов светлот пикселей
            {
                double temp = 0;
                int count = 1;
                for (int j = 0; j < height; j++)
                {
                    colorMatrix[i, j] = Bitmap_rotate.GetPixel(i, j);
                    pic_gray[i, j] = 0.299 * colorMatrix[i, j].R + 0.587 * colorMatrix[i, j].G + 0.114 * colorMatrix[i, j].B;
                    if (pic_gray[i, j] != 0)
                    {
                        temp += pic_gray[i, j];
                        count++;
                    }
                }
                temp = temp / count;
                str[i] = temp;
                for (int j = 0; j < height; j++)
                {
                    x1 = Convert.ToInt32(temp);
                    Bitmap_rotate.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                }
            }
            pic_rotate.Image = Bitmap_rotate;
        }

        private Bitmap RotateImage(Bitmap bitmap, float angle) // Поворот Bitmap
        {
            Bitmap Bitmap_rotate = new Bitmap(bitmap.Width, bitmap.Height);
            Bitmap_rotate.SetResolution(bitmap.HorizontalResolution, bitmap.VerticalResolution);
            using (Graphics g = Graphics.FromImage(Bitmap_rotate))
            {
                // Установка точки поворота в центр матрицы
                g.TranslateTransform(bitmap.Width / 2, bitmap.Height / 2);
                // Поворот на заданный угол
                g.RotateTransform(angle);
                // Востановление точки поворота матрицы
                g.TranslateTransform(-bitmap.Width / 2, -bitmap.Height / 2);
                // Зарисовка изображения в Bitmap
                g.DrawImage(bitmap, new Point(0, 0));
            }
            return Bitmap_rotate;
        }

        private void But_MTF_Click(object sender, EventArgs e) // Вычисление ФПМ
        {
            int width = pic.Width + 4;
            int x1; // Для расчета значения пикселя
            Bitmap bitmap_MTF = new Bitmap(width, width);
            double[] str1 = new double[width];
            for (int i = 0; i < width; i++) // Добавление функции размытия линии
            {
                if (i == 0) 
                    str1[i] = 0;
                else 
                    str1[i] = Math.Abs(str[i] - str[i - 1]);
            }
            for (int i = 0; i < width; i++) // Заполнение изображения
            {
                for (int j = 0; j < width; j++)
                {
                    x1 = Convert.ToInt32(str1[i]);
                    bitmap_MTF.SetPixel(i, j, Color.FromArgb(x1, x1, x1));
                }
            }
            ComplexImage cImgSpec; // Создание комплексного изображения
            var gray = new AForge.Imaging.Filters.Grayscale(0.2125, 0.7154, 0.0721).Apply(bitmap_MTF); // Преобразование изображение в GrayScale
            cImgSpec = ComplexImage.FromBitmap(gray); // Заполнение комплексного изображения
            cImgSpec.ForwardFourierTransform(); // Прямое преобразование Фурье
            bitmap_MTF = cImgSpec.ToBitmap(); // Сохранение изображения после преобразования
            int temp;
            double x2; // Для расчета значения пикселя
            for (int i = 0; i < width; i++) // Поиск МПФ в двумерном изображении
            {
                temp = 0;
                for (int j = 0; j < width; j++)
                {
                    Color pixel1 = bitmap_MTF.GetPixel(i, j);
                    x2 = 0.299 * pixel1.R + 0.587 * pixel1.G + 0.114 * pixel1.B;
                    if (temp < x2)
                        temp = Convert.ToInt32(x2);
                }
                str1[i] = temp;
            }
            DrawGraph(str1); // Построение графика МПФ
            pic_MTF.Image = bitmap_MTF;
        }

        List<PointPairList> lens = new List<PointPairList>(); // Список для расчета графика ФПМ объектива

        private void DrawGraph(double[] str)
        {
            chart1.Series[0].Points.Clear(); // Очистка точек на графике
            chart1.Series[1].Points.Clear();
            PointPairList list = new PointPairList(); // Создание списка точек
            PointPairList list1 = new PointPairList();
            double x = 0;
            double max = str[64];
            for (int i = str.Length / 2; i < str.Length; i++) // Заполнение списка точек и графика
            {
                list.Add(x, str[i] / max);
                list1.Add(x, 0.5);
                chart1.Series[0].Points.AddXY(Math.Round(x, 4), Math.Round(str[i] / max, 5));
                chart1.Series[1].Points.AddXY(Math.Round(x, 4), 0.5);
                x = x + 0.00025;
            }
            lens.Add(list);
        }

        private void But_lens_Click(object sender, EventArgs e) // Вычисление графика ФПМ объектива
        {
            chart1.Series[0].Points.Clear(); // Очистка точек на графике
            chart1.Series[1].Points.Clear();
            chart1.ChartAreas[0].AxisX.Maximum = 0.0014;
            PointPairList list = new PointPairList();
            for (int i = 0; i < lens[0].Count; i++) // Добавление точек в список
            {
                if (lens[1][i].Y == 0) 
                    lens[1][i].Y = 1;
                list.Add(i * 0.00025, lens[0][i].Y / lens[1][i].Y);
                chart1.Series[0].Points.AddXY(Math.Round(i * 0.00025, 4), Math.Round(lens[0][i].Y / lens[1][i].Y, 4));
                if (list[i].Y > 1) 
                    list[i].Y = 1;
                if (list[i].Y < 0) 
                    list[i].Y = 0;
            }
            lens.Clear();
            PointPairList list1 = new PointPairList();
            double x = 0;
            for (int i = 0; i < list.Count; i++) // Добавление точек в список
            {
                list1.Add(x, 0.5);
                chart1.Series[1].Points.AddXY(Math.Round(x, 4), 0.5);
                x = x + 0.00025;
            }
        }

        private void But_open_Click(object sender, EventArgs e) // Загрузка изображения
        {
            Bitmap image; // Bitmap для открываемого изображения
            OpenFileDialog open_dialog = new OpenFileDialog(); //создание диалогового окна для выбора файла
            open_dialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG|All files (*.*)|*.*"; //формат файла
            if (open_dialog.ShowDialog() == DialogResult.OK) //если в окне была нажата кнопка "ОК"
            {
                try
                {
                    image = new Bitmap(open_dialog.FileName);
                    //this.pic.Size = image.Size;
                    pic.Image = image;
                    pic.Invalidate();
                }
                catch
                {
                    DialogResult rezult = MessageBox.Show("Невозможно открыть выбранный файл",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
