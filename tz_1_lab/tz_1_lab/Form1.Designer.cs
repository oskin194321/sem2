﻿namespace tz_1_lab
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.label1 = new System.Windows.Forms.Label();
            this.but_lens = new System.Windows.Forms.Button();
            this.but_MTF = new System.Windows.Forms.Button();
            this.num_angle = new System.Windows.Forms.NumericUpDown();
            this.but_rotate = new System.Windows.Forms.Button();
            this.but_open = new System.Windows.Forms.Button();
            this.pic = new System.Windows.Forms.PictureBox();
            this.pic_rotate = new System.Windows.Forms.PictureBox();
            this.pic_MTF = new System.Windows.Forms.PictureBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.num_angle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_rotate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_MTF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(664, 69);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(370, 68);
            this.label1.TabIndex = 16;
            this.label1.Text = "\r\nПредназначена для рассчета ФПМ объектива\r\nПредварительно рассчитайте изображени" +
    "е \"cam-lens\"\r\nзатем \"cam\"";
            // 
            // but_lens
            // 
            this.but_lens.Location = new System.Drawing.Point(796, 9);
            this.but_lens.Margin = new System.Windows.Forms.Padding(4);
            this.but_lens.Name = "but_lens";
            this.but_lens.Size = new System.Drawing.Size(100, 57);
            this.but_lens.TabIndex = 15;
            this.but_lens.Text = "Для объектива";
            this.but_lens.UseVisualStyleBackColor = true;
            // 
            // but_MTF
            // 
            this.but_MTF.Location = new System.Drawing.Point(459, 9);
            this.but_MTF.Margin = new System.Windows.Forms.Padding(4);
            this.but_MTF.Name = "but_MTF";
            this.but_MTF.Size = new System.Drawing.Size(164, 30);
            this.but_MTF.TabIndex = 14;
            this.but_MTF.Text = "График MTF";
            this.but_MTF.UseVisualStyleBackColor = true;
            // 
            // num_angle
            // 
            this.num_angle.Location = new System.Drawing.Point(291, 10);
            this.num_angle.Margin = new System.Windows.Forms.Padding(4);
            this.num_angle.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.num_angle.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.num_angle.Name = "num_angle";
            this.num_angle.Size = new System.Drawing.Size(160, 22);
            this.num_angle.TabIndex = 13;
            this.num_angle.Value = new decimal(new int[] {
            354,
            0,
            0,
            0});
            // 
            // but_rotate
            // 
            this.but_rotate.Location = new System.Drawing.Point(183, 10);
            this.but_rotate.Margin = new System.Windows.Forms.Padding(4);
            this.but_rotate.Name = "but_rotate";
            this.but_rotate.Size = new System.Drawing.Size(100, 28);
            this.but_rotate.TabIndex = 12;
            this.but_rotate.Text = "Поворот";
            this.but_rotate.UseVisualStyleBackColor = true;
            // 
            // but_open
            // 
            this.but_open.Location = new System.Drawing.Point(9, 10);
            this.but_open.Margin = new System.Windows.Forms.Padding(4);
            this.but_open.Name = "but_open";
            this.but_open.Size = new System.Drawing.Size(100, 28);
            this.but_open.TabIndex = 8;
            this.but_open.Text = "Загрузить";
            this.but_open.UseVisualStyleBackColor = true;
            // 
            // pic
            // 
            this.pic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic.Location = new System.Drawing.Point(12, 51);
            this.pic.Name = "pic";
            this.pic.Size = new System.Drawing.Size(164, 144);
            this.pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic.TabIndex = 17;
            this.pic.TabStop = false;
            // 
            // pic_rotate
            // 
            this.pic_rotate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_rotate.Location = new System.Drawing.Point(183, 51);
            this.pic_rotate.Name = "pic_rotate";
            this.pic_rotate.Size = new System.Drawing.Size(164, 144);
            this.pic_rotate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_rotate.TabIndex = 18;
            this.pic_rotate.TabStop = false;
            // 
            // pic_MTF
            // 
            this.pic_MTF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_MTF.Location = new System.Drawing.Point(459, 51);
            this.pic_MTF.Name = "pic_MTF";
            this.pic_MTF.Size = new System.Drawing.Size(164, 144);
            this.pic_MTF.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_MTF.TabIndex = 19;
            this.pic_MTF.TabStop = false;
            // 
            // chart1
            // 
            chartArea1.AxisX.Maximum = 0.0014D;
            chartArea1.AxisX.Minimum = 0D;
            chartArea1.AxisX.Title = "Циклы/Пиксели";
            chartArea1.AxisY.Maximum = 1D;
            chartArea1.AxisY.Minimum = 0D;
            chartArea1.AxisY.Title = "Модуляция";
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(12, 211);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Legend = "Legend1";
            series1.Name = "ФПМ";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Legend = "Legend1";
            series2.Name = "MTF50";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Size = new System.Drawing.Size(1035, 341);
            this.chart1.TabIndex = 20;
            this.chart1.Text = "chart1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1059, 564);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.pic_MTF);
            this.Controls.Add(this.pic_rotate);
            this.Controls.Add(this.pic);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.but_lens);
            this.Controls.Add(this.but_MTF);
            this.Controls.Add(this.num_angle);
            this.Controls.Add(this.but_rotate);
            this.Controls.Add(this.but_open);
            this.Name = "Form1";
            this.Text = "1 лаб Оценка размытия систем с помощью функции передачи модуляции";
            ((System.ComponentModel.ISupportInitialize)(this.num_angle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_rotate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_MTF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button but_lens;
        private System.Windows.Forms.Button but_MTF;
        private System.Windows.Forms.NumericUpDown num_angle;
        private System.Windows.Forms.Button but_rotate;
        private System.Windows.Forms.Button but_open;
        private System.Windows.Forms.PictureBox pic;
        private System.Windows.Forms.PictureBox pic_rotate;
        private System.Windows.Forms.PictureBox pic_MTF;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
    }
}

